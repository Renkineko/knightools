from django.urls import path

from . import views

app_name = 'api'
urlpatterns = [
    path('weapon/', views.WeaponListView.as_view(), name='weapon_list'),
    path('weapon/<int:pk>/', views.WeaponDetailView.as_view(), name='weapon_detail'),
    path('weapon-category/', views.WeaponCategoryListView.as_view(), name='weapon_category_list'),
    path('weapon-category/<int:pk>/', views.WeaponCategoryDetailView.as_view(), name='weapon_category_detail'),
    path('module/', views.ModuleListView.as_view(), name='module_list'),
    path('module/<int:pk>/', views.ModuleDetailView.as_view(), name='module_detail'),
    path('module-category/', views.ModuleCategoryListView.as_view(), name='module_list'),
    path('module-category/<int:pk>/', views.ModuleCategoryDetailView.as_view(), name='module_detail'),
    path('effect/', views.EffectListView.as_view(), name='effect_list'),
    path('effect/<int:pk>/', views.EffectDetailView.as_view(), name='effect_detail'),
    path('enhancement/', views.EnhancementListView.as_view(), name='enhancement_list'),
    path('enhancement/<int:pk>/', views.EnhancementDetailView.as_view(), name='enhancement_detail'),
    path('overdrive/', views.OverdriveListView.as_view(), name='overdrive_list'),
    path('overdrive/<int:pk>/', views.OverdriveDetailView.as_view(), name='overdrive_detail'),
    path('archetype/', views.ArchetypeListView.as_view(), name='archetype_list'),
    path('archetype/<int:pk>/', views.ArchetypeDetailView.as_view(), name='archetype_detail'),
    path('crest/', views.CrestListView.as_view(), name='crest_list'),
    path('crest/<int:pk>/', views.CrestDetailView.as_view(), name='crest_detail'),
    path('great-deed/', views.GreatDeedListView.as_view(), name='great_deed_list'),
    path('great-deed/<int:pk>/', views.GreatDeedDetailView.as_view(), name='great_deed_detail'),
    path('arcana/', views.ArcanaListView.as_view(), name='arcana_list'),
    path('arcana/<int:pk>/', views.ArcanaDetailView.as_view(), name='arcana_detail'),
    path('division/', views.DivisionListView.as_view(), name='division_list'),
    path('division/<int:pk>/', views.DivisionDetailView.as_view(), name='division_detail'),
    path('armour/', views.ArmourListView.as_view(), name='armour_list'),
    path('armour/<int:pk>/', views.ArmourDetailView.as_view(), name='armour_detail'),
    path('heroic-ability/', views.HeroicAbilityListView.as_view(), name='heroic_ability_list'),
    path('heroic-ability/<int:pk>/', views.HeroicAbilityDetailView.as_view(), name='heroic_ability_detail'),
    path('heroic-ability-category/', views.HeroicAbilityCategoryListView.as_view(), name='heroic_ability_category_list'),
    path('heroic-ability-category/<int:pk>/', views.HeroicAbilityCategoryDetailView.as_view(), name='heroic_ability_category_detail'),
    path('npc-ability/', views.NonPlayerCharacterAbilityListView.as_view(), name='npc_ability_list'),
    path('npc-ability/<int:pk>/', views.NonPlayerCharacterAbilityDetailView.as_view(), name='npc_ability_detail'),
    path('style/', views.StyleListView.as_view(), name='style_list'),
    path('style/<int:pk>/', views.StyleDetailView.as_view(), name='style_detail'),
    path('trauma-category/', views.TraumaCategoryListView.as_view(), name='trauma_category_list'),
    path('trauma-category/<int:pk>/', views.TraumaCategoryDetailView.as_view(), name='trauma_category_detail'),
    path('trauma/<int:pk>/', views.TraumaDetailView.as_view(), name='trauma_detail'),
    path('mecha-armour/', views.MechaArmourListView.as_view(), name='mecha_armour_list'),
    path('mecha-armour/<int:pk>/', views.MechaArmourDetailView.as_view(), name='mecha_armour_detail'),
    
    path('effect/<slug:slug>/', views.EffectDetailSlugView.as_view(), name='effect_slug'), # USED FOR KNIGHTOOLS API!
    
    
    path('npc-capacity/', views.NonPlayerCharacterAbilityListView.as_view(), name='npc_capacity_list'),
    path('npc-capacity/<int:pk>/', views.NonPlayerCharacterAbilityDetailView.as_view(), name='npc_capacity_detail'),
    path('stance/', views.StyleListView.as_view(), name='stance_list'),
    path('stance/<int:pk>/', views.StyleDetailView.as_view(), name='stance_detail'),
    path('great_deed/', views.GreatDeedListView.as_view(), name='retrocomp_great_deed_list'),
    path('great_deed/<int:pk>/', views.GreatDeedDetailView.as_view(), name='retrocomp_great_deed_detail'),
    path('skill/', views.HeroicAbilityListView.as_view(), name='skill_list'),
    path('skill/<int:pk>/', views.HeroicAbilityDetailView.as_view(), name='skill_detail'),
    path('skill-category/', views.HeroicAbilityCategoryListView.as_view(), name='skill_category_list'),
    path('skill-category/<int:pk>/', views.HeroicAbilityCategoryDetailView.as_view(), name='skill_category_detail'),
    
]
