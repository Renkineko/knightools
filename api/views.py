from django.utils.translation import get_language_from_path
from parler.views import TranslatableSlugMixin
from django.db.models import Q, Prefetch

from rest_framework import generics
from .serializers.weapons import (
    WLWeaponSerializer, WDWeaponSerializer, WCWeaponCategorySerializer
)
from .serializers.modules import (
    MLModuleSerializer, MDModuleSerializer, MCModuleCategorySerializer
)
from .serializers.effects import (
    EDEffectSerializer, ELEffectSerializer
)
from .serializers.enhancements import (
    BDEnhancementSerializer, BLEnhancementSerializer
)
from .serializers.overdrives import (
    DetailOverdriveSerializer, ListOverdriveSerializer
)

from .serializers.archetypes import (
    DetailArchetypeSerializer, ListArchetypeSerializer
)
from .serializers.crests import (
    DetailCrestSerializer, ListCrestSerializer
)
from .serializers.great_deeds import (
    DetailGreatDeedSerializer, ListGreatDeedSerializer
)
from .serializers.arcanas import (
    DetailArcanaSerializer, ListArcanaSerializer,
    DetailLimitedArcanaSerializer, ListLimitedArcanaSerializer
)
from .serializers.divisions import (
    DetailDivisionSerializer, ListDivisionSerializer
)
from .serializers.armours import (
    DetailArmourSerializer, ListArmourSerializer
)
from .serializers.heroic_abilities import (
    DetailHeroicAbilitySerializer, ListHeroicAbilitySerializer,
    ListHeroicAbilityCategorySerializer
)
from .serializers.npc_abilities import (
    DetailNonPlayerCharacterAbilitySerializer,
    ListNonPlayerCharacterAbilitySerializer
)
from .serializers.styles import (
    DetailStyleSerializer, ListStyleSerializer
)
from .serializers.trauma import (
    DetailTraumaCategorySerializer, ListTraumaCategorySerializer,
    DetailTraumaSerializer
)
from .serializers.mecha_armour import (
    DetailMechaArmourSerializer, ListMechaArmourSerializer
)

from gm.models.character import (
    Trauma, TraumaCategory, MechaArmour
)
from gm.models.character import (
    ArchetypeI18n, CrestI18n, GreatDeedI18n, ArmourI18n, ArcanaI18n,
    DivisionI18n, NonPlayerCharacterAbilityI18n, StyleI18n
)
from gm.models.weaponry import (
    WeaponI18n, WeaponCategoryI18n, ModuleI18n,
    ModuleCategoryI18n, EffectI18n, EnhancementI18n, OverdriveI18n,
    HeroicAbilityI18n, HeroicAbilityCategoryI18n,
    
    ModuleLevelI18n, ModuleLevelEffectI18n,
    WeaponAttackI18n, WeaponAttackEffectI18n,
    EnhancementEffectI18n
)
# from django.shortcuts import render

def get_queryset_list(model, path):
    language = get_language_from_path(path)
    
    return (
        model
        .objects
        .filter(
            translations__language_code=language
        )
        .all()
    )

def get_queryset_detail(model, path, prefetch=None):
    language = get_language_from_path(path)
    
    queryset = (model
        .objects
        .active_translations(language)
    )
    
    if prefetch:
        queryset = queryset.prefetch_related(*prefetch)
    
    return queryset.all()

class EffectListView(generics.ListAPIView):
    """This class handles the list requests of effects."""
    serializer_class = ELEffectSerializer
    
    def get_queryset(self):
        return get_queryset_list(EffectI18n, self.request.path)
    
class EffectDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an effect."""
    serializer_class = EDEffectSerializer
    
    def get_queryset(self):
        return get_queryset_detail(EffectI18n, self.request.path)
    
class EffectDetailSlugView(TranslatableSlugMixin, generics.RetrieveAPIView):
    """This class handles the get requests of an effect."""
    serializer_class = EDEffectSerializer
    lookup_field = 'slug'
    slug_url_kwarg = 'slug'  # Ensure this matches the URL pattern parameter

    def get_slug_field(self):
        """Return the name of the translated field to use for lookups."""
        return 'slug'

    def get_queryset(self):
        return get_queryset_detail(EffectI18n, self.request.path)

class EnhancementListView(generics.ListAPIView):
    """This class handles the list requests of enhancements."""
    serializer_class = BLEnhancementSerializer
    
    def get_queryset(self):
        return get_queryset_list(EnhancementI18n, self.request.path)
    
class EnhancementDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an enhancement."""
    serializer_class = BDEnhancementSerializer
    
    def get_queryset(self):
        return get_queryset_detail(EnhancementI18n, self.request.path)
    
class ModuleListView(generics.ListAPIView):
    """This class handles the list requests of modules."""
    serializer_class = MLModuleSerializer
    
    def get_queryset(self):
        return get_queryset_list(ModuleI18n, self.request.path)
    
class ModuleDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a module."""
    serializer_class = MDModuleSerializer
    
    def get_queryset(self):
        language = get_language_from_path(self.request.path)
        return get_queryset_detail(
            ModuleI18n,
            self.request.path,
            [
                Prefetch(
                    'levels',
                    queryset=(
                        ModuleLevelI18n
                        .objects
                        .active_translations(language)
                        .filter(
                            translations__language_code=language,
                        )
                    )
                ),
                Prefetch(
                    'levels__effects',
                    queryset=(
                        ModuleLevelEffectI18n
                        .objects
                        .active_translations(language)
                        .filter(
                            Q(
                                translations__language_code=language,
                                effect__translations__language_code=language,
                                module_level__translations__language_code=language, # I don't understand why I must put these filters, but well...
                                module_level__module__translations__language_code=language, # I don't understand why I must put these filters, but well...
                            )
                            | Q(
                                translations__language_code=language,
                                effect=None,
                                module_level__translations__language_code=language, # I don't understand why I must put these filters, but well...
                                module_level__module__translations__language_code=language, # I don't understand why I must put these filters, but well...
                            )
                        )
                    )
                )
            ]
        )
    
class ModuleCategoryListView(generics.ListAPIView):
    """This class handles the list requests of module categories."""
    serializer_class = MCModuleCategorySerializer
    
    def get_queryset(self):
        return get_queryset_list(ModuleCategoryI18n, self.request.path)
    
class ModuleCategoryDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a module category."""
    serializer_class = MCModuleCategorySerializer
    
    def get_queryset(self):
        return get_queryset_detail(ModuleCategoryI18n, self.request.path)
    
class WeaponListView(generics.ListAPIView):
    """This class handles the list requests of weapons."""
    serializer_class = WLWeaponSerializer
    
    def get_queryset(self):
        return get_queryset_list(WeaponI18n, self.request.path)

class WeaponDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a weapon."""
    serializer_class = WDWeaponSerializer
    
    def get_queryset(self):
        language = get_language_from_path(self.request.path)
        
        return get_queryset_detail(
            WeaponI18n,
            self.request.path,
            [
                Prefetch(
                    'attacks',
                    queryset=(
                        WeaponAttackI18n
                        .objects
                        .active_translations(language)
                        .filter(
                            translations__language_code=language,
                        )
                    )
                ),
                Prefetch(
                    'attacks__effects',
                    queryset=(
                        WeaponAttackEffectI18n
                        .objects
                        .active_translations(language)
                        .filter(
                            Q(
                                translations__language_code=language,
                                effect__translations__language_code=language,
                                weapon_attack__translations__language_code=language, # I don't understand why I must put these filters, but well...
                                weapon_attack__weapon__translations__language_code=language, # I don't understand why I must put these filters, but well...
                            )
                            | Q(
                                translations__language_code=language,
                                effect=None,
                                weapon_attack__translations__language_code=language, # I don't understand why I must put these filters, but well...
                                weapon_attack__weapon__translations__language_code=language, # I don't understand why I must put these filters, but well...
                            )
                        )
                    )
                ),
                Prefetch(
                    'enhancements',
                    queryset=(
                        EnhancementI18n
                        .objects
                        .active_translations(language)
                    )
                ),
            ]
        )

class WeaponCategoryListView(generics.ListAPIView):
    """This class handles the list requests of weapon categories."""
    serializer_class = WCWeaponCategorySerializer
    
    def get_queryset(self):
        return get_queryset_list(WeaponCategoryI18n, self.request.path)

class WeaponCategoryDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a weapon category."""
    serializer_class = WCWeaponCategorySerializer
    
    def get_queryset(self):
        return get_queryset_detail(WeaponCategoryI18n, self.request.path)
        
class OverdriveListView(generics.ListAPIView):
    """This class handles the list requests of overdrives."""
    serializer_class = ListOverdriveSerializer
    
    def get_queryset(self):
        return get_queryset_list(OverdriveI18n, self.request.path)
    
class OverdriveDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an overdrive."""
    serializer_class = DetailOverdriveSerializer
    
    def get_queryset(self):
        return get_queryset_detail(OverdriveI18n, self.request.path)

class ArchetypeListView(generics.ListAPIView):
    """This class handles the list requests of archetypes."""
    serializer_class = ListArchetypeSerializer
    
    def get_queryset(self):
        return get_queryset_list(ArchetypeI18n, self.request.path)

class ArchetypeDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an archetype."""
    serializer_class = DetailArchetypeSerializer
    
    def get_queryset(self): 
        return get_queryset_detail(ArchetypeI18n, self.request.path)

class CrestListView(generics.ListAPIView):
    """This class handles the list requests of crests."""
    serializer_class = ListCrestSerializer
    
    def get_queryset(self):
        return get_queryset_list(CrestI18n, self.request.path)

class CrestDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an crest."""
    serializer_class = DetailCrestSerializer
    
    def get_queryset(self): 
        return get_queryset_detail(CrestI18n, self.request.path)

class GreatDeedListView(generics.ListAPIView):
    """This class handles the list requests of great deeds."""
    serializer_class = ListGreatDeedSerializer
    
    def get_queryset(self):
        return get_queryset_list(GreatDeedI18n, self.request.path)
    
class GreatDeedDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a great deed."""
    serializer_class = DetailGreatDeedSerializer
    
    def get_queryset(self):
        return get_queryset_detail(GreatDeedI18n, self.request.path)
    
class DivisionListView(generics.ListAPIView):
    """This class handles the list requests of divisions."""
    serializer_class = ListDivisionSerializer
    
    def get_queryset(self):
        return get_queryset_list(DivisionI18n, self.request.path)
    
class DivisionDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an division."""
    serializer_class = DetailDivisionSerializer
    
    def get_queryset(self):
        return get_queryset_detail(DivisionI18n, self.request.path)
    
class ArcanaListView(generics.ListAPIView):
    """This class handles the list requests of major arcanas."""
    def get_serializer_class(self):
        language = get_language_from_path(self.request.path)
        
        if language == 'fr':
            return ListArcanaSerializer
        else:
            return ListLimitedArcanaSerializer
    
    def get_queryset(self):
        return get_queryset_list(ArcanaI18n, self.request.path)

class ArcanaDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an major arcana."""
    serializer_class = DetailArcanaSerializer
    
    def get_serializer_class(self):
        language = get_language_from_path(self.request.path)
        
        if language == 'fr':
            return DetailArcanaSerializer
        else:
            return DetailLimitedArcanaSerializer
    
    def get_queryset(self):
        return get_queryset_detail(ArcanaI18n, self.request.path)

class ArmourListView(generics.ListAPIView):
    """This class handles the list requests of armours."""
    serializer_class = ListArmourSerializer
    
    def get_queryset(self):
        return get_queryset_list(ArmourI18n, self.request.path)
    
class ArmourDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an armour."""
    serializer_class = DetailArmourSerializer
    
    def get_queryset(self):
        return get_queryset_detail(ArmourI18n, self.request.path)
    
class HeroicAbilityListView(generics.ListAPIView):
    """This class handles the list requests of heroic skills."""
    serializer_class = ListHeroicAbilitySerializer
    
    def get_queryset(self):
        return get_queryset_list(HeroicAbilityI18n, self.request.path)
    
class HeroicAbilityDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an heroic skill."""
    serializer_class = DetailHeroicAbilitySerializer
    
    def get_queryset(self):
        return get_queryset_detail(HeroicAbilityI18n, self.request.path)
    
class HeroicAbilityCategoryListView(generics.ListAPIView):
    """This class handles the list requests of heroic skill categories."""
    serializer_class = ListHeroicAbilityCategorySerializer
    
    def get_queryset(self):
        return get_queryset_list(HeroicAbilityCategoryI18n, self.request.path)
    
class HeroicAbilityCategoryDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a heroic skill category."""
    serializer_class = ListHeroicAbilityCategorySerializer
    
    def get_queryset(self):
        return get_queryset_detail(HeroicAbilityCategoryI18n, self.request.path)
    
class NonPlayerCharacterAbilityListView(generics.ListAPIView):
    """This class handles the list requests of NPC abilities."""
    serializer_class = ListNonPlayerCharacterAbilitySerializer
    
    def get_queryset(self):
        return get_queryset_list(NonPlayerCharacterAbilityI18n, self.request.path)
    
class NonPlayerCharacterAbilityDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an NPC ability."""
    serializer_class = DetailNonPlayerCharacterAbilitySerializer
    
    def get_queryset(self):
        return get_queryset_detail(NonPlayerCharacterAbilityI18n, self.request.path)

class StyleListView(generics.ListAPIView):
    """This class handles the list requests of style."""
    serializer_class = ListStyleSerializer
    
    def get_queryset(self): 
        return get_queryset_list(StyleI18n, self.request.path)

class StyleDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a style."""
    serializer_class = DetailStyleSerializer
    
    def get_queryset(self): 
        return get_queryset_detail(StyleI18n, self.request.path)

class TraumaCategoryListView(generics.ListAPIView):
    """This class handles the list requests of trauma categories."""
    queryset = TraumaCategory.objects.all()
    serializer_class = ListTraumaCategorySerializer
    
class TraumaCategoryDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a trauma categories."""
    queryset = TraumaCategory.objects.all()
    serializer_class = DetailTraumaCategorySerializer
    
class TraumaDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a trauma."""
    queryset = Trauma.objects.all()
    serializer_class = DetailTraumaSerializer
    
class MechaArmourListView(generics.ListAPIView):
    """This class handles the list requests of NPC capacities."""
    queryset = MechaArmour.objects.all()
    serializer_class = ListMechaArmourSerializer
    
class MechaArmourDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a NPC capacity."""
    queryset = MechaArmour.objects.all()
    serializer_class = DetailMechaArmourSerializer
    