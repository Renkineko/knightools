from rest_framework import serializers
from gm.models.weaponry import (
    HeroicAbilityI18n, HeroicAbilityCategoryI18n
)
from .common import NameRelatedSerializer

class DetailCategorySerializer(serializers.ModelSerializer):
    """Serializer to map the Model HeroicAbilityCategoryI18n instance into JSON format from a heroic skill Detail view."""
    
    class Meta:
        model = HeroicAbilityCategoryI18n
        fields = (
            'id',
            'name',
        )
        read_only_fields = (
            'id',
            'name',
        )

class DetailHeroicAbilitySerializer(serializers.ModelSerializer):
    """Serializer to map the Model HeroicAbilityI18n instance into JSON format from a heroic skill Detail view."""
    activation = NameRelatedSerializer(read_only=True)
    category = DetailCategorySerializer(read_only=True)
    
    class Meta:
        model = HeroicAbilityI18n
        fields = (
            'id',
            'name',
            'description',
            'slug',
            'activation',
            'category',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'slug',
            'activation',
            'category',
        )

class ListHeroicAbilitySerializer(serializers.ModelSerializer):
    """Serializer to map the Model HeroicAbilityI18n instance into JSON format from a heroic skill List view."""
    activation = NameRelatedSerializer(read_only=True)
    category = DetailCategorySerializer(read_only=True)
    
    class Meta:
        model = HeroicAbilityI18n
        fields = (
            'id',
            'name',
            'description',
            'slug',
            'activation',
            'category',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'slug',
            'activation',
            'category',
        )

class ListHeroicAbilityWithoutCategorySerializer(serializers.ModelSerializer):
    """Serializer to map the Model HeroicAbilityI18n instance into JSON format from a heroic skill List view."""
    activation = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = HeroicAbilityI18n
        fields = (
            'id',
            'name',
            'description',
            'slug',
            'activation',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'slug',
            'activation',
        )

class ListHeroicAbilityCategorySerializer(serializers.ModelSerializer):
    """Serializer to map the Model HeroicAbilityCategoryI18n instance into JSON format from a heroic skill category (list or detail) view."""
    skills = ListHeroicAbilityWithoutCategorySerializer(read_only=True, many=True)
    
    class Meta:
        model = HeroicAbilityCategoryI18n
        fields = (
            'id',
            'name',
            'skills',
        )
        read_only_fields = (
            'id',
            'name',
            'skills',
        )