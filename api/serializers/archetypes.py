from rest_framework import serializers
from gm.models.character import ArchetypeI18n
from .common import CharacteristicSerializer

class DetailArchetypeSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ArchetypeI18n instance into JSON format from an ArchetypeI18n Detail view."""
    characteristic_bonus_1 = CharacteristicSerializer(read_only=True)
    characteristic_bonus_2 = CharacteristicSerializer(read_only=True)
    characteristic_bonus_3 = CharacteristicSerializer(read_only=True)
    
    class Meta:
        model = ArchetypeI18n
        fields = (
            'id',
            'name',
            'description',
            'characteristic_bonus_1',
            'characteristic_bonus_2',
            'characteristic_bonus_3',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'characteristic_bonus_1',
            'characteristic_bonus_2',
            'characteristic_bonus_3',
            'slug',
        )

class ListArchetypeSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ArchetypeI18n instance into JSON format from an ArchetypeI18n List view."""
    characteristic_bonus_1 = CharacteristicSerializer(read_only=True)
    characteristic_bonus_2 = CharacteristicSerializer(read_only=True)
    characteristic_bonus_3 = CharacteristicSerializer(read_only=True)
    
    class Meta:
        model = ArchetypeI18n
        fields = (
            'id',
            'name',
            'characteristic_bonus_1',
            'characteristic_bonus_2',
            'characteristic_bonus_3',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'characteristic_bonus_1',
            'characteristic_bonus_2',
            'characteristic_bonus_3',
            'slug',
        )