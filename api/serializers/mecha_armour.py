from rest_framework import serializers
from gm.models.character import MechaArmour, MechaArmourConfiguration
from gm.models.weaponry import MechaArmourAction, TitanReach

class DetailMechaArmourActionTitanReachSerializer(serializers.ModelSerializer):
    """Serializer to map the Model TitanReach instance into JSON format from a MechaArmourAction Detail view."""
    
    class Meta:
        model = TitanReach
        fields = (
            'name',
            'description',
            'short_description',
        )
        read_only_fields = (
            'name',
            'description',
            'short_description',
        )

class DetailMechaArmourActionSerializer(serializers.ModelSerializer):
    """Serializer to map the Model MechaArmourAction instance into JSON format from a MechaArmour Detail view."""
    
    description = serializers.CharField(source="description_text_only")
    reach = DetailMechaArmourActionTitanReachSerializer(read_only=True)
    
    class Meta:
        model = MechaArmourAction
        fields = (
            'name',
            'description',
            'activation',
            'core',
            'duration',
            'reach',
            'damage_dice',
            'damage_bonus',
            'violence_dice',
            'violence_bonus',
            'effects',
        )
        read_only_fields = (
            'name',
            'description',
            'activation',
            'core',
            'duration',
            'reach',
            'damage_dice',
            'damage_bonus',
            'violence_dice',
            'violence_bonus',
            'effects',
        )
    

class DetailMechaArmourConfigurationSerializer(serializers.ModelSerializer):
    """Serializer to map the Model MechaArmour instance into JSON format from a MechaArmour Detail view."""
    
    description = serializers.CharField(source="description_text_only")
    actions = DetailMechaArmourActionSerializer(many=True, read_only=True)
    
    class Meta:
        model = MechaArmourConfiguration
        fields = (
            'id',
            'name',
            'description',
            'actions',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'actions',
        )

class DetailMechaArmourSerializer(serializers.ModelSerializer):
    """Serializer to map the Model MechaArmour instance into JSON format from a MechaArmour Detail view."""
    
    description = serializers.CharField(source="description_text_only")
    configurations = DetailMechaArmourConfigurationSerializer(many=True, read_only=True)
    actions = DetailMechaArmourActionSerializer(many=True, read_only=True)
    
    class Meta:
        model = MechaArmour
        fields = (
            'id',
            'name',
            'description',
            'speed',
            'maneuverability',
            'power',
            'sensors',
            'systems',
            'resilience',
            'armour_plating',
            'force_field',
            'energy_core',
            'configurations',
            'actions',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'speed',
            'maneuverability',
            'power',
            'sensors',
            'systems',
            'resilience',
            'armour_plating',
            'force_field',
            'energy_core',
            'configurations',
            'actions',
            'slug',
        )

class ListMechaArmourSerializer(serializers.ModelSerializer):
    """Serializer to map the Model MechaArmour instance into JSON format from a MechaArmour List view."""
    
    class Meta:
        model = MechaArmour
        fields = (
            'id',
            'name',
            'speed',
            'maneuverability',
            'power',
            'sensors',
            'systems',
            'resilience',
            'armour_plating',
            'force_field',
            'energy_core',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'speed',
            'maneuverability',
            'power',
            'sensors',
            'systems',
            'resilience',
            'armour_plating',
            'force_field',
            'energy_core',
            'slug',
        )