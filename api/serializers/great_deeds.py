from rest_framework import serializers
from gm.models.character import GreatDeedI18n
from .common import NameRelatedSerializer

class DetailGreatDeedSerializer(serializers.ModelSerializer):
    """Serializer to map the Model GreatDeed instance into JSON format from a GreatDeed Detail view."""
    aspect_1 = NameRelatedSerializer(read_only=True)
    aspect_2 = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = GreatDeedI18n
        fields = (
            'id',
            'name',
            'description',
            'restriction',
            'aspect_1',
            'aspect_2',
            'nickname_suggestion',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'restriction',
            'aspect_1',
            'aspect_2',
            'nickname_suggestion',
            'slug',
        )

class ListGreatDeedSerializer(serializers.ModelSerializer):
    """Serializer to map the Model GreatDeed instance into JSON format from a GreatDeed List view."""
    aspect_1 = NameRelatedSerializer(read_only=True)
    aspect_2 = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = GreatDeedI18n
        fields = (
            'id',
            'name',
            'description',
            'restriction',
            'aspect_1',
            'aspect_2',
            'nickname_suggestion',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'restriction',
            'aspect_1',
            'aspect_2',
            'nickname_suggestion',
            'slug',
        )