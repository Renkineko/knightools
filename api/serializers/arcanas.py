from rest_framework import serializers
from gm.models.character import ArcanaI18n
from .common import NameRelatedSerializer

class DetailArcanaSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ArcanaI18n instance into JSON format from a major arcana Detail view."""
    forced_aspect = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = ArcanaI18n
        fields = (
            'id',
            'name',
            'description',
            'slug',
            'destiny_quote',
            'destiny_effect',
            'past',
            'creation_bonus',
            'aspect_point',
            'characteristic_point',
            'advantage_name',
            'advantage',
            'disadvantage_name',
            'disadvantage',
            'ai_description',
            'ai_letter',
            'ai_keywords',
            'ai_behavior',
            'ai_advantage_name',
            'ai_advantage',
            'ai_disadvantage_name',
            'ai_disadvantage',
            'roman_number',
            'number',
            'oracle_future',
            'oracle_fortune_name',
            'oracle_fortune_description',
            'oracle_calamity_name',
            'oracle_calamity_description',
            'forced_aspect',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'slug',
            'destiny_quote',
            'destiny_effect',
            'past',
            'creation_bonus',
            'aspect_point',
            'characteristic_point',
            'advantage_name',
            'advantage',
            'disadvantage_name',
            'disadvantage',
            'ai_description',
            'ai_letter',
            'ai_keywords',
            'ai_behavior',
            'ai_advantage_name',
            'ai_advantage',
            'ai_disadvantage_name',
            'ai_disadvantage',
            'roman_number',
            'number',
            'oracle_future',
            'oracle_fortune_name',
            'oracle_fortune_description',
            'oracle_calamity_name',
            'oracle_calamity_description',
            'forced_aspect',
        )

class ListArcanaSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ArcanaI18n instance into JSON format from a major arcana List view."""
    forced_aspect = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = ArcanaI18n
        fields = (
            'id',
            'name',
            'description',
            'slug',
            'destiny_quote',
            'destiny_effect',
            'past',
            'creation_bonus',
            'aspect_point',
            'characteristic_point',
            'advantage_name',
            'advantage',
            'disadvantage_name',
            'disadvantage',
            'ai_description',
            'ai_letter',
            'ai_keywords',
            'ai_behavior',
            'ai_advantage_name',
            'ai_advantage',
            'ai_disadvantage_name',
            'ai_disadvantage',
            'roman_number',
            'number',
            'oracle_future',
            'oracle_fortune_name',
            'oracle_fortune_description',
            'oracle_calamity_name',
            'oracle_calamity_description',
            'forced_aspect',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'slug',
            'destiny_quote',
            'destiny_effect',
            'past',
            'creation_bonus',
            'aspect_point',
            'characteristic_point',
            'advantage_name',
            'advantage',
            'disadvantage_name',
            'disadvantage',
            'ai_description',
            'ai_letter',
            'ai_keywords',
            'ai_behavior',
            'ai_advantage_name',
            'ai_advantage',
            'ai_disadvantage_name',
            'ai_disadvantage',
            'roman_number',
            'number',
            'oracle_future',
            'oracle_fortune_name',
            'oracle_fortune_description',
            'oracle_calamity_name',
            'oracle_calamity_description',
            'forced_aspect',
        )

class DetailLimitedArcanaSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ArcanaI18n instance into JSON format from a major arcana Detail view (limited view, without ai and oracle)."""
    forced_aspect = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = ArcanaI18n
        fields = (
            'id',
            'name',
            'description',
            'slug',
            'destiny_quote',
            'destiny_effect',
            'past',
            'creation_bonus',
            'aspect_point',
            'characteristic_point',
            'advantage_name',
            'advantage',
            'disadvantage_name',
            'disadvantage',
            'roman_number',
            'number',
            'forced_aspect',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'slug',
            'destiny_quote',
            'destiny_effect',
            'past',
            'creation_bonus',
            'aspect_point',
            'characteristic_point',
            'advantage_name',
            'advantage',
            'disadvantage_name',
            'disadvantage',
            'roman_number',
            'number',
            'forced_aspect',
        )

class ListLimitedArcanaSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ArcanaI18n instance into JSON format from a major arcana List view (limited view, without ai and oracle)."""
    forced_aspect = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = ArcanaI18n
        fields = (
            'id',
            'name',
            'description',
            'slug',
            'destiny_quote',
            'destiny_effect',
            'past',
            'creation_bonus',
            'aspect_point',
            'characteristic_point',
            'advantage_name',
            'advantage',
            'disadvantage_name',
            'disadvantage',
            'roman_number',
            'number',
            'forced_aspect',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'slug',
            'destiny_quote',
            'destiny_effect',
            'past',
            'creation_bonus',
            'aspect_point',
            'characteristic_point',
            'advantage_name',
            'advantage',
            'disadvantage_name',
            'disadvantage',
            'roman_number',
            'number',
            'forced_aspect',
        )