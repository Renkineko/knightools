from rest_framework import serializers
from gm.models.weaponry import (
    EffectI18n, EnhancementEffectI18n, EnhancementI18n,
    WeaponAttackEffectI18n, WeaponAttackI18n, WeaponI18n,
    ModuleLevelEffectI18n, ModuleLevelI18n, ModuleI18n
)
from .weapons import WDWeaponCategorySerializer
from .modules import MDModuleCategorySerializer

# EFFECT DETAIL SERIALIZERS: prefix ED

class EDModuleSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Module instance into JSON format from a Effect Detail view."""
    
    category = MDModuleCategorySerializer(read_only=True)
    
    class Meta:
        model = ModuleI18n
        fields = ('id', 'name', 'slug', 'category')
        read_only_fields = ('id', 'name', 'slug')

class EDModuleLevelSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ModuleLevel instance into JSON format from a Effect Detail view."""
    
    module = EDModuleSerializer(read_only=True)
    
    class Meta:
        model = ModuleLevelI18n
        fields = ('module', 'level', 'rarity', 'reach', 'energy')
        read_only_fields = ('level', 'rarity', 'reach', 'energy')

class EDModuleLevelEffectSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ModuleLevelEffect instance into JSON format from a Effect Detail view."""
    
    module_level = EDModuleLevelSerializer(read_only=True)
    
    class Meta:
        model = ModuleLevelEffectI18n
        fields = ('choice_number', 'effect_level', 'damage', 'violence', 'forced_by_previous_level', 'effect_condition', 'module_level')
        read_only_fields = ('choice_number', 'effect_level', 'damage', 'violence', 'forced_by_previous_level', 'effect_condition')

class EDWeaponSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Weapon instance into JSON format from a Effect Detail view."""
    
    category = WDWeaponCategorySerializer(read_only=True)
    
    class Meta:
        model = WeaponI18n
        fields = ('id', 'name', 'slug', 'category')
        read_only_fields = ('id', 'name', 'slug')

class EDWeaponAttackSerializer(serializers.ModelSerializer):
    """Serializer to map the Model WeaponAttack instance into JSON format from a Effect Detail view."""
    
    weapon = EDWeaponSerializer(read_only=True)
    
    class Meta:
        model = WeaponAttackI18n
        fields = ('weapon', 'position', 'name', 'damage_dice', 'damage_bonus', 'violence_dice', 'violence_bonus', 'reach', 'energy')
        read_only_fields = ('position', 'name', 'damage_dice', 'damage_bonus', 'violence_dice', 'violence_bonus', 'reach', 'energy')

class EDWeaponAttackEffectSerializer(serializers.ModelSerializer):
    """Serializer to map the Model WeaponAttackEffect instance into JSON format from a Effect Detail view."""
    
    weapon_attack = EDWeaponAttackSerializer(read_only=True)
    
    class Meta:
        model = WeaponAttackEffectI18n
        fields = ('effect_level', 'energy', 'effect_condition', 'weapon_attack')
        read_only_fields = ('effect_level', 'energy', 'effect_condition')

class EDEnhancementSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Enhancement instance into JSON format from a Effect Detail view."""
    
    class Meta:
        model = EnhancementI18n
        fields = ('id', 'name', 'slug')
        read_only_fields = ('id', 'name', 'slug')

class EDEnhancementEffectSerializer(serializers.ModelSerializer):
    """Serializer to map the Model EnhancementEffect instance into JSON format from a Effect Detail view."""
    
    enhancement = EDEnhancementSerializer(read_only=True)
    
    class Meta:
        model = EnhancementEffectI18n
        fields = ('enhancement', 'effect_level', 'damage_dice', 'violence_dice', 'effect_condition')
        read_only_fields = ('effect_level', 'damage_dice', 'violence_dice', 'effect_condition')

class EDEffectSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Effect instance into JSON format from a Effect Detail view."""
    
    enhancements = EDEnhancementEffectSerializer(many=True, read_only=True)
    weaponattackeffects = EDWeaponAttackEffectSerializer(many=True, read_only=True)
    moduleleveleffects = EDModuleLevelEffectSerializer(many=True, read_only=True)
    
    class Meta:
        model = EffectI18n
        fields = ('id', 'name', 'slug', 'description', 'short_description', 'weaponattackeffects', 'moduleleveleffects', 'enhancements')
        read_only_fields = ('id', 'name', 'slug', 'description', 'short_description')

# EFFECT LIST SERIALIZERS: prefix EL

class ELEffectSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Effect instance into JSON format from a Effect List view."""
    
    class Meta:
        model = EffectI18n
        fields = ('id', 'name', 'slug', 'description')
        fields = ('id', 'name', 'slug', 'description')
