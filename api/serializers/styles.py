from rest_framework import serializers
from gm.models.character import StyleI18n

class DetailStyleSerializer(serializers.ModelSerializer):
    """Serializer to map the Model StyleI18n instance into JSON format from a Style Detail view."""
    
    description = serializers.CharField(source="description_text_only")
    bonus = serializers.CharField(source="bonus_text_only")
    malus = serializers.CharField(source="malus_text_only")
    examples = serializers.CharField(source="examples_text_only")
    constraint = serializers.CharField(source="constraint_text_only")
    
    class Meta:
        model = StyleI18n
        fields = (
            'id',
            'name',
            'constraint',
            'description',
            'bonus',
            'malus',
            'examples',
        )
        read_only_fields = (
            'id',
            'name',
            'constraint',
            'description',
            'bonus',
            'malus',
            'examples',
        )

class ListStyleSerializer(serializers.ModelSerializer):
    """Serializer to map the Model StyleI18n instance into JSON format from a Style List view."""
    
    description = serializers.CharField(source="description_text_only")
    
    class Meta:
        model = StyleI18n
        fields = (
            'id',
            'name',
            'constraint',
            'description',
        )
        read_only_fields = (
            'id',
            'name',
            'constraint',
            'description',
        )