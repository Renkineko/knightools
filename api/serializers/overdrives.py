from rest_framework import serializers
from gm.models.weaponry import OverdriveI18n
from .common import NameRelatedSerializer, CharacteristicSerializer

class DetailOverdriveSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Overdrive instance into JSON format from an Overdrive Detail view."""
    characteristic = CharacteristicSerializer(read_only=True)
    rarity = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = OverdriveI18n
        fields = (
            'id',
            'characteristic',
            'level',
            'description',
            'rarity',
            'cost',
            'slug',
        )
        read_only_fields = (
            'id',
            'characteristic',
            'level',
            'description',
            'rarity',
            'cost',
            'slug',
        )

class ListOverdriveSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Overdrive instance into JSON format from an Overdrive List view."""
    characteristic = CharacteristicSerializer(read_only=True)
    rarity = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = OverdriveI18n
        fields = (
            'id',
            'characteristic',
            'level',
            'description',
            'rarity',
            'cost',
            'slug',
        )
        read_only_fields = (
            'id',
            'characteristic',
            'level',
            'description',
            'rarity',
            'cost',
            'slug',
        )
