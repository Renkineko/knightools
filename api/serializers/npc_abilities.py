from html5lib import serialize
from rest_framework import serializers
from gm.models.character import NonPlayerCharacterAbilityI18n
from .common import NameRelatedSerializer

class DetailNonPlayerCharacterAbilitySerializer(serializers.ModelSerializer):
    """Serializer to map the Model NonPlayerCharacterAbilityI18n instance into JSON format from a NonPlayerCharacterAbility Detail view."""
    
    min_difficulty = NameRelatedSerializer(read_only=True)
    description = serializers.CharField(source="description_text_only")
    
    class Meta:
        model = NonPlayerCharacterAbilityI18n
        fields = (
            'id',
            'name',
            'description',
            'min_difficulty',
            'common',
            'penalty',
            'elite',
            'human_allowed',
            'anathema_allowed',
            'ether_allowed',
            'void_allowed',
            'precision',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'min_difficulty',
            'common',
            'penalty',
            'elite',
            'human_allowed',
            'anathema_allowed',
            'ether_allowed',
            'void_allowed',
            'precision',
            'slug',
        )

class ListNonPlayerCharacterAbilitySerializer(serializers.ModelSerializer):
    """Serializer to map the Model NonPlayerCharacterAbilityI18n instance into JSON format from a NonPlayerCharacterAbility List view."""
    
    min_difficulty = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = NonPlayerCharacterAbilityI18n
        fields = (
            'id',
            'name',
            'min_difficulty',
            'common',
            'penalty',
            'elite',
            'human_allowed',
            'anathema_allowed',
            'ether_allowed',
            'void_allowed',
            'precision',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'min_difficulty',
            'common',
            'penalty',
            'elite',
            'human_allowed',
            'anathema_allowed',
            'ether_allowed',
            'void_allowed',
            'precision',
            'slug',
        )