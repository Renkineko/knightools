from rest_framework import serializers
from gm.models.weaponry import (
    ModuleCategoryI18n, EffectI18n, ModuleI18n, ModuleLevelI18n,
    ModuleLevelEffectI18n, ModuleSlotI18n
)
from .common import NameRelatedSerializer, CommonNonPlayerCharacterSerializer

# MODULE CATEGORY SERIALIZERS: prefix MC

class MCModuleLevelSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ModuleLevel instance into JSON format from a Module Category (list or detail) view."""
    
    rarity = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = ModuleLevelI18n
        fields = ('level', 'rarity', 'cost')
        read_only_fields = ('level', 'rarity', 'cost')

class MCModuleSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Module instance into JSON format from a Module Category (list or detail) view."""
    
    levels = MCModuleLevelSerializer(many=True, read_only=True)
    
    class Meta:
        model = ModuleI18n
        fields = ('id', 'name', 'slug', 'levels')
        read_only_fields = ('id', 'name', 'slug', 'levels')

class MCModuleCategorySerializer(serializers.ModelSerializer):
    """Serializer to map the Model ModuleCategory instance into JSON format from a Module Category (list or detail) view."""
    
    modules = MCModuleSerializer(many=True, read_only=True)
    
    class Meta:
        model = ModuleCategoryI18n
        fields = ('id', 'name', 'modules')
        read_only_fields = ('id', 'name')

# MODULE DETAIL SERIALIZERS: prefix MD

class MDEffectSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Effect instance into JSON format from a Module Detail view."""
    
    class Meta:
        model = EffectI18n
        fields = ('id', 'name', 'slug', 'description')
        fields = ('id', 'name', 'slug', 'description')

class MDModuleCategorySerializer(serializers.ModelSerializer):
    """Serializer to map the Model ModuleCategory instance into JSON format from a Module Detail view."""
    
    class Meta:
        model = ModuleCategoryI18n
        fields = ('id', 'name')
        read_only_fields = ('id', 'name')

class MDModuleSlotSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ModuleSlot instance into JSON format from a Module Detail view."""
    
    
    class Meta:
        model = ModuleSlotI18n
        fields = ('head', 'left_arm', 'right_arm', 'torso', 'left_leg', 'right_leg')
        read_only_fields = ('head', 'left_arm', 'right_arm', 'torso', 'left_leg', 'right_leg')

class MDModuleLevelEffectSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ModuleLevelEffect instance into JSON format from a Module Detail view."""
    
    effect = MDEffectSerializer(read_only=True)
    
    class Meta:
        model = ModuleLevelEffectI18n
        fields = ('choice_number', 'effect', 'effect_level', 'damage', 'violence', 'forced_by_previous_level', 'effect_condition')
        read_only_fields = ('choice_number', 'effect', 'effect_level', 'damage', 'violence', 'forced_by_previous_level', 'effect_condition')

class MDModuleLevelSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ModuleLevel instance into JSON format from a Module Detail view."""
    
    rarity = NameRelatedSerializer(read_only=True)
    activation = NameRelatedSerializer(read_only=True)
    reach = NameRelatedSerializer(read_only=True)
    effects = MDModuleLevelEffectSerializer(many=True,read_only=True)
    npcs = CommonNonPlayerCharacterSerializer(many=True,read_only=True)
    
    class Meta:
        model = ModuleLevelI18n
        fields = ('level', 'description', 'rarity', 'cost', 'activation', 'duration', 'damage_dice', 'damage_bonus', 'violence_dice', 'violence_bonus', 'reach', 'energy', 'effects', 'npcs')
        read_only_fields = ('level', 'description', 'rarity', 'cost', 'activation', 'duration', 'damage_dice', 'damage_bonus', 'violence_dice', 'violence_bonus', 'reach', 'energy')

class MDModuleSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Module instance into JSON format from a Module Detail view."""
    
    levels = MDModuleLevelSerializer(many=True, read_only=True)
    slots = MDModuleSlotSerializer(many=True, read_only=True)
    category = MDModuleCategorySerializer(read_only=True)
    
    class Meta:
        model = ModuleI18n
        fields = ('id', 'name', 'slug', 'category', 'levels', 'slots')
        read_only_fields = ('id', 'name', 'slug', 'category', 'levels', 'slots')

# MODULE LIST SERIALIZERS: prefix ML

class MLModuleSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Module instance into JSON format from a Module List view."""
    
    levels = MCModuleLevelSerializer(many=True, read_only=True)
    category = MDModuleCategorySerializer(read_only=True)
    
    class Meta:
        model = ModuleI18n
        fields = ('id', 'name', 'slug', 'category', 'levels')
        read_only_fields = ('id', 'name', 'slug', 'category', 'levels')