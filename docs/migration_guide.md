# Migrate guide

## v1.* to v2.*

Knightools becomes compatible with parler and i18n in the v2. Every model
has been translated into a i18n version, thanks to the parler plugin.
The old models remains, to help moving the data and prevent any loss,
especially if forks have changed some data type.

Because of conflicts between MPTT and parler, and because MPTT is
technically not under active maintainance, the faq has been migrated to
treebeard plugin... The treebeard also conflicted, but I decided to
keep it so it will be maintained for the futures versions of django.

**Note**: on a fresh database, everything can be done with a simple migrate
command. The issue comes with data already registered in the database...

### How to migrate?

First of all, BACKUP YOUR DATABASE. NOW. REALLY. IT'S VERY, VERY, VERY
IMPORTANT. This migrations can break at anytime, messing up forever your
database scheme. So, now you have backed everything, you can apply the
migrations, but NOT ALL AT ONCE.

First of all, you have to do only the gm migrations.

```shell
./manage.py migrate gm
```

This should do smoothly, because it's only creation of new models.

Once it is done, you will have to finish the migrations with your own
changes in the models. It means, if you changed some fields to become
float or longer varchar or anything, you must report these changes on
the i18n model. Once done, create your own migration:

```shell
./manage.py makemigrations gm
./manage.py migrate gm
```

Once that is done, you will be some kind of ready for the next part.

The second part is to migrate the virtualtable, even if you don't use
it. Beware, if you changed the model of avalon extension, refer to the
section "I must adapt models" and "What if something fails?" below. If
you didn't change anything, the migrations of virtualtable is as
smooth as gm and should be done in one command.

```shell
./manage.py migrate virtualtable
```

Now is time for fun...

Migrate now the users package, but only onto the 0019 because the 0020
WILL break if you continue too far.


```shell
./manage.py migrate users 0019
```

That's because some foreign key of the users models have changed. The
key now point toward i18n models, but we haven't yet migrated the data,
resulting in a foreign key constraint failure.

Now you are ready to get your data in the i18n format. You can then
execute a shell in django:

```shell
./manage.py shell
```

In the shell, paste this script:

```python
from work.migrationi18n import migrate

migrate()
```

You can do this code multiple times, it only imports new data.
The migration assumes, because of the project origin, everything in your
data is in french. If that's not the case, change the 
`work/migrationi18n.py` file so it imports in the desired language.

Now you have finished importing everything in your language, you can
finish the migration of users (and everything else).

### What if something fails?

Don't panick. There is probably a solution. First of all, check what
is the failure, and at which migration it triggered. Restore your
database with the backup you made before migrating. For safety, you
should entirely drop the database, recreate it, then play the dump file.
That's because mysql don't drop tables that wasn't on the initial dump,
leading in new errors when playing migrations files like "the table
already exists"...

Then, identify the reason of the error. Replay your migrations onto the
migration file before the one failing.

#### IntegrityError

If the error is an integrity issue, it means you have to import the data
before going further. For this, use the following command:

```shell
./manage.py shell
```

In the shell, do this:

```python
from work.migrationi18n import migrate_objects_to_i18n

migrate_objects_to_i18n('<your_module_name>', '<ModelName>') # Note: there is more options, like renamed columns, other name class, and so on. Check the doc of the function.
```

#### TypeError 

This error is probably triggered when you try to import data from old
models to the new i18n. You probably changed a field type in a legacy
model, and therefore you can't migrate in the legacy i18n corresponding
model.

See the "I must adapt models" section below, but you will have to
put your migration file so it is played before the migration failing.
When you have done the `makemigrations` command, open the migration file.

Inside of it, you will see a line starting with :

```python
class Migration(migrations.Migration):
    dependencies = [
        ('app', '0xxx_name_of_the_last_migration_file'),
    ]
```

You just have to change the dependencies so the name of the migration
file is the one you stopped at. And, of course, the migration file failing
must now depends of your new migration file. You can rename it so it's
not messy alphabetically speaking.

### I must adapt models

The most important thing is to alter the models in time before the
automatic data import are triggered. Basically, here is a table with
automatic import migration step, where you must alter your own data in time:

| app          | migration file                       | models imported        |
| ------------ | ------------------------------------ | ---------------------- |
| gm           | none                                 | -                      |
| users        | 0018a_gamemasterquestion_to_i18n.py  | GameMasterConfirmation |
| virtualtable | 0046_migrate_avalonextension_to_i18n | AvalonExtension        |

If you didn't changed these models, you can follow the normal guide.

Once you are in the steps users 0019, alter your own models, create your
migrations files, and then play the import script in "How to migrate".

Let me know if something is going wrong.

Remember to backup your database before doing this (sort of complicated)
migration.


