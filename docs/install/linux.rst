Installer KnightOOLS sur Linux
===============================================================================

Installer Git, MariaDB, Python 3.6+, virtualenv et pip
-------------------------------------------------------------------------------

Debian / Ubuntu
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: bash

   sudo apt install git python3 python3-pip mariadb-client mariadb-server virtualenv python3-virtualenv libmariadb-dev-compat libmariadbclient-dev redis libffi-dev python3-testresources

Suite à un manque de possibilité, la version Fedora n'est plus présente dans ce
guide, mais les logiciels à installer sont équivalents et doivent trouver une
correspondance assez facilement en passant par yum au lieu de apt.

Cloner le projet Knightools
-------------------------------------------------------------------------------

Si votre but est seulement d'utiliser KnightOOLS hors ligne depuis votre réseau
local sans contribuer au projet, vous pouvez directement cloner la version
http du projet :

.. code-block:: bash

   cd
   git clone https://gitlab.com/Renkineko/knightools.git


Si vous souhaitez au contraire contribuer au projet, faire des modifications,
il vous faudra d'abord un compte gitlab sur lequel faire ce que l'on appelle un
fork, c'est-à-dire que vous allez avoir une "copie" du projet mais vous
appartenant, et les modifications que vous ferez sur votre projet pourront être
envoyées au projet initial. Lisez le guide de contribution pour connaître
le workflow utilisé pour ce projet.

.. code-block:: bash

   cd
   git clone git@gitlab.com:<votre-pseudo>/knightools.git
   cd knightools
   git remote add upstream git@gitlab.com:Renkineko/knightools.git

Créer son Virtualenv
-------------------------------------------------------------------------------

Afin de faire les choses proprement et éviter des conflits systèmes, nous
allons créer un environnement virtuel pour exécuter le projet. Cela n'ajoute
rien en complexité, et ne coûte qu'une ou deux commandes supplémentaires. Notez
que vous pouvez passer cette étape, mais il est conseillé de la faire.

Dans l'exemple qui suit, nous utiliserons comme nom d'environnement virtuel
"venv", mais il y a deux noms possibles qui sont ignorés par défaut par la
configuration git du projet : "venv" ou "knightenv". Positionnez-vous depuis
votre terminal dans le dossier du projet, et exécutez la commande suivante :

.. code-block:: bash

   KNIGHTOOLS_PYTHON=`which python3`
   virtualenv --python=$KNIGHTOOLS_PYTHON venv

Désormais, vous êtes sûr que le projet ne devrait pas entrer en conflit avec
d'autres projets python que vous pourriez avoir, maintenant ou à l'avenir.

Installer elasticsearch
-------------------------------------------------------------------------------

Rendez-vous sur le site d'elastic-search et suivez les instructions
d'installation : https://www.elastic.co/guide/en/elasticsearch/reference/current/install-elasticsearch.html


Finaliser l'installation
-------------------------------------------------------------------------------

Installation des applications python
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Accédez depuis un terminal au dossier où se trouve votre projet, puis exécutez
les commandes suivantes (notez le nom de l'environnement virtuel sur la
première commande, n'oubliez pas d'adapter si vous en avez choisi un autre) :

.. code-block:: bash

   source venv/bin/activate
   pip install -r work/requirements.pip.txt

Configuration de la base de données
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Connectez-vous à MariaDB :

.. code-block:: bash

   sudo su # Permet de se connecter en tant que root, seul root a accès à mysql après une installation
   mysql

Puis créez la base de données nécessaire à l'application

.. code-block:: sql

   CREATE DATABASE knightools;
   CREATE DATABASE test_knightools;
   GRANT ALL PRIVILEGES ON `knightools`.* TO 'knightools_user'@'%' IDENTIFIED BY 'knightools_pwd';
   GRANT ALL PRIVILEGES ON `test_knightools`.* TO 'knightools_user'@'%' IDENTIFIED BY 'knightools_pwd';
   FLUSH PRIVILEGES;

Une fois ceci fait, quittez mysql et la session root en tapant deux fois
``exit``, puis copiez le fichier my.cnf.example en lui donnant le nom my.cnf :

.. code-block:: bash

   cp my.cnf.example my.cnf

Puis modifiez ce fichier à l'aide de votre éditeur favori pour y saisir les
bonnes informations :

.. code-block:: ini
   :linenos:
   :caption: my.cnf
   :name: my.cnf

   [client]
   database=knightools
   user=knightools_user
   password=knightools_pwd
   host=localhost
   port=3306
   default-character-set=utf8

Migration des models :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Enfin, il ne vous reste plus qu'à exécuter la commande suivante pour en finir
avec l'installation de l'application.

.. code-block:: bash

   ./manage.py migrate

Si une erreur se produit durant la migration avec indiqué comme message
d'erreur "django.db.utils.OperationalError: (1071, 'Specified key was too long;
max key length is 767 bytes')", il va falloir tricher un peu. Ce message
apparaît sur les anciennes versions de mysql (encore très répandues à l'heure
où cette documentation est écrite). Le plus simple est de se connecter à
mysql et de changer l'encodage de la table qui pose problème :

.. code-block:: sql

   ALTER TABLE account_emailaddress CONVERT TO CHARACTER SET utf8;

Ce n'est pas la meilleure méthode, mais elle a le mérite d'être efficace, à
plus forte raison pour un environnement de test.

Création du compte admin
-------------------------------------------------------------------------------

Une dernière chose à configurer, c'est le compte administrateur qui aura accès
à l'url d'administration du site. Pour cela, toujours depuis le terminal en
environnement virtuel, faites :

.. code-block:: bash

   ./manage.py createsuperuser

L'application va vous demander le login, l'adresse mail et le mot de passe.

Exécuter l'application
-------------------------------------------------------------------------------

Pour exécuter l'application, vous avez juste à lancer la commande suivante,
toujours en étant dans l'environnement virtuel :

.. code-block:: bash

   ./manage.py runserver

Puis depuis votre navigateur, accédez à l'adresse `<http://localhost:8000>`_.
