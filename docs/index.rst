.. KnightOOLS documentation master file, created by
   sphinx-quickstart on Tue Oct 22 13:46:58 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue dans la documentation technique de KnightOOLS !
=========================================================

Que vous ayez besoin de récupérer le projet pour le modifier ou tout simplement
pour pouvoir l'utiliser hors ligne avec vos propres données, cette
documentation est faite pour vous.

KnightOOLS est né de l'idée d'intégrer une aide de jeu en ligne au jeu de rôle
sur table Knight. Le projet est codé avec django pour le moteur, mysql pour la
base de données, il utilise Bootstrap comme framework CSS ainsi que jQuery
comme librairie JS.

Le projet lui-même est open-source et peut être utilisé à votre guise, les
modifications que vous y apportez ne regardant que vous : vous êtes libres de
partager votre code et vos améliorations ou pas. Par contre, les données dans
la base de données sont soumises au droit d'auteur car elles reprennent pour la
plupart des passages entiers du livre de base (LdB) du jeu. Certaines données
vous seront fournies (les distances, les aspects et caractéristiques), mais
concernant le cœur même du site à savoir les modules ou les armes, vous devrez
les insérer par vous-même si vous souhaitez contribuer au projet.

.. toctree::
   :maxdepth: 2
   :caption: Contenu:

   install/linux
   install/windows


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
