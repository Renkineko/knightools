# Presentation

KnightOOLS is an online tools for player and game master to have a global sight
of the table-top role playing game Knight weaponry (and more).

The project is french, but commits, issues and description should be in english
to help internationalization.

It is powered by django and is mainly used as a tutorial for me (Renkineko).
Anyone who wants to contribute is welcome, either by posting suggestions or
improve the code.

## Installation

This project is not meant to be executed on windows, but if you want to try and
are able to write a documentation about it, don't hesitate to do a pull request.

To install the project on linux, please follow steps on
[This file](docs/install/linux.rst) (only in french, but commands are the same)

## API

You can find the API for KnightOOLS here in the work folder:

[API](work/api.swagger.json)

Right now, the API does not use authentification. Maybe it will in the future,
it's still unsure.

If you want to help with the API (documentation or possible usage), do not
hesitate to contact me.