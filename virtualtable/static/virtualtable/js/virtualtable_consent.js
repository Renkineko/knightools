$(function() {
	
	let index_added_topics = 0;
	
	function slugify(text) {
		return text
			.toString()                           // Cast to string (optional)
			.normalize('NFKD')            // The normalize() using NFKD method returns the Unicode Normalization Form of a given string.
			.toLowerCase()                  // Convert the string to lowercase letters
			.trim()                                  // Remove whitespace from both sides of a string (optional)
			.replace(/\s+/g, '-')            // Replace spaces with -
			.replace(/[^\w\-]+/g, '')     // Remove all non-word chars
			.replace(/\-\-+/g, '-');        // Replace multiple - with single -
	}
	
	function add_topic_form_submit(e) {
		var name = $('#topic-name-input').val();
		
		if (name.trim() === '') {
			// alert("Le nom doit au moins contenir un caractère autre qu'espace.");
			$('#topic-name-input').addClass('is-invalid').one('input', function() {
				$(this).removeClass('is-invalid');
			});
			e.preventDefault();
			return false;
		}
		
		let node = document.importNode(document.querySelector('#tpl-new-topic').content, true);
		node.querySelector('td[target=topic-name]>span').textContent = name;
		node.querySelector('td[target=topic-name]').setAttribute("slugged", slugify(name));
		node.querySelector('td[target=topic-name]').setAttribute("data-sort-value", slugify(name));
		node.querySelectorAll('td[target=consent] input[type=radio]').forEach(input => {
			input.setAttribute('name', ['consent-n', index_added_topics].join('-') );
		});
		node.querySelector('td[target=consent] input[type=radio][value="1"]').checked = true;
		document.querySelector('tbody').append(node);
		index_added_topics++;
		
		$('#add-topic-modal').modal('hide');
		
		e.preventDefault();
		return false;
	}
	
	$('#topics').stupidtable();
	
	$('#btn-prefill').on('click', function() {
		$.get('/virtualtable/ajax/default_consent.json').done(results => {
			results.forEach(result => {
				if ($('td[slugged=' + slugify(result.name) + ']').length > 0) {
					return;
				}
				
				let node = document.importNode(document.querySelector('#tpl-new-topic').content, true);
				node.querySelector('td[target=topic-name]>span').textContent = result.name;
				node.querySelector('td[target=topic-name]').setAttribute("slugged", slugify(result.name));
				node.querySelector('td[target=topic-name]').setAttribute("data-sort-value", slugify(result.name));
				node.querySelectorAll('td[target=consent] input[type=radio]').forEach(input => {
					input.setAttribute('name', ['consent-n', index_added_topics].join('-') );
				});
				node.querySelector('td[target=consent] input[type=radio][value="1"]').checked = true;
				document.querySelector('tbody').append(node);
				index_added_topics++;
			});
			
			$('#topic-name').attr('data-sort', 'string');
			$('#topic-name').stupidsort('asc');
			$('#topic-name').removeAttr('data-sort');
			
			$('.cancel-topic').off('click').on('click', function() {
				$(this).closest('tr').remove();
			});
		});
	});
	
	$('button.global-setup').on('click', function() {
		$('tbody :radio[value=' + $(this).attr('target') + ']').closest('.btn-group').find('.btn').removeClass('active');
		$('tbody :radio[value=' + $(this).attr('target') + ']').closest('.btn').addClass('active');
		$('tbody :radio[value=' + $(this).attr('target') + ']').prop('checked', true);
	});
	
	$('#answers-form').on('submit', function() {
		let answers = [];
		$('#topics tbody tr').each(function() {
			let answer = {};
			
			answer.topic_id = parseInt($(this).attr('tid'), 10);
			answer.answer = $(this).find(':radio:checked').val();
			answer.comment = $(this).find('textarea').val();
			
			if (answer.topic_id === 0) {
				answer.topic = {
					name: $(this).find('td[target=topic-name]>span').text()
				}
				answer.ask_delete = false;
			}
			else {
				answer.ask_delete = $(this).find(':checkbox').is(':checked');
			}
			
			answers.push(answer);
		});
		
		$('#id_answers_json').val(
			JSON.stringify(
				{
					"ignore_when_left": $('#rule-ignore-when-left').prop('checked'),
					"answers": answers
				}
			)
		);
	});
	
	$('#add-topic-modal').on('show.bs.modal', function (event) {
		$(this).find('#topic-name-input').val('');
	}).on('shown.bs.modal', function () {
		$(this).find('.modal-body #topic-name-input').focus();
	});
	
	$('#add-topic-form').on('submit', add_topic_form_submit);
	$('#submit-add-topic').on('click', add_topic_form_submit);
});