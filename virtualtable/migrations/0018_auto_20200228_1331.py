# Generated by Django 2.2.2 on 2020-02-28 13:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('virtualtable', '0017_auto_20200228_1307'),
    ]

    operations = [
        migrations.RenameField(
            model_name='characterarcana',
            old_name='choosen_ai_favor',
            new_name='choosen_ai_advantage',
        ),
        migrations.RenameField(
            model_name='characterarcana',
            old_name='choosen_ai_weakness',
            new_name='choosen_ai_disadvantage',
        ),
        migrations.RenameField(
            model_name='characterarcana',
            old_name='choosen_knight_favor',
            new_name='choosen_knight_advantage',
        ),
        migrations.RenameField(
            model_name='characterarcana',
            old_name='choosen_knight_weakness',
            new_name='choosen_knight_disadvantage',
        ),
    ]
