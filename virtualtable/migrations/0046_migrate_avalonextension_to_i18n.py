# Generated by Django 5.1.1 on 2024-11-21 21:17

from django.db import migrations
from work.migrationi18n import migrate_objects_to_i18n

def migrate_avalonextension(apps, schema_editor):
    migrate_objects_to_i18n('virtualtable.models', 'AvalonExtension')
    
def reverse_avalonextension(apps, schema_editor):
    AvalonExt = apps.get_model('virtualtable', 'AvalonExtensionI18n')
    AvalonExt.objects.all().delete()

class Migration(migrations.Migration):

    dependencies = [
        ('virtualtable', '0045_avalonextensioni18n_avalonextensioni18ntranslation'),
    ]

    operations = [
        migrations.RunPython(migrate_avalonextension, reverse_avalonextension),
    ]
