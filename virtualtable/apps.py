from django.apps import AppConfig

class VirtualtableConfig(AppConfig):
    name = 'virtualtable'
