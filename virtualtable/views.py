from asyncio.log import logger
from secrets import token_urlsafe
from django.utils.translation import gettext
import json
import logging
import datetime

from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from django.utils import timezone
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Max, Prefetch, Q, Case, When, BooleanField
from django.db import transaction
from django.core.files.storage import default_storage
from PIL import Image
from martor.utils import markdownify

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

from gm.models.character import Aspect, Characteristic, Faction, FactionEffect

from .models import (
    VirtualTable, Character, VirtualTableConsentAnswer,
    VirtualTableConsentRule, VirtualTableConsentTopic,
    VirtualTableFactionNode, VirtualTableFactionNodeEffect,
    VirtualTablePlayer, VirtualTableAvalon,
    HelpingArticle,
)
from .forms import (
    VirtualTableForm, CharacterCreationForm, TestForm,
    PlayerJoinTableForm, ConsentForm
)

# TODO : RANGER LES JS POUR EVITER DE SURCHARGER LES PAGES INUTILEMENT !
# TODO : GERER LE FORM EN CRISPY :)

logger = logging.getLogger(__name__)


class ConsentTableView(LoginRequiredMixin, generic.UpdateView):
    template_name = 'virtualtable/vt_consent.html'
    model = VirtualTable
    form_class = ConsentForm
    # fields = ('name', 'description', 'max_character', 'token_valid_duration')
    redirect_to = 'virtualtable:vt_list'
    success_url = reverse_lazy('virtualtable:vt_list')
    
    def get_object(self):
        vt = super(ConsentTableView, self).get_object()
        
        gm = self.request.user == vt.game_master
        player = vt.players.filter(player=self.request.user).count() > 0
        
        if not gm and not player:
            return None
        
        topics = []
        for topic in vt.topics.order_by('name').all():
            topic.user_answer = None
            if topic.answers.filter(user=self.request.user).count() > 0:
                topic.user_answer = topic.answers.filter(user=self.request.user).first()
                topic.new = False
            else:
                topic.user_answer = {
                    "answer": 1
                }
                topic.new = True
            topics.append(topic)
        
        ignore_when_left = False
        if vt.consent_rules.filter(user=self.request.user).count() > 0:
            ignore_when_left = vt.consent_rules.filter(user=self.request.user).first().ignore_when_left
        
        vt.topics_with_answer = topics
        vt.ignore_when_left = ignore_when_left
        
        return vt
    
    def form_valid(self, form):
        logger.info("[CNST] form valid consent")

        return_value = super().form_valid(form)
        
        # Doing this after the final validation so it sync the extensions with the newly created virtual table
        if form.cleaned_data['answers_json']:
            data = json.loads(form.cleaned_data['answers_json'])
            
            for answer in data['answers']:
                if answer['topic_id'] == 0:
                    topic = VirtualTableConsentTopic()
                    topic.virtual_table = form.instance
                    topic.name = answer['topic']['name']
                    topic.save()
                    answer['topic_id'] = topic.pk
                
                try:
                    db_topic = VirtualTableConsentTopic.objects.get(pk=answer['topic_id'])
                except ObjectDoesNotExist as e:
                    db_topic = VirtualTableConsentTopic()
                    db_topic.virtual_table = form.instance
                    db_topic.name = answer['topic']['name']
                    db_topic.save()
                except Exception as e:
                    raise e
                    
                try:
                    db_answer = VirtualTableConsentAnswer.objects.get(user=self.request.user, topic=db_topic)
                except ObjectDoesNotExist as e:
                    db_answer = VirtualTableConsentAnswer()
                    db_answer.topic = db_topic
                    db_answer.user = self.request.user
                except Exception as e:
                    raise e
                
                db_answer.answer = answer['answer']
                db_answer.comment = answer['comment']
                db_answer.save()
                
                if answer['ask_delete']:
                    db_topic.refresh_from_db()
                    if db_topic.answers.filter(answer=VirtualTableConsentAnswer.ANSWER_UNSAFE).count() > 0:
                        # Silently continue
                        continue
                    if db_topic.answers.filter(answer=VirtualTableConsentAnswer.ANSWER_UNKNOWN).count() > 0:
                        # Silently continue
                        continue
                    
                    # Do I consider comment as answers?
                    db_topic.delete()
            
            try:
                db_rule = VirtualTableConsentRule.objects.get(user=self.request.user, virtual_table=form.instance)
            except ObjectDoesNotExist as e:
                db_rule = VirtualTableConsentRule()
                db_rule.virtual_table = form.instance
                db_rule.user = self.request.user
            except Exception as e:
                raise e
                
            db_rule.ignore_when_left = data['ignore_when_left']
            db_rule.save()
            
        safe_topics, unsafe_topics, unknown_topics = form.instance.get_separated_topics()
        
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            ("virtual_table_%s" % form.instance.id),
            {
                'type': 'vt_message',
                'params': {
                    "command": 'consent',
                    "safe_topics": safe_topics,
                    "unsafe_topics": unsafe_topics,
                    "unknown_topics": unknown_topics,
                }
            }
        )
        
        return return_value
        

class DefaultConsentValues(generic.View):
    def get(self, request):
        data = [
            {
                "name": gettext("Histo-socio-culturel / Anti-sémitisme"),
                "id": 0,
            },
            {
                "name": gettext("Histo-socio-culturel / Enfants-soldats"),
                "id": 0,
            },
            {
                "name": gettext("Histo-socio-culturel / Esclavagisme"),
                "id": 0,
            },
            {
                "name": gettext("Histo-socio-culturel / Génocide"),
                "id": 0,
            },
            {
                "name": gettext("Histo-socio-culturel / Grossophobie"),
                "id": 0,
            },
            {
                "name": gettext("Histo-socio-culturel / Homophobie"),
                "id": 0,
            },
            {
                "name": gettext("Histo-socio-culturel / Négationnisme"),
                "id": 0,
            },
            {
                "name": gettext("Histo-socio-culturel / Problèmes culturels spécifiques"),
                "id": 0,
            },
            {
                "name": gettext("Histo-socio-culturel / Racisme"),
                "id": 0,
            },
            {
                "name": gettext("Histo-socio-culturel / Religions réelles"),
                "id": 0,
            },
            {
                "name": gettext("Histo-socio-culturel / Sexisme"),
                "id": 0,
            },
            {
                "name": gettext("Histo-socio-culturel / Terrorisme"),
                "id": 0,
            },
            {
                "name": gettext("Histo-socio-culturel / Transphobie"),
                "id": 0,
            },
            {
                "name": gettext("Histo-socio-culturel / Violence conjugale"),
                "id": 0,
            },
            {
                "name": gettext("Histo-socio-culturel / Violence policière, armée"),
                "id": 0,
            },
            {
                "name": gettext("Histo-socio-culturel / Xénophobie"),
                "id": 0,
            },
            {
                "name": gettext("Horreur / Aiguilles, piqûres, dards"),
                "id": 0,
            },
            {
                "name": gettext("Horreur / Araignées"),
                "id": 0,
            },
            {
                "name": gettext("Horreur / Claustrophobie"),
                "id": 0,
            },
            {
                "name": gettext("Horreur / Démembrement"),
                "id": 0,
            },
            {
                "name": gettext("Horreur / Démons"),
                "id": 0,
            },
            {
                "name": gettext("Horreur / Globes occulaires, énucléation"),
                "id": 0,
            },
            {
                "name": gettext("Horreur / Gore"),
                "id": 0,
            },
            {
                "name": gettext("Horreur / Insectes"),
                "id": 0,
            },
            {
                "name": gettext("Horreur / Maltraitance sur les animaux"),
                "id": 0,
            },
            {
                "name": gettext("Horreur / Maltraitance sur les enfants"),
                "id": 0,
            },
            {
                "name": gettext("Horreur / Rats, rongeurs"),
                "id": 0,
            },
            {
                "name": gettext("Horreur / Sang"),
                "id": 0,
            },
            {
                "name": gettext("Horreur / Serpents, reptiles"),
                "id": 0,
            },
            {
                "name": gettext("Horreur / Thalassophobie"),
                "id": 0,
            },
            {
                "name": gettext("Horreur / Trypophobie"),
                "id": 0,
            },
            {
                "name": gettext("Phénomènes / Catastrophes naturelles (séismes, incendies de forêts)"),
                "id": 0,
            },
            {
                "name": gettext("Phénomènes / Chaleur extrême, insolation"),
                "id": 0,
            },
            {
                "name": gettext("Phénomènes / Froid extrême, congélation"),
                "id": 0,
            },
            {
                "name": gettext("Phénomènes / Météo extrême (tornades, ouragans)"),
                "id": 0,
            },
            {
                "name": gettext("Relations romantique / Entre PJ et PNJ"),
                "id": 0,
            },
            {
                "name": gettext("Relations romantique / Entre PJs"),
                "id": 0,
            },
            {
                "name": gettext("Relations romantique / Explicite"),
                "id": 0,
            },
            {
                "name": gettext("Relations romantique / Fondu au noir, ellipsé"),
                "id": 0,
            },
            {
                "name": gettext("Relations sexuelles / Entre PJ et PNJ"),
                "id": 0,
            },
            {
                "name": gettext("Relations sexuelles / Entre PJs"),
                "id": 0,
            },
            {
                "name": gettext("Relations sexuelles / Explicite"),
                "id": 0,
            },
            {
                "name": gettext("Relations sexuelles / Fondu au noir, ellipsé"),
                "id": 0,
            },
            {
                "name": gettext("Santé / Agression sexuelle"),
                "id": 0,
            },
            {
                "name": gettext("Santé / Cancer"),
                "id": 0,
            },
            {
                "name": gettext("Santé / Dépression"),
                "id": 0,
            },
            {
                "name": gettext("Santé / Famine"),
                "id": 0,
            },
            {
                "name": gettext("Santé / Grossesse, fausse couche, avortement"),
                "id": 0,
            },
            {
                "name": gettext("Santé / Manipulation, contrôle mental"),
                "id": 0,
            },
            {
                "name": gettext("Santé / Pandémie"),
                "id": 0,
            },
            {
                "name": gettext("Santé / Paralysie, restriction physique"),
                "id": 0,
            },
            {
                "name": gettext("Santé / Scarification, auto-mutilation"),
                "id": 0,
            },
            {
                "name": gettext("Santé / Soif extrême, dessèchement"),
                "id": 0,
            },
            {
                "name": gettext("Santé / Suicide"),
                "id": 0,
            },
            {
                "name": gettext("Santé / Torture"),
                "id": 0,
            }
        ]
        return JsonResponse(data, safe=False)

class CreateTableView(LoginRequiredMixin, generic.CreateView):
    template_name = 'virtualtable/vt_create.html'
    model = VirtualTable
    fields = ('name', 'description', 'max_character', 'token_valid_duration', 'active_avalon', 'active_faction', 'active_aegis')
    redirect_to = 'virtualtable:vt_list'
    success_url = reverse_lazy('virtualtable:vt_list')
    
    def form_valid(self, form):
        form.instance.game_master = self.request.user
        form.instance.token_creation_date = timezone.now()
        form.instance.invitation_token = token_urlsafe(6)
        
        return_value = super().form_valid(form)
        
        # Doing this after the final validation so it sync the extensions with the newly created virtual table
        if form.instance.active_avalon:
            form.instance.avalon = VirtualTableAvalon()
            form.instance.avalon.save()
            form.instance.avalon.sync_extensions()
        
        return return_value
        
class UpdateTableView(LoginRequiredMixin, generic.UpdateView):
    template_name = 'virtualtable/vt_update.html'
    model = VirtualTable
    form_class = VirtualTableForm
    # fields = ('name', 'description', 'max_character', 'token_valid_duration')
    redirect_to = 'virtualtable:vt_list'
    success_url = reverse_lazy('virtualtable:vt_list')
    
    def get_object(self):
        vt = super(UpdateTableView, self).get_object()
        
        if self.request.user != vt.game_master:
            return None
        
        vt.sisters = VirtualTable.objects.filter(game_master=self.request.user).order_by('name').all()
        
        return vt
        
    def form_valid(self, form):
        if form.instance.game_master != self.request.user:
            raise PermissionError(gettext("Cette table virtuelle ne vous appartient pas et ne peut être modifiée"), "tried_to_modify_vt_but_not_gm")
        
        if form.cleaned_data['regen']:
            form.instance.token_creation_date = timezone.now()
            form.instance.invitation_token = token_urlsafe(6)
        
        if (form.cleaned_data['reset_consent']):
            for topic in form.instance.topics.all():
                topic.delete()
            for rule in form.instance.consent_rules.all():
                rule.delete()
            
            safe_topics, unsafe_topics, unknown_topics = form.instance.get_separated_topics()
            
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)(
                ("virtual_table_%s" % form.instance.id),
                {
                    'type': 'vt_message',
                    'params': {
                        "command": 'consent',
                        "safe_topics": safe_topics,
                        "unsafe_topics": unsafe_topics,
                        "unknown_topics": unknown_topics,
                    }
                }
            )
        
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            ("virtual_table_%s" % form.instance.id),
            {
                'type': 'vt_message',
                'params': {
                    "command": 'description',
                    "description": markdownify(form.cleaned_data['description']),
                }
            }
        )
        
        
        if form.instance.active_avalon:
            try:
                avalon = form.instance.avalon
            except ObjectDoesNotExist as e:
                form.instance.avalon = VirtualTableAvalon()
                form.instance.avalon.save()
            except Exception as e:
                raise e
            
            form.instance.avalon.sync_extensions()
        
        if form.cleaned_data['vtp_to_del']:
            vtp_to_del = json.loads(form.cleaned_data['vtp_to_del'])
            
            # Stop autocommit during the deletion of vtp, to be sure we delete all of them at once.
            transaction.set_autocommit(False)
            
            for vtp_id in vtp_to_del:
                if not isinstance(vtp_id, int):
                    transaction.rollback()
                    raise ValueError(gettext("Erreur dans le passage des personnages à supprimer de cette table."), "vtp_not_valid")
                
                if VirtualTablePlayer.objects.get(pk=vtp_id).virtual_table.id != form.instance.id:
                    transaction.rollback()
                    raise Exception(gettext("Le joueur n'est pas dans la table en cours de modification"), "vtp_not_in_vt")
                
                VirtualTablePlayer.objects.get(pk=vtp_id).delete()
            
            transaction.commit()
            transaction.set_autocommit(True)
            
            async_to_sync(channel_layer.group_send)(
                ("virtual_table_%s" % form.instance.id),
                {
                    'type': 'vt_message',
                    'params': {
                        "command": 'del',
                        "vtp": vtp_to_del,
                    }
                }
            )
            
        if form.cleaned_data['vtp_to_migrate']:
            vtp_to_migrate = json.loads(form.cleaned_data['vtp_to_migrate'])
            vtp_to_move = []
            
            for vt_id in vtp_to_migrate:
                vtp_list = vtp_to_migrate[vt_id]
                
                try:
                    vt_id = int(vt_id)
                except ValueError:
                    # Don't know what the people tried, but it's trolling obviously. So we ignore it.
                    continue
                
                # Ignoring player staying in the same virtualtable.
                if vt_id == form.instance.id:
                    continue
                
                try:
                    vt = VirtualTable.objects.get(pk=vt_id)
                except ObjectDoesNotExist:
                    raise Exception(gettext("Impossible de migrer sur la table demandée, elle n'existe pas / plus."), "unknown_migration_target")
                    
                if VirtualTable.objects.get(pk=vt_id).game_master != self.request.user:
                    raise Exception(gettext("Impossible de migrer sur une table dont vous n'êtes pas le MJ."), "unauthorized_migration_target")
                
                for vtp_id in vtp_list:
                    vtp = VirtualTablePlayer.objects.get(pk=vtp_id)
                    vtp.virtual_table = vt
                    vtp.save()
                    
                    vtp_to_move.append(vtp_id)
                    
                    async_to_sync(channel_layer.group_send)(
                        ("virtual_table_%s" % vt.id),
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": 'new',
                                "vtp_id": vtp_id,
                            }
                        }
                    )
            
            async_to_sync(channel_layer.group_send)(
                ("virtual_table_%s" % form.instance.id),
                {
                    'type': 'vt_message',
                    'params': {
                        "command": 'del',
                        "vtp": vtp_to_move,
                    }
                }
            )
        return super().form_valid(form)

class ListTableView(LoginRequiredMixin, generic.ListView):
    template_name = 'virtualtable/vt_list.html'
    context_object_name = 'virtualtable_list'

    def get_queryset(self):
        """Return list of virtual table."""
        return VirtualTable.objects.filter(Q(game_master=self.request.user) | Q(players__player=self.request.user)).annotate( 
            owned_by_user=Case( 
                When(game_master=self.request.user, then=True), 
                default=False, 
                output_field=BooleanField()
            ) 
        ).distinct().order_by('name').prefetch_related('players')

class DeleteTableView(LoginRequiredMixin, generic.DeleteView):
    model = VirtualTable
    success_url = reverse_lazy('virtualtable:vt_list')
    template_name = 'virtualtable/vt_delete.html'
    
    def get_object(self):
        vt = super(DeleteTableView, self).get_object()
        
        if self.request.user != vt.game_master:
            return None
        
        return vt
    
    def delete(self, request, *args, **kwargs):
        return_val = super(DeleteTableView, self).delete(request, *args, **kwargs)
        
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            ("virtual_table_%s" % kwargs['pk']),
            {
                'type': 'vt_message',
                'params': {
                    "command": 'kick',
                }
            }
        )
        
        return return_val

class DetailTableView(LoginRequiredMixin, generic.DetailView):
    model = VirtualTable
    template_name = 'virtualtable/vt_detail.html'
    
    def get_object(self):
        vt = super(DetailTableView, self).get_object()
        
        if self.request.user != vt.game_master and len(vt.players.filter(player=self.request.user)) == 0:
            return None
        
        vt.players_list = VirtualTablePlayer.objects.filter(virtual_table=vt.id).annotate(
            editable_by_user=Case(
                When(edit_level=VirtualTablePlayer.EDIT_LVL_ALL, then=True),
                When(edit_level=VirtualTablePlayer.EDIT_LVL_PLAYER, player=self.request.user, then=True),
                When(virtual_table__game_master=self.request.user, then=True),
                default=False,
                output_field=BooleanField()
            )
        ).order_by('-editable_by_user', 'name').all()
        
        if vt.active_avalon:
            vt.avalon.ordered_extensions_by_user = vt.avalon.get_extensions_by_user_vote(self.request.user)
        
        safe_topics, unsafe_topics, unknown_topics = vt.get_separated_topics()
        
        vt.safe_topics = safe_topics
        vt.unsafe_topics = unsafe_topics
        vt.unknown_topics = unknown_topics
        # import pdb; pdb.set_trace()
        return vt

    def get_context_data(self, **kwargs):
        context = super(DetailTableView, self).get_context_data(**kwargs)
        context['factions'] = Faction.objects.all()
        return context
    
class HelpTableView(LoginRequiredMixin, generic.ListView):
    template_name = 'virtualtable/help/index.html'
    context_object_name = 'articles_list'
    
    def get_queryset(self):
        """Return list of helping articles."""
        return HelpingArticle.objects.order_by('index').all()

    
    
class PlayerJoinTableView(LoginRequiredMixin, generic.CreateView):
    invitation_token = None
    form_class = PlayerJoinTableForm
    template_name = "virtualtable/vt_join.html"
    success_url = reverse_lazy('virtualtable:vt_list')
    
    def form_valid(self, form):
        vt = form.cleaned_data['token']
        # import pdb; pdb.set_trace()
        form.instance.player = self.request.user
        form.instance.virtual_table = vt
        
        if (form.cleaned_data['max_health_points'] is not None
          and (form.cleaned_data['current_health_points'] is None
          or form.cleaned_data['current_health_points'] > form.cleaned_data['max_health_points'])):
            form.instance.current_health_points = form.cleaned_data['max_health_points']
        elif (form.cleaned_data['current_health_points'] is not None
          and form.cleaned_data['max_health_points'] is None):
            form.instance.max_health_points = form.cleaned_data['current_health_points']
        
        if (form.cleaned_data['max_armour_points'] is not None
          and (form.cleaned_data['current_armour_points'] is None
          or form.cleaned_data['current_armour_points'] > form.cleaned_data['max_armour_points'])):
            form.instance.current_armour_points = form.cleaned_data['max_armour_points']
        elif (form.cleaned_data['current_armour_points'] is not None
          and form.cleaned_data['max_armour_points'] is None):
            form.instance.max_armour_points = form.cleaned_data['current_armour_points']
        
        if (form.cleaned_data['max_energy_points'] is not None
          and (form.cleaned_data['current_energy_points'] is None
          or form.cleaned_data['current_energy_points'] > form.cleaned_data['max_energy_points'])):
            form.instance.current_energy_points = form.cleaned_data['max_energy_points']
        elif (form.cleaned_data['current_energy_points'] is not None
          and form.cleaned_data['max_energy_points'] is None):
            form.instance.max_energy_points = form.cleaned_data['current_energy_points']
        
        if (form.cleaned_data['max_hope_points'] is not None
          and (form.cleaned_data['current_hope_points'] is None
          or form.cleaned_data['current_hope_points'] > form.cleaned_data['max_hope_points'])):
            form.instance.current_hope_points = form.cleaned_data['max_hope_points']
        elif (form.cleaned_data['current_hope_points'] is not None
          and form.cleaned_data['max_hope_points'] is None):
            form.instance.max_hope_points = form.cleaned_data['current_hope_points']
        
        return_val = super().form_valid(form)
        
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            ("virtual_table_%s" % vt.id),
            {
                'type': 'vt_message',
                'params': {
                    "command": 'new',
                    "vtp_id": form.instance.id,
                }
            }
        )
        
        # form.instance.name = form.cleaned_data['name']
        return return_val
    
    def get_initial(self):
        return {
            'token': self.invitation_token
        }
    
    def get(self, request, *args, **kwargs):
        self.invitation_token = self.kwargs.get('invitation_token', None)
        
        try:
            vt = VirtualTable.objects.get(invitation_token=self.invitation_token)
        except ObjectDoesNotExist:
            vt = None
        finally:
            
            if vt is None or vt.token_has_expired():
                self.template_name = 'virtualtable/vt_invitation_expired.html'
            elif vt.max_character <= len(VirtualTablePlayer.objects.filter(virtual_table=vt)):
                self.template_name = 'virtualtable/vt_max_reached.html'
                
            return render(request, self.template_name, {
                'token': self.invitation_token,
                'form': self.get_form(),
                'vt': vt,
            })
        

class JoinTableView(LoginRequiredMixin, generic.FormView):
    invitation_token = None
    form_class = CharacterCreationForm
    template_name = "virtualtable/character_creation.html"
    
    # We set the initial form to have the invitation token (if it exists)
    def get_initial(self):
        return {
            'token': self.invitation_token
        }
    
    def get(self, request, *args, **kwargs):
        
        # template_name = 'virtualtable/vt_join.html'
        characters = Character.objects.filter(player=self.request.user)
        
        if len(characters) == 0:
            self.template_name = 'virtualtable/character_creation.html'
        
        self.success_url = 'virtualtable:vt_join'
        self.invitation_token = self.kwargs.get('invitation_token', None)
        # print(self.invitation_token)
        # import ipdb; ipdb.set_trace()
        
        # TODO: check if user has already characters.
        # If not, redirect to character creation.
        # If one, render the join.
        # If multiple, display a choose the character.
        return render(request, self.template_name, {
            'token': self.invitation_token,
            'form': self.get_form(),
            'aspects': Aspect.objects.prefetch_related(Prefetch('characteristics', queryset=Characteristic.objects.annotate(max_level_overdrive=Max('overdrives__level'))))
        })
        # return super().get(request)

class UpdateLogoFactionView(LoginRequiredMixin, generic.View):
    
    def post(self, request, *args, **kwargs):
        result = {
            "success": True, # because we believe in ourselves. We will be successful.
        }
        
        try:
            vt_id = int(kwargs['vt'])
            
            try:
                vt = VirtualTable.objects.get(pk=vt_id, game_master=request.user)
            except ObjectDoesNotExist as e:
                raise Exception("You can not create / update faction if you are not the GM of the table", 'factions_update_create_not_gm')
            except Exception as e:
                raise e
            
            faction_node_id = int(kwargs['faction'])
            
            json_faction = request.POST['faction']
            process_logo = True
            try:
                logo = request.FILES['logo']
            except:
                process_logo = False
            
            form_faction = json.loads(json_faction)
            
            try:
                delete_logo = form_faction['delete_logo']
            except:
                delete_logo = False
                
            if faction_node_id == 0:
                faction = VirtualTableFactionNode()
                faction.virtual_table = vt
            else:
                try:
                    faction = VirtualTableFactionNode.objects.get(pk=faction_node_id, virtual_table=vt)
                except ObjectDoesNotExist as e:
                    raise Exception("You try to update an inexistant faction or a faction you don't owe.", 'factions_update_not_owned_or_inexistant')
                except Exception as e:
                    raise e
                
            faction.name = form_faction['name']
            faction.gm_description = form_faction['gm_desc']
            faction.public_description = form_faction['public_desc']
            faction.pc_faction = form_faction['pc_faction']
            
            original_faction = None
            if form_faction['sync_faction'] > 0:
                try:
                    original_faction = Faction.objects.get(pk=form_faction['sync_faction'])
                except:
                    original_faction = None
                
            faction.faction = original_faction
            
            if faction.pc_faction:
                try:
                    old_pc_faction = VirtualTableFactionNode.objects.get(virtual_table=vt, pc_faction=True)
                    old_pc_faction.pc_faction = False
                    old_pc_faction.save()
                except ObjectDoesNotExist:
                    pass
            
            faction.save()
            
            if process_logo:
                try:
                    img = Image.open(logo)
                except:
                    raise Exception("The logo file is not a valid image", 'factions_update_logo_not_image')
                
                if img.format != 'PNG' and img.format != 'JPEG':
                    raise Exception("The logo file has bad format", 'factions_update_logo_bad_format')
                
                width, height = img.size
                
                if width > 200 or height > 200:
                    raise Exception("The logo file is too big", 'factions_update_logo_too_big')
                
                if faction.logo_file:
                    # import pdb; pdb.set_trace()
                    faction.logo_file.storage.delete(faction.logo_file.name)
                    faction.logo_file.delete()
                faction.logo_file.save("%s_%s.%s" % (request.user.pk,faction.pk, datetime.datetime.today().strftime('%Y%m%d-%H%M%S%f')), logo)
            elif delete_logo:
                # import pdb; pdb.set_trace()
                if faction.logo_file:
                    faction.logo_file.storage.delete(faction.logo_file.name)
                    faction.logo_file.delete()
            
            saved_effects = []
            for form_effect in form_faction['effects']:
                if form_effect['id'] == 0:
                    effect = VirtualTableFactionNodeEffect()
                    effect.node = faction
                else:
                    try:
                        effect = VirtualTableFactionNodeEffect.objects.get(pk=form_effect['id'],node=faction)
                    except ObjectDoesNotExist:
                        effect = VirtualTableFactionNodeEffect()
                        effect.node = faction
                        logger.warning("[FACTFRM] trying to update an effect not belonging to the good faction: effect id: %d - faction: %d" % (form_effect['id'], faction.pk))
                    except Exception as e:
                        logger.exception("[FACTFRM] a global exception occured while modifying/creating effect, json sent: %s" % (request.POST['faction']))
                
                effect.level = form_effect['level']
                effect.level_name = form_effect['level_name']
                effect.level_memo = form_effect['memo']
                effect.effect_name = form_effect['effect_name']
                effect.effect_description = form_effect['effect_desc']
                
                original_effect = None
                if form_effect['sync_effect'] > 0:
                    try:
                        original_effect = FactionEffect.objects.get(pk=form_effect['sync_effect'])
                    except:
                        original_effect = None
                    
                effect.effect = original_effect
                
                effect.save()
                
                effect.refresh_from_db()
                saved_effects.append(effect.id)
            
            faction.effects.exclude(id__in=saved_effects).delete()
            
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)(
                ("virtual_table_%s" % vt_id),
                {
                    'type': 'vt_message',
                    'params': {
                        "command": "refresh_factions",
                        "origin": 0,
                    }
                }
            )
            
        except Exception as e:
            result['success'] = False
            result['error'] = {}
            result['error']['message'] = e.args[0]
            try:
                result['error']['code'] = e.args[1]
            except:
                pass
            result['error']['full'] = str(e)
            
        
        return JsonResponse(result)

class TestView(LoginRequiredMixin, generic.FormView):
    form_class = TestForm
    template_name = "virtualtable/test.html"
