from django.contrib import admin
from parler.admin import TranslatableAdmin
from django.conf import settings

from .models import AvalonExtensionI18n, HelpingArticleI18n

def get_all_languages():
    languages = []
    for obj in settings.PARLER_LANGUAGES[None]:
        languages.append(obj['code'])
    return languages


class AvalonExtensionAdmin(TranslatableAdmin):
    list_display = ('name', 'description', 'cost', 'get_prerequired_names')
    search_fields = ['name', 'description']
    # prepopulated_fields = {"slug": ("name",)}
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class HelpingArticleAdmin(TranslatableAdmin):
    list_display = ('title', 'gm', 'index')
    search_fields = ['title', 'article']

    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

admin.site.register(AvalonExtensionI18n, AvalonExtensionAdmin)
admin.site.register(HelpingArticleI18n, HelpingArticleAdmin)
