from django import forms
from martor.fields import MartorFormField
from colorful.forms import RGBColorField
from colorful.widgets import ColorFieldWidget
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Row, Column, Field, Div
from django.utils.translation import gettext_lazy

from gm.models.character import Armour, Aspect, Characteristic
from .models import VirtualTable, VirtualTablePlayer, Character


class VirtualTableForm(forms.ModelForm):
    regen = forms.BooleanField(required=False,label=gettext_lazy("Régénérer l'invitation"))
    reset_consent = forms.BooleanField(required=False,label=gettext_lazy("Supprimer tous les questionnaires de consentement"))
    vtp_to_del = forms.CharField(required=False,widget=forms.HiddenInput)
    vtp_to_migrate = forms.CharField(required=False,widget=forms.HiddenInput)
    
    class Meta:
        model = VirtualTable
        fields = ['name', 'description', 'max_character', 'token_valid_duration', 'active_avalon', 'active_faction', 'active_aegis']

class ConsentForm(forms.ModelForm):
    answers_json = forms.CharField(required=True, widget=forms.HiddenInput)
    
    class Meta:
        model = VirtualTable
        exclude = [
            'name',
            'description',
            'max_character',
            'game_master',
            'invitation_token',
            'token_valid_duration',
            'token_creation_date',
            'active_avalon',
            'active_faction',
            'active_aegis',
            'destiny_state',
        ]

class PlayerJoinTableForm(forms.ModelForm):
    token = forms.CharField(widget=forms.HiddenInput)
    
    def clean_token(self):
        return VirtualTable.objects.get(invitation_token=self.cleaned_data['token'])
    
    class Meta:
        model = VirtualTablePlayer
        exclude = ['edit_level', 'gest_advantage', 'virtual_table', 'player']
        widgets = {
            # 'name': forms.TextInput(
            #     attrs={'class': 'form-control'},
            # ),
            'current_armour_points': forms.NumberInput(
                attrs={
                    'class': 'numberinput form-control',
                    'placeholder': gettext_lazy('Actuels'),
                },
            ),
            'max_armour_points': forms.NumberInput(
                attrs={
                    'class': 'numberinput form-control',
                    'placeholder': gettext_lazy('Totaux'),
                },
            ),
            'current_energy_points': forms.NumberInput(
                attrs={
                    'class': 'numberinput form-control',
                    'placeholder': gettext_lazy('Actuels'),
                },
            ),
            'max_energy_points': forms.NumberInput(
                attrs={
                    'class': 'numberinput form-control',
                    'placeholder': gettext_lazy('Totaux'),
                },
            ),
            'current_health_points': forms.NumberInput(
                attrs={
                    'class': 'numberinput form-control',
                    'placeholder': gettext_lazy('Actuels'),
                },
            ),
            'max_health_points': forms.NumberInput(
                attrs={
                    'class': 'numberinput form-control',
                    'placeholder': gettext_lazy('Totaux'),
                },
            ),
            'current_hope_points': forms.NumberInput(
                attrs={
                    'class': 'numberinput form-control',
                    'placeholder': gettext_lazy('Actuels'),
                },
            ),
            'max_hope_points': forms.NumberInput(
                attrs={
                    'class': 'numberinput form-control',
                    'placeholder': gettext_lazy('Totaux'),
                },
            ),
            'current_force_field': forms.NumberInput(
                attrs={
                    'class': 'numberinput form-control',
                    'placeholder': gettext_lazy('Actuel'),
                },
            ),
            'default_force_field': forms.NumberInput(
                attrs={
                    'class': 'numberinput form-control',
                    'placeholder': gettext_lazy('Par défaut'),
                },
            ),
        }

class CharacterCreationForm(forms.ModelForm):
    token = forms.CharField(widget=forms.HiddenInput)
    automatically_computed = forms.BooleanField(
        label="Calculs automatiques",
        required=False,
        widget=forms.CheckboxInput(attrs={'onchange': 'knightools.toggle_automatically_computed()'})
    )
    armour = forms.ModelChoiceField(
        Armour.objects.order_by('name'),
        widget=forms.Select(attrs={'onchange': 'knightools.compute_form()'})
    )
    
    def __init__(self, *args, **kwargs):
        super(CharacterCreationForm, self).__init__(*args, **kwargs)
        
        
        ## OK, faut faire tout le formulaire en mode FormHelper, avec les templates personnalisés qui vont bien, et ça devrait le faire.
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Div(
                Field('current_glory_points', css_class='form-group', template='virtualtable/field.html'),
                Field('total_glory_points', css_class='form-group', template='virtualtable/field.html'),
                css_class='input-group',
            ),
        )
        
        
        # for aspect in Aspect.objects.all():
        #     self.fields[("character_aspect_%d" % aspect.pk)] = forms.IntegerField(
        #         initial=2,
        #         label=False
        #     )
        #     for characteristic in Characteristic.objects.filter(aspect=aspect):
        #         self.fields[("character_characteristic_%d_value" % characteristic.pk)] = forms.IntegerField(
        #             initial=1,
        #             label=False,
        #             widget=forms.NumberInput(
        #                 attrs={'class': 'numberinput form-control col-5'}
        #             )
        #         )
        #         self.fields[("character_characteristic_%d_od" % characteristic.pk)] = forms.IntegerField(
        #             label=False,
        #             widget=forms.NumberInput(
        #                 attrs={
        #                     'placeholder': 'OD',
        #                     'class': 'numberinput form-control col-5'
        #                 }
        #             )
        #         )
        #         self.fields[("character_characteristic_%d_color" % characteristic.pk)] = RGBColorField(
        #             label=False,
        #             widget=ColorFieldWidget(
        #                 attrs={'class': 'numberinput form-control col-2'}
        #             )
        #         )
        
    
    class Meta:
        model = Character
        fields = '__all__'

# class VirtualTableForm(forms.Form):
#     name = forms.CharField(widget=forms.TextInput())
#     description = MartorFormField()
    
STATES = (
    ('', 'Choose...'),
    ('MG', 'Minas Gerais'),
    ('SP', 'Sao Paulo'),
    ('RJ', 'Rio de Janeiro')
)

class TestForm(forms.Form):
    email = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Email'}))
    password = forms.CharField(widget=forms.PasswordInput())
    address_1 = forms.CharField(
        label='Address',
        widget=forms.TextInput(attrs={'placeholder': '1234 Main St'})
    )
    address_2 = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Apartment, studio, or floor'})
    )
    city = forms.CharField()
    state = forms.ChoiceField(choices=STATES)
    zip_code = forms.CharField(label='Zip')
    check_me_out = forms.BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('email', css_class='form-group col-md-6 mb-0'),
                Column('password', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            'address_1',
            'address_2',
            Div(
                Field('city', css_class='form-group', template='virtualtable/field.html'),
                Field('state', css_class='form-group', template='virtualtable/field.html'),
                Field('zip_code', css_class='form-group', template='virtualtable/field.html'),
                css_class='input-group'
            ),
            'check_me_out',
            Submit('submit', 'Sign in')
        )
        
