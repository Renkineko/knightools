$(function() {
	if (!$(':checkbox[name=game_master]').is(':checked')) {
		$(':checkbox[name=game_master]').on('change', function() {
			if ($(':checkbox[name=game_master]').is(':checked')) {
				$('#game-master-question').removeClass('d-none');
			}
			else {
				$('#game-master-question').addClass('d-none').find('input').val('');
			}
		});
	}
});