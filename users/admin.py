from django.contrib import admin

# Register your models here.
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser, Feature

class CustomUserAdmin(UserAdmin):
    model = CustomUser
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    
    fieldsets = UserAdmin.fieldsets + ( (None, {'fields': ('game_master',)}), )

class FeatureAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'key')
    prepopulated_fields = {"slug": ("name",)}

admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Feature, FeatureAdmin)