from django.utils.translation import gettext_lazy

from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from allauth.account.forms import SignupForm
from .models import CustomUser

class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = CustomUser
        fields = UserCreationForm.Meta.fields

class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = CustomUser
        fields = UserChangeForm.Meta.fields

class CustomUserProfileForm(UserChangeForm):
    game_master_question = forms.CharField(required=False, label="question", help_text=gettext_lazy("Ne pas mettre d'article défini devant la réponse (le, la, l', les...) ! Évitez les accents si possibles. Exemple : «La Désolation» donnerait : «Desolation» ; «L'Éphémère» deviendrait «ephemere»."))
    activation_code = forms.CharField(max_length=1024, label=gettext_lazy("Code d'activation"), widget=forms.Textarea, required=False, help_text=gettext_lazy('Un code par ligne si vous souhaitez en saisir plusieurs.'))
    first_name = forms.CharField(max_length=30, required=False, label=gettext_lazy("Pseudo (public)"))
    
    def __init__(self, *args, **kwargs):
        super(CustomUserProfileForm, self).__init__(*args, **kwargs)
        
        if self.instance.get_next_game_master_question():
            remaining_tries = 5 - self.instance.asked_game_master_questions.count()
            self.fields["game_master_question"].label = gettext_lazy('%s (Essais restants : %d)' % (self.instance.get_next_game_master_question().question, remaining_tries))
    
    class Meta:
        model = CustomUser
        fields = ('last_name', 'first_name', 'username', 'email', 'game_master', 'gest_account', 'prechecked_sources')

class CustomSignupForm(SignupForm):
    first_name = forms.CharField(max_length=30, required=False, label=gettext_lazy("Pseudo (public)"))
    
    def save(self, request):

        # Ensure you call the parent class's save.
        # .save() returns a User object.
        user = super(CustomSignupForm, self).save(request)

        # Add your own processing here.
        # user.game_master = self.cleaned_data['game_master']
        user.first_name = self.cleaned_data['first_name']
        user.save()
        
        # You must return the original result.
        return user
    
    class Meta:
        model = CustomUser
        fields = UserChangeForm.Meta.fields
    