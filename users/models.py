import random

from django.utils.translation import gettext_lazy
from django.db import models

# Create your models here.

from parler.models import TranslatableModel, TranslatedFields

from django.contrib.auth.models import AbstractUser, UserManager
from gm.models.character import Armour, Trauma, MechaArmour
from gm.models.weaponry import Weapon, Module, Effect, Source, Enhancement

from gm.models.character import (
    ArmourI18n, TraumaI18n, MechaArmourI18n
)
from gm.models.weaponry import (
    WeaponI18n, ModuleI18n, EffectI18n, SourceI18n, EnhancementI18n
)

class CustomUserManager(UserManager):
    pass

class GameMasterConfirmation(models.Model):
    question = models.CharField(max_length=256)
    answer = models.CharField(max_length=64)

class GameMasterConfirmationI18n(TranslatableModel):
    translations = TranslatedFields(
        question = models.CharField(max_length=256),
        answer = models.CharField(max_length=64),
    )

class Feature(models.Model):
    name = models.CharField("feature", max_length=128)
    slug = models.SlugField("slug", max_length=128)
    key = models.CharField("clé", max_length=256)
    
    def __str__(self):
        return self.name

class CustomUser(AbstractUser):
    objects = CustomUserManager()
    game_master = models.BooleanField(gettext_lazy('Accès MJ'), default=False, help_text=gettext_lazy("En cochant cette case, vous devrez répondre à une question dont la réponse se trouve dans la section MJ du Livre de Base. Si vous répondez faux, la case ne sera pas prise en compte. Vous n'avez que 5 essais en tout. Si vous répondez juste, vous pourrez créer des tables virtuelles ou avoir accès à des portions du site uniquement accessibles aux MJs (ou PJ ayant les accréditations nécessaires par leur MJ)."))
    theme_name = models.CharField(max_length=64, default='', blank=True)
    prechecked_sources = models.ManyToManyField(SourceI18n, verbose_name=gettext_lazy("Sources par défaut"), blank=True)
    asked_game_master_questions = models.ManyToManyField(GameMasterConfirmationI18n, 'asked_questions', blank=True)
    unlocked_armours = models.ManyToManyField(ArmourI18n, 'unlocked_armours', blank=True)
    unlocked_traumas = models.ManyToManyField(TraumaI18n, 'unlocked_traumas', blank=True)
    unlocked_mechas = models.ManyToManyField(MechaArmourI18n, 'unlocked_mechas', blank=True)
    unlocked_features = models.ManyToManyField(Feature, related_name='unlocked_by', blank=True)
    gest_account = models.BooleanField(gettext_lazy('Accès Geste'), default=False, blank=True, help_text=gettext_lazy("En cochant cette case, vous accéderez en tant que MJ aux contenus Geste. Cette case n'impacte que les portions du site compatible avec la Geste et non public (donc, les armures Shaman ou Warlock ne sont pas impactés par exemple). Les capacités PNJs seront filtrées pour correspondre à une vision sans la campagne. Note : les données venant de la Geste n'ont pas de traduction sur le site !"))
    
    def get_next_game_master_question(self):
        """
        This deterministic method (unless new questions are added in database) returns always the same next question to become a game master.
        It avoids the issue of not knowing the answer and just refreshing the page until it gives you (maybe the only one) question you can answer.
        """
        already_asked_questions = self.asked_game_master_questions.all()
        already_asked_questions_count = len(already_asked_questions)
        if already_asked_questions_count < 5:
            random.seed(self.pk)
            
            while already_asked_questions_count > 0:
                # We move forward the random values until we reach the first not already used
                random.random()
                already_asked_questions_count = already_asked_questions_count - 1
            
            if GameMasterConfirmationI18n.objects.count() == 0:
                return None
                
            game_master_confirmations = GameMasterConfirmationI18n.objects.filter(~models.Q(pk__in=already_asked_questions)).all()
            
            if len(game_master_confirmations) == 0:
                return None
            
            return random.sample(list(GameMasterConfirmationI18n.objects.filter(~models.Q(pk__in=already_asked_questions)).all()), 1)[0]
        else:
            return None
    
    def access_to_restricted_area(self):
        return (
            self.is_trauma_visible()
         or self.is_mecha_visible()
         or self.is_twin_ability_visible()
         or self.is_ultimate_aptitude_visible()
         or self.is_npc_capacity_visible()
        )
    
    def is_trauma_visible(self):
        return self.is_authenticated and (self.is_gest_game_master() or self.unlocked_traumas.count() > 0)
    
    def is_mecha_visible(self):
        return self.is_authenticated and (self.is_gest_game_master() or self.unlocked_mechas.count() > 0)
    
    def is_npc_capacity_visible(self):
        return self.is_authenticated and self.game_master
    
    def is_twin_ability_visible(self):
        return self.is_authenticated and (self.is_gest_game_master() or self.unlocked_features.filter(slug='twin-ability').count() > 0)
    
    def is_ultimate_aptitude_visible(self):
        return self.is_authenticated and (self.is_gest_game_master() or self.unlocked_features.filter(slug='ultimate-aptitude').count() > 0)
    
    def is_gest_game_master(self):
        return self.is_authenticated and self.game_master and self.gest_account
    
    
class FavoriteList(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='fav_lists')
    name = models.TextField(max_length=64, blank=True)
    order = models.PositiveSmallIntegerField('index', default=0)
    
    def __str__(self):
        return self.name
    
    def define_default_name(self):
        if self.name == '':
            # Get the last list without name
            if FavoriteList.objects.filter(user=self.user, name__istartswith=gettext_lazy("Sans nom")).count() > 0:
                # get the name attribute, and sub string it to get the number.
                last_no_name = FavoriteList.objects.filter(user=self.user, name__istartswith=gettext_lazy("Sans nom")).order_by('-name')[0].name[9:]
                
                if last_no_name == '':
                    self.name = '%s %s' % (gettext_lazy("Sans nom"), 1)
                elif last_no_name.isnumeric():
                    self.name = '%s %s' % (gettext_lazy("Sans nom"), (int(last_no_name) + 1))
                else:
                    self.name = gettext_lazy("Sans nom")
            else:
                self.name = gettext_lazy("Sans nom")
        
        return self
    
    def set_last_order(self):
        max_order = FavoriteList.objects.filter(user=self.user).aggregate(models.Max('order'))['order__max']
        if max_order is None:
            max_order = 0
        self.order = max_order + 1
        return self
        
    class Meta:
        ordering = ['order', 'name']
    
class FavoriteMixin(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    note = models.TextField(max_length=1024, blank=True)
    lists = models.ManyToManyField(FavoriteList, verbose_name=gettext_lazy("listes"), blank=True)
    
    def get_lists_id(self):
        return self.lists.values_list('pk', flat=True)
    
    class Meta:
        abstract = True

class FavoriteWeapon(FavoriteMixin):
    weapon = models.ForeignKey(Weapon, on_delete=models.CASCADE, related_name="user_favorites")
    
    class Meta:
        ordering = ['weapon__name']
    
class FavoriteEffect(FavoriteMixin):
    effect = models.ForeignKey(Effect, on_delete=models.CASCADE, related_name="user_favorites")
    
    class Meta:
        ordering = ['effect__name']
    
class FavoriteModule(FavoriteMixin):
    module = models.ForeignKey(Module, on_delete=models.CASCADE, related_name="user_favorites")
    
    class Meta:
        ordering = ['module__name']
    
class FavoriteArmour(FavoriteMixin):
    armour = models.ForeignKey(Armour, on_delete=models.CASCADE, related_name="user_favorites")
    
    class Meta:
        ordering = ['armour__name']

class FavoriteEnhancement(FavoriteMixin):
    enhancement = models.ForeignKey(Enhancement, on_delete=models.CASCADE, related_name="user_favorites")
    
    class Meta:
        ordering = ['enhancement__name']

class FavoriteMechaArmour(FavoriteMixin):
    mecha = models.ForeignKey(MechaArmour, on_delete=models.CASCADE, related_name="user_favorites")
    
    class Meta:
        ordering = ['mecha__name']

class FavoriteWeaponI18n(FavoriteMixin):
    weapon = models.ForeignKey(WeaponI18n, on_delete=models.CASCADE, related_name="user_favorites")
    
    class Meta:
        ordering = ['weapon__translations__name']
    
class FavoriteEffectI18n(FavoriteMixin):
    effect = models.ForeignKey(EffectI18n, on_delete=models.CASCADE, related_name="user_favorites")
    
    class Meta:
        ordering = ['effect__translations__name']
    
class FavoriteModuleI18n(FavoriteMixin):
    module = models.ForeignKey(ModuleI18n, on_delete=models.CASCADE, related_name="user_favorites")
    
    class Meta:
        ordering = ['module__translations__name']
    
class FavoriteArmourI18n(FavoriteMixin):
    armour = models.ForeignKey(ArmourI18n, on_delete=models.CASCADE, related_name="user_favorites")
    
    class Meta:
        ordering = ['armour__translations__name']

class FavoriteEnhancementI18n(FavoriteMixin):
    enhancement = models.ForeignKey(EnhancementI18n, on_delete=models.CASCADE, related_name="user_favorites")
    
    class Meta:
        ordering = ['enhancement__translations__name']

class FavoriteMechaArmourI18n(FavoriteMixin):
    mecha = models.ForeignKey(MechaArmourI18n, on_delete=models.CASCADE, related_name="user_favorites")
    
    class Meta:
        ordering = ['mecha__translations__name']