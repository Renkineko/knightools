# """
# ASGI entrypoint. Configures Django and then runs the application
# defined in the ASGI_APPLICATION setting.
# """

# import os
# import django
# from django.core.asgi import get_asgi_application
# from channels.routing import ProtocolTypeRouter

# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "knight_master_overview.settings")
# django.setup()
# # application = get_asgi_application()


# application = ProtocolTypeRouter({
#     "http": get_asgi_application(),
#     # Other protocols here.
# })


import os

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator
from django.core.asgi import get_asgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "knight_master_overview.settings")
# Initialize Django ASGI application early to ensure the AppRegistry
# is populated before importing code that may import ORM models.
django_asgi_app = get_asgi_application()

import virtualtable.routing

application = ProtocolTypeRouter(
    {
        "http": django_asgi_app,
        "websocket": AllowedHostsOriginValidator(
            AuthMiddlewareStack(URLRouter(virtualtable.routing.websocket_urlpatterns))
        ),
    }
)
