from django.conf import settings

def site_info(request):
    return {
        'KNIGHTOOLS_SITE_NAME': settings.KNIGHTOOLS_SITE_NAME,
        'KNIGHTOOLS_SITE_DESC': settings.KNIGHTOOLS_SITE_DESCRIPTION,
        'KNIGHTOOLS_SITE_BETA': settings.KNIGHTOOLS_SITE_BETA,
        'KNIGHTOOLS_SITE_FINAL_URL': settings.KNIGHTOOLS_SITE_FINAL_URL,
    }