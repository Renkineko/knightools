from django.db import transaction
from gm.models.faq import Category, CategoryTreebeard, CategoryTreebeardI18n, CategoryTreebeardI18nName  # Assure-toi d'importer les modèles

def migrate_mptt_to_treebeard():
    # Utilisation d'une transaction pour garantir l'intégrité des données
    with transaction.atomic():
        # On parcourt les catégories MPTT dans l'ordre de l'arborescence
        for category in Category.objects.all().order_by('tree_id', 'lft'):
            # Création du nouveau nœud dans TreeBeard
            if category.parent:
                # Trouver la catégorie parent dans TreeBeard
                parent_node = CategoryTreebeard.objects.get(id=category.parent.id)
                new_category = parent_node.add_child(
                    id=category.id,  # Conserver la même clé primaire
                    name=category.name,
                    css_classes=category.css_classes
                )  # Ajouter comme enfant du parent
            else:
                # Si la catégorie n'a pas de parent, on l'ajoute comme racine avec add_root()
                new_category = CategoryTreebeard.add_root(
                    id=category.id,  # Conserver la même clé primaire
                    name=category.name,
                    css_classes=category.css_classes
                )  # Sauvegarde comme un nœud racine
            
            print(f"Category {category.name} (ID {category.id}) migrated to TreeBeard.")
    
    print("Migration terminée avec succès.")

def migrate_mptt_to_treebeardi18n():
    # Utilisation d'une transaction pour garantir l'intégrité des données
    with transaction.atomic():
        # On parcourt les catégories MPTT dans l'ordre de l'arborescence
        for category in Category.objects.exclude(pk__in=CategoryTreebeardI18n.objects.all()).all().order_by('tree_id', 'lft'):
            # Création du nouveau nœud dans TreeBeard
            if category.parent:
                # Trouver la catégorie parent dans TreeBeard
                parent_node = CategoryTreebeardI18n.objects.get(id=category.parent.id)
                new_category = parent_node.add_child(
                    id=category.id,  # Conserver la même clé primaire
                    internal_name=('%s %d' % ('Initial cat.', category.id)),
                    css_classes=category.css_classes
                )  # Ajouter comme enfant du parent
            else:
                # Si la catégorie n'a pas de parent, on l'ajoute comme racine avec add_root()
                new_category = CategoryTreebeardI18n.add_root(
                    id=category.id,  # Conserver la même clé primaire
                    internal_name=('%s %d' % ('Initial cat.', category.id)),
                    css_classes=category.css_classes
                )  # Sauvegarde comme un nœud racine
                
            new_category_name = CategoryTreebeardI18nName(category=new_category)
            new_category_name.set_current_language('fr')
            new_category_name.name = category.name
            new_category_name.save()
            
            print(f"Category {category.name} (ID {category.id}) migrated to TreeBeard.")
    
    print("Migration terminée avec succès.")
