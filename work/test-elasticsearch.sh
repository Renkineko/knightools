#!/usr/bash

curl -X POST "localhost:9200/modules/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "query": {
    "bool" : {
      "filter": {
        "term" : { "name" : "aigle" }
      }
    }
  }
}
'

curl -X GET "localhost:9200/modules/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "from": 0,
  "size": 150
}
'
