import importlib
from django.db import transaction
from django.core.exceptions import FieldDoesNotExist, ObjectDoesNotExist
from work.mptt_to_treebeard import migrate_mptt_to_treebeardi18n


def class_for_name(module_name, class_name):
    m = importlib.import_module(module_name)
    c = getattr(m, class_name)
    return c

def migrate_objects_to_i18n(module_name, class_name, rename_fields=None, set_current_language=True, custom_i18n_class_name=None, custom_i18n_relation_name=None):
    """
    Migrates objects from a source model to its internationalized (i18n) version. 
    Handles field copying, renaming, and relationships (foreign key and many-to-many), 
    ensuring deduplication and optional language setting.
    
    Args:
        module_name (str): The name of the Python module where the models are defined.
        class_name (str): The name of the source model to migrate.
        rename_fields (dict, optional): A mapping of field names in the source model 
            to their new names in the i18n model. Defaults to None.
        set_current_language (bool, optional): If True, sets the default language 
            for the i18n object to 'fr'. If model does not inherit TranslatableModel,
            set this to False. Defaults to True.
        custom_i18n_class_name (str, optional): Custom name for the i18n model class. 
            If not provided, the function appends 'I18n' to the original class name. 
            Defaults to None.
        custom_i18n_relation_name (dict, optional): A mapping of related model names 
            to their respective i18n model names. Used to resolve foreign key relationships. 
            Defaults to None.
    
    Behavior:
        - Resolves the source and i18n models using the provided parameters.
        - Executes the migration within a transaction to ensure data consistency.
        - Optionally sets the current language of the i18n object to 'fr'.
        - Copies fields from the source model to the i18n model, supporting field renaming.
        - Migrates foreign key relationships, resolving to i18n versions of related models.
        - Migrates many-to-many relationships, handling related model translations.
        - Prints progress messages and warnings for ignored fields or encountered exceptions.
        - Skips objects that already exist in the i18n model to prevent duplication.
    
    Prints:
        Progress updates, warnings for ignored fields, and exceptions for issues encountered 
        during migration.
    
    Example:
        migrate_objects_to_i18n(
            module_name="gm.models.character",
            class_name="Characteristic",
            rename_fields={"npc_description": "my_npc_description"},
            set_current_language=True,
            custom_i18n_class_name="MyCharacTranslation",
            custom_i18n_relation_name={"Aspect": "MyOwnAspectTranslation"}
        )

    This example migrates the `Characteristic` objects from `gm.models.character`
    to its i18n counterpart MyCharacTranslation, renaming the field `npc_description`
    to `my_npc_description`, and setting the default language to 'fr'. 
    Custom class and relation names are provided for i18n versions of the models.

    Raises:
        FieldDoesNotExist: If a specified field does not exist in the source or i18n model.
        Exception: For errors during foreign key or many-to-many relationship migrations.
    """
    
    red = "\033[0;31m"
    green = "\033[0;32m"
    reset = "\033[0m"
    yellow = "\033[0;33m"
    
    print("  Migrating %s ... " % class_name)
    model = class_for_name(module_name, class_name)
    
    model_i18n_class_name = "%sI18n" % class_name
    if custom_i18n_class_name is not None:
        model_i18n_class_name = custom_i18n_class_name
    model_i18n = class_for_name(module_name, model_i18n_class_name)
    
    with transaction.atomic():
        for object in model.objects.exclude(pk__in=model_i18n.objects.values_list('pk', flat=True)).all():
            i18n = model_i18n()
            
            # Allow for relations class to be integrated by this function
            if set_current_language:
                i18n.set_current_language('fr')
            
            # For each field in the _meta data, we copy the value
            for field in object._meta.fields:
                try:
                    field_name = field.name
                    i18n_field_name = field.name
                    if rename_fields is not None and field.name in rename_fields:
                        i18n_field_name = rename_fields[field.name]
                        
                    
                    if not field.is_relation:
                        setattr(i18n, i18n_field_name, getattr(object, field_name))
                    else:
                        foreign_obj = getattr(object, field.name)
                        if foreign_obj is None:
                            continue
                        if i18n._meta.get_field(field.name).related_model == field.related_model:
                            # print('  i18n._meta.get_field(field.name).related_model (%s) == field.related_model (%s)' % (i18n._meta.get_field(field.name).related_model, field.related_model))
                            setattr(i18n, i18n_field_name, getattr(object, field_name))
                        else:
                            try:
                                i18n_relation_name = '%sI18n' % field.related_model.__name__
                                if custom_i18n_relation_name is not None and field.related_model.__name__ in custom_i18n_relation_name:
                                    i18n_relation_name = custom_i18n_relation_name[field.related_model.__name__]
                                i18n_foreign_class = class_for_name(field.related_model.__module__, i18n_relation_name)
                                foreign_i18n_obj = i18n_foreign_class.objects.get(pk=foreign_obj.pk)
                                setattr(i18n, i18n_field_name, foreign_i18n_obj)
                            except Exception as e:
                                print("%s  Except trying to get foreign object: %s %s" % (red, str(e), reset))
                except FieldDoesNotExist as e:
                    print("%s  Field %s ignored %s" % (yellow, field, reset))
                    continue
            print("%s  Migrate object with key %d %s" % (green, object.pk, reset))
            i18n.save()
            
            # Traiter les relations ManyToMany
            for m2m_field in object._meta.many_to_many:
                try:
                    m2m_field_name = m2m_field.name
                    i18n_m2m_field_name = m2m_field.name
                    if rename_fields is not None and m2m_field.name in rename_fields:
                        i18n_m2m_field_name = rename_fields[m2m_field.name]
                    m2m_objects = getattr(object, m2m_field_name).all()
                    related_model = m2m_field.related_model
                    i18n_related_model = i18n._meta.get_field(i18n_m2m_field_name).related_model

                    if i18n_related_model == related_model:
                        # Si le modèle lié est identique, on peut copier directement
                        getattr(i18n, i18n_m2m_field_name).set(m2m_objects)
                    else:
                        # Si le modèle est différent, il faut transposer les objets liés en version traduite
                        i18n_m2m_objects = []
                        try:
                            i18n_foreign_class = class_for_name(i18n_related_model.__module__, i18n_related_model.__name__)
                            for m2m_object in m2m_objects:
                                foreign_i18n_obj = i18n_foreign_class.objects.get(pk=m2m_object.pk)
                                i18n_m2m_objects.append(foreign_i18n_obj)
                            getattr(i18n, i18n_m2m_field_name).set(i18n_m2m_objects)
                        except Exception as e:
                            print("%s  Exception trying to set many-to-many related object: %s %s" % (red, str(e), reset))
                except FieldDoesNotExist as e:
                    print("%s  Field %s ignored %s" % (yellow, m2m_field, reset))
                    continue
            print("%s  Save M2M relations for object with key %d %s" % (green, object.pk, reset))
            i18n.save()
            

# migrate_objects_to_i18n('gm.models.character', 'Aspect')
# migrate_objects_to_i18n('gm.models.character', 'Characteristic')
# migrate_objects_to_i18n('gm.models.character', 'Archetype')

def migrate():
    
    blue = "\033[0;34m"
    cyan = "\033[0;36m"
    magenta = "\033[0;35m"
    reset = "\033[0m"
    
    print(blue + '========== START MIGRATING ========== ' + reset)
    
    # Category must be done 
    print(cyan + '=== Migrating gm.models.faq ===' + reset)
    print('Category (mptt to treebeard AND i18n)...')
    migrate_mptt_to_treebeardi18n()
    print('Question...')
    migrate_objects_to_i18n('gm.models.faq', 'Question') # TranslatableModel
    print(magenta + '=== End gm.models.faq ===' + reset)
    
    print(cyan + '=== Migrating gm.models.character ===' + reset)
    print('Aspect...')
    migrate_objects_to_i18n('gm.models.character', 'Aspect') # TranslatableModel
    print('Characteristic...')
    migrate_objects_to_i18n('gm.models.character', 'Characteristic') # TranslatableModel
    print('Source...')
    migrate_objects_to_i18n('gm.models.character', 'Source') # TranslatableModel
    print('Archetype...')
    migrate_objects_to_i18n('gm.models.character', 'Archetype') # TranslatableModel
    print('GreatDeed...')
    migrate_objects_to_i18n('gm.models.character', 'GreatDeed') # TranslatableModel
    print('Crest...')
    migrate_objects_to_i18n('gm.models.character', 'Crest') # TranslatableModel
    print('MajorArcana (into ArcanaI18n)...')
    migrate_objects_to_i18n('gm.models.character', 'MajorArcana', custom_i18n_class_name='ArcanaI18n') # TranslatableModel
    print('Division...')
    migrate_objects_to_i18n('gm.models.character', 'Division') # TranslatableModel
    print('DivisionOverdrive...')
    migrate_objects_to_i18n('gm.models.character', 'DivisionOverdrive', set_current_language=False) # models
    print('Armour...')
    migrate_objects_to_i18n('gm.models.character', 'Armour') # TranslatableModel
    print('ArmourOverdrive...')
    migrate_objects_to_i18n('gm.models.character', 'ArmourOverdrive', set_current_language=False) # models
    print('ArmourAbility...')
    migrate_objects_to_i18n('gm.models.character', 'ArmourAbility') # TranslatableModel
    print('ArmourTwinAbility...')
    migrate_objects_to_i18n('gm.models.character', 'ArmourTwinAbility') # TranslatableModel
    print('ArmourEvolution...')
    migrate_objects_to_i18n('gm.models.character', 'ArmourEvolution') # TranslatableModel
    print('NonPlayerCharacter...')
    migrate_objects_to_i18n('gm.models.character', 'NonPlayerCharacter') # TranslatableModel
    print('NonPlayerCharacterAspect...')
    migrate_objects_to_i18n('gm.models.character', 'NonPlayerCharacterAspect', set_current_language=False) # models
    print('NonPlayerCharacterCharacteristic...')
    migrate_objects_to_i18n('gm.models.character', 'NonPlayerCharacterCharacteristic', set_current_language=False) # models
    print('NonPlayerCharacterDifficulty...')
    migrate_objects_to_i18n('gm.models.character', 'NonPlayerCharacterDifficulty') # TranslatableModel
    print('NonPlayerCharacterType...')
    migrate_objects_to_i18n('gm.models.character', 'NonPlayerCharacterType') # TranslatableModel
    print('NonPlayerCharacterCapacity (into NonPlayerCharacterAbilityI18n)...')
    migrate_objects_to_i18n('gm.models.character', 'NonPlayerCharacterCapacity', custom_i18n_class_name='NonPlayerCharacterAbilityI18n') # TranslatableModel
    print('Stance (into StyleI18n)...')
    migrate_objects_to_i18n('gm.models.character', 'Stance', custom_i18n_class_name='StyleI18n') # TranslatableModel
    print('TraumaCategoryI18n...')
    migrate_objects_to_i18n('gm.models.character', 'TraumaCategory') # TranslatableModel
    print('TraumaI18n...')
    migrate_objects_to_i18n('gm.models.character', 'Trauma') # TranslatableModel
    print('MechaArmourI18n...')
    migrate_objects_to_i18n('gm.models.character', 'MechaArmour') # TranslatableModel
    print('MechaArmourConfigurationI18n...')
    migrate_objects_to_i18n('gm.models.character', 'MechaArmourConfiguration') # TranslatableModel
    print(magenta + '=== End gm.models.character ===' + reset)
    
    print(cyan + '=== Migrating gm.models.weaponry ===' + reset)
    print('Activation...')
    migrate_objects_to_i18n('gm.models.weaponry', 'Activation') # TranslatableModel
    print('Effect...')
    migrate_objects_to_i18n('gm.models.weaponry', 'Effect') # TranslatableModel
    print('ModuleCategory...')
    migrate_objects_to_i18n('gm.models.weaponry', 'ModuleCategory') # TranslatableModel
    print('WeaponCategory...')
    migrate_objects_to_i18n('gm.models.weaponry', 'WeaponCategory') # TranslatableModel
    print('Rarity...')
    migrate_objects_to_i18n('gm.models.weaponry', 'Rarity') # TranslatableModel
    print('Reach...')
    migrate_objects_to_i18n('gm.models.weaponry', 'Reach') # TranslatableModel
    print('TitanReach...')
    migrate_objects_to_i18n('gm.models.weaponry', 'TitanReach') # TranslatableModel
    print('Enhancement...')
    migrate_objects_to_i18n('gm.models.weaponry', 'Enhancement') # TranslatableModel
    print('Module...')
    migrate_objects_to_i18n('gm.models.weaponry', 'Module') # TranslatableModel
    print('ModuleLevel...')
    migrate_objects_to_i18n('gm.models.weaponry', 'ModuleLevel') # TranslatableModel
    print('ModuleLevelEffect...')
    migrate_objects_to_i18n('gm.models.weaponry', 'ModuleLevelEffect') # TranslatableModel
    print('ModuleSlot...')
    migrate_objects_to_i18n('gm.models.weaponry', 'ModuleSlot', set_current_language=False) # models
    print('Overdrive...')
    migrate_objects_to_i18n('gm.models.weaponry', 'Overdrive') # TranslatableModel
    print('EnhancementEffect...')
    migrate_objects_to_i18n('gm.models.weaponry', 'EnhancementEffect') # TranslatableModel
    print('Weapon...')
    migrate_objects_to_i18n('gm.models.weaponry', 'Weapon') # TranslatableModel
    print('WeaponAttack...')
    migrate_objects_to_i18n('gm.models.weaponry', 'WeaponAttack') # TranslatableModel
    print('WeaponAttackEffect...')
    migrate_objects_to_i18n('gm.models.weaponry', 'WeaponAttackEffect') # TranslatableModel
    print('DivisionModule...')
    migrate_objects_to_i18n('gm.models.weaponry', 'DivisionModule', set_current_language=False) # models
    print('DivisionWeapon...')
    migrate_objects_to_i18n('gm.models.weaponry', 'DivisionWeapon', set_current_language=False) # models
    print('ArmourAbilityVariant...')
    migrate_objects_to_i18n('gm.models.weaponry', 'ArmourAbilityVariant') # TranslatableModel
    print('NonPlayerCharacterWeapon...')
    migrate_objects_to_i18n('gm.models.weaponry', 'NonPlayerCharacterWeapon', set_current_language=False) # models
    print('NonPlayerCharacterModuleLevel...')
    migrate_objects_to_i18n('gm.models.weaponry', 'NonPlayerCharacterModuleLevel', set_current_language=False) # models
    print('HeroicAbilityCategory...')
    migrate_objects_to_i18n('gm.models.weaponry', 'HeroicSkillCategory', custom_i18n_class_name='HeroicAbilityCategoryI18n') # TranslatableModel
    print('HeroicAbility...')
    migrate_objects_to_i18n('gm.models.weaponry', 'HeroicSkill', custom_i18n_class_name='HeroicAbilityI18n', custom_i18n_relation_name={'HeroicSkillCategory': 'HeroicAbilityCategoryI18n'}) # TranslatableModel
    print('Vehicle...')
    migrate_objects_to_i18n('gm.models.weaponry', 'Vehicle') # TranslatableModel
    print('ModuleVehicle...')
    migrate_objects_to_i18n('gm.models.weaponry', 'ModuleVehicle', set_current_language=False) # models
    print('UltimateAptitudeType...')
    migrate_objects_to_i18n('gm.models.weaponry', 'UltimateAptitudeType') # TranslatableModel
    print('UltimateAptitude...')
    migrate_objects_to_i18n('gm.models.weaponry', 'UltimateAptitude') # TranslatableModel
    # print('MechaArmourAction...')
    # migrate_objects_to_i18n('gm.models.weaponry', 'MechaArmourAction') # TranslatableModel
    print('MechaArmourActionCommon...')
    migrate_objects_to_i18n('gm.models.weaponry', 'MechaArmourActionCommon') # MechaArmourActionI18n
    print('MechaArmourActionConfiguration...')
    migrate_objects_to_i18n('gm.models.weaponry', 'MechaArmourActionConfiguration') # MechaArmourActionI18n
    print(magenta + '=== End gm.models.weaponry ===' + reset)
    
    print(cyan + '=== Migrating users.models ===' + reset)
    # Disabled because managed directly in migration file
    # print('GameMasterConfirmation...')
    # migrate_objects_to_i18n('users.models', 'GameMasterConfirmation') # TranslatableModel
    print(magenta + '=== End users.models ===' + reset)
    
    print(cyan + '=== Migrating virtualtable.models ===' + reset)
    print('HelpingArticle...')
    migrate_objects_to_i18n('virtualtable.models', 'HelpingArticle') # TranslatableModel
    print(magenta + '=== End virtualtable.models ===' + reset)
    
    print('========== STOP MIGRATING ========== ')



def migrate_favorites():
    
    print('========== START MIGRATING ========== ')
    
    print('FavoriteWeapon...')
    migrate_objects_to_i18n('users.models', 'FavoriteWeapon', set_current_language=False)
    print('FavoriteEffect...')
    migrate_objects_to_i18n('users.models', 'FavoriteEffect', set_current_language=False)
    print('FavoriteModule...')
    migrate_objects_to_i18n('users.models', 'FavoriteModule', set_current_language=False)
    print('FavoriteArmour...')
    migrate_objects_to_i18n('users.models', 'FavoriteArmour', set_current_language=False)
    print('FavoriteEnhancement...')
    migrate_objects_to_i18n('users.models', 'FavoriteEnhancement', set_current_language=False)
    print('FavoriteMechaArmour...')
    migrate_objects_to_i18n('users.models', 'FavoriteMechaArmour', set_current_language=False)