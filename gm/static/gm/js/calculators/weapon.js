var weapon_calculator = {
	
	change_boost: function() {
		const $tab = $(this).closest('.tab-pane');
		
		if ($tab.find('.boost-damage').val() < 0) {
			$tab.find('.boost-damage').val('');
		}
		
		if ($tab.find('.boost-violence').val() < 0) {
			$tab.find('.boost-violence').val('');
		}
		
		const boost_damage = parseInt($tab.find('.boost-damage').val(), 10) || 0;
		if (boost_damage > parseInt($tab.find('.boost-damage').attr('max'), 10)) {
			$tab.find('.boost-damage').addClass('bg-danger');
		}
		else {
			$tab.find('.boost-damage').removeClass('bg-danger');
		}
		
		const boost_violence = parseInt($tab.find('.boost-violence').val(), 10) || 0;
		if (boost_violence > parseInt($tab.find('.boost-violence').attr('max'), 10)) {
			$tab.find('.boost-violence').addClass('bg-danger');
		}
		else {
			$tab.find('.boost-violence').removeClass('bg-danger');
		}
		
		const damage_sum = parseInt($tab.find('.damage span[data-default-value]').attr('data-raw-value'), 10) + boost_damage;
		const violence_sum = parseInt($tab.find('.violence span[data-default-value]').attr('data-raw-value'), 10) + boost_violence;
		$tab.find('.damage span[data-default-value]').text(damage_sum);
		$tab.find('.violence span[data-default-value]').text(violence_sum);
		$tab.find('.damage span[data-avg-value]').text(damage_sum * 3);
		$tab.find('.violence span[data-avg-value]').text(violence_sum * 3);
		
		const damage_bonus = parseInt($tab.find('.damage span[data-raw-bonus]').attr('data-raw-bonus'), 10);
		if (damage_bonus > 0) {
			$tab.find('.damage span[data-raw-bonus]').text(`+${damage_bonus}`);
		}
		else {
			$tab.find('.damage span[data-raw-bonus]').text('');
		}
		
		const violence_bonus = $tab.find('.violence span[data-raw-bonus]').attr('data-raw-bonus');
		if (violence_bonus > 0) {
			$tab.find('.violence span[data-raw-bonus]').text(`+${violence_bonus}`);
		}
		else {
			$tab.find('.violence span[data-raw-bonus]').text('');
		}
		
		weapon_calculator.refresh_effect.apply(this, [true]);
	},
	
	change_evolution: function() {
		const $tab = $(this).closest('.tab-pane');
		const attack_id = $(this).attr('data-attack-id');
		
		localStorage.setItem(`weapon/attack${attack_id}`, JSON.stringify($(this).prop('checked')));
		
		switch ($(this).val()) {
			case 'REP': // Replace base damage
				const span_damage = $tab.find('.damage span[data-default-value]');
				const span_violence = $tab.find('.violence span[data-default-value]');
				const span_damage_bonus = $tab.find('.damage span[data-default-bonus]');
				const span_violence_bonus = $tab.find('.violence span[data-default-bonus]');
				
				if ($(this).prop('checked')) {
					span_damage.attr('data-raw-value', parseInt($(this).attr('data-attack-damage-dice'), 10));
					span_violence.attr('data-raw-value', parseInt($(this).attr('data-attack-violence-dice'), 10));
					span_damage_bonus.attr('data-raw-bonus', parseInt($(this).attr('data-attack-damage-bonus'), 10));
					span_violence_bonus.attr('data-raw-bonus', parseInt($(this).attr('data-attack-violence-bonus'), 10));
					
					let reach, boost;
					if ($(this).attr('data-attack-effects')) {
						const raw_effects = $(this).attr('data-attack-effects').split('|');
						raw_effects.forEach(raw_effect => {
							const effect_params = raw_effect.split(':');
							switch (effect_params[0]) {
								case 'BST':
									boost = {
										effect_level: parseInt(effect_params[2], 10),
									}
								break;
								
								case 'RCH':
									reach = {
										energy_by_level: parseInt(effect_params[1], 10),
									};
								break;
								
							}
						});
					}
					
					if (boost) {
						$tab.find('.boost-damage').attr('max', boost.effect_level);
						$tab.find('.boost-violence').attr('max', boost.effect_level);
					}
					
					const min_reach = $(this).attr('data-min-reach');
					if ($(this).attr('data-min-reach') > $tab.find('.reach option:first').val()) {
						// The reach of this attack is better than the base stats. Must rework the select...
						let ep_step = parseInt($tab.find('.reach').attr('data-energy-by-step'), 10);
						let ep_cost = 0;
						if (reach) {
							ep_step = reach.energy_by_level;
						}
						const old_reach = $tab.find('.reach').val();
						$tab.find('.reach option').each(function() {
							if ($(this).val() < min_reach) {
								$(this).prop('disabled', true).hide();
							}
							else {
								let reach_label = $(this).text();
								if ($(this).val() == min_reach) {
									reach_label = '-';
									if (old_reach < min_reach) {
										$tab.find('.reach').val($(this).val());
									}
								}
								else {
									reach_label = reach_label.replace(`+${$(this).attr('data-ini-cost')}`, `+${ep_cost}`);
								}
								$(this).attr('data-cost', ep_cost);
								$(this).text(reach_label);
								ep_cost+= ep_step;
							}
						});
						weapon_calculator.change_reach.apply(this);
					}
				}
				else {
					span_damage.attr('data-raw-value', span_damage.attr('data-default-value'));
					span_violence.attr('data-raw-value', span_violence.attr('data-default-value'));
					span_damage_bonus.attr('data-raw-bonus', span_damage_bonus.attr('data-default-bonus'));
					span_violence_bonus.attr('data-raw-bonus', span_violence_bonus.attr('data-default-bonus'));
					
					$tab.find('.boost-damage').attr('max', $tab.find('.boost-damage').attr('data-ini-max'));
					$tab.find('.boost-violence').attr('max', $tab.find('.boost-damage').attr('data-ini-max'));
					if ($tab.find('.boost-damage').val() > $tab.find('.boost-damage').attr('data-ini-max')) {
						$tab.find('.boost-damage').val($tab.find('.boost-damage').attr('data-ini-max'));
					}
					if ($tab.find('.boost-violence').val() > $tab.find('.boost-damage').attr('data-ini-max')) {
						$tab.find('.boost-violence').val($tab.find('.boost-damage').attr('data-ini-max'));
					}
					$tab.find('.reach option').each(function() {
						$(this).text($(this).attr('data-ini-label'))
						$(this).attr('data-cost', $(this).attr('data-ini-cost'));
						$(this).prop('disabled', false).show();
					});
					weapon_calculator.change_reach.apply(this);
				}
				weapon_calculator.change_boost();
			break;
			
			case 'LST':
				if ($(this).prop('checked')) {
					$tab.find(`.unlockable-list-${attack_id} :checkbox`).prop('disabled', false);
				}
				else {
					$tab.find(`.unlockable-list-${attack_id} :checkbox`).prop('disabled', true).prop('checked', false);
					weapon_calculator.refresh_effect.apply(this, [false]);
				}
			break;
			
			case 'RED':
				if ($(this).prop('checked')) {
					const energy_minimum = $(this).attr('data-energy-minimum');
					const energy_reduction = $(this).attr('data-energy-reduction');
					$tab.find('.effect-lists span[data-default-value]').each(function() {
						var new_cost = parseInt($(this).attr('data-default-value'), 10) - energy_reduction;
						if (new_cost < energy_minimum) {
							new_cost = energy_minimum;
						}
						$(this).text(new_cost);
					});
				}
				else {
					$tab.find('.effect-lists span[data-default-value]').each(function() {
						$(this).text($(this).attr('data-default-value'));
					});
				}
				weapon_calculator.refresh_effect.apply(this, [true]);
			break;
			
			case 'REM':
				if ($(this).prop('checked')) {
					$tab.find(`.hidden-by-attack-${attack_id}`).hide();
				}
				else {
					$tab.find(`.hidden-by-attack-${attack_id}`).show();
				}
			break;
		}
	},
	
	change_effect: function() {
		const $tab = $(this).closest('.tab-pane');
		var checked = $(this).prop('checked');
		if ($(this).is(':checkbox') && $(this).attr('name')) {
			$tab.find('.effect-lists :checkbox[name=' + $(this).attr('name') + ']').prop('checked', false);
			$(this).prop('checked', checked);
		}
		
		weapon_calculator.refresh_effect.apply(this, [false]);
	},
	
	change_reach: function() {
		const $tab = $(this).closest('.tab-pane');
		
		$tab.find('.final-reach').text($tab.find('.reach>option:selected').attr('data-reach-label'));
		weapon_calculator.refresh_effect.apply(this, [true]);
	},
	
	refresh_effect: function(compute_only_cost) {
		const $tab = $(this).closest('.tab-pane');
		const max_effect_by_list = parseInt($tab.attr('data-max-effect-by-list'), 10);
		compute_only_cost = compute_only_cost || false;
		
		let final_cost = 0;
		
		if (!compute_only_cost) {
			$tab.find('.final-effect-list li:not(.unremovable)').remove();
		}
		
		$('div.effect-lists').each(function() {
			let current_cost = parseInt($(this).find('span[data-default-value]').text(), 10);
			
			if (!compute_only_cost) {
				if ($(this).find(':checkbox:checked').length > max_effect_by_list) {
					$(this).find('div.state').removeClass('p-success').addClass('p-danger');
				}
				else {
					$(this).find('div.state').removeClass('p-danger').addClass('p-success');
				}
			}
			
			$(this).find(':checkbox:checked').each(function() {
				final_cost += current_cost;
				if (!compute_only_cost) {
					$tab.find('.final-effect-list').append('<li><a href="/effect/' + $(this).val() + '">' + $(this).parent().find('label').text() + '</a></li>');
				}
			});
		});
		
		if (!compute_only_cost) {
			$tab.find('.final-effect-list a[href*=effect]').each(knightools.process_effect_link);
		}
		
		final_cost += parseInt($tab.find('.boost-damage').val(), 10) || 0;
		final_cost += parseInt($tab.find('.boost-violence').val(), 10) || 0;
		final_cost += parseInt($tab.find('.reach option:selected').attr('data-cost'), 10) || 0;
		
		$tab.find('.final-cost').text(final_cost);
	},
	
	reset_form: function() {
		const $tab = $(this).closest('.tab-pane');
		
		$tab.find('.effect-lists :checkbox').prop('checked', false);
		$tab.find('.boost-damage').val('');
		$tab.find('.boost-violence').val('');
		$tab.find('.reach').val($tab.find('.reach>option:not(:disabled)[data-cost=0]').val());
		
		weapon_calculator.change_boost.apply(this);
		weapon_calculator.change_reach.apply(this);
		weapon_calculator.refresh_effect.apply(this, [false]);
	},
	
	reset_form_global: function() {
		$('.effect-lists :checkbox').prop('checked', false);
		$('.boost-damage').val('');
		$('.boost-violence').val('');
		$('.reach').val($('.reach>option:not(:disabled)[data-cost=0]').val());
		
		$('.tab-pane').each(function() {
			weapon_calculator.change_boost.apply(this);
			weapon_calculator.change_reach.apply(this);
			weapon_calculator.refresh_effect.apply(this, [false]);
		})
	},
};

$(function() {
	$('.evolutions-check :checkbox').on('change', weapon_calculator.change_evolution);
	$('.effect-lists :checkbox').on('change', weapon_calculator.change_effect);
	$('.boost-damage').on('change', weapon_calculator.change_boost);
	$('.boost-violence').on('change', weapon_calculator.change_boost);
	$('.reach').on('change', weapon_calculator.change_reach);
	
	$('.reset-form').on('click', weapon_calculator.reset_form);
	
	$('.evolutions-check :checkbox').each(function() {
		$(this).prop('checked', JSON.parse(localStorage.getItem('weapon/attack' + $(this).attr('data-attack-id') ) ) );
		$(this).trigger('change');
	});
	
	weapon_calculator.reset_form_global();
});
