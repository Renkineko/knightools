knightools = {
    processed_effect_link : {},
    
    copy_invitation: function(node) {
        var input = $(node).closest('.input-group').find('input');
        
        var value = input.val()
        var final_url = document.location.origin + input.attr('final-url');
        
        input.val(final_url);
        input[0].select();
        document.execCommand("copy");
        input.val(value);
        
        input.selectionStart = input.selectionEnd;
    },
    
    dt_search: function() {
        if (!knightools.datatables) {
            return false;
        }
        
        if ($('#dt-rarity-search').length !== 0) {
            var rarities = $('input:checkbox[name="rarity-search"]:checked').map(function() {
                return this.value;
            }).get().join('|');
              
            // Cherche via regexp (la liste des value des checkbox rarities séparées par | ), sans faire de smart search ni de case insensitive.
            knightools.datatables.columns('.rarity').search(rarities, true, false, false).draw();
        }
        
        if ($('#dt-source-search').length !== 0) {
            var sources = $('input:checkbox[name="source-search"]:checked').map(function() {
                return this.value;
            }).get().join('|');
              
            // Cherche via regexp (la liste des value des checkbox sources séparées par | ), sans faire de smart search ni de case insensitive.
            knightools.datatables.columns('.source').search(sources, true, false, false).draw();
        }
        
        if ($('#dt-activation-search').length !== 0) {
            var activations = $('input:checkbox[name="activation-search"]:checked').map(function() {
                return this.value;
            }).get().join('|');
            // console.log(activations);
            // Cherche via regexp (la liste des value des checkbox activations séparées par | ), sans faire de smart search ni de case insensitive.
            knightools.datatables.columns('.activation').search(activations, true, false, false).draw();
        }
        
        if ($('#dt-category-search').length !== 0) {
            var categories = $('input:checkbox[name="category-search"]:checked').map(function() {
                return this.value;
            }).get().join('|');
            // console.log(categories);
            // Cherche via regexp (la liste des value des checkbox categories séparées par | ), sans faire de smart search ni de case insensitive.
            knightools.datatables.columns('.category').search(categories, true, false, false).draw();
        }
        
        if ($('#dt-npc-ability-search').length !== 0) {
            var npc_ability_common = $('input:checkbox[name="npc-ability-common-search"]:checked').map(function() {
                return this.value;
            }).get().join('|');
            
            knightools.datatables.columns('.common').search(npc_ability_common, true, false, false).draw();
            knightools.datatables.columns('.human-allowed').search($('#human-allowed-search').val(), true, false, false).draw();
            knightools.datatables.columns('.anathema-allowed').search($('#anathema-allowed-search').val(), true, false, false).draw();
            knightools.datatables.columns('.ether-allowed').search($('#ether-allowed-search').val(), true, false, false).draw();
            knightools.datatables.columns('.void-allowed').search($('#void-allowed-search').val(), true, false, false).draw();
            
            $('input:checkbox[name="npc-ability-type-search"]').each(function() {
                knightools.datatables.columns('.npc-ability-type-'+$(this).attr('data-type-id')).search($(this).is(':checked') ? $(this).val() : '', true, false, false).draw();
            });
        }
    },
    
    process_effect_link: function() {
        const link = $(this);
        const language = window.location.pathname.split('/')[1];
        
        function transform_link(data) {
            if (data) {
                var title = data.short_description;
                var level = 'X';
                // console.log(title, level);
                if (data.slug.substr(-2) === '-x' && title.indexOf('$X') !== -1) {
                    // We are on an effect waiting for a level, we'll try to find it.
                    if (link.text().indexOf('(') !== -1 && link.text().indexOf(')') !== -1 && link.text().indexOf('(') < link.text().indexOf(')')) {
                        var index_level = link.text().indexOf('(') + 1;
                        level = link.text().substr(index_level, link.text().indexOf(')') - index_level);
                    }
                    else {
                        var splitted_name = link.text().split(' ');
                        var last_part_splitted_name = splitted_name.splice(-1);
                        // console.log(splitted_name, last_part_splitted_name);
                        if (!isNaN(parseInt(last_part_splitted_name, 10))) {
                            level = last_part_splitted_name;
                        }
                    }
                }
                // console.log(title, level);
                
                var button = $(
                    '<span class="d-none d-sm-inline-block">'
                        + '<a href="/' + language + link.attr('href') + '" title="' + title.replace(/\$X/gi, level) + '" data-html="true" data-toggle="tooltip">' + link.text() + '</a>'
                    + '</span>'
                    + '<span class="d-inline-block d-sm-none">'
                        + '<a onclick="$(this).tooltip(\'toggle\');" title="' + title.replace(/\$X/gi, level) + '" data-html="true" data-toggle="tooltip">' + link.text() + '</a>'
                        + '<a href="/' + language + link.attr('href') + '"><i class="ml-2 fas fa-xs fa-external-link-alt"></i></a>'
                    + '</span>');
                
                
                // $('<a title="'+title.replace(/\$X/gi, level)+'" data-toggle="tooltip">'+link.text()+'</a>');
                link.before(button);
                button.find('[data-toggle=tooltip]').tooltip();
                
                link.remove();
                knightools.processed_effect_link[link.attr('href')] = data;
                // link.attr('title', link.text()).html('<i class="ml-2 fas fa-xs fa-external-link-alt"></i>');
                // console.log(button, link);
            }
        }
        
        if (link.find('i,img').length > 0 || link.attr('href') === '/effect/') {
            // if the link is for the effect list or if the link has at least one child like i or img (it's possibly a complex link): no need to touch it.
            console.error('no touchy touchy');
            return;
        }
        
        if (knightools.processed_effect_link[link.attr('href')]) {
            transform_link(knightools.processed_effect_link[link.attr('href')]);
            return;
        }
        
        $.ajax('/' + language + '/api' + link.attr('href')) // Get the target effect JSON
            .done(transform_link);
    },
    
    set_theme: function() {
        $.ajax({
            url: $('#set-theme').attr("action"),
            data: {
                theme: $(this).attr('data-theme'),
            },
            dataType: 'json',
            method: 'post',
            success: function (data) {
                if (data.error) {
                    alert(data.error);
                    return;
                }
                
                if (data.updated) {
                    window.location.reload();
                }
            }
        });
        
        return false;
    },
};

$(function () {
    const language = window.location.pathname.split('/')[1];
    
    // basic tooltip
    $('[data-toggle="tooltip"]').tooltip();
    // tooltip for helper with NPC
    $('[data-toggle="tooltip-next"]').tooltip({
        html: true,
        title: function() {
            return $(this).next('span.d-none[tooltip-title]').html();
        },
    });
    
    // For each effect link contained in a markdown div, create a button with tooltip.
    $('.container .markdown-format a[href*=effect]').each(knightools.process_effect_link);
    
    $('.container .markdown-format table').addClass('table table-sm table-bordered table-striped');
    $('.container .markdown-format table thead').addClass('thead-light');
    
    $('.container .markdown-format.previewed-img img').each(function() {
        $(this).parent().addClass('text-center').append('<a href="' + $(this).attr('src') + '" target="_blank"></a>');
        $(this).parent().find('a').append($(this));
    });
    
    $('.container .markdown-format a[href^="i.fa-"]').each(function() {
        var fas_options = $(this).attr('href').substring(2).split('/');
        $(this).after('<i class="' + fas_options.join(' ') + '"></i>');
        $(this).remove();
    });
    
    /* a try to enhance faq feedback */
    $('#faq').find('h1:not(:first),h2:not(:first),h3:not(:first),h4:not(:first),h5:not(:first),h6:not(:first)').each(function() {
        $(this).before('<div class="text-center"><div class="knight-hr my-3 d-inline-block w-50"></div></div>');
    });
    /* */
    
    // hotfix submenu
    $(document).on('click', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });
  
    // make it as accordion for smaller screens
    if ($(window).width() < 992) {
        $('.dropdown-menu a').click(function(e){
            if($(this).next('.submenu').length){
                e.preventDefault();
                $(this).next('.submenu').toggle();
            }
            $('.dropdown').on('hide.bs.dropdown', function () {
                $(this).find('.submenu').hide();
            });
        });
    }
    // end hotfix submenu
    
    if (jQuery().DataTable) {
        knightools.datatables = $('table.dt-enable').DataTable({
            "paging": false,
            "info": false,
            "language": dt_lang,
            "dom": "lrtip",
        });
        
        $('#dt-filter input').on('input', function() {
            knightools
                .datatables
                .search( $(this).val() )
                .draw();
        });
        
        $('#dt-rarity-search :checkbox,#dt-source-search :checkbox,#dt-activation-search :checkbox,#dt-npc-ability-type-search :checkbox,#dt-category-search :checkbox,#dt-npc-ability-common-search :checkbox,#dt-npc-ability-search select').on('change', knightools.dt_search);
        
        knightools.dt_search();
    }
    
    $('.collapse').on('hidden.bs.collapse', function() {
        $('a[href="#'+$(this).attr('id')+'"] i.fa.fa-minus-square').removeClass('fa-minus-square').addClass('fa-plus-square');
    });
    $('.collapse').on('shown.bs.collapse', function() {
        $('a[href="#'+$(this).attr('id')+'"] i.fa.fa-plus-square').removeClass('fa-plus-square').addClass('fa-minus-square');
    });
    
    if ($('main.container .nav').length > 0 && $('main.container .nav .nav-link.active').length === 0) {
        $('main.container .nav .nav-link:not(.disabled):first').click();
    }
    
    $('#global-search').autocomplete({
        minLength: 2,
        source: `/${language}/ajax/autocomplete.json`,
        select: function(event, ui) {
            document.location = ui.item.value;
            return false;
        },
        focus: function(event, ui) {
            return false;
        },
    });
    
    $('a[data-theme]').on('click', knightools.set_theme);
    
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                var csrftoken = getCookie('csrftoken');
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
});



function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}