from django.apps import AppConfig


class GmConfig(AppConfig):
    name = 'gm'
    verbose_name="Game Mirador" # okay, it was originally for game master, but renaming will be a PITA, so let's go for strange name instead.