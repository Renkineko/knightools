from django import template
from django.contrib.staticfiles import finders
from django.template.defaultfilters import stringfilter
from django.utils.html import conditional_escape, escape
from django.utils.safestring import SafeData, mark_safe
# from haystack.utils.highlighting import Highlighter
from markdown import markdown

from gm.utils import build_new_language_url as build_language_no_url

from virtualtable.models import VirtualTable, VirtualTablePlayer

import bleach, unidecode, re

register = template.Library()

@register.simple_tag(name='get_themed')
def get_themed(filename, user, session, *args, **kwargs):
    """
        Return the filename with the current theme for the visitor. This template tag requires 3 informations:
            - filename: the filename static should look at. The filename must contains the string "$THEME$" where the theme name needs to be placed.
            - user: the user variable of the template
            - session: the request.session variable of the template
            
        In addition, you can setup 4 more informations:
            - append: append some string after the theme name if the name is not empty. Must be boolean True or False
            - prepend: prepend some string before the theme name if the name is not empty. Must be boolean True or False
            - append_str: the string to append to the theme name.
            - prepend_str: the string to prepend to the theme name.
    """
    # filename contains the static name of the wanted file, with $THEME$ to delimit where the theme must be placed.
    try:
        prepend_str = kwargs['prepend_str']
    except KeyError:
        prepend_str = ''
    except Exception as e:
        raise e
    
    try:
        append_str = kwargs['append_str']
    except KeyError:
        append_str = ''
    except Exception as e:
        raise e
    
    try:
        prepend = kwargs['prepend']
    except KeyError:
        prepend = False
    except Exception as e:
        raise e
    
    try:
        append = kwargs['append']
    except KeyError:
        append = False
    except Exception as e:
        raise e
    
    try:
        only_name = kwargs['only_name']
    except KeyError:
        only_name = False
    except Exception as e:
        raise e
        
    # import pdb; pdb.set_trace()
    
    if user.is_authenticated:
        if user.theme_name != '':
            theme_name = user.theme_name
            if prepend and prepend_str != '':
                theme_name = '%s%s' % (prepend_str, theme_name)
            if append and append_str != '':
                theme_name = '%s%s' % (theme_name, append_str)
        else:
            theme_name = 'anathema'
    else:
        if 'theme_name' in session and session['theme_name'] and session['theme_name'] != '':
            theme_name = session['theme_name']
            if prepend and prepend_str != '':
                theme_name = '%s%s' % (prepend_str, theme_name)
            if append and append_str != '':
                theme_name = '%s%s' % (theme_name, append_str)
        else:
            theme_name = 'anathema'
    
    if only_name:
        return theme_name
    
    final_filename = filename.replace('$THEME$', theme_name)
    
    theme_exists = finders.find(final_filename)
    
    if theme_exists is not None:
        return final_filename
    
    return filename.replace('$THEME$', 'anathema.')

@register.simple_tag(name='get_vt_destiny_state')
def get_vt_destiny_state(key):
    return getattr(VirtualTable, key)

@register.simple_tag(name='get_vtp_edit_lvl')
def get_vtp_edit_lvl(key):
    return getattr(VirtualTablePlayer, key)

@register.simple_tag(name='get_vtp_gest_advantage')
def get_vtp_gest_advantage(key):
    return getattr(VirtualTablePlayer, key)


@register.filter(is_safe=True, needs_autoescape=True)
@stringfilter
def highlight(text, searchterm, autoescape=True):
    """Englobes all values of searchterm from the given text string with a span, markdowned."""
    autoescape = autoescape and not isinstance(text, SafeData)
    if autoescape:
        text = escape(text)
    text = bleach.clean(text, tags=[], strip=True)
    
    # highlight = Highlighter(searchterm, css_class="search-highlight")
    marked_text = markdown(text)
    
    return mark_safe(marked_text)

@register.filter(is_safe=True, needs_autoescape=True)
@stringfilter
def get_title(key, titles, autoescape=True):
    """ Return the corresponding key from titles """
    autoescape = autoescape and not isinstance(key, SafeData)
    if autoescape:
        key = escape(key)
    
    try:
        return mark_safe(titles[key])
    except KeyError as e:
        return '[WRONGKEY %s]' % key
    except Exception as e:
        return '???'
    
@register.filter
def get_highlights(hit_meta, autoescape=True):
    """ Return a dictionnary with meta fragments from elasticsearch """
    
    dict_to_return = {}
    for attr in hit_meta:
        dict_to_return[attr] = list(set(hit_meta[attr]))
    
    return dict_to_return

@register.filter(is_safe=True)
@stringfilter
def file_exists(file):
    result = finders.find(file)
    return result is not None

@register.filter
def get_item(obj, key):
    return obj[key]

@register.filter
def build_new_language_url(current_url, new_language_code):
    return build_language_no_url(new_language_code, current_url)

@register.simple_tag
def set_var(value):
    return value