from django.db import models
from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin
from django.forms import SelectMultiple
from parler.admin import (
    TranslatableAdmin, TranslatableTabularInline,
    TranslatableStackedInline
)
from django.conf import settings
from treebeard.admin import TreeAdmin
from treebeard.forms import movenodeform_factory
from django.utils.translation import get_language
from django.db.models import Q

from .models.character import (
    Faction, FactionEffect, FactionDefaultEdge)

from .models.character import (
    ArchetypeI18n, GreatDeedI18n, ArmourI18n, ArmourAbilityI18n,
    ArmourEvolutionI18n, ArmourOverdriveI18n, CharacteristicI18n,
    ArcanaI18n, NonPlayerCharacterI18n, NonPlayerCharacterAspectI18n,
    NonPlayerCharacterCharacteristicI18n, DivisionI18n,
    DivisionOverdriveI18n, TraumaCategoryI18n, TraumaI18n, MechaArmourI18n,
    MechaArmourConfigurationI18n, NonPlayerCharacterAbilityI18n, CrestI18n,
    Faction, FactionEffect, FactionDefaultEdge, StyleI18n, ArmourTwinAbilityI18n,
    SourceI18n, AspectI18n, CharacteristicI18n
)

from .models.weaponry import (
    ActivationI18n, OverdriveI18n, ArmourAbilityVariantI18n, EffectI18n,
    EnhancementI18n, EnhancementEffectI18n, HeroicAbilityI18n,
    HeroicAbilityCategoryI18n, ModuleI18n, ModuleLevelI18n,
    ModuleLevelEffectI18n, ModuleSlotI18n,
    NonPlayerCharacterModuleLevelI18n, NonPlayerCharacterWeaponI18n,
    DivisionModuleI18n, DivisionWeaponI18n, RarityI18n, WeaponI18n,
    WeaponAttackI18n, WeaponAttackEffectI18n, VehicleI18n,
    ModuleVehicleI18n, MechaArmourActionCommonI18n,
    MechaArmourActionConfigurationI18n, TitanReachI18n, ReachI18n,
    UltimateAptitudeI18n, UltimateAptitudeTypeI18n
)

from .models.faq import Category

from .models.faq import (
    CategoryTreebeardI18n, CategoryTreebeardI18nName, QuestionI18n
)

def get_all_languages():
    languages = []
    for obj in settings.PARLER_LANGUAGES[None]:
        languages.append(obj['code'])
    return languages

# Register your models here.

class HeroicAbilityAdmin(TranslatableAdmin):
    list_display = ('name', 'category', 'xp_cost', 'heroic_cost')
    # prepopulated_fields = {"slug": ("name",)}
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class OverdriveAdmin(TranslatableAdmin):
    list_display = ('characteristic', 'level', 'description')
    # prepopulated_fields = {"slug": ("characteristic","level")}
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('characteristic','level')
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class HeroicSkillCategoryAdmin(TranslatableAdmin):
    list_display = ('name',)
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()
    
class DivisionModuleInline(TranslatableTabularInline):
    model = DivisionModuleI18n
    extra = 1
    
class DivisionWeaponInline(TranslatableTabularInline):
    model = DivisionWeaponI18n
    extra = 2
    
class DivisionOverdriveInline(TranslatableTabularInline):
    model = DivisionOverdriveI18n
    extra = 1

class GreatDeedAdmin(TranslatableAdmin):
    list_display = ('name', 'restriction')
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class ArcanaAdmin(TranslatableAdmin):
    list_display = ('name', 'roman_number')
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class CrestAdmin(TranslatableAdmin):
    list_display = ('name', 'description')
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class CategoryAdmin(DraggableMPTTAdmin):
    mptt_indent_field = "name"
    list_display = ('tree_actions', 'indented_title',)
    list_display_links = ('indented_title',)

class CategoryTreebeardI18nAdmin(TreeAdmin):
    form = movenodeform_factory(CategoryTreebeardI18n)
    list_display = ('get_translated_name', 'css_classes')
    search_fields = ('translations__translations__name',)

    def get_translated_name(self, obj):
        """
        Retrieves the translated name from the related CategoryTreebeardI18nName model.
        """
        return obj.translations.safe_translation_getter('name', any_language=True)
    get_translated_name.short_description = "Translated Name"

admin.site.register(CategoryTreebeardI18n, CategoryTreebeardI18nAdmin)

# Admin for CategoryTreebeardI18nName
class CategoryTreebeardI18nNameAdmin(TranslatableAdmin):
    list_display = ('category', 'name', 'get_languages')
    search_fields = ('translations__name',)
    autocomplete_fields = ('category',)
    
    def get_languages(self, obj):
        return get_all_languages()
    
    get_languages.short_description = "Languages"

admin.site.register(CategoryTreebeardI18nName, CategoryTreebeardI18nNameAdmin)


class ArchetypeQuestionInline(admin.TabularInline):
    model = ArchetypeI18n.questions.through
    extra = 1
class GreatDeedQuestionInline(admin.TabularInline):
    model = GreatDeedI18n.questions.through
    extra = 1
class CrestQuestionInline(admin.TabularInline):
    model = CrestI18n.questions.through
    extra = 1
class ArcanaQuestionInline(admin.TabularInline):
    model = ArcanaI18n.questions.through
    extra = 1
class DivisionQuestionInline(admin.TabularInline):
    model = DivisionI18n.questions.through
    extra = 1
class ArmourQuestionInline(admin.TabularInline):
    model = ArmourI18n.questions.through
    extra = 1
class TraumaQuestionInline(admin.TabularInline):
    model = TraumaI18n.questions.through
    extra = 1
class MechaArmourQuestionInline(admin.TabularInline):
    model = MechaArmourI18n.questions.through
    extra = 1
class NonPlayerCharacterAbilityQuestionInline(admin.TabularInline):
    model = NonPlayerCharacterAbilityI18n.questions.through
    extra = 1
class StyleQuestionInline(admin.TabularInline):
    model = StyleI18n.questions.through
    extra = 1
class EffectQuestionInline(admin.TabularInline):
    model = EffectI18n.questions.through
    extra = 1
class EnhancementQuestionInline(admin.TabularInline):
    model = EnhancementI18n.questions.through
    extra = 1
class ModuleQuestionInline(admin.TabularInline):
    model = ModuleI18n.questions.through
    extra = 1
class OverdriveQuestionInline(admin.TabularInline):
    model = OverdriveI18n.questions.through
    extra = 1
class WeaponQuestionInline(admin.TabularInline):
    model = WeaponI18n.questions.through
    extra = 1
class HeroicAbilityQuestionInline(admin.TabularInline):
    model = HeroicAbilityI18n.questions.through
    extra = 1
class VehicleQuestionInline(admin.TabularInline):
    model = VehicleI18n.questions.through
    extra = 1

class QuestionAdmin(TranslatableAdmin):
    list_display = ('question', 'answer', 'gm', 'v1', 'v1_5', 'source_type', 'get_full_category')
    ordering = ['category__path', '-updated']
    search_fields = ['translations__question', 'translations__answer']
    inlines = [
        ArchetypeQuestionInline,
        GreatDeedQuestionInline,
        CrestQuestionInline,
        ArcanaQuestionInline,
        DivisionQuestionInline,
        ArmourQuestionInline,
        TraumaQuestionInline,
        MechaArmourQuestionInline,
        NonPlayerCharacterAbilityQuestionInline,
        StyleQuestionInline,
        EffectQuestionInline,
        EnhancementQuestionInline,
        ModuleQuestionInline,
        OverdriveQuestionInline,
        WeaponQuestionInline,
        HeroicAbilityQuestionInline,
        VehicleQuestionInline
    ]
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()


class ArchetypeAdmin(TranslatableAdmin):
    list_display = ('name', 'description')
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class DivisionAdmin(TranslatableAdmin):
    list_display = ('name', 'aspect_bonus', 'disadvantage_name', 'motto')
    inlines = [DivisionWeaponInline, DivisionModuleInline, DivisionOverdriveInline]
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }

class ArmourEvolutionStacked(TranslatableStackedInline):
    model = ArmourEvolutionI18n
    extra = 3

class ArmourOverdriveInline(admin.TabularInline):
    model = ArmourOverdriveI18n
    extra = 1

class ArmourAdmin(TranslatableAdmin):
    list_display = ('name', 'generation')
    inlines = [ArmourOverdriveInline, ArmourEvolutionStacked]
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class ArmourAbilityVariantInline(TranslatableStackedInline):
    model = ArmourAbilityVariantI18n
    extra = 1

class ArmourAbilityAdmin(TranslatableAdmin):
    list_display = ('armour', 'name')
    inlines = [ArmourAbilityVariantInline]
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class ArmourTwinAbilityAdmin(TranslatableAdmin):
    list_display = ('__str__', 'get_excluded_armours')
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class CharacteristicAdmin(TranslatableAdmin):
    list_display = ('name', 'description', 'aspect', 'overdrive_global_description')
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class EffectAdmin(TranslatableAdmin):
    list_display = ('name', 'origin', 'description', 'short_description')
    search_fields = ['translations__name', 'translations__description', 'source__translations__name']
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class ModuleLevelEffectInline(TranslatableTabularInline):
    model = ModuleLevelEffectI18n
    extra = 1
    
    def get_queryset(self, request):
        # Get the current queryset
        qs = super().get_queryset(request)

        # Filter the queryset by the current language
        language = get_language()
        return qs.filter(
            Q(
                translations__language_code=language,
                effect__translations__language_code=language
            )
            | Q(
                translations__language_code=language,
                effect=None
            )
        )
    
class ModuleSlotInline(admin.TabularInline):
    model = ModuleSlotI18n
    extra = 1

class ModuleAdmin(TranslatableAdmin):
    list_display = ('name', 'gear')
    search_fields = ['translations__name']
    inlines = [ModuleSlotInline]
    # prepopulated_fields = {"slug": ("name",)}
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class ModuleLevelAdmin(TranslatableAdmin):
    list_display = ('module', 'level', 'rarity')
    search_fields = ['module__translations__name', 'module__category__translations__name']
    inlines = [ModuleLevelEffectInline]
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class EnhancementEffectInline(TranslatableTabularInline):
    model = EnhancementEffectI18n
    extra = 1
    
class WeaponAttackEffectInline(TranslatableTabularInline):
    model = WeaponAttackEffectI18n
    extra = 1
    
    def get_queryset(self, request):
        # Get the current queryset
        qs = super().get_queryset(request)

        # Filter the queryset by the current language
        language = get_language()
        return qs.filter(
            Q(
                translations__language_code=language,
                effect__translations__language_code=language
            )
            | Q(
                translations__language_code=language,
                effect=None
            )
        )


class WeaponAttackAdmin(TranslatableAdmin):
    list_display = ('weapon', 'name')
    model = WeaponAttackI18n
    inlines = [WeaponAttackEffectInline]
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class WeaponAdmin(TranslatableAdmin):
    list_display = ('name', 'rarity', 'category')
    search_fields = ['translations__name']
    # prepopulated_fields = {"slug": ("name",)}
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class EnhancementAdmin(TranslatableAdmin):
    list_display = ('name', 'restriction', 'group', 'category')
    search_fields = ['translations__restriction']
    ordering = ['category__translations__name', 'translations__name']
    inlines = [EnhancementEffectInline]
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class ModuleVehicleInline(TranslatableTabularInline):
    model = ModuleVehicleI18n
    extra = 1

class VehicleAdmin(TranslatableAdmin):
    list_display = ('name', 'weaponry')
    search_fields = ['translations__name']
    ordering = ['translations__name']
    # prepopulated_fields = {"slug": ("name",)}
    inlines = [ModuleVehicleInline]
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class NonPlayerCharacterAspectInline(TranslatableTabularInline):
    model = NonPlayerCharacterAspectI18n
    extra = 5
    
class NonPlayerCharacterCharacteristicInline(TranslatableTabularInline):
    model = NonPlayerCharacterCharacteristicI18n
    extra = 5
    
class NonPlayerCharacterModuleLevelInline(TranslatableTabularInline):
    model = NonPlayerCharacterModuleLevelI18n
    extra = 1
    
class NonPlayerCharacterWeaponInline(TranslatableTabularInline):
    model = NonPlayerCharacterWeaponI18n
    extra = 1
    
class NonPlayerCharacterAdmin(TranslatableAdmin):
    list_display = ('name', 'defence', 'reaction', 'initiative')
    inlines = [NonPlayerCharacterAspectInline, NonPlayerCharacterCharacteristicInline, NonPlayerCharacterModuleLevelInline, NonPlayerCharacterWeaponInline]
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class TraumaCategoryAdmin(TranslatableAdmin):
    list_display = ('name', 'description', 'hop_recovered')
    search_fields = ['translations__name', 'translations__description']
    
class TraumaAdmin(TranslatableAdmin):
    list_display = ('name', 'description', 'category')
    search_fields = ['translations__name', 'translations__description', 'category__translations__name']
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }

    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()


class MechaArmourActionCommonStacked(TranslatableStackedInline):
    model = MechaArmourActionCommonI18n
    extra = 2

class MechaArmourActionConfigurationStacked(TranslatableStackedInline):
    model = MechaArmourActionConfigurationI18n
    extra = 2
    
class MechaArmourConfigurationAdmin(TranslatableAdmin):
    list_display = ('name', 'mecha')
    inlines = [MechaArmourActionConfigurationStacked]

class MechaArmourAdmin(TranslatableAdmin):
    list_display = ('name',)
    inlines = [MechaArmourActionCommonStacked]
    # prepopulated_fields = {"slug": ("name",)}
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class TitanReachAdmin(TranslatableAdmin):
    list_display = ('name',)
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class FactionEffectInline(admin.TabularInline):
    model = FactionEffect
    extra = 1

class FactionAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ['translations__name',]
    inlines = [FactionEffectInline]

class FactionDefaultEdgeAdmin(admin.ModelAdmin):
    list_display = ('node_source', 'node_target', 'level')

class NonPlayerCharacterAbilityAdmin(TranslatableAdmin):
    list_display = ('name', 'precision', 'description')
    search_fields = ['translations__name', 'translations__description']
    # prepopulated_fields = {"slug": ("name",)}
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

class StyleAdmin(TranslatableAdmin):
    list_display = ('name', 'description', 'fighter_available', 'shooter_available', 'constraint')
    # prepopulated_fields = {"slug": ("name",)}
    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'style': 'max-width:1000px'})}, }
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {
            'slug': ('name',)
        }
    
    def get_available_languages(self, obj):
        # Forcing parler to recognize both languages
        return get_all_languages()

admin.site.register(ArchetypeI18n, ArchetypeAdmin)
admin.site.register(ArmourI18n, ArmourAdmin)
admin.site.register(ArmourAbilityI18n, ArmourAbilityAdmin)
admin.site.register(ArmourTwinAbilityI18n, ArmourTwinAbilityAdmin)
admin.site.register(Category, CategoryAdmin) # TODO !!
admin.site.register(CrestI18n, CrestAdmin)
admin.site.register(DivisionI18n, DivisionAdmin)
admin.site.register(EffectI18n, EffectAdmin)
admin.site.register(EnhancementI18n, EnhancementAdmin)
admin.site.register(Faction, FactionAdmin)
admin.site.register(FactionDefaultEdge, FactionDefaultEdgeAdmin)
admin.site.register(GreatDeedI18n, GreatDeedAdmin)
admin.site.register(HeroicAbilityI18n, HeroicAbilityAdmin)
admin.site.register(HeroicAbilityCategoryI18n, HeroicSkillCategoryAdmin)
admin.site.register(ArcanaI18n, ArcanaAdmin)
admin.site.register(MechaArmourI18n, MechaArmourAdmin)
admin.site.register(MechaArmourConfigurationI18n, MechaArmourConfigurationAdmin)
admin.site.register(ModuleI18n, ModuleAdmin)
admin.site.register(ModuleLevelI18n, ModuleLevelAdmin)
admin.site.register(NonPlayerCharacterI18n, NonPlayerCharacterAdmin)
admin.site.register(NonPlayerCharacterAbilityI18n, NonPlayerCharacterAbilityAdmin)
admin.site.register(OverdriveI18n, OverdriveAdmin)
admin.site.register(QuestionI18n, QuestionAdmin)
admin.site.register(StyleI18n, StyleAdmin)
admin.site.register(TitanReachI18n, TitanReachAdmin)
admin.site.register(TraumaI18n, TraumaAdmin)
admin.site.register(TraumaCategoryI18n, TraumaCategoryAdmin)
admin.site.register(VehicleI18n, VehicleAdmin)
admin.site.register(WeaponI18n, WeaponAdmin)
admin.site.register(WeaponAttackI18n, WeaponAttackAdmin)
admin.site.register(CharacteristicI18n, CharacteristicAdmin)


admin.site.register(SourceI18n, TranslatableAdmin)
admin.site.register(AspectI18n, TranslatableAdmin)
admin.site.register(ActivationI18n, TranslatableAdmin)
admin.site.register(ReachI18n, TranslatableAdmin)
admin.site.register(RarityI18n, TranslatableAdmin)
admin.site.register(UltimateAptitudeI18n, TranslatableAdmin)
admin.site.register(UltimateAptitudeTypeI18n, TranslatableAdmin)
# admin.site.register(WeaponAttack)
# admin.site.register(WeaponAttackEffect)
