from django_elasticsearch_dsl import fields
from django_elasticsearch_dsl import Document
from elasticsearch_dsl import analyzer, tokenizer
from django_elasticsearch_dsl.registries import registry
from elasticsearch.helpers import bulk

from django.db.models import Q

from gm.models.weaponry import ArmourAbilityVariant, Effect, Enhancement, HeroicSkill, Module, Overdrive, Weapon
from gm.models.character import GreatDeed, Archetype, Armour, ArmourAbility, ArmourEvolution, Crest, MajorArcana, Division, Trauma, MechaArmour, NonPlayerCharacterCapacity, Stance
from gm.models.faq import Question, Category
# New import line just for related_models, for readability
from gm.models.weaponry import WeaponAttack, WeaponAttackEffect, ModuleLevel, ModuleLevelEffect, EnhancementEffect, ArmourAbilityVariant, MechaArmourActionCommon, MechaArmourActionConfiguration
from gm.models.character import Characteristic, Aspect, ArmourAbility, ArmourEvolution, ArmourOverdrive, MechaArmourConfiguration

from gm.models.weaponry import (
    EffectI18n, EnhancementI18n, HeroicAbilityI18n, ModuleI18n,
    OverdriveI18n, WeaponI18n, VehicleI18n,
    # related_models...
    WeaponAttackI18n, WeaponAttackEffectI18n, ModuleLevelI18n,
    ModuleLevelEffectI18n, EnhancementEffectI18n,
    ArmourAbilityVariantI18n, MechaArmourActionCommonI18n,
    MechaArmourActionConfigurationI18n
)
from gm.models.character import (
    GreatDeedI18n, ArchetypeI18n, ArmourI18n, CrestI18n, ArcanaI18n,
    DivisionI18n, TraumaI18n, MechaArmourI18n,
    NonPlayerCharacterAbilityI18n, StyleI18n,
    # related_models...
    CharacteristicI18n, AspectI18n, ArmourAbilityI18n,
    ArmourEvolutionI18n, ArmourOverdriveI18n,
    MechaArmourConfigurationI18n
)
from gm.models.faq import (
    QuestionI18n, CategoryTreebeardI18n, CategoryTreebeardI18nName
)

ascii_folding = analyzer(
    'ascii_folding', 
    tokenizer=tokenizer('standard'), 
    filter=['lowercase', 'asciifolding', "word_delimiter"] 
)

class TranslatableDocument(Document):
    language_code = fields.KeywordField()
    
    def generate_actions(self, qs=None):
        """
        Generate actions for bulk indexing multilingual translations.
        """
        if qs is None:
            qs = self.get_queryset()

        actions = []
        for instance in qs:
            translations = instance.translations.all()
            for translation in translations:
                lang_code = translation.language_code
                doc = self.prepare_translation(instance, lang_code, translation)
                doc["_id"] = f"{instance.pk}-{lang_code}"  # Unique ID per language
                doc["_index"] = self._index._name
                doc["_op_type"] = "index"
                doc["language_code"] = lang_code
                actions.append(doc)
        return actions

    def prepare_translation(self, instance, lang_code, translation):
        """
        Prepare a single translation document for Elasticsearch.
        Override this method in subclasses to customize logic.
        """
        raise NotImplementedError("Subclasses must implement `prepare_translation`.")

    def update(self, qs=None, parallel=False, refresh=True):
        """
        Update Elasticsearch index with multilingual documents.
        """
        actions = self.generate_actions(qs=qs)
        client = self._get_connection()
        bulk(client, actions, refresh=refresh)


@registry.register_document
class WeaponDocument(Document):
    class Index:
        name = "weapons"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    nicknames = fields.TextField(attr="nicknames", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    attacks = fields.NestedField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
        "effects": fields.NestedField(properties={
            "name": fields.TextField(attr="get_name", analyzer=ascii_folding),
        }),
    })
    
    class Django:
        model = Weapon
        related_models = [WeaponAttack, WeaponAttackEffect, Effect]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, WeaponAttack):
            return related_instance.weapon
        elif isinstance(related_instance, WeaponAttackEffect):
            return related_instance.weapon_attack.weapon
        elif isinstance(related_instance, Effect):
            return Weapon.objects.filter(attacks__effects__effect__pk=related_instance.pk).all() # don't know if there is a better way...

@registry.register_document
class ModuleDocument(Document):
    class Index:
        name = "modules"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    levels = fields.NestedField(properties={
        "description": fields.TextField(attr="description_text_only", analyzer=ascii_folding),
        "effects": fields.NestedField(properties={
            "name": fields.TextField(attr="get_title", analyzer=ascii_folding),
        }),
    })
    
    class Django:
        model = Module
        related_models = [ModuleLevel, ModuleLevelEffect, Effect]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, ModuleLevel):
            return related_instance.module
        elif isinstance(related_instance, ModuleLevelEffect):
            return related_instance.module_level.module
        elif isinstance(related_instance, Effect):
            return Module.objects.filter(levels__effects__effect__pk=related_instance.pk).all() # don't know if there is a better way...

@registry.register_document
class EffectDocument(Document):
    class Index:
        name = "effects"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    short_description = fields.TextField(attr="short_description", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    
    class Django:
        model = Effect

@registry.register_document
class EnhancementDocument(Document):
    class Index:
        name = "enhancements"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
        
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    restriction = fields.TextField(attr="restriction", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    effects = fields.NestedField(properties={
        "name": fields.TextField(attr="get_name", analyzer=ascii_folding),
    })
    
    class Django:
        model = Enhancement
        related_models = [EnhancementEffect, Effect]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, EnhancementEffect):
            return related_instance.enhancement
        elif isinstance(related_instance, Effect):
            return Enhancement.objects.filter(effects__effect__pk=related_instance.pk).all() # don't know if there is a better way...

@registry.register_document
class OverdriveDocument(Document):
    class Index:
        name = "overdrives"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
        
    name = fields.TextField(attr="full_name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    
    characteristic = fields.ObjectField(properties={
        "overdrive_global_description": fields.TextField(attr="overdrive_global_description", analyzer=ascii_folding),
        "aspect": fields.ObjectField(properties={
            "name": fields.TextField(attr="name", analyzer=ascii_folding),
        }),
    })
    
    class Django:
        model = Overdrive
        related_models = [Characteristic, Aspect]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Characteristic):
            return related_instance.overdrives.all()
        elif isinstance(related_instance, Aspect):
            return Overdrive.objects.filter(characteristic__aspect__pk=related_instance.pk).all() # don't know if there is a better way...

@registry.register_document
class ArchetypeDocument(Document):
    class Index:
        name = "archetypes"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
        
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    characteristic_bonus_1 = fields.ObjectField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
    })
    characteristic_bonus_2 = fields.ObjectField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
    })
    characteristic_bonus_3 = fields.ObjectField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
    })
    
    class Django:
        model = Archetype
        related_models = [Characteristic]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Characteristic):
            return Archetype.objects.filter(Q(characteristic_bonus_1__pk=related_instance.pk) | Q(characteristic_bonus_2__pk=related_instance.pk) | Q(characteristic_bonus_3__pk=related_instance.pk)).all(),

@registry.register_document
class MajorArcanaDocument(Document):
    class Index:
        name = "arcanas"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    
    name = fields.TextField(attr='name', analyzer=ascii_folding)
    description = fields.TextField(attr='description', analyzer=ascii_folding)
    past = fields.TextField(attr='past', analyzer=ascii_folding)
    advantage_name = fields.TextField(attr='advantage_name', analyzer=ascii_folding)
    disadvantage_name = fields.TextField(attr='disadvantage_name', analyzer=ascii_folding)
    ai_description = fields.TextField(attr='ai_description', analyzer=ascii_folding)
    ai_advantage_name = fields.TextField(attr='ai_advantage_name', analyzer=ascii_folding)
    ai_disadvantage_name = fields.TextField(attr='ai_disadvantage_name', analyzer=ascii_folding)
    ai_behavior = fields.TextField(attr='ai_behavior', analyzer=ascii_folding)
    slug = fields.TextField(attr='slug', analyzer=ascii_folding)
    destiny_effect = fields.TextField(attr="destiny_effect_text_only", analyzer=ascii_folding)
    advantage = fields.TextField(attr="advantage_text_only", analyzer=ascii_folding)
    disadvantage = fields.TextField(attr="disadvantage_text_only", analyzer=ascii_folding)
    ai_advantage = fields.TextField(attr="ai_advantage_text_only", analyzer=ascii_folding)
    ai_disadvantage = fields.TextField(attr="ai_disadvantage_text_only", analyzer=ascii_folding)
    forced_aspect = fields.ObjectField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
    })
    
    class Django:
        model = MajorArcana
        related_models = [Aspect]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Aspect):
            return related_instance.majorarcana_set.all()

@registry.register_document
class GreatDeedDocument(Document):
    class Index:
        name = "great_deeds"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    restriction = fields.TextField(attr="restriction_text_only", analyzer=ascii_folding)
    aspect_1 = fields.ObjectField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
    })
    aspect_2 = fields.ObjectField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
    })
    
    class Django:
        model = GreatDeed
        related_models = [Aspect]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Characteristic):
            return GreatDeed.objects.filter(Q(aspect_1__pk=related_instance.pk) | Q(aspect_2__pk=related_instance.pk)).all(),

@registry.register_document
class CrestDocument(Document):
    class Index:
        name = "crests"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description", analyzer=ascii_folding)
    vow = fields.TextField(attr="vow", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    
    class Django:
        model = Crest

@registry.register_document
class ArmourDocument(Document):
    class Index:
        name = "armours"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    id = fields.IntegerField(attr="id") # used to filter allowed armours
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    background_description = fields.TextField(attr="background_description", analyzer=ascii_folding)
    technical_description = fields.TextField(attr="technical_description", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    additional_notes = fields.TextField(attr="additional_notes_text_only", analyzer=ascii_folding)
    overdrives = fields.NestedField(properties={
        "characteristic": fields.ObjectField(properties={
            "name": fields.TextField(attr="name", analyzer=ascii_folding),
        }),
    })
    evolutions = fields.NestedField(properties={
        "description": fields.TextField(attr="description_text_only", analyzer=ascii_folding),
    })
    abilities = fields.NestedField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
        "description": fields.TextField(attr="description_text_only", analyzer=ascii_folding),
        "variants" : fields.NestedField(properties={
            "name": fields.TextField(attr="name", analyzer=ascii_folding),
            "description": fields.TextField(attr="description_text_only", analyzer=ascii_folding),
        }),
    })
    
    class Django:
        model = Armour
        related_models = [ArmourAbility, ArmourAbilityVariant, ArmourEvolution, ArmourOverdrive, Characteristic]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, ArmourAbility):
            return related_instance.armour
        if isinstance(related_instance, ArmourAbilityVariant):
            return related_instance.armour_ability.armour
        if isinstance(related_instance, ArmourEvolution):
            return related_instance.armour
        if isinstance(related_instance, ArmourOverdrive):
            return related_instance.armour
        if isinstance(related_instance, Characteristic):
            return Armour.objects.filter(overdrives__characteristic__pk=related_instance.pk).all()

@registry.register_document
class DivisionDocument(Document):
    class Index:
        name = "divisions"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description", analyzer=ascii_folding)
    disadvantage_name = fields.TextField(attr="disadvantage_name", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    aspect_bonus = fields.ObjectField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
    })
    disadvantage = fields.TextField(attr="disadvantage_text_only", analyzer=ascii_folding)
    
    class Django:
        model = Division
        related_models = [Aspect]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Aspect):
            return related_instance.division_set.all()

@registry.register_document
class HeroicSkillDocument(Document):
    class Index:
        name = "heroic_skills"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    
    class Django:
        model = HeroicSkill

@registry.register_document
class TraumaDocument(Document):
    class Index:
        name = "traumas"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    id = fields.IntegerField(attr="id") # used to filter allowed traumas
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    
    class Django:
        model = Trauma

@registry.register_document
class MechaArmourDocument(Document):
    class Index:
        name = "mecha_armours"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    id = fields.IntegerField(attr="id") # used to filter allowed MechaArmours
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    actions = fields.NestedField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
        "description": fields.TextField(attr="description_text_only", analyzer=ascii_folding),
        "effects": fields.TextField(attr="effects_text_only", analyzer=ascii_folding),
    })
    configurations = fields.NestedField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
        "description": fields.TextField(attr="description_text_only", analyzer=ascii_folding),
        "actions" : fields.NestedField(properties={
            "name": fields.TextField(attr="name", analyzer=ascii_folding),
            "description": fields.TextField(attr="description_text_only", analyzer=ascii_folding),
            "effects": fields.TextField(attr="effects_text_only", analyzer=ascii_folding),
        }),
    })
    
    class Django:
        model = MechaArmour
        related_models = [MechaArmourConfiguration, MechaArmourActionCommon, MechaArmourActionConfiguration]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, MechaArmourConfiguration):
            return related_instance.mecha
        if isinstance(related_instance, MechaArmourActionCommon):
            return related_instance.mecha
        if isinstance(related_instance, MechaArmourActionConfiguration):
            return related_instance.configuration.mecha

@registry.register_document
class NonPlayerCharacterCapacityDocument(Document):
    class Index:
        name = "npc_capacities"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    id = fields.IntegerField(attr="id") # used to filter allowed capacities
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    
    class Django:
        model = NonPlayerCharacterCapacity

@registry.register_document
class StanceDocument(Document):
    class Index:
        name = "stances"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    bonus = fields.TextField(attr="bonus_text_only", analyzer=ascii_folding)
    malus = fields.TextField(attr="malus_text_only", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    
    class Django:
        model = Stance

@registry.register_document
class QuestionDocument(Document):
    class Index:
        name = "faq"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    id = fields.IntegerField(attr="id") # used to filter allowed capacities
    v1_5 = fields.BooleanField(attr="v1_5")
    v1 = fields.BooleanField(attr="v1")
    gm = fields.BooleanField(attr="gm")
    updated = fields.DateField(attr="updated")
    question = fields.TextField(attr="question_text_only", analyzer=ascii_folding)
    answer = fields.TextField(attr="answer_text_only", analyzer=ascii_folding)
    category = fields.NestedField(properties={
        "id": fields.IntegerField(),
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
        "get_family": fields.NestedField(properties={
            "id": fields.IntegerField(),
            "name": fields.TextField(attr="name", analyzer=ascii_folding),
        }),
    })
    
    class Django:
        model = Question
        # related_models = [Category,]

@registry.register_document
class WeaponI18nDocument(TranslatableDocument):
    
    name = fields.TextField(analyzer=ascii_folding)
    description = fields.TextField(analyzer=ascii_folding)
    nicknames = fields.TextField(analyzer=ascii_folding)
    slug = fields.TextField(analyzer=ascii_folding)
    attacks = fields.NestedField(properties={
        "name": fields.TextField(analyzer=ascii_folding),
        "effects": fields.NestedField(properties={
            "name": fields.TextField(analyzer=ascii_folding),
        }),
    })
    
    class Index:
        name = "weapons_i18n"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    class Django:
        model = WeaponI18n
    
    def prepare_translation(self, instance, lang_code, translation):
        return {
            "name": translation.name,
            "description": instance.description_text_only(lang_code),
            "nicknames": translation.nicknames,
            "slug": translation.slug,
            "attacks": [
                {
                    "name": attack.safe_translation_getter('name', language_code=lang_code, any_language=True),
                    "effects": [
                        {
                            "name": effect.get_name(lang_code),
                        }
                        for effect in attack.effects.all()
                    ],
                }
                for attack in instance.attacks.all()
            ]
        }

@registry.register_document
class ModuleI18nDocument(TranslatableDocument):
    name = fields.TextField(analyzer=ascii_folding)
    slug = fields.TextField(analyzer=ascii_folding)
    levels = fields.NestedField(properties={
        "description": fields.TextField(analyzer=ascii_folding),
        "effects": fields.NestedField(properties={
            "name": fields.TextField(analyzer=ascii_folding),
        }),
    })
    
    class Index:
        name = "modules_i18n"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    class Django:
        model = ModuleI18n
    
    def prepare_translation(self, instance, lang_code, translation):
        return {
            "name": translation.name,
            "slug": translation.slug,
            "levels": [
                {
                    "description": level.description_text_only(lang_code),
                    "effects": [
                        {
                            "name": effect.get_title(lang_code),
                        }
                        for effect in level.effects.all()
                    ],
                }
                for level in instance.levels.all()
            ],
        }

@registry.register_document
class EffectI18nDocument(TranslatableDocument):
    name = fields.TextField(analyzer=ascii_folding)
    description = fields.TextField(analyzer=ascii_folding)
    short_description = fields.TextField(analyzer=ascii_folding)
    slug = fields.TextField(analyzer=ascii_folding)
    
    class Index:
        name = "effects_i18n"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    class Django:
        model = EffectI18n
    
    def prepare_translation(self, instance, lang_code, translation):
        return  {
            "name": translation.name,
            "description": instance.description_text_only(lang_code),
            "short_description": translation.short_description,
            "slug": translation.slug,
        }

@registry.register_document
class EnhancementI18nDocument(TranslatableDocument):
    name = fields.TextField(analyzer=ascii_folding)
    description = fields.TextField(analyzer=ascii_folding)
    restriction = fields.TextField(analyzer=ascii_folding)
    slug = fields.TextField(analyzer=ascii_folding)
    effects = fields.NestedField(properties={
        "name": fields.TextField(analyzer=ascii_folding),
    })
    
    class Index:
        name = "enhancements_i18n"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    class Django:
        model = EnhancementI18n
    
    def prepare_translation(self, instance, lang_code, translation):
        return {
            "name": instance.name,
            "description": instance.description_text_only(lang_code),
            "restriction": translation.restriction,
            "slug": translation.slug,
            "effects": [
                {
                    "name": effect.get_name(lang_code)
                }
                for effect in instance.effects.all()
            ]
        }

@registry.register_document
class OverdriveI18nDocument(TranslatableDocument):
    name = fields.TextField(analyzer=ascii_folding)
    description = fields.TextField(analyzer=ascii_folding)
    slug = fields.TextField(analyzer=ascii_folding)
    
    characteristic = fields.ObjectField(properties={
        "overdrive_global_description": fields.TextField(analyzer=ascii_folding),
        "aspect": fields.ObjectField(properties={
            "name": fields.TextField(analyzer=ascii_folding),
        }),
    })
    
    class Index:
        name = "overdrives_i18n"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    class Django:
        model = OverdriveI18n
    
    def prepare_translation(self, instance, lang_code, translation):
        """
        Prepare the document for a specific language.
        """
        return {
            "name": instance.full_name(lang_code=lang_code),
            "description": instance.description_text_only(lang_code),
            "slug": translation.slug,
            "characteristic": {
                "overdrive_global_description": instance.characteristic.safe_translation_getter(
                    'overdrive_global_description', language_code=lang_code, any_language=True
                ),
                "aspect": {
                    "name": instance.characteristic.aspect.safe_translation_getter(
                        'name', language_code=lang_code, any_language=True
                    ),
                },
            }
        }

@registry.register_document
class ArchetypeI18nDocument(TranslatableDocument):
    name = fields.TextField(analyzer=ascii_folding)
    description = fields.TextField(analyzer=ascii_folding)
    slug = fields.TextField(analyzer=ascii_folding)
    
    characteristic_bonus_1 = fields.ObjectField(properties={
        "name": fields.TextField(analyzer=ascii_folding),
    })
    characteristic_bonus_2 = fields.ObjectField(properties={
        "name": fields.TextField(analyzer=ascii_folding),
    })
    characteristic_bonus_3 = fields.ObjectField(properties={
        "name": fields.TextField(analyzer=ascii_folding),
    })

    class Index:
        name = "archetypes_i18n"
        settings = {
            "number_of_shards": 1,
            "number_of_replicas": 0,
        }
    
    class Django:
        model = ArchetypeI18n
    
    def prepare_translation(self, instance, lang_code, translation):
        return {
            "name": translation.name,
            "description": translation.description,
            "slug": translation.slug,
            # "characteristic_bonus_1": instance.characteristic_bonus_1.safe_translation_getter('name', language_code=lang_code, any_language=True),
            # "characteristic_bonus_2": instance.characteristic_bonus_2.safe_translation_getter('name', language_code=lang_code, any_language=True),
            # "characteristic_bonus_3": instance.characteristic_bonus_3.safe_translation_getter('name', language_code=lang_code, any_language=True),
            "characteristic_bonus_1": self.prepare_characteristic(instance.characteristic_bonus_1, lang_code),
            "characteristic_bonus_2": self.prepare_characteristic(instance.characteristic_bonus_2, lang_code),
            "characteristic_bonus_3": self.prepare_characteristic(instance.characteristic_bonus_3, lang_code),
        }
    
    def prepare_characteristic(self, characteristic, lang_code):
        """
        Prepare characteristic with translated name.
        """
        if characteristic:
            return {
                "name": characteristic.safe_translation_getter('name', language_code=lang_code, any_language=True) or "",
            }
        return None

@registry.register_document
class ArcanaI18nDocument(TranslatableDocument):
    name = fields.TextField(analyzer=ascii_folding)
    description = fields.TextField(analyzer=ascii_folding)
    past = fields.TextField(analyzer=ascii_folding)
    advantage_name = fields.TextField(analyzer=ascii_folding)
    disadvantage_name = fields.TextField(analyzer=ascii_folding)
    ai_description = fields.TextField(analyzer=ascii_folding)
    ai_advantage_name = fields.TextField(analyzer=ascii_folding)
    ai_disadvantage_name = fields.TextField(analyzer=ascii_folding)
    oracle_fortune_name = fields.TextField(analyzer=ascii_folding)
    oracle_calamity_name = fields.TextField(analyzer=ascii_folding)
    ai_behavior = fields.TextField(analyzer=ascii_folding)
    slug = fields.TextField(analyzer=ascii_folding)
    destiny_effect = fields.TextField(analyzer=ascii_folding)
    advantage = fields.TextField(analyzer=ascii_folding)
    disadvantage = fields.TextField(analyzer=ascii_folding)
    ai_advantage = fields.TextField(analyzer=ascii_folding)
    ai_disadvantage = fields.TextField(analyzer=ascii_folding)
    oracle_future = fields.TextField(analyzer=ascii_folding)
    oracle_fortune_description = fields.TextField(analyzer=ascii_folding)
    oracle_calamity_description = fields.TextField(analyzer=ascii_folding)
    forced_aspect = fields.ObjectField(properties={
        "name": fields.TextField(analyzer=ascii_folding),
    })
    
    class Index:
        name = "arcanas_i18n"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    class Django:
        model = ArcanaI18n
    
    def prepare_translation(self, instance, lang_code, translation):
        return {
            "name": translation.name,
            "description": translation.description,
            "past": translation.past,
            "advantage_name": translation.advantage_name,
            "disadvantage_name": translation.disadvantage_name,
            "ai_description": translation.ai_description,
            "ai_advantage_name": translation.ai_advantage_name,
            "ai_disadvantage_name": translation.ai_disadvantage_name,
            "oracle_fortune_name": translation.oracle_fortune_name,
            "oracle_calamity_name": translation.oracle_calamity_name,
            "ai_behavior": translation.ai_behavior,
            "slug": translation.slug,
            "destiny_effect": instance.destiny_effect_text_only(lang_code),
            "advantage": instance.advantage_text_only(lang_code),
            "disadvantage": instance.disadvantage_text_only(lang_code),
            "ai_advantage": instance.ai_advantage_text_only(lang_code),
            "ai_disadvantage": instance.ai_disadvantage_text_only(lang_code),
            "oracle_future": instance.oracle_future_text_only(lang_code),
            "oracle_fortune_description": instance.oracle_fortune_description_text_only(lang_code),
            "oracle_calamity_description": instance.oracle_calamity_description_text_only(lang_code),
            "forced_aspect": self.prepare_aspect(instance.forced_aspect, lang_code),
        }
    
    def prepare_aspect(self, aspect, lang_code):
        """
        Prepare aspect with translated name.
        """
        if aspect:
            return {
                "name": aspect.safe_translation_getter('name', language_code=lang_code, any_language=True) or "",
            }
        return None

@registry.register_document
class GreatDeedI18nDocument(TranslatableDocument):
    name = fields.TextField(analyzer=ascii_folding)
    description = fields.TextField(analyzer=ascii_folding)
    slug = fields.TextField(analyzer=ascii_folding)
    restriction = fields.TextField(analyzer=ascii_folding)
    aspect_1 = fields.ObjectField(properties={
        "name": fields.TextField(analyzer=ascii_folding),
    })
    aspect_2 = fields.ObjectField(properties={
        "name": fields.TextField(analyzer=ascii_folding),
    })
    
    class Index:
        name = "great_deeds_i18n"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    class Django:
        model = GreatDeedI18n
    
    def prepare_translation(self, instance, lang_code, translation):
        return {
            "name": translation.name,
            "description": translation.description,
            "slug": translation.slug,
            "restriction": instance.restriction_text_only(lang_code),
            "aspect_1": self.prepare_aspect(instance.aspect_1, lang_code),
            "aspect_2": self.prepare_aspect(instance.aspect_2, lang_code),
        }
    
    def prepare_aspect(self, aspect, lang_code):
        """
        Prepare aspect with translated name.
        """
        if aspect:
            return {
                "name": aspect.safe_translation_getter('name', language_code=lang_code, any_language=True) or "",
            }
        return None

@registry.register_document
class CrestI18nDocument(TranslatableDocument):
    name = fields.TextField(analyzer=ascii_folding)
    description = fields.TextField(analyzer=ascii_folding)
    vow = fields.TextField(analyzer=ascii_folding)
    slug = fields.TextField(analyzer=ascii_folding)
    
    class Index:
        name = "crests_i18n"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    class Django:
        model = CrestI18n
    
    def prepare_translation(self, instance, lang_code, translation):
        return {
            "name": translation.name,
            "description": translation.description,
            "vow": translation.vow,
            "slug": translation.slug,
        }

@registry.register_document
class ArmourI18nDocument(TranslatableDocument):
    id = fields.IntegerField() # used to filter allowed armours
    name = fields.TextField(analyzer=ascii_folding)
    background_description = fields.TextField(analyzer=ascii_folding)
    technical_description = fields.TextField(analyzer=ascii_folding)
    slug = fields.TextField(analyzer=ascii_folding)
    additional_notes = fields.TextField(analyzer=ascii_folding)
    overdrives = fields.NestedField(properties={
        "characteristic": fields.ObjectField(properties={
            "name": fields.TextField(analyzer=ascii_folding),
        }),
    })
    evolutions = fields.NestedField(properties={
        "description": fields.TextField(analyzer=ascii_folding),
    })
    abilities = fields.NestedField(properties={
        "name": fields.TextField(analyzer=ascii_folding),
        "description": fields.TextField(analyzer=ascii_folding),
        "variants" : fields.NestedField(properties={
            "name": fields.TextField(analyzer=ascii_folding),
            "description": fields.TextField(analyzer=ascii_folding),
        }),
    })
    
    class Index:
        name = "armours_i18n"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    class Django:
        model = ArmourI18n
    
    def prepare_translation(self, instance, lang_code, translation):
        return {
            "id": instance.id,
            "name": translation.name,
            "background_description": translation.background_description,
            "technical_description": translation.technical_description,
            "slug": translation.slug,
            "additional_notes": instance.additional_notes_text_only(lang_code),
            "overdrives": [
                {
                    "characteristic": {
                        "name": overdrive.characteristic.safe_translation_getter('name', language_code=lang_code, any_language=True),
                    }
                }
                for overdrive in instance.overdrives.all()
            ],
            "evolutions": [
                {
                    "description": evolution.description_text_only(lang_code),
                }
                for evolution in instance.evolutions.all()
            ],
            "abilities": [
                {
                    "name": ability.safe_translation_getter('name', language_code=lang_code, any_language=True),
                    "description": ability.description_text_only(lang_code),
                    "variants": [
                        {
                            "name": variant.safe_translation_getter('name', language_code=lang_code, any_language=True),
                            "description": variant.description_text_only(lang_code),
                        }
                        for variant in ability.variants.all()
                    ]
                }
                for ability in instance.abilities.all()
            ],
        }

@registry.register_document
class DivisionI18nDocument(TranslatableDocument):
    name = fields.TextField(analyzer=ascii_folding)
    description = fields.TextField(analyzer=ascii_folding)
    disadvantage_name = fields.TextField(analyzer=ascii_folding)
    slug = fields.TextField(analyzer=ascii_folding)
    aspect_bonus = fields.ObjectField(properties={
        "name": fields.TextField(analyzer=ascii_folding),
    })
    disadvantage = fields.TextField(analyzer=ascii_folding)
    
    class Index:
        name = "divisions_i18n"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    class Django:
        model = DivisionI18n
    
    def prepare_translation(self, instance, lang_code, translation):
        return {
            "name": translation.name,
            "description": translation.description,
            "disadvantage_name": translation.disadvantage_name,
            "slug": translation.slug,
            "aspect_bonus": {
                "name": instance.aspect_bonus.safe_translation_getter('name', language_code=lang_code, any_language=True)
            },
            "disadvantage": instance.disadvantage_text_only(lang_code),
        }

@registry.register_document
class HeroicAbilityI18nDocument(TranslatableDocument):
    name = fields.TextField(analyzer=ascii_folding)
    description = fields.TextField(analyzer=ascii_folding)
    slug = fields.TextField(analyzer=ascii_folding)
    
    class Index:
        name = "heroic_abilities_i18n"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    class Django:
        model = HeroicAbilityI18n
    
    def prepare_translation(self, instance, lang_code, translation):
        return {
            "name": translation.name,
            "description": instance.description_text_only(lang_code),
            "slug": translation.slug,
        }

@registry.register_document
class TraumaI18nDocument(TranslatableDocument):
    id = fields.IntegerField() # used to filter allowed traumas
    name = fields.TextField(analyzer=ascii_folding)
    description = fields.TextField(analyzer=ascii_folding)
    slug = fields.TextField(analyzer=ascii_folding)
    
    class Index:
        name = "traumas_i18n"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    class Django:
        model = TraumaI18n
    
    def prepare_translation(self, instance, lang_code, translation):
        return {
            "id": instance.id,
            "name": translation.name,
            "description": instance.description_text_only(lang_code),
            "slug": translation.slug,
        }

@registry.register_document
class MechaArmourI18nDocument(TranslatableDocument):
    id = fields.IntegerField() # used to filter allowed MechaArmours
    name = fields.TextField(analyzer=ascii_folding)
    description = fields.TextField(analyzer=ascii_folding)
    slug = fields.TextField(analyzer=ascii_folding)
    actions = fields.NestedField(properties={
        "name": fields.TextField(analyzer=ascii_folding),
        "description": fields.TextField(analyzer=ascii_folding),
        "effects": fields.TextField(analyzer=ascii_folding),
    })
    configurations = fields.NestedField(properties={
        "name": fields.TextField(analyzer=ascii_folding),
        "description": fields.TextField(analyzer=ascii_folding),
        "actions" : fields.NestedField(properties={
            "name": fields.TextField(analyzer=ascii_folding),
            "description": fields.TextField(analyzer=ascii_folding),
            "effects": fields.TextField(analyzer=ascii_folding),
        }),
    })
    
    class Index:
        name = "mecha_armours_i18n"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    class Django:
        model = MechaArmourI18n
        
    def prepare_translation(self, instance, lang_code, translation):
        return {
            "id": instance.id,
            "name": translation.name,
            "description": instance.description_text_only(lang_code),
            "slug": translation.slug,
            "actions": [
                {
                    "name": action.safe_translation_getter('name', language_code=lang_code, any_language=True),
                    "description": action.description_text_only(lang_code),
                    "effects": action.effects_text_only(lang_code),
                }
                for action in instance.actions.all()
            ],
            "configurations": [
                {
                    "name": configuration.safe_translation_getter('name', language_code=lang_code, any_language=True),
                    "description": configuration.description_text_only(lang_code),
                    "actions": [
                        {
                            "name": action.safe_translation_getter('name', language_code=lang_code, any_language=True),
                            "description": action.description_text_only(lang_code),
                            "effects": action.effects_text_only(lang_code),
                        }
                        for action in configuration.actions.all()
                    ],
                }
                for configuration in instance.configurations.all()
            ],
        }

@registry.register_document
class NonPlayerCharacterAbilityI18nDocument(TranslatableDocument):
    id = fields.IntegerField() # used to filter allowed capacities
    name = fields.TextField(analyzer=ascii_folding)
    description = fields.TextField(analyzer=ascii_folding)
    slug = fields.TextField(analyzer=ascii_folding)
    
    class Index:
        name = "npc_abilities_i18n"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    class Django:
        model = NonPlayerCharacterAbilityI18n
    
    def prepare_translation(self, instance, lang_code, translation):
        return {
            "id": instance.id,
            "name": translation.name,
            "description": instance.description_text_only(lang_code),
            "slug": translation.slug,
        }

@registry.register_document
class StyleI18nDocument(TranslatableDocument):
    name = fields.TextField(analyzer=ascii_folding)
    description = fields.TextField(analyzer=ascii_folding)
    bonus = fields.TextField(analyzer=ascii_folding)
    malus = fields.TextField(analyzer=ascii_folding)
    slug = fields.TextField(analyzer=ascii_folding)
    
    class Index:
        name = "styles_i18n"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    class Django:
        model = StyleI18n
    
    def prepare_translation(self, instance, lang_code, translation):
        return {
            "name": translation.name,
            "description": instance.description_text_only(lang_code),
            "bonus": instance.bonus_text_only(lang_code),
            "malus": instance.malus_text_only(lang_code),
            "slug": translation.slug,
        }

@registry.register_document
class QuestionI18nDocument(TranslatableDocument):
    id = fields.IntegerField(attr="id") # used to filter allowed capacities
    v1_5 = fields.BooleanField(attr="v1_5")
    v1 = fields.BooleanField(attr="v1")
    gm = fields.BooleanField(attr="gm")
    updated = fields.DateField(attr="updated")
    question = fields.TextField(attr="question_text_only", analyzer=ascii_folding)
    answer = fields.TextField(attr="answer_text_only", analyzer=ascii_folding)
    category = fields.NestedField(properties={
        "id": fields.IntegerField(),
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
        "get_family": fields.NestedField(properties={
            "id": fields.IntegerField(),
            "name": fields.TextField(attr="name", analyzer=ascii_folding),
        }),
    })
    
    class Index:
        name = "faq_i18n"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    class Django:
        model = QuestionI18n
    
    def prepare_translation(self, instance, lang_code, translation):
        return {
            "id": instance.id,
            "v1_5": instance.v1_5,
            "v1": instance.v1,
            "gm": instance.gm,
            "updated": instance.updated,
            "question": instance.question_text_only(lang_code),
            "answer": instance.answer_text_only(lang_code),
            "category": [
                {
                    "id": category.id,
                    "name": category.translations.safe_translation_getter('name', language_code=lang_code, any_language=True),
                    "get_family": [
                        {
                            "id": parent_category.id,
                            "name": parent_category.translations.safe_translation_getter('name', language_code=lang_code, any_language=True),
                        }
                        for parent_category in category.get_ancestors()
                    ]
                }
                for category in instance.category.all()
            ],
        }
