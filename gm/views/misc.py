from django.utils.translation import gettext, pgettext
from random import sample, choice

from django.http import JsonResponse
from django.shortcuts import render
from django.views import generic
from django.urls import reverse
from django.utils.translation import get_language

from ..models.character import (
    GreatDeedI18n, ArchetypeI18n, ArmourI18n, CrestI18n, ArcanaI18n,
    DivisionI18n, TraumaI18n, MechaArmourI18n,
    NonPlayerCharacterAbilityI18n, StyleI18n
)
from ..models.weaponry import (
    EffectI18n, EnhancementI18n, HeroicAbilityI18n, ModuleI18n,
    OverdriveI18n, WeaponI18n, VehicleI18n
)

from ..utils import (
    get_allowed_armours, get_allowed_traumas, get_allowed_mechas,
    get_allowed_npc_abilities
)


class AboutView(generic.View):
    def get(self, request):
        return render(request, 'gm/misc/about.html')
        
class IndexView(generic.View):
    # template_name = 'gm/index.html'
    def get(self, request):
        return render(request, 'gm/misc/index.html')
    
class AutocompleteView(generic.View):
    def get(self, request):
        term = request.GET.get('term', None)
        results = []
        
        language = get_language()
        
        for res in WeaponI18n.objects.filter(translations__name__icontains=term, translations__language_code=language).order_by('translations__name'):
            results.append({
                'value': reverse('gm:weapon_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (pgettext('titre', 'Arme'), res.safe_translation_getter('name', any_language=True).upper())
            })
        
        for res in ModuleI18n.objects.filter(translations__name__icontains=term, translations__language_code=language).order_by('translations__name'):
            results.append({
                'value': reverse('gm:module_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (pgettext('titre', 'Module'), res.safe_translation_getter('name', any_language=True).upper())
            })
        
        for res in OverdriveI18n.objects.filter(characteristic__translations__name__icontains=term, translations__language_code=language).order_by('characteristic__translations__name', 'level'):
            results.append({
                'value': reverse('gm:overdrive_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Overdrive'), str(res).upper())
            })
        
        for res in EnhancementI18n.objects.filter(translations__name__icontains=term, translations__language_code=language).order_by('translations__name'):
            results.append({
                'value': reverse('gm:enhancement_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Amélioration'), res.safe_translation_getter('name', any_language=True).upper())
            })
        
        for res in EffectI18n.objects.filter(translations__name__icontains=term, translations__language_code=language).order_by('translations__name'):
            results.append({
                'value': reverse('gm:effect_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (pgettext('titre', 'Effet'), res.safe_translation_getter('name', any_language=True).upper())
            })
        
        for res in HeroicAbilityI18n.objects.filter(translations__name__icontains=term, translations__language_code=language).order_by('translations__name'):
            results.append({
                'value': reverse('gm:skill_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Capacité héroïque'), res.safe_translation_getter('name', any_language=True).upper())
            })
        
        for res in ArchetypeI18n.objects.filter(translations__name__icontains=term, translations__language_code=language).order_by('translations__name'):
            # import pdb; pdb.set_trace()
            results.append({
                'value': reverse('gm:archetype_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Archétype'), res.safe_translation_getter('name', any_language=True).upper())
            })
        
        for res in ArcanaI18n.objects.filter(translations__name__icontains=term, translations__language_code=language).order_by('translations__name'):
            results.append({
                'value': reverse('gm:arcana_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Arcane majeure'), res.safe_translation_getter('name', any_language=True).upper())
            })
        
        for res in GreatDeedI18n.objects.filter(translations__name__icontains=term, translations__language_code=language).order_by('translations__name'):
            results.append({
                'value': reverse('gm:great_deed_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Haut-fait'), res.safe_translation_getter('name', any_language=True).upper())
            })
        
        for res in CrestI18n.objects.filter(translations__name__icontains=term, translations__language_code=language).order_by('translations__name'):
            results.append({
                'value': reverse('gm:crest_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Blason'), res.safe_translation_getter('name', any_language=True).upper())
            })
        
        allowed_armours = get_allowed_armours(request.user)
        armours_pk_allowed = list(allowed_armours.values_list('pk', flat=True).distinct())
        
        for res in ArmourI18n.objects.filter(translations__name__icontains=term, translations__language_code=language, pk__in=armours_pk_allowed).order_by('translations__name'):
            results.append({
                'value': reverse('gm:armour_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Armure'), res.safe_translation_getter('name', any_language=True).upper())
            })
        
        for res in DivisionI18n.objects.filter(translations__name__icontains=term, translations__language_code=language).order_by('translations__name'):
            results.append({
                'value': reverse('gm:division_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Section'), res.safe_translation_getter('name', any_language=True).upper())
            })
        
        for res in VehicleI18n.objects.filter(translations__name__icontains=term, translations__language_code=language).order_by('translations__name'):
            results.append({
                'value': reverse('gm:vehicle_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (pgettext('titre', 'Véhicule'), res.safe_translation_getter('name', any_language=True).upper())
            })
        
        for res in StyleI18n.objects.filter(translations__name__icontains=term, translations__language_code=language).order_by('translations__name'):
            results.append({
                'value': reverse('gm:stance_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Style de combat'), res.safe_translation_getter('name', any_language=True).upper())
            })
        
        if request.user.is_authenticated:
            allowed_traumas = get_allowed_traumas(request.user)
            traumas_pk_allowed = list(allowed_traumas.values_list('pk', flat=True).distinct())
            
            for res in TraumaI18n.objects.filter(translations__name__icontains=term, translations__language_code=language, pk__in=traumas_pk_allowed).order_by('translations__name'):
                results.append({
                    'value': reverse('gm:trauma_detail', kwargs={'slug': res.slug}),
                    'label': "[%s] %s" % (gettext('Trauma'), res.safe_translation_getter('name', any_language=True).upper())
                })
            
            allowed_mechas = get_allowed_mechas(request.user)
            mechas_pk_allowed = list(allowed_mechas.values_list('pk', flat=True).distinct())
            
            for res in MechaArmourI18n.objects.filter(translations__name__icontains=term, translations__language_code=language, pk__in=mechas_pk_allowed).order_by('translations__name'):
                results.append({
                    'value': reverse('gm:mecha_detail', kwargs={'slug': res.slug}),
                    'label': "[%s] %s" % (gettext('Mecha'), res.safe_translation_getter('name', any_language=True).upper())
                })
            
            if request.user.game_master:
                allowed_npc_capacities = get_allowed_npc_abilities(request.user)
                npc_abilities_pk_allowed = list(allowed_npc_capacities.values_list('pk', flat=True).distinct())
                
                for res in NonPlayerCharacterAbilityI18n.objects.filter(translations__name__icontains=term, translations__language_code=language, pk__in=npc_abilities_pk_allowed).order_by('translations__name'):
                    results.append({
                        'value': reverse('gm:npc_capacity_detail', kwargs={'slug': res.slug}),
                        'label': "[%s] %s" % (gettext('Capacité PNJ'), res.safe_translation_getter('name', any_language=True).upper())
                    })
        
        # results = sorted(results, key=lambda obj: obj['label'])
        
        return JsonResponse(results, safe=False)
    
def generateAI(request, codename=None):
    def get_random():
        keys = ArcanaI18n.objects.values_list('id', flat=True)
        return sample(list(keys), 5)
    
    template_name = 'gm/misc/generator_ai.html'
    keys = []
    ai_letters = []
    ai_fullcode = ''
    
    if codename is not None:
        codenames = codename.upper().split('.')
        
        if len(codenames) < 5:
            keys = get_random()
        else:
            arcanas = list(ArcanaI18n.objects.filter(ai_letter__in=codenames))
            if (len(arcanas) < 5):
                keys = get_random()
            else:
                arcanas.sort(key=lambda arcana: codenames.index(arcana.ai_letter))
                
                for a in arcanas:
                    keys.append(a.id)
    else:
        keys = get_random()
    
    final_arcanas = list(ArcanaI18n.objects.filter(pk__in=keys))
    final_arcanas.sort(key=lambda arcana: keys.index(arcana.pk))
    
    for arcana in final_arcanas:
        ai_letters.append(arcana.ai_letter)
        arcana.ai_keyword = choice(arcana.ai_keywords.split(','))
    
    ai_fullcode = '.'.join(ai_letters)
    
    return render(request, template_name, {
        'ai_list': final_arcanas,
        'ai_fullcode': ai_fullcode
    })
    
class SetTheme(generic.View):
    
    def post(self, request, *args, **kwargs):
        data = {
            'updated': False,
        }
        
        try:
            theme = request.POST.get('theme', '')
            request.session['theme_name'] = theme
            # import pdb; pdb.set_trace()
            if request.user.is_authenticated:
                request.user.theme_name = theme
                request.user.save()
                
            data['updated'] = True
        except Exception as e:
            data = {
                'error': str(e)
            }
        finally:
            return JsonResponse(data)
    