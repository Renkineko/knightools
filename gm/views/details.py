from django.http import Http404
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import generic
from django.db.models import Q, Min, Prefetch
from django.contrib.auth.mixins import LoginRequiredMixin
from parler.views import TranslatableSlugMixin
from django.utils.translation import get_language
from parler.utils.context import switch_language
from django.utils.translation import gettext

from users.models import (
    FavoriteMechaArmour, Feature
)

from ..models.character import (
    ArmourI18n, ArmourTwinAbilityI18n, DivisionI18n,
    ArcanaI18n, GreatDeedI18n, ArmourAbilityI18n, ArmourEvolutionI18n,
    ArmourOverdriveI18n, DivisionOverdriveI18n, CrestI18n, ArchetypeI18n,
    MechaArmourConfigurationI18n, NonPlayerCharacterAbilityI18n,
    NonPlayerCharacterTypeI18n, StyleI18n, TraumaI18n, MechaArmourI18n,
)
from ..models.weaponry import (
    WeaponI18n, EffectI18n, ModuleI18n, OverdriveI18n,
    WeaponAttackEffectI18n, ModuleLevelEffectI18n,
    ArmourAbilityVariantI18n,
    EnhancementI18n, HeroicAbilityI18n, EnhancementEffectI18n,
    ModuleLevelI18n, ModuleSlotI18n, DivisionModuleI18n,
    DivisionWeaponI18n, WeaponAttackI18n, VehicleI18n,
    ModuleVehicleI18n, MechaArmourActionCommonI18n, UltimateAptitudeI18n
)
from users.models import (
    FavoriteWeaponI18n, FavoriteEffectI18n,
    FavoriteEnhancementI18n, FavoriteArmourI18n, FavoriteModuleI18n
)

from ..utils import get_bound_questions, get_allowed_armours

class WeaponDetailView(TranslatableSlugMixin, generic.DetailView):
    model = WeaponI18n
    template_name = 'gm/details/weapon.html'
    context_object_name = 'weapon'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:weapon_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        weapon = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not weapon:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return weapon
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            weapon = context['weapon']
            language = get_language()
            
            weapon.questions_list = get_bound_questions(weapon, self.request.user)
            
            weapon.attacks_list = (
                WeaponAttackI18n
                .objects
                .filter(
                    weapon=weapon.id,
                    translations__language_code=language
                )
                .prefetch_related(
                    Prefetch(
                        'effects',
                        queryset=WeaponAttackEffectI18n.objects.filter(
                            Q(
                                effect__translations__language_code=language
                            )
                            | Q(
                                effect=None
                            )
                        ).select_related('effect'),
                        to_attr='localized_effects'
                    )
                )
                .select_related('reach')
                .order_by('position')
                .all()
            )
            weapon.enhancements_list = (
                EnhancementI18n
                .objects
                .filter(
                    weapons=weapon.id,
                    translations__language_code=language
                )
                .prefetch_related('effects', 'effects__effect')
                .all()
            )
            weapon.count_attacks = len(weapon.attacks_list)
            weapon.count_enhancements = len(weapon.enhancements_list)
            weapon.is_favorite = (
                self.request.user.is_authenticated
                and len(
                    FavoriteWeaponI18n.objects.filter(
                        user=self.request.user,
                        weapon=weapon
                    )
                )
            )
            
            return context
        except Exception as e:
            return super().get_context_data(**kwargs)

class EffectDetailView(TranslatableSlugMixin, generic.DetailView):
    model = EffectI18n
    template_name = 'gm/details/effect.html'
    context_object_name = 'effect'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:effect_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        effect = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not effect:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return effect
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            effect = context['effect']
            language = get_language()
            
            effect.questions_list = get_bound_questions(effect, self.request.user)
           
            effect.weapons_list = (
                WeaponI18n
                .objects
                .filter(
                    pk__in=effect.weaponattackeffects.values_list('weapon_attack__weapon__pk', flat=True),
                    translations__language_code=language
                )
                .order_by('rarity', 'translations__name')
                .all()
            )
            effect.modules_list = (
                ModuleI18n
                .objects
                .filter(
                    pk__in=effect.moduleleveleffects.values_list('module_level__module__pk', flat=True),
                    translations__language_code=language
                )
                .annotate(
                    min_rarity=Min(
                        'levels__rarity__id',
                        filter=Q(levels__pk__in=effect.moduleleveleffects.values_list('module_level__pk', flat=True))
                    ),
                )
                .select_related('category')
                .order_by('min_rarity', 'translations__name')
                .all()
            )
            effect.enhancements_list = (
                EnhancementI18n
                .objects
                .filter(
                    pk__in=effect.enhancements.values_list('enhancement__pk', flat=True),
                    translations__language_code=language
                )
                .all()
            )
            effect.count_weapons = len(effect.weapons_list)
            effect.count_modules = len(effect.modules_list)
            effect.count_enhancements = len(effect.enhancements_list)
            effect.is_favorite = (
                self.request.user.is_authenticated
                and len(
                    FavoriteEffectI18n.objects.filter(
                        user=self.request.user,
                        effect=effect
                    )
                )
            )
            
            return context
        except Exception:
            return super().get_context_data(**kwargs)

class ModuleDetailView(TranslatableSlugMixin, generic.DetailView):
    model = ModuleI18n
    template_name = 'gm/details/module.html'
    context_object_name = 'module'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:module_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        module = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not module:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return module
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            module = context['module']
            language = get_language()
            
            module.questions_list = get_bound_questions(module, self.request.user)
            module.slots_list = (
                ModuleSlotI18n
                .objects
                .filter(
                    module=module.id
                )
                .all()
            )
            module.levels_list = (
                ModuleLevelI18n
                .objects
                .filter(
                    module=module.id,
                    translations__language_code=language
                )
                .select_related('reach', 'activation', 'rarity')
                .prefetch_related(
                    Prefetch(
                        'effects',
                        queryset=ModuleLevelEffectI18n.objects.filter(
                            Q(
                                translations__language_code=language,
                                effect__translations__language_code=language,
                                module_level__translations__language_code=language, # I don't understand why I must put these filters, but well...
                                module_level__module__translations__language_code=language, # I don't understand why I must put these filters, but well...
                            )
                            | Q(
                                translations__language_code=language,
                                effect=None,
                                module_level__translations__language_code=language, # I don't understand why I must put these filters, but well...
                                module_level__module__translations__language_code=language, # I don't understand why I must put these filters, but well...
                            )
                        ).select_related('effect'),
                        to_attr='localized_effects'
                    ),
                    'npcs', 'npcs__characteristics', 'npcs__characteristics__characteristic'
                )
                .all()
            )
            module.vehicles_list = (
                ModuleVehicleI18n
                .objects
                .filter(
                    module=module.id,
                    vehicle__translations__language_code=language
                )
                .select_related('vehicle')
                .all()
            )
            module.enhancements_list = (
                EnhancementI18n
                .objects
                .filter(
                    modules=module.id,
                    translations__language_code=language
                )
                .prefetch_related('effects', 'effects__effect')
                .all()
            )
            module.count_slots = len(module.slots_list)
            module.count_enhancements = len(module.enhancements_list)
            module.is_favorite = (
                self.request.user.is_authenticated
                and len(
                    FavoriteModuleI18n.objects.filter(
                        user=self.request.user,
                        module=module
                    )
                )
            )
            
            return context
        except Exception as e:
            print(e)
            return super().get_context_data(**kwargs)

class OverdriveDetailView(TranslatableSlugMixin, generic.DetailView):
    model = OverdriveI18n
    template_name = 'gm/details/overdrive.html'
    context_object_name = 'overdrive'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:overdrive_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        overdrive = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not overdrive:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return overdrive
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            overdrive = context['overdrive']
            
            overdrive.questions_list = get_bound_questions(overdrive, self.request.user)
            
            return context
        except Exception as e:
            print(e)
            return super().get_context_data(**kwargs)

class EnhancementDetailView(TranslatableSlugMixin, generic.DetailView):
    model = EnhancementI18n
    template_name = 'gm/details/enhancement.html'
    context_object_name = 'enhancement'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:enhancement_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        enhancement = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not enhancement:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return enhancement
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            enhancement = context['enhancement']
            language = get_language()
            
            enhancement.questions_list = get_bound_questions(enhancement, self.request.user)
            
            enhancement.effects_list = (
                EnhancementEffectI18n
                .objects
                .filter(
                    Q(
                        enhancement=enhancement.id,
                        effect__translations__language_code=language
                    ) |
                    Q(
                        enhancement=enhancement.id,
                        effect=None
                    )
                )
                .select_related('effect')
                .all()
            )
            enhancement.count_effects = len(enhancement.effects_list)
            enhancement.is_favorite = (
                self.request.user.is_authenticated
                and len(
                    FavoriteEnhancementI18n.objects.filter(
                        user=self.request.user,
                        enhancement=enhancement
                    )
                )
            )
            
            return context
        except Exception as e:
            print(e)
            return super().get_context_data(**kwargs)

class ArmourClassifiedView(generic.View):
    def get(self, request):
        return render(request, 'gm/details/armour_classified.html')

class ArmourDetailView(TranslatableSlugMixin, generic.DetailView):
    model = ArmourI18n
    template_name = 'gm/details/armour.html'
    context_object_name = 'armour'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:armour_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        armour = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not armour:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return armour
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            armour = context['armour']
            language = get_language()
            
            armour.questions_list = get_bound_questions(armour, self.request.user)
            
            armour.is_favorite = (
                self.request.user.is_authenticated
                and len(
                    FavoriteArmourI18n.objects.filter(
                        user=self.request.user,
                        armour=armour
                    )
                )
            )
            
            armour.overdrives_list = (
                ArmourOverdriveI18n
                .objects
                .filter(
                    armour=armour.id,
                    characteristic__translations__language_code=language
                )
                .prefetch_related('characteristic')
                .all()
            )
            armour.evolutions_list = (
                ArmourEvolutionI18n
                .objects
                .filter(
                    armour=armour.id,
                    translations__language_code=language
                )
                .all()
            )
            armour.abilities_list = (
                ArmourAbilityI18n
                .objects
                .filter(
                    armour=armour.id,
                    translations__language_code=language
                )
                .prefetch_related('variants', 'variants__reach', 'variants__npc', 'variants__npc__aspects', 'variants__npc__aspects__aspect')
                .all()
            )
            armour.twin_abilities_list = (
                ArmourTwinAbilityI18n
                .objects
                .exclude(
                    excluded_armours=armour
                )
                .filter(
                    translations__language_code=language,
                    ability__translations__language_code=language,
                    ability__armour__translations__language_code=language,
                )
                .select_related('ability','ability__armour')
                .prefetch_related('ability__variants')
                .order_by('ability__armour__translations__name')
                .all()
            )
            
            armour.is_allowed = True
            if armour.password is not None:
                if not self.request.user.is_authenticated:
                    armour.is_allowed = False
                elif not self.request.user.game_master and armour not in self.request.user.unlocked_armours.all():
                    armour.is_allowed = False
            
            try:
                context['twin_ability_feature'] = Feature.objects.get(slug="twin-ability")
            except:
                pass # Feature not set on this server, no password to show
            
            try:
                context['ultimate'] = UltimateAptitudeI18n.objects.get(armour=context['armour'])
            except:
                pass # no ultimate aptitude to show
            
            return context
        except Exception as e:
            print(e)
            return super().get_context_data(**kwargs)

class DivisionDetailView(TranslatableSlugMixin, generic.DetailView):
    model = DivisionI18n
    template_name = 'gm/details/division.html'
    context_object_name = 'division'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:division_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        division = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not division:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return division
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            division = context['division']
            language = get_language()
            
            division.questions_list = get_bound_questions(division, self.request.user)
            
            division.overdrives_list = (
                DivisionOverdriveI18n
                .objects
                .filter(
                    division=division.id,
                    characteristic__translations__language_code=language
                )
                .prefetch_related('characteristic')
                .all()
            )
            division.modules_list = (
                DivisionModuleI18n
                .objects
                .filter(
                    division=division.id,
                    module_level__module__translations__language_code=language
                )
                .select_related('module_level', 'module_level__module', 'forced_choice_effect')
                .all()
            )
            division.weapons_list = (
                DivisionWeaponI18n
                .objects
                .filter(
                    division=division.id,
                    weapon__translations__language_code=language
                )
                .select_related('weapon', 'weapon__category')
                .all()
            )
            
            return context
        except Exception as e:
            print(e)
            return super().get_context_data(**kwargs)

class ArcanaDetailView(TranslatableSlugMixin, generic.DetailView):
    model = ArcanaI18n
    template_name = 'gm/details/arcana.html'
    context_object_name = 'arcana'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:arcana_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        arcana = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not arcana:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return arcana
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            arcana = context['arcana']
            
            arcana.questions_list = get_bound_questions(arcana, self.request.user)
            
            return context
        except Exception as e:
            print(e)
            return super().get_context_data(**kwargs)

class GreatDeedDetailView(TranslatableSlugMixin, generic.DetailView):
    model = GreatDeedI18n
    template_name = 'gm/details/great_deed.html'
    context_object_name = 'great_deed'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:great_deed_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        great_deed = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not great_deed:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return great_deed
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            great_deed = context['great_deed']
            
            great_deed.questions_list = get_bound_questions(great_deed, self.request.user)
            
            return context
        except Exception as e:
            print(e)
            return super().get_context_data(**kwargs)

class CrestDetailView(TranslatableSlugMixin, generic.DetailView):
    model = CrestI18n
    template_name = 'gm/details/crest.html'
    context_object_name = 'crest'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:crest_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        crest = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not crest:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return crest
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            crest = context['crest']
            
            crest.questions_list = get_bound_questions(crest, self.request.user)
            context['ultimate'] = UltimateAptitudeI18n.objects.get(crest=context['crest'])
            
            return context
        except Exception as e:
            print(e)
            return super().get_context_data(**kwargs)

class ArchetypeDetailView(TranslatableSlugMixin, generic.DetailView):
    model = ArchetypeI18n
    template_name = 'gm/details/archetype.html'
    context_object_name = 'archetype'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:archetype_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        archetype = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not archetype:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return archetype
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            archetype = context['archetype']
            
            archetype.questions_list = get_bound_questions(archetype, self.request.user)
            
            return context
        except Exception:
            return super().get_context_data(**kwargs)

class HeroicAbilityDetailView(TranslatableSlugMixin, generic.DetailView):
    model = HeroicAbilityI18n
    template_name = 'gm/details/heroic_ability.html'
    context_object_name = 'heroic_ability'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:heroic_ability_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        heroic_ability = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not heroic_ability:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return heroic_ability
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            heroic_ability = context['heroic_ability']
            
            heroic_ability.questions_list = get_bound_questions(heroic_ability, self.request.user)
            
            return context
        except Exception:
            return super().get_context_data(**kwargs)

class VehicleDetailView(TranslatableSlugMixin, generic.DetailView):
    model = VehicleI18n
    template_name = 'gm/details/vehicle.html'
    context_object_name = 'vehicle'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:vehicle_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        vehicle = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not vehicle:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return vehicle
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            vehicle = context['vehicle']
            
            vehicle.questions_list = get_bound_questions(vehicle, self.request.user)
            
            return context
        except Exception:
            return super().get_context_data(**kwargs)

class TraumaDetailView(LoginRequiredMixin, TranslatableSlugMixin, generic.DetailView):
    model = TraumaI18n
    template_name = 'gm/details/trauma.html'
    context_object_name = 'trauma'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:trauma_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        trauma = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not trauma:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return trauma
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            trauma = context['trauma']
            
            trauma.questions_list = get_bound_questions(trauma, self.request.user)
            
            trauma.is_allowed = True
            if trauma.password is not None:
                if not self.request.user.is_authenticated:
                    trauma.is_allowed = False
                elif not self.request.user.game_master and trauma not in self.request.user.unlocked_traumas.all():
                    trauma.is_allowed = False
            
            return context
        except Exception:
            return super().get_context_data(**kwargs)

class MechaArmourClassifiedView(generic.View):
    def get(self, request):
        return render(request, 'gm/details/mecha_classified.html')

class MechaArmourDetailView(LoginRequiredMixin, TranslatableSlugMixin, generic.DetailView):
    model = MechaArmourI18n
    template_name = 'gm/details/mecha.html'
    context_object_name = 'mecha'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:mecha_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        mecha = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not mecha:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return mecha
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            mecha = context['mecha']
            
            mecha.questions_list = get_bound_questions(mecha, self.request.user)
            
            mecha.is_allowed = True
            if mecha.password is not None:
                if not self.request.user.is_authenticated:
                    mecha.is_allowed = False
                elif not self.request.user.game_master and mecha not in self.request.user.unlocked_mechas.all():
                    mecha.is_allowed = False
            
            mecha.configurations_list = MechaArmourConfigurationI18n.objects.filter(mecha=mecha.id).prefetch_related('actions', 'actions__reach').all()
            mecha.actions_list = MechaArmourActionCommonI18n.objects.filter(mecha=mecha.id).prefetch_related('reach').all()
            
            mecha.is_favorite = (
                self.request.user.is_authenticated
                and len(
                    FavoriteMechaArmour
                    .objects
                    .filter(
                        user=self.request.user,
                        mecha=mecha
                    )
                )
            )
            return context
        except Exception:
            return super().get_context_data(**kwargs)

class NonPlayerCharacterAbilityDetailView(LoginRequiredMixin, TranslatableSlugMixin, generic.DetailView):
    model = NonPlayerCharacterAbilityI18n
    template_name = 'gm/details/npc_ability.html'
    context_object_name = 'npc_ability'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:npc_ability_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        npc_ability = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not npc_ability:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return npc_ability
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            language = get_language()
            npc_ability = context['npc_ability']
            
            npc_ability.questions_list = get_bound_questions(npc_ability, self.request.user)
            
            context['npc_types'] = (
                NonPlayerCharacterTypeI18n
                .objects
                .filter(
                    translations__language_code=language
                )
                .all()
            )
            
            return context
        except Exception:
            return super().get_context_data(**kwargs)

class StyleDetailView(TranslatableSlugMixin, generic.DetailView):
    model = StyleI18n
    template_name = 'gm/details/style.html'
    context_object_name = 'style'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:style_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        style = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not style:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return style
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            style = context['style']
            
            style.questions_list = get_bound_questions(style, self.request.user)
            
            return context
        except Exception:
            return super().get_context_data(**kwargs)

class UltimateAptitudeDetailView(LoginRequiredMixin, TranslatableSlugMixin, generic.DetailView):
    model = UltimateAptitudeI18n
    template_name = 'gm/details/ultimate.html'
    context_object_name = 'ultimate'
    
    def get(self, request, *args, **kwargs):
        if not request.user.is_ultimate_aptitude_visible():
            return redirect('gm:index')
        
        self.object = self.get_object()
        slug = self.kwargs.get('slug')
        current_language = get_language()
        
        # use the current language to verify the slug
        with switch_language(self.object, current_language):
            translated_slug = self.object.safe_translation_getter('slug', default=None)
            if translated_slug and translated_slug != slug:
                return redirect('gm:ultimate_detail', slug=translated_slug)
        
        return super().get(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        language = get_language()
        
        ultimate = self.model.objects.active_translations(language).filter(translations__slug=slug).first()
        
        if not ultimate:
            raise Http404(gettext("Aucun objet trouvé pour ce slug dans aucune langue"))
        
        return ultimate
    
    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            ultimate = context['ultimate']
            
            ultimate.questions_list = get_bound_questions(ultimate, self.request.user)
            context['allowed_armours'] = get_allowed_armours(self.request.user)
            
            return context
        except Exception:
            return super().get_context_data(**kwargs)

# RETROCOMP REDIRECT VIEWS

class HeroicSkillDetailRedirectView(generic.RedirectView):
    permanent = True  # Use a permanent redirect (HTTP 301)
    
    def get_redirect_url(self, *args, **kwargs):
        return reverse('gm:heroic_ability_detail', kwargs=kwargs)

class StanceDetailRedirectView(generic.RedirectView):
    permanent = True  # Use a permanent redirect (HTTP 301)
    
    def get_redirect_url(self, *args, **kwargs):
        return reverse('gm:style_detail', kwargs=kwargs)

class NonPlayerCharacterCapacityDetailRedirectView(generic.RedirectView):
    permanent = True  # Use a permanent redirect (HTTP 301)
    
    def get_redirect_url(self, *args, **kwargs):
        return reverse('gm:npc_ability_detail', kwargs=kwargs)

