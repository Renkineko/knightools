from django.shortcuts import render
from django.views import generic
from django.utils.translation import get_language
from django.db.models import Prefetch

from ..models.weaponry import (
    Weapon, WeaponI18n, ReachI18n, WeaponAttackI18n,
    WeaponAttackEffectI18n
)

class LongbowCalculatorView(generic.View):
    def get(self, request):
        return render(request, 'gm/calculators/longbow.html', {
            "longbow": Weapon.objects.get(slug='fusil-longbow'),
        })

class WeaponCalculatorView(generic.View):
    def get(self, request):
        language = get_language()
        weapons = (
            WeaponI18n
            .objects
            .filter(
                translations__language_code=language,
                calculator=True
            )
            .prefetch_related(
                Prefetch(
                    'attacks',
                    queryset=WeaponAttackI18n.objects.filter(
                        calculator_behaviour='INI',
                        translations__language_code=language,
                        weapon__translations__language_code=language
                    ).order_by('calculator_order_appliance'),
                    to_attr='initial_attacks'
                ),
                Prefetch(
                    'attacks',
                    queryset=WeaponAttackI18n.objects.filter(
                        calculator_behaviour='LST',
                        translations__language_code=language,
                        weapon__translations__language_code=language
                    ).order_by('calculator_order_appliance'),
                    to_attr='listing_attacks'
                ),
                Prefetch(
                    'attacks',
                    queryset=WeaponAttackI18n.objects.filter(
                        calculator_behaviour='REP',
                        translations__language_code=language,
                        weapon__translations__language_code=language
                    ).order_by('calculator_order_appliance'),
                    to_attr='replacement_attacks'
                ),
                Prefetch(
                    'attacks',
                    queryset=WeaponAttackI18n.objects.filter(
                        calculator_behaviour='REM',
                        translations__language_code=language,
                        weapon__translations__language_code=language
                    ).order_by('calculator_order_appliance'),
                    to_attr='removal_attacks'
                ),
                Prefetch(
                    'attacks',
                    queryset=WeaponAttackI18n.objects.filter(
                        calculator_behaviour='RED',
                        translations__language_code=language,
                        weapon__translations__language_code=language
                    ).order_by('calculator_order_appliance'),
                    to_attr='reduction_attacks'
                ),
                Prefetch(
                    'attacks',
                    queryset=WeaponAttackI18n.objects.exclude(
                        calculator_behaviour='INI'
                    ).filter(
                        translations__language_code=language,
                        weapon__translations__language_code=language
                    ).order_by('calculator_order_appliance'),
                    to_attr='non_initial_attacks'
                ),
            )
            .all()
        )
        reaches = ReachI18n.objects.filter(translations__language_code=language).all()
        
        return render(request, 'gm/calculators/weapon.html', {
            "weapons": weapons,
            "reaches": reaches
        })
    