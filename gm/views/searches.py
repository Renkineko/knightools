from functools import reduce
from operator import attrgetter
from django.utils.translation import gettext, pgettext
import unidecode, re, logging

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils.translation import get_language
from django.db.models import Count, Q
from elasticsearch.exceptions import ConnectionTimeout, ConnectionError

from ..models.character import (
    GreatDeedI18n, ArchetypeI18n, ArmourI18n, ArmourAbilityI18n,
    ArmourEvolutionI18n, CrestI18n, ArcanaI18n, DivisionI18n, TraumaI18n,
    MechaArmourI18n, MechaArmourConfigurationI18n,
    NonPlayerCharacterAbilityI18n, StyleI18n
)
from ..models.weaponry import (
    ArmourAbilityVariantI18n, EffectI18n, EnhancementI18n,
    HeroicAbilityI18n, ModuleI18n, ModuleLevelI18n, OverdriveI18n,
    WeaponI18n, WeaponAttackI18n, MechaArmourI18n,
    MechaArmourActionCommonI18n, MechaArmourActionConfigurationI18n)

from ..models.faq import QuestionI18n

from ..documents import (
    WeaponI18nDocument, ModuleI18nDocument, EffectI18nDocument,
    EnhancementI18nDocument, OverdriveI18nDocument,
    ArchetypeI18nDocument, ArcanaI18nDocument, GreatDeedI18nDocument,
    CrestI18nDocument, ArmourI18nDocument, DivisionI18nDocument,
    HeroicAbilityI18nDocument, TraumaI18nDocument,
    MechaArmourI18nDocument, StyleI18nDocument,
    NonPlayerCharacterAbilityI18nDocument, QuestionI18nDocument
)
# from ..templatetags import gm_extras

from ..utils import (
    get_allowed_armours, get_allowed_mechas, get_allowed_traumas
)

def search(request):
    """
    This search method uses the powerness of elasticsearch to gather results and send each type
    of result (archetype results, weapon results, and so on) in the template.
    
    If a query goes wrong in the process, the fallback calls search_fallback, where elasticsearch
    is not longer used, and basic searches are done.
    """
    template_name = 'gm/searches/search.html'
    
    query = request.GET.get('s', None)
    
    current_language = get_language()
    
    pre_tags = ['<span class="search-highlight">']
    post_tags = ['</span>']
    
    def build_query(field_list):
        """
        Build a list of query for elasticsearch to pass on the dis_max queries.
        """
        
        queries = []
        
        for field in field_list:
            if "path" in field:
                queries.append({
                    "nested": {
                        "path": field['path'],
                        "query": {
                            "match": {
                                field['name']: {
                                    "query": query,
                                    "boost": field['boost']
                                }
                            }
                        }
                    }
                })
            else:
                queries.append({
                    "match": {
                        field['name']: {
                            "query": query,
                            "boost": field['boost']
                        }
                    }
                })
        
        return queries
    
    def make_query(document, field_list, filter_ids=None):
        """
        Create an elasticsearch query and execute it depending on the field_list and document passed in parameters.
        
        If the query without fuzziness return 0 hit, a fuzziness with "AUTO" value is added
        
        Returns the document.search().query(xxx).hits and the titles dict.
        """
        
        queries = build_query(field_list)
        titles = {
            'es_fuzzy': False,
        }
        
        doc_search = (document
            .search()
            .filter("term", language_code=current_language)  # Prefilter by language_code
            .query(
                "dis_max",
                queries=queries
            )
            .highlight_options(
                order="_score",
                pre_tags=pre_tags,
                post_tags=post_tags,
            )
            [0:50] # search on 50 items, already a lot of items IMHO...
        )
        
        if (filter_ids):
            doc_search = doc_search.filter("ids", values=filter_ids)
        
        for field in field_list:
            doc_search = doc_search.highlight(field['name'], fragment_size=0)
            titles[field['name']] = field['title']
        
        # import pdb; pdb.set_trace()
        
        doc_search = doc_search.execute()
        
        if len(doc_search) == 0:
            titles['es_fuzzy'] = True
            
            for query in queries:
                if 'nested' in query:
                    for q in query['nested']['query']['match']:
                        query['nested']['query']['match'][q]['fuzziness'] = 'AUTO'
                else:
                    for q in query['match']:
                        query['match'][q]['fuzziness'] = 'AUTO'
            
            doc_search = (document
                .search()
                .filter("term", language_code=current_language)  # Prefilter by language_code
                .query(
                    "dis_max",
                    queries=queries
                )
                .highlight_options(
                    order="_score",
                    pre_tags=pre_tags,
                    post_tags=post_tags,
                )
            )
            
            if filter_ids:
                doc_search = doc_search.filter("ids", values=filter_ids)
            
            for field in field_list:
                doc_search = doc_search.highlight(field['name'], fragment_size=0)
            
            doc_search = doc_search.execute()
            
        return doc_search, titles
    
    
    try:
        if query is None or query == '':
            raise ValueError('No empty search is allowed', 'empty_search')
        
        
        weapons_result_val, weapons_titles = make_query(WeaponI18nDocument, [
            { "name": "name", "boost": 5, "title": gettext("Nom"), },
            { "name": "description", "boost": 3, "title": gettext("Description"), },
            { "name": "nicknames", "boost": 2, "title": gettext("Surnoms"), },
            { "name": "attacks.name", "path": "attacks", "boost": 1.5, "title": pgettext("titre", "Attaque"), },
            { "name": "attacks.effects.name", "path": "attacks.effects", "boost": 1, "title": gettext("Effet(s)"), },
        ])
        
        modules_result_val, modules_titles = make_query(ModuleI18nDocument, [
            { "name": "name", "boost": 5, "title": gettext("Nom"), },
            { "name": "levels.description", "path": "levels", "boost": 3, "title": gettext("Description"), },
            { "name": "levels.effects.name", "path": "levels.effects", "boost": 1, "title": gettext("Effet(s)"), },
        ])
        
        effects_result_val, effects_titles = make_query(EffectI18nDocument, [
            { "name": "name", "boost": 5, "title": gettext("Nom"), },
            { "name": "description", "boost": 3, "title": gettext("Description"), },
            { "name": "short_description", "boost": 1, "title": gettext("Mémo"), },
        ])
        
        enhancements_result_val, enhancements_titles = make_query(EnhancementI18nDocument, [
            { "name": "name", "boost": 5, "title": gettext("Nom"), },
            { "name": "description", "boost": 3, "title": gettext("Description"), },
            { "name": "restriction", "boost": 2, "title": gettext("Restriction"), },
            { "name": "effects.name", "path": "effects", "boost": 1, "title": gettext("Effet(s)"), },
        ])
        
        overdrives_result_val, overdrives_titles = make_query(OverdriveI18nDocument, [
            { "name": "name", "boost": 5, "title": gettext("Nom"), },
            { "name": "characteristic.aspect.name", "boost": 3, "title": gettext("Aspect"), },
            { "name": "description", "boost": 2, "title": gettext("Description"), },
            { "name": "characteristic.overdrive_global_description", "boost": 1, "title": gettext("Info tech."), },
        ])
        
        archetypes_result_val, archetypes_titles = make_query(ArchetypeI18nDocument, [
            { "name": "name", "boost": 5, "title": gettext("Nom"), },
            { "name": "description", "boost": 3, "title": gettext("Description"), },
            { "name": "characteristic_bonus_1.name", "boost": 2, "title": gettext("Caractéristiques bonus"), },
            { "name": "characteristic_bonus_2.name", "boost": 2, "title": gettext("Caractéristiques bonus"), },
            { "name": "characteristic_bonus_3.name", "boost": 2, "title": gettext("Caractéristiques bonus"), },
        ])
        
        arcanas_result_val, arcanas_titles = make_query(ArcanaI18nDocument, [
            { "name": "name", "boost": 15, "title": gettext("Nom"), },
            { "name": "description", "boost": 14, "title": gettext("Description"), },
            { "name": "advantage_name", "boost": 13, "title": gettext("Avantage (nom)"), },
            { "name": "disadvantage_name", "boost": 13, "title": gettext("Inconvénient (nom)"), },
            { "name": "forced_aspect.name", "boost": 12, "title": gettext("Aspect lié"), },
            { "name": "advantage", "boost": 11, "title": gettext("Avantage"), },
            { "name": "disadvantage", "boost": 11, "title": gettext("Inconvénient"), },
            { "name": "destiny_effect", "boost": 10, "title": gettext("Effet de destinée"), },
            { "name": "past", "boost": 9, "title": gettext("Passé"), },
            { "name": "ai_description", "boost": 8, "title": gettext("Description IA"), },
            { "name": "ai_advantage_name", "boost": 7, "title": gettext("Avantage IA (nom)"), },
            { "name": "ai_disadvantage_name", "boost": 7, "title": gettext("Inconvénient IA (nom)"), },
            { "name": "ai_advantage", "boost": 6, "title": gettext("Avantage IA"), },
            { "name": "ai_disadvantage", "boost": 6, "title": gettext("Inconvénient IA"), },
            { "name": "ai_behavior", "boost": 4, "title": gettext("Comportement IA"), },
            { "name": "oracle_future", "boost": 9, "title": gettext("Avenir"), },
            { "name": "oracle_fortune_name", "boost": 7, "title": gettext("Fortune (nom)"), },
            { "name": "oracle_calamity_name", "boost": 7, "title": gettext("Calamité (nom)"), },
            { "name": "oracle_fortune_description", "boost": 6, "title": gettext("Fortune"), },
            { "name": "oracle_calamity_description", "boost": 6, "title": gettext("Calamité"), },
        ])
        
        great_deeds_result_val, great_deeds_titles = make_query(GreatDeedI18nDocument, [
            { "name": "name", "boost": 5, "title": gettext("Nom"), },
            { "name": "description", "boost": 3, "title": gettext("Description"), },
            { "name": "aspect_1.name", "boost": 2, "title": gettext("Aspect bonus"), },
            { "name": "aspect_2.name", "boost": 2, "title": gettext("Aspect bonus"), },
            { "name": "restriction", "boost": 1, "title": gettext("Restriction"), },
        ])
        
        crests_result_val, crests_titles = make_query(CrestI18nDocument, [
            { "name": "name", "boost": 5, "title": gettext("Nom"), },
            { "name": "description", "boost": 3, "title": gettext("Description"), },
            { "name": "vow", "boost": 1, "title": gettext("Vœu"), },
        ])
        
        allowed_armours = get_allowed_armours(request.user)
        armours_pk_allowed = list(allowed_armours.values_list('pk', flat=True).distinct())
        
        armours_result_val, armours_titles = make_query(ArmourI18nDocument, [
            { "name": "name", "boost": 9, "title": gettext("Nom"), },
            { "name": "abilities.name", "path": "abilities", "boost": 8, "title": gettext("Nom capacité"), },
            { "name": "abilities.variants.name", "path": "abilities.variants", "boost": 8, "title": gettext("Nom capacité"), },
            { "name": "overdrives.characteristic.name", "path": "overdrives", "boost": 7, "title": gettext("Overdrives"), },
            # { "name": "description", "boost": 6, "title": gettext("Description"), },
            { "name": "background_description", "boost": 5, "title": gettext("Historique"), },
            { "name": "technical_description", "boost": 5, "title": gettext("Description technique"), },
            { "name": "evolutions.description", "path": "evolutions", "boost": 4, "title": gettext("Description sur evolution"), },
            { "name": "abilities.description", "path": "abilities", "boost": 3, "title": gettext("Description capacité"), },
            { "name": "abilities.variants.description", "path": "abilities.variants", "boost": 2, "title": gettext("Description capacité"), },
            { "name": "additional_notes", "boost": 1, "title": gettext("Notes"), },
        ], armours_pk_allowed)
        
        divisions_result_val, divisions_titles = make_query(DivisionI18nDocument, [
            { "name": "name", "boost": 5, "title": gettext("Nom"), },
            { "name": "disadvantage_name", "boost": 4, "title": gettext("Inconvénient (Nom)"), },
            { "name": "aspect_bonus.name", "boost": 3, "title": gettext("Aspect bonus"), },
            { "name": "description", "boost": 2, "title": gettext("Description"), },
            { "name": "disadvantage", "boost": 1, "title": gettext("Inconvénient"), },
        ])
        
        skills_result_val, skills_titles = make_query(HeroicAbilityI18nDocument, [
            { "name": "name", "boost": 5, "title": gettext("Nom"), },
            { "name": "description", "boost": 3, "title": gettext("Description"), },
        ])
        
        stances_result_val, stances_titles = make_query(StyleI18nDocument, [
            { "name": "name", "boost": 5, "title": gettext("Nom"), },
            { "name": "bonus", "boost": 3, "title": gettext("Bonus"), },
            { "name": "malus", "boost": 3, "title": gettext("Malus"), },
            { "name": "description", "boost": 2, "title": gettext("Description"), },
        ])
        
        allowed_traumas = get_allowed_traumas(request.user)
        traumas_pk_allowed = list(allowed_traumas.values_list('pk', flat=True).distinct())
        
        traumas_result_val, traumas_titles = make_query(TraumaI18nDocument, [
            { "name": "name", "boost": 5, "title": gettext("Nom"), },
            { "name": "description", "boost": 3, "title": gettext("Description"), },
        ], traumas_pk_allowed)
        
        
        allowed_questions = QuestionI18n.objects.filter(gm=False)
        if request.user.is_authenticated:
            if request.user.game_master:
                allowed_questions = QuestionI18n.objects.all()
            
        questions_pk_allowed = list(allowed_questions.values_list('pk', flat=True).distinct())
        
        questions_result_val_tmp, questions_titles = make_query(QuestionI18nDocument, [
            { "name": "question", "boost": 5, "title": gettext("Question"), },
            { "name": "answer", "boost": 5, "title": gettext("Réponse"), },
        ], questions_pk_allowed)
        
        questions_result_val = []
        
        # import pdb; pdb.set_trace()
        
        for question in questions_result_val_tmp:
            final_question = {
                'gm': question.gm,
                'updated': question.updated,
                'v1_5': question.v1_5,
                'v1': question.v1,
                'category': question.category,
            }
            if 'question' in question.meta.highlight:
                final_question['question'] = ''.join(question.meta.highlight['question'])
            else:
                final_question['question'] = question.question
            
            if 'answer' in question.meta.highlight:
                final_question['answer'] = ' ... '.join(question.meta.highlight['answer'])
            else:
                final_question['answer'] = question.answer
            
            questions_result_val.append(final_question)
        
        allowed_mechas = get_allowed_mechas(request.user)
        mechas_pk_allowed = list(allowed_mechas.values_list('pk', flat=True).distinct())
        
        mechas_result_val, mechas_titles = make_query(MechaArmourI18nDocument, [
            { "name": "name", "boost": 9, "title": gettext("Nom"), },
            { "name": "configurations.name", "path": "configurations", "boost": 8, "title": gettext("Configuration"), },
            { "name": "actions.name", "path": "actions", "boost": 7, "title": gettext("Module/arme"), },
            { "name": "configurations.actions.name", "path": "configurations.actions", "boost": 7, "title": gettext("Capacité"), },
            { "name": "description", "boost": 6, "title": gettext("Description"), },
            { "name": "configurations.description", "path": "configurations", "boost": 8, "title": gettext("Description configuration"), },
            { "name": "actions.description", "path": "actions", "boost": 7, "title": gettext("Description module/arme"), },
            { "name": "configurations.actions.description", "path": "configurations.actions", "boost": 7, "title": gettext("Description capacité"), },
            { "name": "actions.effects", "path": "actions", "boost": 7, "title": gettext("Effets module/arme"), },
            { "name": "configurations.actions.effects", "path": "configurations.actions", "boost": 7, "title": gettext("Effets capacité"), },
        ], mechas_pk_allowed)
        
        if request.user.is_authenticated and request.user.game_master:
            npc_capacities_result_val, npc_capacities_titles = make_query(NonPlayerCharacterAbilityI18nDocument, [
                { "name": "name", "boost": 5, "title": gettext("Nom"), },
                { "name": "description", "boost": 3, "title": gettext("Description"), },
            ])
        else:
            npc_capacities_result_val = {}
            npc_capacities_titles = {}
        
        # import pdb; pdb.set_trace()
        
        return render(request, template_name, {
            'searchterm': query,
            'weapons_result': weapons_result_val,
            'weapons_length': len(weapons_result_val),
            'weapons_titles': weapons_titles,
            'modules_result': modules_result_val,
            'modules_length': len(modules_result_val),
            'modules_titles': modules_titles,
            'effects_result': effects_result_val,
            'effects_length': len(effects_result_val),
            'effects_titles': effects_titles,
            'enhancements_result': enhancements_result_val,
            'enhancements_length': len(enhancements_result_val),
            'enhancements_titles': enhancements_titles,
            'overdrives_result': overdrives_result_val,
            'overdrives_length': len(overdrives_result_val),
            'overdrives_titles': overdrives_titles,
            'archetypes_result': archetypes_result_val,
            'archetypes_length': len(archetypes_result_val),
            'archetypes_titles': archetypes_titles,
            'arcanas_result': arcanas_result_val,
            'arcanas_length': len(arcanas_result_val),
            'arcanas_titles': arcanas_titles,
            'great_deeds_result': great_deeds_result_val,
            'great_deeds_length': len(great_deeds_result_val),
            'great_deeds_titles': great_deeds_titles,
            'crests_result': crests_result_val,
            'crests_length': len(crests_result_val),
            'crests_titles': crests_titles,
            'armours_result': armours_result_val,
            'armours_length': len(armours_result_val),
            'armours_titles': armours_titles,
            'divisions_result': divisions_result_val,
            'divisions_length': len(divisions_result_val),
            'divisions_titles': divisions_titles,
            'skills_result': skills_result_val,
            'skills_length': len(skills_result_val),
            'skills_titles': skills_titles,
            'stances_result': stances_result_val,
            'stances_length': len(stances_result_val),
            'stances_titles': stances_titles,
            'traumas_result': traumas_result_val,
            'traumas_length': len(traumas_result_val),
            'traumas_titles': traumas_titles,
            'mechas_result': mechas_result_val,
            'mechas_length': len(mechas_result_val),
            'mechas_titles': mechas_titles,
            'npc_capacities_result': npc_capacities_result_val,
            'npc_capacities_length': len(npc_capacities_result_val),
            'npc_capacities_titles': npc_capacities_titles,
            'questions_result': questions_result_val,
            'questions_length': len(questions_result_val),
            'questions_titles': questions_titles,
            'result_template': 'gm/searches/search_result.html',
            'result_template_faq': 'gm/searches/search_result_faq.html',
            'request': request,
        })
    except ValueError as e:
        template_name = 'gm/searches/empty_search.html'
        # print(e)
        return render(request, template_name)
    except ConnectionTimeout as e:
        # Fallback because of the timeout of elasticsearch
        return search_fallback(request)
    except ConnectionError as e:
        # Fallback because of the timeout of elasticsearch
        return search_fallback(request)
    except Exception as e :
        raise e




def search_fallback(request):
    class SearchResult:
        def __init__(self, slug='', name=''):
            self.slug = slug
            self.name = name
            self.counter = 0
            self.results = []
    
    class SearchResultElement:
        def __init__(self, count_matches=0, desc='', context=None):
            self.count_matches = count_matches
            self.context = context
            self.desc = desc
    
    def rgetattr(obj, attr, *args):
        """Return the attribute, even if the attribute is on a nested object.
        Based on https://stackoverflow.com/questions/31174295/getattr-and-setattr-on-nested-objects
        """
        def _getattr(obj, attr):
            return getattr(obj, attr, *args)
        attribute = reduce(_getattr, [obj] + attr.split('.'))
        
        if callable(attribute):
            return attribute()
        
        return attribute
    
    def parse_search(
            wrapper, results, search_target, description,
            object_path='', suffix_attr='',
            counter_property=False, with_context=False,
            name_attr='name', slug_attr='get_slug'):
        """
        Parse the results of a queryset to dispatch them accordingly to what's searched
        
        wrapper: (Dict) the dictionary to update. The key is the slug of the object. If a key already exists, it's updated
                 with updated informations
        results: (Queryset) a queryset of the current search to parse
        search_target: (String) the property or method of the object we're counting for this search.
        description: (String) the name we want to give to the results of this search, a.k.a "x corresponding match in <description>"
        object_path: (String, optionnal) if the property is not directly under the main object, the path to get to the property.
        suffix_attr: (String, optionnal) in the case of a multiple match possible (like effects of a weapon attack),
                     the property to separate it, like "x corresponding match in <description> <suffix attr>"
        counter_property: (Boolean, optionnal) if the queryset is annotated with a count property, set this
                          to True and the name of the annotate property must be "count"
        with_context: (Boolean, optionnal) if set to True, add the content of the search_target in the context
                      property, so it can be displayed with highlight.
        name_attr: (String, optionnal) default is to take the name of the object path, but if
                   the object has a __str__ method, you can set it to empty to get directly the object
        """
        
        if object_path != '':
            slug_attr = '%s.%s' % (object_path, slug_attr)
            if name_attr != '':
                name_attr = '%s.%s' % (object_path, name_attr)
            else:
                name_attr = object_path
        
        query_decoded = unidecode.unidecode(query)
        regex = re.compile(query_decoded, re.IGNORECASE)
        
        for result in results:
            slug = rgetattr(result, slug_attr)
            desc = description
            if slug in wrapper:
                res = wrapper[slug]
            elif name_attr != '':
                res = SearchResult(name=rgetattr(result, name_attr), slug=slug)
            else:
                res = SearchResult(name=str(result), slug=slug)
            
            if suffix_attr != '':
                desc = '%s «%s»' % (description, rgetattr(result, suffix_attr))
            
            if counter_property:
                cnt = result.counter
            else:
                cnt = len(regex.findall(unidecode.unidecode(rgetattr(result, search_target))))
            
            context = None
            if with_context:
                context = rgetattr(result, search_target)
            
            res_element = SearchResultElement(
                count_matches=cnt,
                desc=desc,
                context=context
            )
            
            res.results.append(res_element)
            res.counter += cnt
            wrapper[slug] = res
        
        return wrapper
    
    template_name = 'gm/searches/search.html'
    weapons_result = {}
    modules_result = {}
    effects_result = {}
    enhancements_result = {}
    overdrives_result = {}
    archetypes_result = {}
    arcanas_result = {}
    great_deeds_result = {}
    crests_result = {}
    armours_result = {}
    divisions_result = {}
    skills_result = {}
    stances_result = {}
    traumas_result = {}
    mechas_result = {}
    npc_capacities_result = {}
    questions_result = {}
    
    query = request.GET.get('s', None)
    
    language = get_language()
    
    try:
        if query is None or query == '':
            raise ValueError('No empty search is allowed', 'empty_search')
        
        weapons_result = parse_search(
            weapons_result,
            WeaponI18n.objects.filter(translations__language_code=language, translations__name__icontains=query),
            'name',
            gettext('Nom')
        )
        
        weapons_result = parse_search(
            weapons_result,
            WeaponI18n.objects.filter(translations__language_code=language, translations__description__icontains=query),
            'description_text_only',
            gettext('Description'),
            # with_context=True
        )
        
        weapons_result = parse_search(
            weapons_result,
            WeaponI18n.objects.filter(translations__language_code=language, translations__nicknames__icontains=query),
            'nicknames',
            gettext('Surnoms'),
            # with_context=True
        )
        
        weapons_result = parse_search(
            weapons_result,
            WeaponAttackI18n.objects.filter(translations__language_code=language, translations__name__icontains=query),
            'name',
            pgettext('titre', 'Attaque'),
            object_path='weapon',
            suffix_attr='name'
        )
        
        weapons_result = parse_search(
            weapons_result,
            WeaponAttackI18n.objects.filter(effects__effect__translations__language_code=language, effects__effect__translations__name__icontains=query).annotate(counter=Count('effects__effect')),
            'effect',
            gettext("Effets de l'attaque"),
            object_path='weapon',
            suffix_attr='name',
            counter_property=True
        )
        
        weapons_result_val = weapons_result.values()
        weapons_result_val = sorted(weapons_result_val, key=attrgetter('name'))  # first sort is less important than second one
        weapons_result_val = sorted(weapons_result_val, key=attrgetter('counter'), reverse=True)
        
        
        # 'modules': ModuleLevel.objects.filter(Q(module__name__icontains=query) | Q(description__icontains=query) | Q(effects__effect_condition__icontains=query) | Q(effects__effect__name__icontains=query)).distinct().order_by('module__name', 'level').select_related('module').all(),
        modules_result = parse_search(
            modules_result,
            ModuleI18n.objects.filter(translations__language_code=language, translations__name__icontains=query),
            'name',
            gettext("Nom")
        )
        
        modules_result = parse_search(
            modules_result,
            ModuleLevelI18n.objects.filter(translations__language_code=language, translations__description__icontains=query).select_related('module'),
            'description_text_only',
            gettext('Description niveau'),
            # with_context=True,
            object_path='module',
            suffix_attr='level'
        )
        
        modules_result = parse_search(
            modules_result,
            ModuleLevelI18n.objects.filter(effects__effect__translations__language_code=language, effects__effect__translations__name__icontains=query).annotate(counter=Count('effects__effect')),
            'effect',
            gettext("Effets du niveau"),
            object_path='module',
            suffix_attr='level',
            counter_property=True
        )
        
        modules_result_val = modules_result.values()
        modules_result_val = sorted(modules_result_val, key=attrgetter('name'))  # first sort is less important than second one
        modules_result_val = sorted(modules_result_val, key=attrgetter('counter'), reverse=True)
        
        
        # 'effects': Effect.objects.filter(Q(name__icontains=query) | Q(description__icontains=query) | Q(short_description__icontains=query)).distinct().order_by('name').all(),
        effects_result = parse_search(
            effects_result,
            EffectI18n.objects.filter(translations__language_code=language, translations__name__icontains=query),
            'name',
            gettext("Nom")
        )
        
        effects_result = parse_search(
            effects_result,
            EffectI18n.objects.filter(translations__language_code=language, translations__description__icontains=query),
            'description_text_only',
            gettext('Description'),
            # with_context=True
        )
        
        effects_result = parse_search(
            effects_result,
            EffectI18n.objects.filter(translations__language_code=language, translations__short_description__icontains=query),
            'short_description',
            gettext('Mémo'),
            # with_context=True
        )
        
        effects_result_val = effects_result.values()
        effects_result_val = sorted(effects_result_val, key=attrgetter('name'))  # first sort is less important than second one
        effects_result_val = sorted(effects_result_val, key=attrgetter('counter'), reverse=True)
        
        
        # 'enhancements': Enhancement.objects.filter(Q(name__icontains=query) | Q(effects__effect_condition__icontains=query) | Q(effects__effect__name__icontains=query)).select_related('category').distinct().order_by('name').all(),
        enhancements_result = parse_search(
            enhancements_result,
            EnhancementI18n.objects.filter(translations__language_code=language, translations__name__icontains=query).select_related('category'),
            'name',
            gettext('Nom'),
            name_attr=''
        )
        
        enhancements_result = parse_search(
            enhancements_result,
            EnhancementI18n.objects.filter(translations__language_code=language, translations__description__icontains=query).select_related('category'),
            'description_text_only',
            gettext('Description'),
            # with_context=True,
            name_attr=''
        )
        
        enhancements_result = parse_search(
            enhancements_result,
            EnhancementI18n.objects.filter(translations__language_code=language, translations__restriction__icontains=query).select_related('category'),
            'restriction',
            gettext('Restriction'),
            # with_context=True,
            name_attr=''
        )
        
        enhancements_result = parse_search(
            enhancements_result,
            EnhancementI18n.objects.filter(effects__effect__translations__language_code=language, effects__effect__translations__name__icontains=query).annotate(counter=Count('effects__effect')).select_related('category'),
            'effect',
            gettext("Effet(s)"),
            counter_property=True,
            name_attr=''
        )
        
        enhancements_result_val = enhancements_result.values()
        enhancements_result_val = sorted(enhancements_result_val, key=attrgetter('name'))  # first sort is less important than second one
        enhancements_result_val = sorted(enhancements_result_val, key=attrgetter('counter'), reverse=True)
        
        
        # 'overdrives': Overdrive.objects.filter(Q(description__icontains=query) | Q(characteristic__name__icontains=query) | Q(characteristic__overdrive_global_description__icontains=query) | Q(characteristic__aspect__name__icontains=query)).select_related('characteristic').distinct().all(),
        overdrives_result = parse_search(
            overdrives_result,
            OverdriveI18n.objects.filter(characteristic__translations__language_code=language, characteristic__translations__name__icontains=query).select_related('characteristic'),
            'characteristic.name',
            gettext('Nom'),
            name_attr=''
        )
        
        overdrives_result = parse_search(
            overdrives_result,
            OverdriveI18n.objects.filter(translations__language_code=language, translations__description__icontains=query).select_related('characteristic'),
            'description_text_only',
            gettext('Description'),
            name_attr='',
            # with_context=True
        )
        
        overdrives_result = parse_search(
            overdrives_result,
            OverdriveI18n.objects.filter(characteristic__translations__language_code=language, characteristic__translations__overdrive_global_description__icontains=query).select_related('characteristic'),
            'characteristic.overdrive_global_description',
            gettext('Info tech.'),
            name_attr='',
            # with_context=True
        )
        
        overdrives_result = parse_search(
            overdrives_result,
            OverdriveI18n.objects.filter(characteristic__aspect__translations__language_code=language, characteristic__aspect__translations__name__icontains=query).select_related('characteristic', 'characteristic__aspect'),
            'characteristic.aspect.name',
            gettext('Aspect'),
            name_attr=''
        )
        
        overdrives_result_val = overdrives_result.values()
        overdrives_result_val = sorted(overdrives_result_val, key=attrgetter('name'))  # first sort is less important than second one
        overdrives_result_val = sorted(overdrives_result_val, key=attrgetter('counter'), reverse=True)
        
        
        archetypes_result = parse_search(
            archetypes_result,
            ArchetypeI18n.objects.filter(translations__language_code=language, translations__name__icontains=query),
            'name',
            gettext('Nom'),
        )
        
        archetypes_result = parse_search(
            archetypes_result,
            ArchetypeI18n.objects.filter(translations__language_code=language, translations__description__icontains=query),
            'description',
            gettext('Description'),
            # with_context=True
        )
        
        archetypes_result = parse_search(
            archetypes_result,
            ArchetypeI18n.objects.filter(
                  Q(characteristic_bonus_1__translations__language_code=language, characteristic_bonus_1__translations__name__icontains=query)
                | Q(characteristic_bonus_2__translations__language_code=language, characteristic_bonus_2__translations__name__icontains=query)
                | Q(characteristic_bonus_3__translations__language_code=language, characteristic_bonus_3__translations__name__icontains=query)
            ).annotate(counter=Count('translations__name')),
            '',
            gettext('Caractéristiques bonus'),
            counter_property=True
        )
        
        archetypes_result_val = archetypes_result.values()
        archetypes_result_val = sorted(archetypes_result_val, key=attrgetter('name'))  # first sort is less important than second one
        archetypes_result_val = sorted(archetypes_result_val, key=attrgetter('counter'), reverse=True)
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__name__icontains=query),
            'name',
            gettext('Nom'),
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__description__icontains=query),
            'description',
            gettext('Description'),
            # with_context=True
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__past__icontains=query),
            'past',
            gettext('Passé'),
            # with_context=True
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__destiny_effect__icontains=query),
            'destiny_effect_text_only',
            gettext('Effet de destinée'),
            # with_context=True
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(forced_aspect__translations__language_code=language, forced_aspect__translations__name__icontains=query).select_related('forced_aspect'),
            'forced_aspect.name',
            gettext('Aspect lié'),
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__advantage_name__icontains=query),
            'advantage_name',
            gettext('Avantage (nom)'),
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__advantage__icontains=query),
            'advantage_text_only',
            gettext('Avantage'),
            # with_context=True
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__disadvantage_name__icontains=query),
            'disadvantage_name',
            gettext('Inconvénient (nom)'),
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__disadvantage__icontains=query),
            'disadvantage_text_only',
            gettext('Inconvénient'),
            # with_context=True
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__ai_description__icontains=query),
            'ai_description',
            gettext('Description IA'),
            # with_context=True
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__ai_advantage_name__icontains=query),
            'ai_advantage_name',
            gettext('Avantage IA (nom)'),
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__ai_advantage__icontains=query),
            'ai_advantage_text_only',
            gettext('Avantage IA'),
            # with_context=True
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__ai_disadvantage_name__icontains=query),
            'ai_disadvantage_name',
            gettext('Inconvénient IA (nom)'),
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__ai_disadvantage__icontains=query),
            'ai_disadvantage_text_only',
            gettext('Inconvénient IA'),
            # with_context=True
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__ai_behavior__icontains=query),
            'ai_behavior',
            gettext('Comportement IA'),
            # with_context=True
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__oracle_future__icontains=query),
            'oracle_future',
            gettext('Avenir'),
            # with_context=True
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__oracle_fortune_name__icontains=query),
            'oracle_fortune_name',
            gettext('Fortune (nom)'),
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__oracle_fortune_description__icontains=query),
            'oracle_fortune_description_text_only',
            gettext('Fortune'),
            # with_context=True
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__oracle_calamity_name__icontains=query),
            'oracle_calamity_name',
            gettext('Calamité (nom)'),
        )
        
        arcanas_result = parse_search(
            arcanas_result,
            ArcanaI18n.objects.filter(translations__language_code=language, translations__oracle_calamity_description__icontains=query),
            'oracle_calamity_description_text_only',
            gettext('Calamité'),
            # with_context=True
        )
        
        arcanas_result_val = arcanas_result.values()
        arcanas_result_val = sorted(arcanas_result_val, key=attrgetter('name'))  # first sort is less important than second one
        arcanas_result_val = sorted(arcanas_result_val, key=attrgetter('counter'), reverse=True)
        
        
        great_deeds_result = parse_search(
            great_deeds_result,
            GreatDeedI18n.objects.filter(translations__language_code=language, translations__name__icontains=query),
            'name',
            gettext('Nom'),
        )
        
        great_deeds_result = parse_search(
            great_deeds_result,
            GreatDeedI18n.objects.filter(translations__language_code=language, translations__description__icontains=query),
            'description',
            gettext('Description'),
            # with_context=True
        )
        
        great_deeds_result = parse_search(
            great_deeds_result,
            GreatDeedI18n.objects.filter(translations__language_code=language, translations__restriction__icontains=query),
            'restriction_text_only',
            gettext('Restriction'),
            # with_context=True
        )
        
        great_deeds_result = parse_search(
            great_deeds_result,
            GreatDeedI18n.objects.filter(
                  Q(aspect_1__translations__language_code=language, aspect_1__translations__name__icontains=query)
                | Q(aspect_2__translations__language_code=language, aspect_2__translations__name__icontains=query)
            ).annotate(counter=Count('translations__name')),
            '',
            gettext('Aspect bonus'),
            counter_property=True
        )
        
        great_deeds_result_val = great_deeds_result.values()
        great_deeds_result_val = sorted(great_deeds_result_val, key=attrgetter('name'))  # first sort is less important than second one
        great_deeds_result_val = sorted(great_deeds_result_val, key=attrgetter('counter'), reverse=True)
        
        crests_result = parse_search(
            crests_result,
            CrestI18n.objects.filter(translations__language_code=language, translations__name__icontains=query),
            'name',
            gettext('Nom'),
        )
        
        crests_result = parse_search(
            crests_result,
            CrestI18n.objects.filter(translations__language_code=language, translations__description__icontains=query),
            'description',
            gettext('Description'),
            # with_context=True
        )
        
        crests_result = parse_search(
            crests_result,
            CrestI18n.objects.filter(translations__language_code=language, translations__vow__icontains=query),
            'vow',
            gettext('Vœu'),
            # with_context=True
        )
        
        crests_result_val = crests_result.values()
        crests_result_val = sorted(crests_result_val, key=attrgetter('name'))  # first sort is less important than second one
        crests_result_val = sorted(crests_result_val, key=attrgetter('counter'), reverse=True)
        
        allowed_armours = get_allowed_armours(request.user)
        armours_pk_allowed = list(allowed_armours.values_list('pk', flat=True).distinct())
        
        armours_result = parse_search(
            armours_result,
            ArmourI18n.objects.filter(pk__in=armours_pk_allowed, translations__language_code=language, translations__name__icontains=query),
            'name',
            gettext('Nom'),
        )
        
        armours_result = parse_search(
            armours_result,
            ArmourI18n.objects.filter(pk__in=armours_pk_allowed, translations__language_code=language, translations__background_description__icontains=query),
            'background_description',
            gettext('Historique'),
            # with_context=True
        )
        
        armours_result = parse_search(
            armours_result,
            ArmourI18n.objects.filter(pk__in=armours_pk_allowed, translations__language_code=language, translations__technical_description__icontains=query),
            'technical_description',
            gettext('Description technique'),
            # with_context=True
        )
        
        armours_result = parse_search(
            armours_result,
            ArmourI18n.objects.filter(pk__in=armours_pk_allowed, translations__language_code=language, translations__additional_notes__icontains=query),
            'additional_notes_text_only',
            gettext('Notes'),
            # with_context=True
        )
        
        armours_result = parse_search(
            armours_result,
            ArmourI18n.objects.filter(pk__in=armours_pk_allowed, overdrives__characteristic__translations__language_code=language, overdrives__characteristic__translations__name__icontains=query).annotate(counter=Count('overdrives__characteristic')),
            '',
            gettext('Overdrives'),
            counter_property=True
        )
        
        armours_result = parse_search(
            armours_result,
            ArmourEvolutionI18n.objects.filter(armour__pk__in=armours_pk_allowed, translations__language_code=language, translations__description__icontains=query).select_related('armour'),
            'description_text_only',
            gettext('Description sur evolution'),
            object_path='armour',
            suffix_attr='unlock_at',
            # with_context=True
        )
        
        armours_result = parse_search(
            armours_result,
            ArmourAbilityI18n.objects.filter(armour__pk__in=armours_pk_allowed, translations__language_code=language, translations__name__icontains=query).select_related('armour'),
            'name',
            gettext('Nom capacité'),
            object_path='armour',
            suffix_attr='name'
        )
        
        armours_result = parse_search(
            armours_result,
            ArmourAbilityI18n.objects.filter(armour__pk__in=armours_pk_allowed, translations__language_code=language, translations__description__icontains=query).select_related('armour'),
            'description_text_only',
            gettext('Description capacité'),
            object_path='armour',
            suffix_attr='name',
            # with_context=True
        )
        
        armours_result = parse_search(
            armours_result,
            ArmourAbilityVariantI18n.objects.filter(armour_ability__armour__pk__in=armours_pk_allowed, translations__language_code=language, translations__name__icontains=query).select_related('armour_ability', 'armour_ability__armour'),
            'name',
            gettext('Nom capacité'),
            object_path='armour_ability.armour',
            suffix_attr='name'
        )
        
        armours_result = parse_search(
            armours_result,
            ArmourAbilityVariantI18n.objects.filter(armour_ability__armour__pk__in=armours_pk_allowed, translations__language_code=language, translations__description__icontains=query).select_related('armour_ability', 'armour_ability__armour'),
            'description_text_only',
            gettext('Description capacité'),
            object_path='armour_ability.armour',
            suffix_attr='name',
            # with_context=True
        )
        
        armours_result_val = armours_result.values()
        armours_result_val = sorted(armours_result_val, key=attrgetter('name'))  # first sort is less important than second one
        armours_result_val = sorted(armours_result_val, key=attrgetter('counter'), reverse=True)
        
        divisions_result = parse_search(
            divisions_result,
            DivisionI18n.objects.filter(translations__language_code=language, translations__name__icontains=query),
            'name',
            gettext('Nom'),
        )
        
        divisions_result = parse_search(
            divisions_result,
            DivisionI18n.objects.filter(translations__language_code=language, translations__description__icontains=query),
            'description',
            gettext('Description'),
            # with_context=True
        )
        
        divisions_result = parse_search(
            divisions_result,
            DivisionI18n.objects.filter(aspect_bonus__translations__language_code=language, aspect_bonus__translations__name__icontains=query).select_related('aspect_bonus'),
            'aspect_bonus.name',
            gettext('Aspect bonus')
        )
        
        divisions_result = parse_search(
            divisions_result,
            DivisionI18n.objects.filter(translations__language_code=language, translations__disadvantage_name__icontains=query),
            'disadvantage_name',
            gettext('Inconvénient (Nom)')
        )
        
        divisions_result = parse_search(
            divisions_result,
            DivisionI18n.objects.filter(translations__language_code=language, translations__disadvantage__icontains=query),
            'disadvantage_text_only',
            gettext('Inconvénient'),
            # with_context=True
        )
        
        divisions_result_val = divisions_result.values()
        divisions_result_val = sorted(divisions_result_val, key=attrgetter('name'))  # first sort is less important than second one
        divisions_result_val = sorted(divisions_result_val, key=attrgetter('counter'), reverse=True)
        
        
        skills_result = parse_search(
            skills_result,
            HeroicAbilityI18n.objects.filter(translations__language_code=language, translations__name__icontains=query),
            'name',
            gettext('Nom'),
        )
        
        skills_result = parse_search(
            skills_result,
            HeroicAbilityI18n.objects.filter(translations__language_code=language, translations__description__icontains=query),
            'description_text_only',
            gettext('Description'),
            # with_context=True
        )
        
        skills_result_val = skills_result.values()
        skills_result_val = sorted(skills_result_val, key=attrgetter('name'))  # first sort is less important than second one
        skills_result_val = sorted(skills_result_val, key=attrgetter('counter'), reverse=True)
        
        
        stances_result = parse_search(
            stances_result,
            StyleI18n.objects.filter(translations__language_code=language, translations__name__icontains=query),
            'name',
            gettext('Nom'),
        )
        
        stances_result = parse_search(
            stances_result,
            StyleI18n.objects.filter(translations__language_code=language, translations__description__icontains=query),
            'description_text_only',
            gettext('Description'),
            # with_context=True
        )
        
        stances_result = parse_search(
            stances_result,
            StyleI18n.objects.filter(translations__language_code=language, translations__bonus__icontains=query),
            'bonus_text_only',
            gettext('Bonus'),
            # with_context=True
        )
        
        stances_result = parse_search(
            stances_result,
            StyleI18n.objects.filter(translations__language_code=language, translations__malus__icontains=query),
            'malus_text_only',
            gettext('Malus'),
            # with_context=True
        )
        
        stances_result_val = stances_result.values()
        stances_result_val = sorted(stances_result_val, key=attrgetter('name'))  # first sort is less important than second one
        stances_result_val = sorted(stances_result_val, key=attrgetter('counter'), reverse=True)
        
        allowed_traumas = get_allowed_traumas(request.user)
        traumas_pk_allowed = list(allowed_traumas.values_list('pk', flat=True).distinct())
        
        traumas_result = parse_search(
            traumas_result,
            TraumaI18n.objects.filter(pk__in=traumas_pk_allowed, translations__language_code=language, translations__name__icontains=query),
            'name',
            gettext('Nom'),
        )
        
        traumas_result = parse_search(
            traumas_result,
            TraumaI18n.objects.filter(pk__in=traumas_pk_allowed, translations__language_code=language, translations__description__icontains=query),
            'description_text_only',
            gettext('Description'),
            # with_context=True
        )
        
        traumas_result_val = traumas_result.values()
        traumas_result_val = sorted(traumas_result_val, key=attrgetter('name'))  # first sort is less important than second one
        traumas_result_val = sorted(traumas_result_val, key=attrgetter('counter'), reverse=True)
        
        
        gm_question = False
        if request.user.is_authenticated:
            if request.user.game_master:
                gm_question = True
        
        if gm_question:
            questions_result = QuestionI18n.objects.filter(Q(translations__language_code=language, translations__question__icontains=query) | Q(translations__language_code=language, translations__answer__icontains=query))
        else:
            questions_result = QuestionI18n.objects.filter(gm=False).filter(Q(translations__language_code=language, translations__question__icontains=query) | Q(translations__language_code=language, translations__answer__icontains=query))
        
        questions_result_val = questions_result.all()
        # questions_result_val = sorted(questions_result_val, key=attrgetter('question'))  # first sort is less important than second one
        
        allowed_mechas = get_allowed_mechas(request.user)
        mechas_pk_allowed = list(allowed_mechas.values_list('pk', flat=True).distinct())
        
        mechas_result = parse_search(
            mechas_result,
            MechaArmourI18n.objects.filter(pk__in=mechas_pk_allowed, translations__language_code=language, translations__name__icontains=query),
            'name',
            gettext('Nom'),
        )
        
        mechas_result = parse_search(
            mechas_result,
            MechaArmourI18n.objects.filter(pk__in=mechas_pk_allowed, translations__language_code=language, translations__description__icontains=query),
            'description_text_only',
            gettext('Description capacité'),
            # with_context=True
        )
        
        mechas_result = parse_search(
            mechas_result,
            MechaArmourActionCommonI18n.objects.filter(mecha__pk__in=mechas_pk_allowed, translations__language_code=language, translations__name__icontains=query),
            'name',
            gettext('Module/arme'),
            object_path='mecha',
            suffix_attr='name'
        )
        
        mechas_result = parse_search(
            mechas_result,
            MechaArmourActionCommonI18n.objects.filter(mecha__pk__in=mechas_pk_allowed, translations__language_code=language, translations__description__icontains=query),
            'description_text_only',
            gettext('Description module/arme'),
            object_path='mecha',
            suffix_attr='name'
        )
        
        mechas_result = parse_search(
            mechas_result,
            MechaArmourActionCommonI18n.objects.filter(mecha__pk__in=mechas_pk_allowed, translations__language_code=language, translations__effects__icontains=query),
            'effects_text_only',
            gettext('Effets module/arme'),
            object_path='mecha',
            suffix_attr='name'
        )
        
        mechas_result = parse_search(
            mechas_result,
            MechaArmourConfigurationI18n.objects.filter(mecha__pk__in=mechas_pk_allowed, translations__language_code=language, translations__name__icontains=query),
            'name',
            gettext('Configuration'),
            object_path='mecha',
            suffix_attr='name'
        )
        
        mechas_result = parse_search(
            mechas_result,
            MechaArmourConfigurationI18n.objects.filter(mecha__pk__in=mechas_pk_allowed, translations__language_code=language, translations__description__icontains=query),
            'description_text_only',
            gettext('Description configuration'),
            object_path='mecha',
            suffix_attr='name',
            # with_context=True
        )
        
        mechas_result = parse_search(
            mechas_result,
            MechaArmourActionConfigurationI18n.objects.filter(configuration__mecha__pk__in=mechas_pk_allowed, translations__language_code=language, translations__name__icontains=query),
            'name',
            gettext('Capacité'),
            object_path='configuration.mecha',
            suffix_attr='name'
        )
        
        mechas_result = parse_search(
            mechas_result,
            MechaArmourActionConfigurationI18n.objects.filter(configuration__mecha__pk__in=mechas_pk_allowed, translations__language_code=language, translations__description__icontains=query),
            'description_text_only',
            gettext('Description capacité'),
            object_path='configuration.mecha',
            suffix_attr='name'
        )
        
        mechas_result = parse_search(
            mechas_result,
            MechaArmourActionConfigurationI18n.objects.filter(configuration__mecha__pk__in=mechas_pk_allowed, translations__language_code=language, translations__effects__icontains=query),
            'effects_text_only',
            gettext('Effets capacité'),
            object_path='configuration.mecha',
            suffix_attr='name'
        )
        
        mechas_result_val = mechas_result.values()
        mechas_result_val = sorted(mechas_result_val, key=attrgetter('name'))  # first sort is less important than second one
        mechas_result_val = sorted(mechas_result_val, key=attrgetter('counter'), reverse=True)
        
        
        if request.user.is_authenticated and request.user.game_master:
            npc_capacities_result = parse_search(
                npc_capacities_result,
                NonPlayerCharacterAbilityI18n.objects.filter(translations__language_code=language, translations__name__icontains=query),
                'name',
                gettext('Nom'),
            )
            
            npc_capacities_result = parse_search(
                npc_capacities_result,
                NonPlayerCharacterAbilityI18n.objects.filter(translations__language_code=language, translations__description__icontains=query),
                'description_text_only',
                gettext('Description'),
                # with_context=True
            )
            
        npc_capacities_result_val = npc_capacities_result.values()
        npc_capacities_result_val = sorted(npc_capacities_result_val, key=attrgetter('name'))  # first sort is less important than second one
        npc_capacities_result_val = sorted(npc_capacities_result_val, key=attrgetter('counter'), reverse=True)
        # import pdb; pdb.set_trace()
        return render(request, template_name, {
            'searchterm': query,
            'weapons_result': weapons_result_val,
            'weapons_length': len(weapons_result_val),
            'modules_result': modules_result_val,
            'modules_length': len(modules_result_val),
            'effects_result': effects_result_val,
            'effects_length': len(effects_result_val),
            'enhancements_result': enhancements_result_val,
            'enhancements_length': len(enhancements_result_val),
            'overdrives_result': overdrives_result_val,
            'overdrives_length': len(overdrives_result_val),
            'archetypes_result': archetypes_result_val,
            'archetypes_length': len(archetypes_result_val),
            'arcanas_result': arcanas_result_val,
            'arcanas_length': len(arcanas_result_val),
            'great_deeds_result': great_deeds_result_val,
            'great_deeds_length': len(great_deeds_result_val),
            'crests_result': crests_result_val,
            'crests_length': len(crests_result_val),
            'armours_result': armours_result_val,
            'armours_length': len(armours_result_val),
            'divisions_result': divisions_result_val,
            'divisions_length': len(divisions_result_val),
            'skills_result': skills_result_val,
            'skills_length': len(skills_result_val),
            'stances_result': stances_result_val,
            'stances_length': len(stances_result_val),
            'traumas_result': traumas_result_val,
            'traumas_length': len(traumas_result_val),
            'mechas_result': mechas_result_val,
            'mechas_length': len(mechas_result_val),
            'npc_capacities_result': npc_capacities_result_val,
            'npc_capacities_length': len(npc_capacities_result_val),
            'questions_result': questions_result_val,
            'questions_length': len(questions_result_val),
            'result_template': 'gm/searches/search_result_fallback.html',
            'result_template_faq': 'gm/searches/search_result_faq_fallback.html',
            'request': request,
        })
    except ValueError:
        template_name = 'gm/searches/empty_search.html'
        return render(request, template_name)
    except Exception as e :
        raise e