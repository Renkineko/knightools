from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.db.models import Min, Max, Count, Q
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import translation

from ..models.character import (Trauma, MechaArmour)
from ..models.character import (
    ArmourI18n, DivisionI18n, ArcanaI18n, CrestI18n,
    ArchetypeI18n, GreatDeedI18n, TraumaI18n, MechaArmourI18n,
    NonPlayerCharacterAbilityI18n, NonPlayerCharacterTypeI18n,
    StyleI18n, ArmourTwinAbilityI18n
)
from ..models.weaponry import (
    ModuleCategoryI18n, WeaponI18n, EffectI18n, ModuleI18n,
    OverdriveI18n, EnhancementI18n, HeroicAbilityI18n, RarityI18n, SourceI18n,
    ActivationI18n, VehicleI18n, UltimateAptitudeI18n
)
from users.models import Feature

from ..utils import (
    get_allowed_armours, get_allowed_traumas, get_allowed_mechas,
    get_allowed_npc_abilities
)

class WeaponListView(generic.ListView):
    template_name = 'gm/lists/weapon.html'
    context_object_name = 'weapon_list'
    
    def get_context_data(self, **kwargs):
        context = super(WeaponListView, self).get_context_data(**kwargs)
        language = translation.get_language()
        
        context['rarities'] = (
            RarityI18n
            .objects
            .filter(
                pk__in=WeaponI18n.objects.values_list('rarity__pk', flat=True).distinct(),
                translations__language_code=language
            )
        )
        context['sources'] = (
            SourceI18n
            .objects
            .filter(
                pk__in=WeaponI18n.objects.values_list('source__pk', flat=True).distinct(),
                translations__language_code=language
            )
        )
        
        return context
        
    def get_queryset(self):
        """Return list of weapon."""
        language = translation.get_language()
        
        return (
            WeaponI18n
            .objects
            .filter(
                player_enabled=True,
                translations__language_code=language
            )
            .select_related('rarity', 'category')
            .order_by('category', 'rarity', 'translations__name')
        )

class EffectListView(generic.ListView):
    template_name = 'gm/lists/effect.html'
    context_object_name = 'effect_list'

    def get_context_data(self, **kwargs):
        context = super(EffectListView, self).get_context_data(**kwargs)
        language = translation.get_language()
        
        context['sources'] = (
            SourceI18n
            .objects
            .filter(
                pk__in=EffectI18n.objects.values_list('source__pk', flat=True).distinct(),
                translations__language_code=language
            )
        )
        
        return context
    
    def get_queryset(self):
        """Return list of effect."""
        language = translation.get_language()
        
        return (
            EffectI18n
            .objects
            .filter(
                translations__language_code=language
            )
            .order_by('translations__name')
        )

class ModuleListView(generic.ListView):
    template_name = 'gm/lists/module.html'
    context_object_name = 'module_list'

    def get_context_data(self, **kwargs):
        context = super(ModuleListView, self).get_context_data(**kwargs)
        language = translation.get_language()
        
        context['rarities'] = (
            RarityI18n
            .objects
            .filter(
                pk__in=ModuleI18n.objects.values_list('levels__rarity__pk', flat=True).distinct(),
                translations__language_code=language
            )
        )
        context['sources'] = (
            SourceI18n
            .objects
            .filter(
                pk__in=ModuleI18n.objects.values_list('source__pk', flat=True).distinct(),
                translations__language_code=language
            )
        )
        context['activations'] = (
            ActivationI18n
            .objects
            .filter(
                pk__in=ModuleI18n.objects.values_list('levels__activation__pk', flat=True).distinct(),
                translations__language_code=language
            )
        )
        context['categories'] = (
            ModuleCategoryI18n
            .objects
            .filter(
                pk__in=ModuleI18n.objects.values_list('category__pk', flat=True).distinct(),
                translations__language_code=language,
                filterable=True,
            )
            .order_by('translations__name')
        )
        
        return context
        
    def get_queryset(self):
        """Return list of module."""
        language = translation.get_language()
        return (ModuleI18n
            .objects
            .filter(
                translations__language_code=language
            )
            .annotate(
                min_rarity=Min('levels__rarity__id'),
                max_level=Max('levels__level')
            )
            .select_related('category')
            .prefetch_related('levels', 'levels__rarity')
            .order_by('category', 'min_rarity', 'translations__name'))

class OverdriveListView(generic.ListView):
    template_name = 'gm/lists/overdrive.html'
    context_object_name = 'overdrive_list'

    def get_queryset(self):
        """Return list of overdrive."""
        language = translation.get_language()
        
        return (
            OverdriveI18n
            .objects
            .filter(
                translations__language_code=language
            )
            .select_related('characteristic', 'characteristic__aspect', 'rarity')
            .order_by('characteristic__aspect__id', 'characteristic__id', 'level')
        )

class EnhancementListView(generic.ListView):
    template_name = 'gm/lists/enhancement.html'
    context_object_name = 'enhancement_list'

    def get_context_data(self, **kwargs):
        context = super(EnhancementListView, self).get_context_data(**kwargs)
        language = translation.get_language()
        
        context['sources'] = (
            SourceI18n
            .objects
            .filter(
                pk__in=EnhancementI18n.objects.values_list('source__pk', flat=True).distinct(),
                translations__language_code=language
            )
        )
        return context
    
    def get_queryset(self):
        """Return list of enhancement."""
        language = translation.get_language()
        
        return (
            EnhancementI18n
            .objects
            .filter(
                translations__language_code=language
            )
            .annotate(
                with_conditions=Count(
                    'effects__translations__effect_condition',
                    # filter=Q(effects__effect_condition__regex=r'^.+$')
                    filter=~Q(effects__translations__effect_condition='') # The ~ reverse the Q list to get all objects where condition is NOT ''.
                )
            )
            .select_related('category')
            .prefetch_related('effects')
            .order_by('group', 'translations__name')
        )

class ArmourListView(generic.ListView):
    template_name = 'gm/lists/armour.html'
    context_object_name = 'armour_list'
    
    def get_context_data(self, **kwargs):
        context = super(ArmourListView, self).get_context_data(**kwargs)
        language = translation.get_language()
        
        allowed_armours = get_allowed_armours(self.request.user)
        
        context['sources'] = (
            SourceI18n
            .objects
            .filter(
                pk__in=allowed_armours.values_list('source__pk', flat=True).distinct(),
                translations__language_code=language
            )
        )
        
        context['allowed_armours'] = allowed_armours
        
        return context
    
    def get_queryset(self):
        """Return list of armour."""
        language = translation.get_language()
        
        return (
            ArmourI18n
            .objects
            .filter(
                translations__language_code=language
            )
            .prefetch_related('abilities', 'overdrives', 'overdrives__characteristic')
            .order_by('translations__name')
        )

class DivisionListView(generic.ListView):
    template_name = 'gm/lists/division.html'
    context_object_name = 'division_list'

    def get_queryset(self):
        """Return list of division."""
        language = translation.get_language()
        
        return (
            DivisionI18n
            .objects
            .filter(
                translations__language_code=language
            )
            .prefetch_related(
                'overdrives',
                'overdrives__characteristic',
                'modules',
                'modules__module_level',
                'modules__module_level__module',
                'modules__forced_choice_effect',
                'weapons'
            )
            .order_by('translations__name')
        )

class ArcanaListView(generic.ListView):
    template_name = 'gm/lists/arcana.html'
    context_object_name = 'arcana_list'

    def get_queryset(self):
        """Return list of arcana."""
        language = translation.get_language()
        
        return (
            ArcanaI18n
            .objects
            .filter(
                translations__language_code=language
            )
            .select_related('forced_aspect')
            .order_by('number')
        )

class CrestListView(generic.ListView):
    template_name = 'gm/lists/crest.html'
    context_object_name = 'crest_list'

    def get_queryset(self):
        """Return list of crest."""
        language = translation.get_language()
        
        return (
            CrestI18n
            .objects
            .filter(
                translations__language_code=language
            )
            .order_by('translations__name')
        )

class ArchetypeListView(generic.ListView):
    template_name = 'gm/lists/archetype.html'
    context_object_name = 'archetype_list'

    def get_queryset(self):
        """Return list of archetype."""
        language = translation.get_language()
        
        return (
            ArchetypeI18n
            .objects
            .filter(
                translations__language_code=language
            )
            .select_related('characteristic_bonus_1', 'characteristic_bonus_2', 'characteristic_bonus_3')
            .order_by('translations__name')
        )

class GreatDeedListView(generic.ListView):
    template_name = 'gm/lists/great_deed.html'
    context_object_name = 'great_deed_list'

    def get_queryset(self):
        """Return list of great_deed."""
        language = translation.get_language()
        
        return (
            GreatDeedI18n
            .objects
            .filter(
                translations__language_code=language
            )
            .select_related('aspect_1', 'aspect_2')
            .order_by('translations__name')
        )

class HeroicAbilityListView(generic.ListView):
    template_name = 'gm/lists/heroic_ability.html'
    context_object_name = 'heroic_ability_list'

    def get_queryset(self):
        """Return list of heroic abilities."""
        language = translation.get_language()
        
        return (
            HeroicAbilityI18n
            .objects
            .filter(
                translations__language_code=language
            )
            .select_related('category')
            .order_by('category', 'translations__name')
        )

class VehicleListView(generic.ListView):
    template_name = 'gm/lists/vehicle.html'
    context_object_name = 'vehicle_list'

    def get_queryset(self):
        """Return list of vehicles."""
        language = translation.get_language()
        
        return (
            VehicleI18n
            .objects
            .filter(
                translations__language_code=language
            )
            .order_by('translations__name')
        )

class TraumaListView(LoginRequiredMixin, generic.ListView):
    template_name = 'gm/lists/trauma.html'
    context_object_name = 'trauma_list'

    # def get_context_data(self, **kwargs):
    #     context = super(TraumaListView, self).get_context_data(**kwargs)
        
    #     context['allowed_traumas'] = get_allowed_traumas(self.request.user)
        
    #     return context
    
    def get_queryset(self):
        """Return list of trauma."""
        return (
            get_allowed_traumas(self.request.user)
            .select_related('category')
            .order_by('category__hop_recovered', 'die_value')
        )
        language = translation.get_language()
        
        return (
            TraumaI18n
            .objects
            .filter(
                translations__language_code=language,
                pk__in=get_allowed_traumas(self.request.user).values_list('pk', flat=True)
            )
            .select_related('category')
            .order_by('category__hop_recovered', 'die_value')
        )

    # def get_queryset(self):
    #     """Return list of traumas."""
    #     allowed_traumas = TraumaI18n.objects.filter(password=None).select_related('category').order_by('category__hop_recovered', 'die_value')
    #     if self.request.user.is_authenticated:
    #         if self.request.user.game_master:
    #             allowed_traumas = Trauma.objects.select_related('category').order_by('category__hop_recovered', 'die_value')
    #         else:
    #             allowed_traumas = allowed_traumas.union(self.request.user.unlocked_traumas.all().select_related('category')).order_by('category__hop_recovered', 'die_value')
                
    #     return allowed_traumas

class MechaArmourListView(LoginRequiredMixin, generic.ListView):
    template_name = 'gm/lists/mecha.html'
    context_object_name = 'mecha_list'
    
    def get_context_data(self, **kwargs):
        context = super(MechaArmourListView, self).get_context_data(**kwargs)
        
        context['allowed_mechas'] = get_allowed_mechas(self.request.user)
        
        return context
    
    def get_queryset(self):
        """Return list of mecha."""
        language = translation.get_language()
        
        return (
            MechaArmourI18n
            .objects
            .filter(
                translations__language_code=language
            )
            .prefetch_related('configurations')
            .order_by('translations__name')
        )

class NonPlayerCharacterAbilityListView(LoginRequiredMixin, generic.ListView):
    template_name = 'gm/lists/npc_ability.html'
    context_object_name = 'npc_ability_list'
    
    def get_context_data(self, **kwargs):
        context = super(NonPlayerCharacterAbilityListView, self).get_context_data(**kwargs)
        language = translation.get_language()
        
        context['npc_types'] = (
            NonPlayerCharacterTypeI18n
            .objects
            .filter(
                translations__language_code=language
            )
            .all()
        )
        return context
        
    def get_queryset(self):
        """Return list of npc abilities."""
        return get_allowed_npc_abilities(self.request.user).prefetch_related('types')
    

class StyleListView(generic.ListView):
    template_name = 'gm/lists/style.html'
    context_object_name = 'style_list'

    def get_queryset(self):
        """Return list of styles."""
        language = translation.get_language()
        
        return (
            StyleI18n
            .objects
            .filter(
                translations__language_code=language
            )
            .order_by('translations__name')
        )

class ArmourTwinAbilityListView(LoginRequiredMixin, generic.ListView):
    template_name = 'gm/lists/twin.html'
    context_object_name = 'twin_list'

    def get_context_data(self, **kwargs):
        context = super(ArmourTwinAbilityListView, self).get_context_data(**kwargs)
        language = translation.get_language()
        
        context['sources'] = (
            SourceI18n
            .objects
            .filter(
                pk__in=ArmourTwinAbilityI18n.objects.values_list('ability__armour__source__pk', flat=True).distinct(),
                translations__language_code=language
            )
        )
        
        context['allowed_armours'] = get_allowed_armours(self.request.user)
        context['twin_ability_feature'] = Feature.objects.get(slug="twin-ability")
        
        return context
    
    def get_queryset(self):
        """Return list of armour twin abilities."""
        language = translation.get_language()
        
        return (
            ArmourTwinAbilityI18n
            .objects
            .filter(
                translations__language_code=language,
                ability__translations__language_code=language,
                ability__armour__translations__language_code=language
            )
            .select_related('ability', 'ability__armour', 'ability__armour__source')
            .prefetch_related('excluded_armours')
            .order_by('ability__armour__translations__name')
        )
    
    def get(self, request, *args, **kwargs):
        if not request.user.is_twin_ability_visible():
            return redirect(reverse_lazy('gm:index'))
        return super(ArmourTwinAbilityListView, self).get(request, *args, **kwargs)

class UltimateAptitudeListView(LoginRequiredMixin, generic.ListView):
    template_name = 'gm/lists/ultimate.html'
    context_object_name = 'ultimate_list'

    def get_context_data(self, **kwargs):
        context = super(UltimateAptitudeListView, self).get_context_data(**kwargs)
        
        context['allowed_armours'] = get_allowed_armours(self.request.user)
        context['ultimate_aptitude_feature'] = Feature.objects.get(slug="ultimate-aptitude")
        return context
    
    def get_queryset(self):
        """Return list of ultimate aptitudes."""
        language = translation.get_language()
        
        return (
            UltimateAptitudeI18n
            .objects
            .filter(
                translations__language_code=language
            )
            .select_related('type')
            .order_by('type', 'translations__name')
        )
    
    def get(self, request, *args, **kwargs):
        if not request.user.is_ultimate_aptitude_visible():
            return redirect(reverse_lazy('gm:index'))
        return super(UltimateAptitudeListView, self).get(request, *args, **kwargs)

# RETROCOMP REDIRECT VIEWS

class HeroicSkillListRedirectView(generic.RedirectView):
    permanent = True  # Use a permanent redirect (HTTP 301)
    
    def get_redirect_url(self, *args, **kwargs):
        return reverse('gm:heroic_ability_list', kwargs=kwargs)

class StanceListRedirectView(generic.RedirectView):
    permanent = True  # Use a permanent redirect (HTTP 301)
    
    def get_redirect_url(self, *args, **kwargs):
        return reverse('gm:style_list', kwargs=kwargs)

class NonPlayerCharacterCapacityListRedirectView(generic.RedirectView):
    permanent = True  # Use a permanent redirect (HTTP 301)
    
    def get_redirect_url(self, *args, **kwargs):
        return reverse('gm:npc_ability_list', kwargs=kwargs)

