# Generated by Django 2.1.4 on 2019-03-12 12:16

from django.db import migrations
import martor.models


class Migration(migrations.Migration):

    dependencies = [
        ('gm', '0037_auto_20190307_1330'),
    ]

    operations = [
        migrations.RenameField(
            model_name='achievement',
            old_name='aspect_id_1',
            new_name='aspect_1',
        ),
        migrations.RenameField(
            model_name='achievement',
            old_name='aspect_id_2',
            new_name='aspect_2',
        ),
        migrations.AlterField(
            model_name='majorarcana',
            name='ai_favor',
            field=martor.models.MartorField(verbose_name='avantage IA détaillé'),
        ),
        migrations.AlterField(
            model_name='majorarcana',
            name='ai_weakness',
            field=martor.models.MartorField(verbose_name='inconvénient IA détaillé'),
        ),
        migrations.AlterField(
            model_name='majorarcana',
            name='creation_bonus',
            field=martor.models.MartorField(verbose_name='bonus en création'),
        ),
        migrations.AlterField(
            model_name='majorarcana',
            name='favor',
            field=martor.models.MartorField(verbose_name='avantage détaillé'),
        ),
        migrations.AlterField(
            model_name='majorarcana',
            name='weakness',
            field=martor.models.MartorField(verbose_name='inconvénient détaillé'),
        ),
    ]
