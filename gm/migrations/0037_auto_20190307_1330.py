# Generated by Django 2.1.4 on 2019-03-07 13:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gm', '0036_auto_20190307_1234'),
    ]

    operations = [
        migrations.AlterField(
            model_name='majorarcana',
            name='ai_keywords',
            field=models.CharField(max_length=256, verbose_name='mots-clés IA'),
        ),
    ]
