# Generated by Django 2.2.2 on 2021-05-28 14:42

from django.db import migrations, models
import django.db.models.deletion
import martor.models
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('gm', '0079_auto_20210210_1231'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128, verbose_name='nom')),
                ('css_classes', models.CharField(blank=True, max_length=256, verbose_name='classes CSS')),
                ('lft', models.PositiveIntegerField(editable=False)),
                ('rght', models.PositiveIntegerField(editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(editable=False)),
                ('parent', mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='children', to='gm.Category', verbose_name='Catégorie parent')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.CharField(max_length=512, verbose_name='question')),
                ('answer', martor.models.MartorField(blank=True, null=True, verbose_name='réponse')),
                ('gm', models.BooleanField(blank=True, default=False, verbose_name='filtre MJ')),
                ('v1', models.BooleanField(blank=True, default=True, verbose_name='compatible v1')),
                ('v1_5', models.BooleanField(blank=True, default=True, verbose_name='compatible v1.5')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='questions', to='gm.Category', verbose_name='catégorie')),
            ],
        ),
        migrations.AddField(
            model_name='archetype',
            name='questions',
            field=models.ManyToManyField(blank=True, to='gm.Question', verbose_name='faq'),
        ),
        migrations.AddField(
            model_name='armour',
            name='questions',
            field=models.ManyToManyField(blank=True, to='gm.Question', verbose_name='faq'),
        ),
        migrations.AddField(
            model_name='crest',
            name='questions',
            field=models.ManyToManyField(blank=True, to='gm.Question', verbose_name='faq'),
        ),
        migrations.AddField(
            model_name='division',
            name='questions',
            field=models.ManyToManyField(blank=True, to='gm.Question', verbose_name='faq'),
        ),
        migrations.AddField(
            model_name='effect',
            name='questions',
            field=models.ManyToManyField(blank=True, to='gm.Question', verbose_name='faq'),
        ),
        migrations.AddField(
            model_name='enhancement',
            name='questions',
            field=models.ManyToManyField(blank=True, to='gm.Question', verbose_name='faq'),
        ),
        migrations.AddField(
            model_name='greatdeed',
            name='questions',
            field=models.ManyToManyField(blank=True, to='gm.Question', verbose_name='faq'),
        ),
        migrations.AddField(
            model_name='heroicskill',
            name='questions',
            field=models.ManyToManyField(blank=True, to='gm.Question', verbose_name='faq'),
        ),
        migrations.AddField(
            model_name='majorarcana',
            name='questions',
            field=models.ManyToManyField(blank=True, to='gm.Question', verbose_name='faq'),
        ),
        migrations.AddField(
            model_name='mechaarmour',
            name='questions',
            field=models.ManyToManyField(blank=True, to='gm.Question', verbose_name='faq'),
        ),
        migrations.AddField(
            model_name='module',
            name='questions',
            field=models.ManyToManyField(blank=True, to='gm.Question', verbose_name='faq'),
        ),
        migrations.AddField(
            model_name='nonplayercharactercapacity',
            name='questions',
            field=models.ManyToManyField(blank=True, to='gm.Question', verbose_name='faq'),
        ),
        migrations.AddField(
            model_name='overdrive',
            name='questions',
            field=models.ManyToManyField(blank=True, to='gm.Question', verbose_name='faq'),
        ),
        migrations.AddField(
            model_name='trauma',
            name='questions',
            field=models.ManyToManyField(blank=True, to='gm.Question', verbose_name='faq'),
        ),
        migrations.AddField(
            model_name='vehicle',
            name='questions',
            field=models.ManyToManyField(blank=True, to='gm.Question', verbose_name='faq'),
        ),
        migrations.AddField(
            model_name='weapon',
            name='questions',
            field=models.ManyToManyField(blank=True, to='gm.Question', verbose_name='faq'),
        ),
    ]
