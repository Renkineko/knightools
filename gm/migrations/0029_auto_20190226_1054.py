# Generated by Django 2.1.4 on 2019-02-26 10:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gm', '0028_auto_20190222_1230'),
    ]

    operations = [
        migrations.CreateModel(
            name='NonPlayerCharacter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='nom')),
                ('defense', models.PositiveSmallIntegerField(default=1, verbose_name='défense')),
                ('reaction', models.PositiveSmallIntegerField(default=1, verbose_name='réaction')),
                ('initiative', models.PositiveSmallIntegerField(default=1, verbose_name='réaction')),
                ('health_points', models.PositiveSmallIntegerField(blank=True, default=0, null=True, verbose_name='points de santé (ps)')),
                ('energy_points', models.PositiveSmallIntegerField(blank=True, default=0, null=True, verbose_name="points d'énergie (pe)")),
                ('armor_points', models.PositiveSmallIntegerField(blank=True, default=0, null=True, verbose_name="points d'armure (pa)")),
                ('force_field', models.PositiveSmallIntegerField(blank=True, default=0, null=True, verbose_name='champ de force (cdf)')),
                ('cohesion', models.PositiveSmallIntegerField(blank=True, default=0, null=True, verbose_name='cohésion')),
                ('outbreak', models.PositiveSmallIntegerField(blank=True, default=0, null=True, verbose_name='débordement')),
            ],
        ),
        migrations.CreateModel(
            name='NonPlayerCharacterAspect',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('basic_value', models.SmallIntegerField(default=1, verbose_name='valeur de base')),
                ('except_value', models.PositiveSmallIntegerField(blank=True, default=0, help_text="En dessous de 6, l'aspect exceptionnel est mineur, sinon il est majeur", null=True, verbose_name='valeur exceptionnelle')),
                ('aspect', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='npcs', to='gm.Aspect')),
                ('npc', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='aspects', to='gm.NonPlayerCharacter')),
            ],
        ),
        migrations.CreateModel(
            name='NonPlayerCharacterModuleLevel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('module_level', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='owning_npcs', to='gm.ModuleLevel')),
                ('npc', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='modules', to='gm.NonPlayerCharacter')),
            ],
        ),
        migrations.CreateModel(
            name='NonPlayerCharacterWeapon',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.PositiveSmallIntegerField(default=1, verbose_name='quantité')),
                ('npc', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='weapons', to='gm.NonPlayerCharacter')),
            ],
        ),
        migrations.RenameField(
            model_name='armor',
            old_name='armor',
            new_name='armor_points',
        ),
        migrations.RenameField(
            model_name='armor',
            old_name='energy',
            new_name='energy_points',
        ),
        migrations.RenameField(
            model_name='modulelevelnpc',
            old_name='armor_point',
            new_name='armor_points',
        ),
        migrations.AddField(
            model_name='weapon',
            name='npc_enabled',
            field=models.BooleanField(default=True, verbose_name='accessible PNJ'),
        ),
        migrations.AddField(
            model_name='weapon',
            name='player_enabled',
            field=models.BooleanField(default=True, verbose_name='accessible PJ'),
        ),
        migrations.AddField(
            model_name='nonplayercharacterweapon',
            name='weapon',
            field=models.ForeignKey(limit_choices_to={'npc_enabled': True}, on_delete=django.db.models.deletion.CASCADE, related_name='npcs', to='gm.Weapon'),
        ),
        migrations.AlterUniqueTogether(
            name='nonplayercharacterweapon',
            unique_together={('weapon', 'npc')},
        ),
        migrations.AlterUniqueTogether(
            name='nonplayercharactermodulelevel',
            unique_together={('module_level', 'npc')},
        ),
    ]
