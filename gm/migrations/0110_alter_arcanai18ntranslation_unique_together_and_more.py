# Generated by Django 5.1.1 on 2024-12-03 20:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gm', '0109_titanreachi18n_traumacategoryi18n_mechaarmouri18n_and_more'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='arcanai18ntranslation',
            unique_together={('language_code', 'master')},
        ),
        migrations.AlterUniqueTogether(
            name='archetypei18ntranslation',
            unique_together={('language_code', 'master')},
        ),
        migrations.AlterUniqueTogether(
            name='armouri18ntranslation',
            unique_together={('language_code', 'master')},
        ),
        migrations.AlterUniqueTogether(
            name='cresti18ntranslation',
            unique_together={('language_code', 'master')},
        ),
        migrations.AlterUniqueTogether(
            name='divisioni18ntranslation',
            unique_together={('language_code', 'master')},
        ),
        migrations.AlterUniqueTogether(
            name='effecti18ntranslation',
            unique_together={('language_code', 'master')},
        ),
        migrations.AlterUniqueTogether(
            name='enhancementi18ntranslation',
            unique_together={('language_code', 'master')},
        ),
        migrations.AlterUniqueTogether(
            name='greatdeedi18ntranslation',
            unique_together={('language_code', 'master')},
        ),
        migrations.AlterUniqueTogether(
            name='heroicabilityi18ntranslation',
            unique_together={('language_code', 'master')},
        ),
        migrations.AlterUniqueTogether(
            name='mechaarmouri18ntranslation',
            unique_together={('language_code', 'master')},
        ),
        migrations.AlterUniqueTogether(
            name='modulei18ntranslation',
            unique_together={('language_code', 'master')},
        ),
        migrations.AlterUniqueTogether(
            name='overdrivei18ntranslation',
            unique_together={('language_code', 'master')},
        ),
        migrations.AlterUniqueTogether(
            name='traumai18ntranslation',
            unique_together={('language_code', 'master')},
        ),
        migrations.AlterUniqueTogether(
            name='ultimateaptitudei18ntranslation',
            unique_together={('language_code', 'master')},
        ),
        migrations.AlterUniqueTogether(
            name='vehiclei18ntranslation',
            unique_together={('language_code', 'master')},
        ),
        migrations.AlterUniqueTogether(
            name='weaponi18ntranslation',
            unique_together={('language_code', 'master')},
        ),
        migrations.AlterField(
            model_name='arcanai18ntranslation',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='archetypei18ntranslation',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='armouri18ntranslation',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='cresti18ntranslation',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='divisioni18ntranslation',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='effecti18ntranslation',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='enhancementi18ntranslation',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='greatdeedi18ntranslation',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='heroicabilityi18ntranslation',
            name='slug',
            field=models.SlugField(),
        ),
        migrations.AlterField(
            model_name='mechaarmouri18ntranslation',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='modulei18ntranslation',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='overdrivei18ntranslation',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='traumai18ntranslation',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='ultimateaptitudei18ntranslation',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='vehiclei18ntranslation',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='weaponi18ntranslation',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='arcanai18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='archetypei18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='armouri18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='cresti18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='divisioni18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='effecti18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='enhancementi18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='greatdeedi18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='heroicabilityi18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='mechaarmouri18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='modulei18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='nonplayercharacterabilityi18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='overdrivei18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='stylei18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='traumai18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='ultimateaptitudei18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='vehiclei18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
        migrations.AlterUniqueTogether(
            name='weaponi18ntranslation',
            unique_together={('language_code', 'master'), ('language_code', 'slug')},
        ),
    ]
