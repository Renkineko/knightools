from django.db import models
from django.urls import reverse
from mptt.models import MPTTModel, TreeForeignKey, TreeManyToManyField
from parler.models import TranslatableModel, TranslatedFields
from django.utils.translation import gettext_lazy
# from categories_i18n.models import Category as BaseCategoryI18n
from treebeard.mp_tree import MP_Node

from martor.models import MartorField
from gettext import gettext
import bleach
from markdown import markdown

class Category(MPTTModel):
    name = models.CharField(gettext_lazy("nom"), max_length=128)
    css_classes = models.CharField(gettext_lazy("classes CSS"), max_length=256, blank=True)
    parent = TreeForeignKey(
        'self',
        on_delete=models.CASCADE,
        verbose_name=gettext_lazy("Catégorie parent"),
        related_name="children",
        blank=True,
        null=True
    )
    
    class MPTTMeta:
        order_insertion_by = ['name']
    
    def __str__(self):
        return self.name
    
    
class CategoryTreebeard(MP_Node):
    name = models.CharField(gettext_lazy("nom"), max_length=128)
    css_classes = models.CharField(gettext_lazy("classes CSS"), max_length=256, blank=True)
    
    node_order_by = ['name']
    
    def __str__(self):
        return 'Category %s' % (self.name)
    

class Question(models.Model):
    SRC_DISCORD = 'discord'
    SRC_FACEBOOK = 'facebook'
    SRC_FORUM = 'forum'
    SRC_MAIL = 'mail'
    SRC_PDF = 'pdf'
    source_type_choices = (
        (SRC_PDF, gettext_lazy('FAQ PDF')),
        (SRC_FORUM, gettext_lazy('Forum')),
        (SRC_DISCORD, gettext_lazy('Discord')),
        (SRC_FACEBOOK, gettext_lazy('Facebook')),
        (SRC_MAIL, gettext_lazy('Mail')),
    )
    
    question = models.CharField(gettext_lazy("question"), max_length=512)
    answer = MartorField(gettext_lazy("réponse"), blank=True, null=True)
    gm = models.BooleanField(gettext_lazy('filtre MJ'), blank=True, default=False)
    v1 = models.BooleanField(gettext_lazy('compatible v1'), blank=True, default=True)
    v1_5 = models.BooleanField(gettext_lazy('compatible v1.5'), blank=True, default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    category = TreeManyToManyField(
        Category,
        verbose_name=gettext_lazy("catégorie(s)"),
        related_name="questions"
    )
    categotree = models.ManyToManyField(CategoryTreebeard,
        verbose_name=gettext_lazy("catégorie(s)"),
        related_name="questions")
    source_type = models.CharField(gettext_lazy('type de source'), max_length=8, choices=source_type_choices, blank=True, null=True)
    
    def __str__(self):
        if self.gm:
            return ('[%s] %s: %s' % (gettext_lazy("MJ"), self.get_full_category(), self.question))
        return ('%s: %s' % (self.get_full_category(), self.question))
    
    def get_full_category(self):
        extended_fullpathname = []
        
        for categ in self.category.all():
            fullpathname = []
            
            for cat in categ.get_family():
                fullpathname.append(cat.name)
            
            extended_fullpathname.append(' > '.join(fullpathname))
            
        return " ; ".join(extended_fullpathname)
    
    def get_fontawesome_by_source_type(self):
        if self.source_type == self.SRC_DISCORD:
            return 'fab fa-discord'
        if self.source_type == self.SRC_FACEBOOK:
            return 'fab fa-facebook'
        if self.source_type == self.SRC_FORUM:
            return 'fas fa-university'
        if self.source_type == self.SRC_MAIL:
            return 'fas fa-envelope'
        if self.source_type == self.SRC_PDF:
            return 'fas fa-file-pdf'
        
        return ''
    
    def question_text_only(self):
        return bleach.clean(markdown(self.question), tags=[], strip=True)
    
    def answer_text_only(self):
        if self.answer:
            return bleach.clean(markdown(self.answer), tags=[], strip=True)
        else:
            return ''

# class CategoryI18n(BaseCategoryI18n):
#     css_classes = models.CharField(gettext_lazy("classes CSS"), max_length=256, blank=True)
    

class CategoryTreebeardI18n(MP_Node):
    internal_name = models.CharField(gettext_lazy("nom interne"), max_length=128)
    css_classes = models.CharField(gettext_lazy("classes CSS"), max_length=256, blank=True)
    
    node_order_by = ['internal_name']
    
    def __str__(self):
        return '%s %s' % ('-' * ((self.depth - 1) * 2), self.translations.safe_translation_getter('name', any_language=True))

class CategoryTreebeardI18nName(TranslatableModel):
    category = models.OneToOneField(CategoryTreebeardI18n, on_delete=models.CASCADE, related_name='translations')
    translations = TranslatedFields(
        name=models.CharField(gettext_lazy("nom"), max_length=128)
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)

class QuestionI18n(TranslatableModel):
    SRC_DISCORD = 'discord'
    SRC_FACEBOOK = 'facebook'
    SRC_FORUM = 'forum'
    SRC_MAIL = 'mail'
    SRC_PDF = 'pdf'
    source_type_choices = (
        (SRC_PDF, gettext_lazy('FAQ PDF')),
        (SRC_FORUM, gettext_lazy('Forum')),
        (SRC_DISCORD, gettext_lazy('Discord')),
        (SRC_FACEBOOK, gettext_lazy('Facebook')),
        (SRC_MAIL, gettext_lazy('Mail')),
    )
    
    translations = TranslatedFields(
        question = models.CharField(gettext_lazy("question"), max_length=512),
        answer = MartorField(gettext_lazy("réponse"), blank=True, null=True),
        source_type = models.CharField(gettext_lazy('type de source'), max_length=8, choices=source_type_choices, blank=True, null=True),
    )
    gm = models.BooleanField(gettext_lazy('filtre MJ'), blank=True, default=False)
    v1 = models.BooleanField(gettext_lazy('compatible v1'), blank=True, default=True)
    v1_5 = models.BooleanField(gettext_lazy('compatible v1.5'), blank=True, default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    category = models.ManyToManyField(
        CategoryTreebeardI18n,
        verbose_name=gettext_lazy("catégorie(s)"),
        related_name="questions",
        blank=True
    )
    
    def __str__(self):
        if self.gm:
            return ('[%s] %s: %s' % (gettext("MJ"), self.get_full_category(), self.safe_translation_getter('question', any_language=True)))
        return ('%s: %s' % (self.get_full_category(), self.safe_translation_getter('question', any_language=True)))
    
    def get_full_category(self):
        extended_fullpathname = []
        
        for categ in self.category.all():
            fullpathname = []
            
            for cat in categ.get_ancestors():
                fullpathname.append(cat.translations.safe_translation_getter('name', any_language=True))
            
            fullpathname.append(categ.translations.safe_translation_getter('name', any_language=True))
            
            extended_fullpathname.append(' > '.join(fullpathname))
            
        return " ; ".join(extended_fullpathname)
    
    def get_fontawesome_by_source_type(self):
        if self.source_type == self.SRC_DISCORD:
            return 'fab fa-discord'
        if self.source_type == self.SRC_FACEBOOK:
            return 'fab fa-facebook'
        if self.source_type == self.SRC_FORUM:
            return 'fas fa-university'
        if self.source_type == self.SRC_MAIL:
            return 'fas fa-envelope'
        if self.source_type == self.SRC_PDF:
            return 'fas fa-file-pdf'
        
        return ''
    
    def question_text_only(self, lang_code):
        return bleach.clean(markdown(self.safe_translation_getter('question', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def answer_text_only(self, lang_code):
        answer = self.safe_translation_getter('answer', language_code=lang_code, any_language=True)
        if answer:
            return bleach.clean(markdown(answer), tags=[], strip=True)
        else:
            return ''
