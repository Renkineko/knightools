from django.db import models
from django.urls import reverse

from parler.models import TranslatableModel, TranslatedFields
from martor.models import MartorField
from django.utils.translation import gettext_lazy, gettext
import bleach
from markdown import markdown

from .faq import Question, QuestionI18n

class Aspect(models.Model):
    name = models.CharField("nom", max_length=32)
    player_description = models.TextField("description")
    adjectives = models.CharField("adjectifs", max_length=128)
    npc_description = models.TextField("description pour NPC")
    exceptional_global_description = models.CharField("description aspect exceptionnel", max_length=256)
    exceptional_minor_effect_description = models.CharField("effet exceptionnel mineur", max_length=256)
    exceptional_major_effect_description = models.CharField("effet exceptionnel majeur", max_length=256)
    
    def __str__(self):
        return self.name

class Characteristic(models.Model):
    name = models.CharField("nom", max_length=32)
    description = models.TextField()
    overdrive_global_description = models.CharField("Descriptif général overdrive", max_length=128)
    aspect = models.ForeignKey(Aspect, on_delete=models.PROTECT, related_name="characteristics")
    
    def __str__(self):
        return self.name

class Source(models.Model):
    name = models.CharField(max_length=32,default='Livre de Base')
    def __str__(self):
        return self.name

class Archetype(models.Model):
    name = models.CharField("nom", max_length=64)
    description = models.TextField()
    characteristic_bonus_1 = models.ForeignKey(Characteristic, on_delete=models.PROTECT, related_name='+', verbose_name="carac. bonus 1", blank=True, null=True) # # Custom archetype forced the optional attribute for this one...
    characteristic_bonus_2 = models.ForeignKey(Characteristic, on_delete=models.PROTECT, related_name='+', verbose_name="carac. bonus 2", blank=True, null=True) # # Custom archetype forced the optional attribute for this one...
    characteristic_bonus_3 = models.ForeignKey(Characteristic, on_delete=models.PROTECT, related_name='+', verbose_name="carac. bonus 3", blank=True, null=True)
    slug = models.SlugField(null=True, blank=True, unique=True)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
            
    def get_absolute_url(self):
        return reverse('gm:archetype_detail', args=[self.slug])
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''

class GreatDeed(models.Model):
    name = models.CharField("nom", max_length=64)
    description = models.TextField()
    restriction = MartorField(max_length=256)
    aspect_1 = models.ForeignKey(Aspect, on_delete=models.PROTECT, related_name='+', verbose_name="choix aspect", blank=True, null=True) # Custom great deed forced the optional attribute for this one...
    aspect_2 = models.ForeignKey(Aspect, on_delete=models.PROTECT, related_name='+', verbose_name="choix aspect", blank=True, null=True)
    nickname_suggestion = models.CharField("surnoms possibles", max_length=256)
    slug = models.SlugField(null=True, blank=True, unique=True)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def restriction_text_only(self):
        return bleach.clean(markdown(self.restriction), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
            
    def get_absolute_url(self):
        return reverse('gm:great_deed_detail', args=[self.slug])

class Crest(models.Model):
    name = models.CharField("nom", max_length=32)
    description = models.CharField(max_length=512)
    vow = models.CharField("vœu", max_length=128)
    slug = models.SlugField(null=True, blank=True, unique=True)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
            
    def get_absolute_url(self):
        return reverse('gm:crest_detail', args=[self.slug])

class MajorArcana(models.Model):
    name = models.CharField("nom", max_length=64)
    description = models.TextField()
    destiny_quote = models.CharField("citation destinée", max_length=128) 
    destiny_effect = MartorField("effet destinée")
    past = models.TextField("passé")
    creation_bonus = MartorField("bonus en création")
    aspect_point = models.PositiveSmallIntegerField("points d'aspect", default=1)
    characteristic_point = models.PositiveSmallIntegerField("points de carac.", default=3)
    forced_aspect = models.ForeignKey(Aspect,on_delete=models.PROTECT,verbose_name="aspect associé",blank=True,null=True) # Arcana 0 and 16 forced the optional attribute for this one...
    advantage_name = models.CharField("avantage", max_length=64)
    advantage = MartorField("avantage détaillé")
    disadvantage_name = models.CharField("inconvénient", max_length=64)
    disadvantage = MartorField("inconvénient détaillé")
    ai_description = models.TextField("description IA")
    ai_letter = models.CharField("lettre IA", max_length=2)
    ai_keywords = models.CharField("mots-clés IA", max_length=256)
    ai_behavior = models.TextField("caractère IA")
    ai_advantage_name = models.CharField("avantage IA", max_length=64)  
    ai_advantage = MartorField("avantage IA détaillé")
    ai_disadvantage_name = models.CharField("inconvénient IA", max_length=64)  
    ai_disadvantage = MartorField("inconvénient IA détaillé")
    slug = models.SlugField(null=True, blank=True, unique=True)
    roman_number = models.CharField("numéro romain", max_length=5, blank=True)
    number = models.PositiveSmallIntegerField("numéro", default=0)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def destiny_effect_text_only(self):
        return bleach.clean(markdown(self.destiny_effect), tags=[], strip=True)
    
    def creation_bonus_text_only(self):
        return bleach.clean(markdown(self.creation_bonus), tags=[], strip=True)
    
    def advantage_text_only(self):
        return bleach.clean(markdown(self.advantage), tags=[], strip=True)
    
    def disadvantage_text_only(self):
        return bleach.clean(markdown(self.disadvantage), tags=[], strip=True)
    
    def ai_advantage_text_only(self):
        return bleach.clean(markdown(self.ai_advantage), tags=[], strip=True)
    
    def ai_disadvantage_text_only(self):
        return bleach.clean(markdown(self.ai_disadvantage), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
            
    def get_absolute_url(self):
        return reverse('gm:arcana_detail', args=[self.slug])

class Division(models.Model):
    name = models.CharField("nom", max_length=64)
    description = models.TextField()
    aspect_bonus = models.ForeignKey(Aspect, on_delete=models.PROTECT)
    disadvantage_name = models.CharField("inconvénient", max_length=64)
    disadvantage = MartorField("inconvénient détaillé")
    slug = models.SlugField(null=True, blank=True, unique=True)
    motto = models.CharField("devise", max_length=128, default="")
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def disadvantage_text_only(self):
        return bleach.clean(markdown(self.disadvantage), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
            
    def get_absolute_url(self):
        return reverse('gm:division_detail', args=[self.slug])

class DivisionOverdrive(models.Model):
    division = models.ForeignKey(Division, on_delete=models.CASCADE, related_name="overdrives")
    characteristic = models.ForeignKey(Characteristic, on_delete=models.PROTECT, related_name="divisions")
    
    def __str__(self):
        return self.division.name + ' ' + self.characteristic.name
    
    class Meta:
        unique_together = ("division", "characteristic")

class Armour(models.Model):
    name = models.CharField("nom", max_length=64)
    background_description = models.TextField("description background", blank=True)
    technical_description = models.TextField("description technique", blank=True)
    additional_notes = MartorField("addenda", blank=True)
    generation = models.PositiveSmallIntegerField("génération", default=1)
    energy_points = models.PositiveSmallIntegerField("points d'énergie (pe)")
    armour_points = models.PositiveSmallIntegerField("points d'armure (pa)")
    force_field = models.PositiveSmallIntegerField("champ de force (cdf)",default=12)
    slot_head = models.PositiveSmallIntegerField("slots tête",default=5)
    slot_left_arm = models.PositiveSmallIntegerField("slots bras g.",default=5)
    slot_right_arm = models.PositiveSmallIntegerField("slots bras d.",default=5)
    slot_torso = models.PositiveSmallIntegerField("slots torse",default=8)
    slot_left_leg = models.PositiveSmallIntegerField("slots jambe g.",default=5)
    slot_right_leg = models.PositiveSmallIntegerField("slots jambe d.",default=5)
    slug = models.SlugField(null=True, blank=True, unique=True)
    source = models.ForeignKey(Source, on_delete=models.CASCADE, related_name="armours")
    faq_corrected = models.BooleanField("corrigé par FAQ", default=False)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    password = models.CharField("mot de passe", max_length=256, blank=True, null=True)
    aegis = models.PositiveSmallIntegerField("égide", blank=True, null=True)
    
    def __str__(self):
        return self.name
    
    def additional_notes_text_only(self):
        return bleach.clean(markdown(self.additional_notes), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
    def origin(self):
        if self.faq_corrected:
            return '%s (corrigé par FAQ)' % self.source.name
        else:
            return self.source.name
            
    def get_absolute_url(self):
        return reverse('gm:armour_detail', args=[self.slug])

class ArmourOverdrive(models.Model):
    armour = models.ForeignKey(Armour,on_delete=models.CASCADE,verbose_name="armure", related_name="overdrives")
    characteristic = models.ForeignKey(Characteristic,on_delete=models.PROTECT,verbose_name="caractéristique", related_name="armours")
    
    def __str__(self):
        return self.armour.name + ' ' + self.characteristic.name
    
    class Meta:
        unique_together = ("armour", "characteristic")

class ArmourAbility(models.Model):
    armour = models.ForeignKey(Armour,on_delete=models.CASCADE,verbose_name="armure", related_name="abilities")
    name = models.CharField("nom", max_length=64)
    description = MartorField()
    default_max_variant = models.PositiveSmallIntegerField("max variantes / défaut", blank=True, null=True)
    energy = models.CharField("énergie", max_length=128, null=True, blank=True) # yes, this is confusing, but energy in armour abilities are so conditionals...
    activation = models.CharField("activation", max_length=256, null=True, blank=True) # yes, also confusing, but again: activation can be very conditional here.
    hope = models.CharField("espoir", max_length=128, null=True, blank=True) # here again, not an int but a char because it may vary depending on a lot of things...
    duration = models.CharField("durée", max_length=128, null=True, blank=True)
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)

class ArmourTwinAbility(models.Model):
    ability = models.OneToOneField(ArmourAbility, on_delete=models.CASCADE, verbose_name="capacité", related_name="twin")
    description = MartorField()
    energy = models.TextField("énergie", max_length=128, null=True, blank=True) # yes, this is confusing, but energy in armour abilities are so conditionals...
    activation = models.TextField("activation", max_length=256, null=True, blank=True) # yes, also confusing, but again: activation can be very conditional here.
    duration = models.TextField("durée", max_length=128, null=True, blank=True)
    excluded_armours = models.ManyToManyField(Armour, verbose_name="armures exclues", blank=True)
    
    def __str__(self):
        return '%s - %s' % (self.ability.armour.name, self.ability.name)
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
        
    def get_excluded_armours(self):
        names = []
        for armour in self.excluded_armours.all():
            names.append(armour.name)
        return ' - '.join(names)

class ArmourEvolution(models.Model):
    armour = models.ForeignKey(Armour,on_delete=models.CASCADE,verbose_name="armure", related_name="evolutions")
    level = models.PositiveSmallIntegerField("niveau")
    unlock_at = models.PositiveSmallIntegerField("condition d'accès")
    description = MartorField()
    
    def __str__(self):
        return self.armour.name + ' evol. ' + str(self.level)
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)

class NonPlayerCharacter(models.Model):
    name = models.CharField("nom", max_length=64)
    defence = models.PositiveSmallIntegerField("défense", null=True, blank=True, default=1)
    reaction = models.PositiveSmallIntegerField("réaction", null=True, blank=True, default=1)
    initiative = models.PositiveSmallIntegerField("initiative", null=True, blank=True, default=1)
    health_points = models.PositiveSmallIntegerField("points de santé (ps)", null=True, blank=True, default=0)
    energy_points = models.PositiveSmallIntegerField("points d'énergie (pe)", null=True, blank=True, default=0)
    armour_points = models.PositiveSmallIntegerField("points d'armure (pa)", null=True, blank=True, default=0)
    force_field = models.PositiveSmallIntegerField("champ de force (cdf)", null=True, blank=True, default=0)
    cohesion = models.PositiveSmallIntegerField("cohésion", null=True, blank=True, default=0)
    outbreak = models.PositiveSmallIntegerField("débordement", null=True, blank=True, default=0)
    speed = models.SmallIntegerField("vitesse", null=True, blank=True, default=0)
    slot_head = models.PositiveSmallIntegerField("slots tête", blank=True, null=True)
    slot_left_arm = models.PositiveSmallIntegerField("slots bras g.", blank=True, null=True)
    slot_right_arm = models.PositiveSmallIntegerField("slots bras d.", blank=True, null=True)
    slot_torso = models.PositiveSmallIntegerField("slots torse", blank=True, null=True)
    slot_left_leg = models.PositiveSmallIntegerField("slots jambe g.", blank=True, null=True)
    slot_right_leg = models.PositiveSmallIntegerField("slots jambe d.", blank=True, null=True)
    simplified = models.BooleanField("NPC Simplifié", default=False) # If False, use NonPlayerCharacterAspect ; if True, use NonPlayerCharacterCharacteristic
    
    def __str__(self):
        return self.name
        
    def get_colspan(self):
        counter = 1
        if (self.initiative):
            counter += 1
        if (self.health_points):
            counter += 1
        if (self.energy_points):
            counter += 1
        if (self.armour_points):
            counter += 1
        if (self.force_field):
            counter += 1
        if (self.cohesion):
            counter += 1
        if (self.outbreak):
            counter += 1
        if (self.speed):
            counter += 1
        return counter
        
    def has_slots(self):
        return (not self.simplified
            and (
                (self.slot_head is not None and self.slot_head > 0)
                or (self.slot_left_arm is not None and self.slot_left_arm > 0)
                or (self.slot_right_arm is not None and self.slot_right_arm > 0)
                or (self.slot_torso is not None and self.slot_torso > 0)
                or (self.slot_left_leg is not None and self.slot_left_leg > 0)
                or (self.slot_right_leg is not None and self.slot_right_leg > 0)
            )
        )
        
    def get_weapons(self):
        return self.weapons.order_by('weapon__category', 'weapon__name').all()
        
    def get_modules(self):
        return self.modules.order_by('module_level__module__name').all()

class NonPlayerCharacterAspect(models.Model):
    npc = models.ForeignKey(NonPlayerCharacter, on_delete=models.CASCADE, related_name="aspects", limit_choices_to={"simplified": False})
    aspect = models.ForeignKey(Aspect, on_delete=models.PROTECT, related_name="npcs")
    basic_value = models.SmallIntegerField("valeur de base", default=1)
    except_value = models.PositiveSmallIntegerField("valeur exceptionnelle", null=True, blank=True, default=0)
    except_major = models.BooleanField("aspect except. majeure", default=False)
    
    def __str__(self):
        if self.except_value > 0:
            if self.except_major:
                return "%s - %s : %d (Majeur %d) " % (self.npc.name, self.aspect.name, self.basic_value, self.except_value)
            else:
                return "%s - %s : %d (Mineur %d) " % (self.npc.name, self.aspect.name, self.basic_value, self.except_value)
        else:
            return "%s - %s : %d " % (self.npc.name, self.aspect.name, self.basic_value)
    
    class Meta:
        unique_together = ("npc", "aspect")

class NonPlayerCharacterCharacteristic(models.Model):
    npc = models.ForeignKey(NonPlayerCharacter, on_delete=models.CASCADE, related_name="characteristics", limit_choices_to={"simplified": True})
    characteristic = models.ForeignKey(Characteristic,on_delete=models.PROTECT,verbose_name="caractéristique")
    test_dice = models.PositiveSmallIntegerField("dés de test",default=0)
    overdrive_bonus = models.PositiveSmallIntegerField("bonus overdrive",default=0)
    
    def __str__(self):
        return self.characteristic.name
    
    def get_test(self):
        result = str(self.test_dice) + 'D6'
        if self.overdrive_bonus > 0:
            result += ' + ' + str(self.overdrive_bonus) + 'OD'
        return result

class TraumaCategory(models.Model):
    name = models.CharField("nom", max_length=64)
    description = models.TextField()
    hop_recovered = models.PositiveSmallIntegerField()
    
    def __str__(self):
        return self.name

class Trauma(models.Model):
    name = models.CharField("nom", max_length=64)
    description = MartorField()
    die_value = models.PositiveSmallIntegerField("valeur du dé", blank=True, null=True)
    category = models.ForeignKey(TraumaCategory, on_delete=models.CASCADE, verbose_name="catégorie", related_name="traumas")
    slug = models.SlugField(null=True, blank=True, unique=True)
    password = models.CharField("mot de passe", max_length=256, blank=True, null=True)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
            
    def get_absolute_url(self):
        return reverse('gm:trauma_detail', args=[self.slug])
    
class MechaArmour(models.Model):
    name = models.CharField("nom", max_length=64)
    description = MartorField()
    speed = models.PositiveSmallIntegerField("vitesse", default=0)
    maneuverability = models.SmallIntegerField("manœuvrabilité", default=0)
    power = models.PositiveSmallIntegerField("puissance", default=0)
    sensors = models.PositiveSmallIntegerField("senseurs", default=0)
    systems = models.PositiveSmallIntegerField("systèmes", default=0)
    resilience = models.PositiveSmallIntegerField("résilience", default=0)
    armour_plating = models.PositiveSmallIntegerField("blindage", default=0)
    force_field = models.PositiveSmallIntegerField("champ de force", default=0)
    energy_core = models.PositiveSmallIntegerField("noyau d'énergie", default=100)
    password = models.CharField("mot de passe", max_length=256, blank=True, null=True)
    slug = models.SlugField(null=True, blank=True, unique=True)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
            
    def get_absolute_url(self):
        return reverse('gm:mecha_detail', args=[self.slug])
    
class MechaArmourConfiguration(models.Model):
    name = models.CharField("nom", max_length=64)
    description = MartorField()
    mecha = models.ForeignKey(MechaArmour, on_delete=models.CASCADE, verbose_name="mécha-armure", related_name="configurations")
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)

class NonPlayerCharacterDifficulty(models.Model):
    name = models.CharField("nom", max_length=64)
    
    def __str__(self):
        return self.name

class NonPlayerCharacterType(models.Model):
    name = models.CharField("nom", max_length=64)
    
    def __str__(self):
        return self.name

class NonPlayerCharacterCapacity(models.Model):
    name = models.CharField("nom", max_length=128)
    description = MartorField()
    min_difficulty = models.ForeignKey(NonPlayerCharacterDifficulty, on_delete=models.CASCADE, verbose_name="diff. minimum")
    common = models.BooleanField("commune", default=True)
    penalty = models.BooleanField("malus", default=False)
    elite = models.BooleanField("élite", default=False)
    human_allowed = models.BooleanField("compatible humain", default=False)
    anathema_allowed = models.BooleanField("compatible anathème", default=False)
    ether_allowed = models.BooleanField("compatible éther", default=False)
    void_allowed = models.BooleanField("compatible vide", default=False)
    precision = models.CharField("précision", max_length=128, blank=True)
    slug = models.SlugField(blank=True)
    types = models.ManyToManyField(NonPlayerCharacterType, related_name="capacities")
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
        
    def get_absolute_url(self):
        return reverse('gm:npc_capacity_detail', args=[self.slug])
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
    def get_absolute_url(self):
        return reverse('gm:npc_capacity_detail', args=[self.slug])

class Stance(models.Model):
    name = models.CharField("nom", max_length=128)
    description = MartorField()
    bonus = MartorField(blank=True)
    malus = MartorField(blank=True)
    examples = MartorField("exemples", blank=True)
    slug = models.SlugField()
    constraint = MartorField("Contrainte", blank=True,default='-')
    fighter_available = models.BooleanField("dispo au contact", blank=True, default=True)
    shooter_available = models.BooleanField("dispo au tir", blank=True, default=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def bonus_text_only(self):
        return bleach.clean(markdown(self.bonus), tags=[], strip=True)
    
    def malus_text_only(self):
        return bleach.clean(markdown(self.malus), tags=[], strip=True)
    
    def examples_text_only(self):
        return bleach.clean(markdown(self.examples), tags=[], strip=True)
    
    def constraint_text_only(self):
        return bleach.clean(markdown(self.constraint), tags=[], strip=True)

class Faction(models.Model):
    name = models.CharField("nom", max_length=128)
    logo_file_name = models.CharField("nom fichier logo", max_length=128)
    default_public_description = models.TextField("description publique", blank=True)
    default_gm_description = models.TextField("description MJ", blank=True, null=True)
    default_level = models.SmallIntegerField('niveau par défaut', default=0, blank=True)
    default_visible_player = models.BooleanField('visible par défaut aux PJ', default=False, blank=True)
    default_pc_faction = models.BooleanField('faction PJ par défaut', default=False, blank=True)
    
    def __str__(self):
        return self.name

class FactionEffect(models.Model):
    faction = models.ForeignKey(Faction, on_delete=models.CASCADE, related_name="effects")
    level_name = models.CharField("nom niveau", max_length=128)
    level_memo = models.TextField("mémo niveau (MJ only)", blank=True)
    level = models.SmallIntegerField("niveau")
    effect_name = models.CharField("nom capacité ou malus", max_length=128, blank=True)
    effect_description = models.TextField("description capacité ou malus", blank=True)
    effect_default_visible_to_player = models.BooleanField("visible par défaut aux joueurs", blank=True, default=False)
    
class FactionDefaultEdge(models.Model):
    node_source = models.ForeignKey(Faction, on_delete=models.CASCADE, related_name="default_sources")
    node_target = models.ForeignKey(Faction, on_delete=models.CASCADE, related_name="default_targets")
    level = models.SmallIntegerField("niveau de relation", default=0)
    
    def get_effect_visibility(self):
        # On negative effect, 
        if self.level < 0:
            effect = self.node_source.effects.filter(level__gte=self.level, level__lt=0).order_by('level').first()
            if effect:
                return effect.effect_default_visible_to_player
            
        if self.level > 0:
            effect = self.node_source.effects.filter(level__lte=self.level, level__gt=0).order_by('-level').first()
            if effect:
                return effect.effect_default_visible_to_player
        
        return False

class AspectI18n(TranslatableModel):
    translations = TranslatedFields(  # Define translatable fields
        name = models.CharField(gettext_lazy("nom"), max_length=32),
        player_description = models.TextField(gettext_lazy("description")),
        adjectives = models.CharField(gettext_lazy("adjectifs"), max_length=128),
        npc_description = models.TextField(gettext_lazy("description pour NPC")),
        exceptional_global_description = models.CharField(gettext_lazy("description aspect exceptionnel"), max_length=256),
        exceptional_minor_effect_description = models.CharField(gettext_lazy("effet exceptionnel mineur"), max_length=256),
        exceptional_major_effect_description = models.CharField(gettext_lazy("effet exceptionnel majeur"), max_length=256),
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)

class CharacteristicI18n(TranslatableModel):
    translations = TranslatedFields(  # Define translatable fields
        name = models.CharField(gettext_lazy("nom"), max_length=32),
        description = models.TextField(),
        overdrive_global_description = models.CharField(gettext_lazy("Descriptif général overdrive"), max_length=128),
    )
    aspect = models.ForeignKey(AspectI18n, on_delete=models.PROTECT, related_name="characteristics")
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)

class SourceI18n(TranslatableModel):
    translations = TranslatedFields(  # Define translatable fields
        name = models.CharField(gettext_lazy("nom"), max_length=32, default='Livre de Base')
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)

class ArchetypeI18n(TranslatableModel):
    translations = TranslatedFields(  # Define translatable fields
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        description = models.TextField(),
        slug = models.SlugField(null=True, blank=True),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    
    characteristic_bonus_1 = models.ForeignKey(CharacteristicI18n, on_delete=models.PROTECT, related_name='+', verbose_name="carac. bonus 1", blank=True, null=True)
    characteristic_bonus_2 = models.ForeignKey(CharacteristicI18n, on_delete=models.PROTECT, related_name='+', verbose_name="carac. bonus 2", blank=True, null=True)
    characteristic_bonus_3 = models.ForeignKey(CharacteristicI18n, on_delete=models.PROTECT, related_name='+', verbose_name="carac. bonus 3", blank=True, null=True)
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name="faq",
        blank=True,
    )

    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
            
    def get_absolute_url(self):
        return reverse('gm:archetype_detail', args=[self.slug])

class GreatDeedI18n(TranslatableModel):
    translations = TranslatedFields(  # Define translatable fields
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        description = models.TextField(),
        restriction = MartorField(max_length=256),
        nickname_suggestion = models.CharField(gettext_lazy("surnoms possibles"), max_length=256),
        slug = models.SlugField(null=True, blank=True),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    aspect_1 = models.ForeignKey(AspectI18n, on_delete=models.PROTECT, related_name='+', verbose_name=gettext_lazy("choix aspect"), blank=True, null=True) # Custom great deed forced the optional attribute for this one...
    aspect_2 = models.ForeignKey(AspectI18n, on_delete=models.PROTECT, related_name='+', verbose_name=gettext_lazy("choix aspect"), blank=True, null=True)
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def restriction_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('restriction', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
    
    def get_absolute_url(self):
        return reverse('gm:great_deed_detail', args=[self.slug])

class CrestI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=32),
        description = models.CharField(max_length=512),
        vow = models.CharField(gettext_lazy("vœu"), max_length=128),
        slug = models.SlugField(null=True, blank=True),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
    
    def get_absolute_url(self):
        return reverse('gm:crest_detail', args=[self.slug])

class ArcanaI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        description = models.TextField(),
        destiny_quote = models.CharField(gettext_lazy("citation destinée"), max_length=128) ,
        destiny_effect = MartorField(gettext_lazy("effet destinée")),
        past = models.TextField(gettext_lazy("passé")),
        creation_bonus = MartorField(gettext_lazy("bonus en création")),
        advantage_name = models.CharField(gettext_lazy("avantage"), max_length=64),
        advantage = MartorField(gettext_lazy("avantage détaillé")),
        disadvantage_name = models.CharField(gettext_lazy("inconvénient"), max_length=64),
        disadvantage = MartorField(gettext_lazy("inconvénient détaillé")),
        ai_description = models.TextField(gettext_lazy("description IA")),
        ai_keywords = models.CharField(gettext_lazy("mots-clés IA"), max_length=256),
        ai_behavior = models.TextField(gettext_lazy("caractère IA")),
        ai_advantage_name = models.CharField(gettext_lazy("avantage IA"), max_length=64),
        ai_advantage = MartorField(gettext_lazy("avantage IA détaillé")),
        ai_disadvantage_name = models.CharField(gettext_lazy("inconvénient IA"), max_length=64),
        ai_disadvantage = MartorField(gettext_lazy("inconvénient IA détaillé")),
        oracle_future = MartorField(gettext_lazy("avenir"), blank=True,null=True),
        oracle_fortune_name = models.CharField(gettext_lazy("nom fortune"), max_length=64, blank=True,null=True),
        oracle_fortune_description = MartorField(gettext_lazy("description fortune"), blank=True,null=True),
        oracle_calamity_name = models.CharField(gettext_lazy("nom calamité"), max_length=64, blank=True,null=True),
        oracle_calamity_description = MartorField(gettext_lazy("description calamité"), blank=True,null=True),
        slug = models.SlugField(null=True, blank=True),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    
    aspect_point = models.PositiveSmallIntegerField(gettext_lazy("points d'aspect"), default=1)
    characteristic_point = models.PositiveSmallIntegerField(gettext_lazy("points de carac."), default=3)
    forced_aspect = models.ForeignKey(AspectI18n,on_delete=models.PROTECT,verbose_name=gettext_lazy("aspect associé"),blank=True,null=True) # Arcana 0 and 16 forced the optional attribute for this one...
    ai_letter = models.CharField(gettext_lazy("lettre IA"), max_length=2)
    roman_number = models.CharField(gettext_lazy("numéro romain"), max_length=5, blank=True)
    number = models.PositiveSmallIntegerField(gettext_lazy("numéro"), default=0)
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def destiny_effect_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('destiny_effect', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def creation_bonus_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('creation_bonus', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def advantage_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('advantage', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def disadvantage_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('disadvantage', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def ai_advantage_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('ai_advantage', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def ai_disadvantage_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('ai_disadvantage', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def oracle_future_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('oracle_future', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def oracle_fortune_description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('oracle_fortune_description', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def oracle_calamity_description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('oracle_calamity_description', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
    
    def get_absolute_url(self):
        return reverse('gm:arcana_detail', args=[self.slug])

class DivisionI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        description = models.TextField(),
        disadvantage_name = models.CharField(gettext_lazy("inconvénient"), max_length=64),
        disadvantage = MartorField(gettext_lazy("inconvénient détaillé")),
        motto = models.CharField(gettext_lazy("devise"), max_length=128, default=""),
        slug = models.SlugField(null=True, blank=True),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    aspect_bonus = models.ForeignKey(AspectI18n, on_delete=models.PROTECT)
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def disadvantage_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('disadvantage', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
    
    def get_absolute_url(self):
        return reverse('gm:division_detail', args=[self.slug])

class DivisionOverdriveI18n(models.Model):
    division = models.ForeignKey(DivisionI18n, on_delete=models.CASCADE, related_name="overdrives")
    characteristic = models.ForeignKey(CharacteristicI18n, on_delete=models.PROTECT, related_name="divisions")
    
    def __str__(self):
        return self.division.safe_translation_getter('name', any_language=True) + ' ' + self.characteristic.safe_translation_getter('name', any_language=True)
    
    class Meta:
        unique_together = ("division", "characteristic")

class ArmourI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        background_description = models.TextField(gettext_lazy("description background"), blank=True),
        technical_description = models.TextField(gettext_lazy("description technique"), blank=True),
        additional_notes = MartorField(gettext_lazy("addenda"), blank=True),
        slug = models.SlugField(null=True, blank=True),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    generation = models.PositiveSmallIntegerField(gettext_lazy("génération"), default=1)
    energy_points = models.PositiveSmallIntegerField(gettext_lazy("points d'énergie (pe)"))
    armour_points = models.PositiveSmallIntegerField(gettext_lazy("points d'armure (pa)"))
    force_field = models.PositiveSmallIntegerField(gettext_lazy("champ de force (cdf)"),default=12)
    slot_head = models.PositiveSmallIntegerField(gettext_lazy("slots tête"),default=5)
    slot_left_arm = models.PositiveSmallIntegerField(gettext_lazy("slots bras g."),default=5)
    slot_right_arm = models.PositiveSmallIntegerField(gettext_lazy("slots bras d."),default=5)
    slot_torso = models.PositiveSmallIntegerField(gettext_lazy("slots torse"),default=8)
    slot_left_leg = models.PositiveSmallIntegerField(gettext_lazy("slots jambe g."),default=5)
    slot_right_leg = models.PositiveSmallIntegerField(gettext_lazy("slots jambe d."),default=5)
    source = models.ForeignKey(SourceI18n, on_delete=models.CASCADE, related_name="armours")
    faq_corrected = models.BooleanField(gettext_lazy("corrigé par FAQ"), default=False)
    gest_spoiler = models.BooleanField(gettext_lazy('spoiler Geste ?'), default=False, blank=True)
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name="faq",
        blank=True,
    )
    password = models.CharField(gettext_lazy("mot de passe"), max_length=256, blank=True, null=True)
    aegis = models.PositiveSmallIntegerField(gettext_lazy("égide"), blank=True, null=True)
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def additional_notes_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('additional_notes', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def origin(self):
        if self.faq_corrected:
            return '%s (%s)' % (self.source.safe_translation_getter('name', any_language=True), gettext("corrigé par FAQ"))
        else:
            return self.source.safe_translation_getter('name', any_language=True)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
    
    def get_absolute_url(self):
        return reverse('gm:armour_detail', args=[self.slug])

class ArmourOverdriveI18n(models.Model):
    armour = models.ForeignKey(ArmourI18n,on_delete=models.CASCADE,verbose_name=gettext_lazy("armure"), related_name="overdrives")
    characteristic = models.ForeignKey(CharacteristicI18n,on_delete=models.PROTECT,verbose_name=gettext_lazy("caractéristique"), related_name="armours")
    
    def __str__(self):
        return self.armour.safe_translation_getter('name', any_language=True) + ' ' + self.characteristic.safe_translation_getter('name', any_language=True)
    
    class Meta:
        unique_together = ("armour", "characteristic")

class ArmourAbilityI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        description = MartorField(),
        energy = models.CharField(gettext_lazy("énergie"), max_length=128, null=True, blank=True), # yes, this is confusing, but energy in armour abilities are so conditionals...
        activation = models.CharField(gettext_lazy("activation"), max_length=256, null=True, blank=True), # yes, also confusing, but again: activation can be very conditional here.
        hope = models.CharField(gettext_lazy("espoir"), max_length=128, null=True, blank=True), # here again, not an int but a char because it may vary depending on a lot of things...
        duration = models.CharField(gettext_lazy("durée"), max_length=128, null=True, blank=True),
    )
    
    armour = models.ForeignKey(ArmourI18n,on_delete=models.CASCADE,verbose_name=gettext_lazy("armure"), related_name="abilities")
    default_max_variant = models.PositiveSmallIntegerField(gettext_lazy("max variantes / défaut"), blank=True, null=True)
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)

class ArmourTwinAbilityI18n(TranslatableModel):
    translations = TranslatedFields(
        description = MartorField(),
        energy = models.TextField(gettext_lazy("énergie"), max_length=128, null=True, blank=True), # yes, this is confusing, but energy in armour abilities are so conditionals...
        activation = models.TextField(gettext_lazy("activation"), max_length=256, null=True, blank=True), # yes, also confusing, but again: activation can be very conditional here.
        duration = models.TextField(gettext_lazy("durée"), max_length=128, null=True, blank=True),
    )
    
    ability = models.OneToOneField(ArmourAbilityI18n, on_delete=models.CASCADE, verbose_name="capacité", related_name="twin")
    excluded_armours = models.ManyToManyField(
        ArmourI18n,
        verbose_name=gettext_lazy("armures exclues"),
        blank=True
    )
    
    def __str__(self):
        return '%s - %s' % (self.ability.armour.safe_translation_getter('name', any_language=True), self.ability.safe_translation_getter('name', any_language=True))
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)
        
    def get_excluded_armours(self):
        names = []
        for armour in self.excluded_armours.all():
            names.append(armour.safe_translation_getter('name', any_language=True))
        return ' - '.join(names)

class ArmourEvolutionI18n(TranslatableModel):
    translations = TranslatedFields(
        description = MartorField(),
    )
    
    armour = models.ForeignKey(ArmourI18n, on_delete=models.CASCADE, verbose_name=gettext_lazy("armure"), related_name="evolutions", null=True) # should not be null... I failed a migration.
    level = models.PositiveSmallIntegerField(gettext_lazy("niveau"))
    unlock_at = models.PositiveSmallIntegerField(gettext_lazy("condition d'accès"))
    
    def __str__(self):
        return self.armour.safe_translation_getter('name', any_language=True) + ' evol. ' + str(self.level)
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)

class NonPlayerCharacterI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
    )
    defence = models.PositiveSmallIntegerField(gettext_lazy("défense"), null=True, blank=True, default=1)
    reaction = models.PositiveSmallIntegerField(gettext_lazy("réaction"), null=True, blank=True, default=1)
    initiative = models.PositiveSmallIntegerField(gettext_lazy("initiative"), null=True, blank=True, default=1)
    health_points = models.PositiveSmallIntegerField(gettext_lazy("points de santé (ps)"), null=True, blank=True, default=0)
    energy_points = models.PositiveSmallIntegerField(gettext_lazy("points d'énergie (pe)"), null=True, blank=True, default=0)
    armour_points = models.PositiveSmallIntegerField(gettext_lazy("points d'armure (pa)"), null=True, blank=True, default=0)
    force_field = models.PositiveSmallIntegerField(gettext_lazy("champ de force (cdf)"), null=True, blank=True, default=0)
    cohesion = models.PositiveSmallIntegerField(gettext_lazy("cohésion"), null=True, blank=True, default=0)
    outbreak = models.PositiveSmallIntegerField(gettext_lazy("débordement"), null=True, blank=True, default=0)
    speed = models.SmallIntegerField(gettext_lazy("vitesse"), null=True, blank=True, default=0)
    slot_head = models.PositiveSmallIntegerField(gettext_lazy("slots tête"), blank=True, null=True)
    slot_left_arm = models.PositiveSmallIntegerField(gettext_lazy("slots bras g."), blank=True, null=True)
    slot_right_arm = models.PositiveSmallIntegerField(gettext_lazy("slots bras d."), blank=True, null=True)
    slot_torso = models.PositiveSmallIntegerField(gettext_lazy("slots torse"), blank=True, null=True)
    slot_left_leg = models.PositiveSmallIntegerField(gettext_lazy("slots jambe g."), blank=True, null=True)
    slot_right_leg = models.PositiveSmallIntegerField(gettext_lazy("slots jambe d."), blank=True, null=True)
    simplified = models.BooleanField(gettext_lazy("NPC Simplifié"), default=False) # If False, use NonPlayerCharacterAspect ; if True, use NonPlayerCharacterCharacteristic
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
        
    def get_colspan(self):
        counter = 1
        if (self.initiative):
            counter += 1
        if (self.health_points):
            counter += 1
        if (self.energy_points):
            counter += 1
        if (self.armour_points):
            counter += 1
        if (self.force_field):
            counter += 1
        if (self.cohesion):
            counter += 1
        if (self.outbreak):
            counter += 1
        if (self.speed):
            counter += 1
        return counter
        
    def has_slots(self):
        return (not self.simplified
            and (
                (self.slot_head is not None and self.slot_head > 0)
                or (self.slot_left_arm is not None and self.slot_left_arm > 0)
                or (self.slot_right_arm is not None and self.slot_right_arm > 0)
                or (self.slot_torso is not None and self.slot_torso > 0)
                or (self.slot_left_leg is not None and self.slot_left_leg > 0)
                or (self.slot_right_leg is not None and self.slot_right_leg > 0)
            )
        )
        
    def get_weapons(self):
        return self.weapons.order_by('weapon__category', 'weapon__translations__name').all()
        
    def get_modules(self):
        return self.modules.order_by('module_level__module__translations__name').all()

class NonPlayerCharacterAspectI18n(models.Model):
    npc = models.ForeignKey(NonPlayerCharacterI18n, on_delete=models.CASCADE, related_name="aspects", limit_choices_to={"simplified": False})
    aspect = models.ForeignKey(AspectI18n, on_delete=models.PROTECT, related_name="npcs")
    basic_value = models.SmallIntegerField(gettext_lazy("valeur de base"), default=1)
    except_value = models.PositiveSmallIntegerField(gettext_lazy("valeur exceptionnelle"), null=True, blank=True, default=0)
    except_major = models.BooleanField(gettext_lazy("aspect except. majeure"), default=False)
    
    def __str__(self):
        if self.except_value > 0:
            exceptional_name = gettext('Mineur')
            if self.except_major:
                exceptional_name = gettext('Majeur')
            return "%s - %s : %d (%s %d) " % (self.npc.name, self.aspect.name, self.basic_value, exceptional_name, self.except_value)
        else:
            return "%s - %s : %d " % (self.npc.name, self.aspect.name, self.basic_value)
    
    class Meta:
        unique_together = ("npc", "aspect")

class NonPlayerCharacterCharacteristicI18n(models.Model):
    npc = models.ForeignKey(NonPlayerCharacterI18n, on_delete=models.CASCADE, related_name="characteristics", limit_choices_to={"simplified": True})
    characteristic = models.ForeignKey(CharacteristicI18n, on_delete=models.PROTECT,verbose_name="caractéristique")
    test_dice = models.PositiveSmallIntegerField(gettext_lazy("dés de test"), default=0)
    overdrive_bonus = models.PositiveSmallIntegerField(gettext_lazy("bonus overdrive"), default=0)
    
    def __str__(self):
        return self.characteristic.safe_translation_getter('name', any_language=True)
    
    def get_test(self):
        result = str(self.test_dice) + 'D6'
        if self.overdrive_bonus > 0:
            result += ' + ' + str(self.overdrive_bonus) + 'OD'
        return result

class NonPlayerCharacterDifficultyI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)

class NonPlayerCharacterTypeI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64)
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)

class NonPlayerCharacterAbilityI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=128),
        description = MartorField(),
        precision = models.CharField(gettext_lazy("précision"), max_length=128, blank=True),
        slug = models.SlugField(blank=True),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    min_difficulty = models.ForeignKey(NonPlayerCharacterDifficultyI18n, on_delete=models.CASCADE, verbose_name=gettext_lazy("diff. minimum"))
    common = models.BooleanField(gettext_lazy("commune"), default=True)
    penalty = models.BooleanField(gettext_lazy("malus"), default=False)
    elite = models.BooleanField(gettext_lazy("élite"), default=False)
    human_allowed = models.BooleanField(gettext_lazy("compatible humain"), default=False)
    anathema_allowed = models.BooleanField(gettext_lazy("compatible anathème"), default=False)
    ether_allowed = models.BooleanField(gettext_lazy("compatible éther"), default=False)
    void_allowed = models.BooleanField(gettext_lazy("compatible vide"), default=False)
    types = models.ManyToManyField(NonPlayerCharacterTypeI18n, related_name="capacities")
    gest_spoiler = models.BooleanField(gettext_lazy('spoiler Geste ?'), default=False, blank=True)
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
    
    def get_absolute_url(self):
        return reverse('gm:npc_ability_detail', args=[self.slug])

class TraumaCategoryI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        description = models.TextField(),
    )
    hop_recovered = models.PositiveSmallIntegerField()
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)

class TraumaI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        description = MartorField(),
        slug = models.SlugField(null=True, blank=True),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    die_value = models.PositiveSmallIntegerField(gettext_lazy("valeur du dé"), blank=True, null=True)
    category = models.ForeignKey(TraumaCategoryI18n, on_delete=models.CASCADE, verbose_name=gettext_lazy("catégorie"), related_name="traumas")
    password = models.CharField(gettext_lazy("mot de passe"), max_length=256, blank=True, null=True)
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
    
    def get_absolute_url(self):
        return reverse('gm:trauma_detail', args=[self.slug])

class MechaArmourI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        description = MartorField(),
        slug = models.SlugField(null=True, blank=True),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    speed = models.PositiveSmallIntegerField(gettext_lazy("vitesse"), default=0)
    maneuverability = models.SmallIntegerField(gettext_lazy("manœuvrabilité"), default=0)
    power = models.PositiveSmallIntegerField(gettext_lazy("puissance"), default=0)
    sensors = models.PositiveSmallIntegerField(gettext_lazy("senseurs"), default=0)
    systems = models.PositiveSmallIntegerField(gettext_lazy("systèmes"), default=0)
    resilience = models.PositiveSmallIntegerField(gettext_lazy("résilience"), default=0)
    armour_plating = models.PositiveSmallIntegerField(gettext_lazy("blindage"), default=0)
    force_field = models.PositiveSmallIntegerField(gettext_lazy("champ de force"), default=0)
    energy_core = models.PositiveSmallIntegerField(gettext_lazy("noyau d'énergie"), default=100)
    password = models.CharField(gettext_lazy("mot de passe"), max_length=256, blank=True, null=True)
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
    
    def get_absolute_url(self):
        return reverse('gm:mecha_detail', args=[self.slug])

class MechaArmourConfigurationI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        description = MartorField(),
    )
    mecha = models.ForeignKey(MechaArmourI18n, on_delete=models.CASCADE, verbose_name=gettext_lazy("mécha-armure"), related_name="configurations")
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)

class StyleI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=128),
        description = MartorField(),
        bonus = MartorField(blank=True),
        malus = MartorField(blank=True),
        examples = MartorField(gettext_lazy("exemples"), blank=True),
        constraint = MartorField(gettext_lazy("Contrainte"), blank=True,default='-'),
        slug = models.SlugField(),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    fighter_available = models.BooleanField(gettext_lazy("dispo au contact"), blank=True, default=True)
    shooter_available = models.BooleanField(gettext_lazy("dispo au tir"), blank=True, default=True)
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def bonus_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('bonus', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def malus_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('malus', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def examples_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('examples', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def constraint_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('constraint', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
    
    def get_absolute_url(self):
        return reverse('gm:style_detail', args=[self.slug])

