from django.db import models
from django.urls import reverse
from django.utils.translation import (
    activate, deactivate, gettext_lazy, gettext, get_language
)

from parler.models import TranslatableModel, TranslatedFields
from martor.models import MartorField
# import gettext
import bleach
import re
import logging
from markdown import markdown

from .character import (Characteristic, Division, ArmourAbility,
    NonPlayerCharacter, Source, MechaArmour,
    MechaArmourConfiguration, Armour, Crest)

from .character import (CharacteristicI18n, DivisionI18n, ArmourAbilityI18n,
    NonPlayerCharacterI18n, SourceI18n, MechaArmourI18n,
    MechaArmourConfigurationI18n, ArmourI18n, CrestI18n)

from .faq import Question, QuestionI18n

class Activation(models.Model):
    name = models.CharField("nom", max_length=256)
    def __str__(self):
        return self.name

class Effect(models.Model):
    name = models.CharField("nom", max_length=256)
    description = MartorField()
    example = models.TextField("exemple",blank=True)
    short_description = models.TextField(default='')
    source = models.ForeignKey(Source, on_delete=models.CASCADE, related_name="effects")
    faq_corrected = models.BooleanField("Corrigé par FAQ", default=False)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    slug = models.SlugField(null=True, blank=True, unique=True)
    characteristic_damage_bonus = models.ForeignKey(Characteristic, on_delete=models.PROTECT, verbose_name='Carac. bonus dégâts', null=True, blank=True, related_name="effect_damage_bonus")
    characteristic_violence_bonus = models.ForeignKey(Characteristic, on_delete=models.PROTECT, verbose_name='Carac. bonus violence', null=True, blank=True, related_name="effect_violence_bonus")
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
    def origin(self):
        if self.faq_corrected:
            return '%s (corrigé par FAQ)' % self.source.name
        else:
            return self.source.name
            
    def get_absolute_url(self):
        return reverse('gm:effect_detail', args=[self.slug])
    
    class Meta:
        ordering = ['name']

class ModuleCategory(models.Model):
    name = models.CharField("nom", max_length=64)
    short_name = models.CharField("nom court", max_length=8, blank=True, default='')
    filterable = models.BooleanField("filtrable", blank=True, default=True)
    
    def __str__(self):
        return self.name

class WeaponCategory(models.Model):
    name = models.CharField("nom", max_length=64)
    def __str__(self):
        return self.name

    def get_src(self):
        if self.id == 1:
            return "army-knife"
        else:
            return "gun"

class Rarity(models.Model):
    name = models.CharField("nom", max_length=32)
    unlocked = models.PositiveSmallIntegerField("condition d'accès")
    def __str__(self):
        return self.name

class Reach(models.Model):
    name = models.CharField("nom", max_length=32)
    short_description = models.TextField("description courte", max_length=32, blank=True, null=True)
    description = models.TextField()
    def __str__(self):
        return self.name

class TitanReach(models.Model):
    name = models.CharField("nom", max_length=32)
    short_description = models.TextField("description courte", max_length=32, blank=True, null=True)
    description = models.TextField()
    def __str__(self):
        return self.name

class Enhancement(models.Model):
    name = models.CharField("nom", max_length=64)
    description = MartorField(blank=True)
    cost = models.PositiveSmallIntegerField("prix")
    effect_desc = models.CharField("Description effet (DEPRECATED)", max_length=512, blank=True)
    restriction = models.CharField(max_length=512)
    category = models.ForeignKey(WeaponCategory,on_delete=models.PROTECT,verbose_name="catégorie")
    group = models.CharField(max_length=2,choices=(
        ('CS', 'Structurelle'),
        ('CO', 'Ornementale')
    ), blank=True, default='')
    source = models.ForeignKey(Source, on_delete=models.CASCADE, related_name="enhancements")
    faq_corrected = models.BooleanField("Corrigé par FAQ", default=False)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    slug = models.SlugField(null=True, blank=True, unique=True)
    
    def __str__(self):
        if self.group:
            return '[%s] %s - %s' % (self.get_group_display(), self.category.name, self.name)
        
        return '%s - %s' % (self.category.name, self.name)
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
    def origin(self):
        if self.faq_corrected:
            return '%s (corrigé par FAQ)' % self.source.name
        else:
            return self.source.name
            
    def get_absolute_url(self):
        return reverse('gm:enhancement_detail', args=[self.slug])
    
    class Meta:
        ordering = ['category', 'group', 'name']

class Module(models.Model):
    name = models.CharField("nom", max_length=64)
    gear = models.CharField("équipement", max_length=256, blank=True)
    category = models.ForeignKey(ModuleCategory, on_delete=models.PROTECT, verbose_name="catégorie", related_name="modules")
    source = models.ForeignKey(Source, on_delete=models.CASCADE, related_name="modules")
    faq_corrected = models.BooleanField("Corrigé par FAQ", default=False)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    slug = models.SlugField(null=True, blank=True, unique=True)
    enhancements = models.ManyToManyField(
        Enhancement,
        verbose_name="améliorations",
        related_name="modules",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
    def origin(self):
        if self.faq_corrected:
            return '%s (corrigé par FAQ)' % self.source.name
        else:
            return self.source.name
            
    def get_absolute_url(self):
        return reverse('gm:module_detail', args=[self.slug])

class ModuleLevel(models.Model):
    module = models.ForeignKey(Module,on_delete=models.PROTECT, related_name="levels")
    level = models.PositiveSmallIntegerField("niveau")
    description = MartorField()
    rarity = models.ForeignKey(Rarity,on_delete=models.PROTECT,verbose_name="rareté")
    cost = models.PositiveSmallIntegerField("prix")
    activation = models.ForeignKey(Activation,on_delete=models.PROTECT, blank=True, null=True)
    duration = models.CharField("durée", max_length=128)
    damage_dice = models.SmallIntegerField("dés dégât")
    damage_bonus = models.SmallIntegerField("dégât bonus")
    characteristic_damage_bonus = models.ForeignKey(Characteristic, on_delete=models.PROTECT, verbose_name='Carac. bonus dégâts', null=True, blank=True, related_name="module_level_damage_bonus")
    characteristic_violence_bonus = models.ForeignKey(Characteristic, on_delete=models.PROTECT, verbose_name='Carac. bonus violence', null=True, blank=True, related_name="module_level_violence_bonus")
    violence_dice = models.SmallIntegerField("dés violence")
    violence_bonus = models.SmallIntegerField("violence bonus")
    reach = models.ForeignKey(Reach,on_delete=models.PROTECT,verbose_name="portée", blank=True, null=True)
    energy = models.SmallIntegerField("énergie")
    npcs = models.ManyToManyField(NonPlayerCharacter, related_name="modules_source", blank=True)
    
    def __str__(self):
        if self.module.levels.count() > 1:
            return self.module.name + ' niv.' + str(self.level)
        else:
            return self.module.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
        
    def get_damage(self):
        val = ''
        if self.damage_dice > 0:
            val = str(self.damage_dice) + 'D6 (' + str(self.damage_dice * 3) + ')'
        if self.damage_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.damage_bonus)
        
        if self.characteristic_damage_bonus:
            if val != '':
                val = val + ' + '
            
            val = val + self.characteristic_damage_bonus.name
        
        for fx in self.effects.all():
            if fx.choice_number == 0 and fx.effect and fx.effect.characteristic_damage_bonus:
                if val != '':
                    val = val + ' + '
                
                val = "%s %s (par %s)" % (val, fx.effect.characteristic_damage_bonus.name, fx.effect.name)
        
        return val
    
    def get_violence(self):
        val = ''
        if self.violence_dice > 0:
            val = str(self.violence_dice) + 'D6 (' + str(self.violence_dice * 3) + ')'
        if self.violence_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.violence_bonus)
        
        if self.characteristic_violence_bonus:
            if val != '':
                val = val + ' + '
            
            val = val + self.characteristic_violence_bonus.name
        
        for fx in self.effects.all():
            if fx.choice_number == 0 and fx.effect and fx.effect.characteristic_violence_bonus:
                if val != '':
                    val = val + ' + '
                
                val = "%s %s (par %s)" % (val, fx.effect.characteristic_violence_bonus.name, fx.effect.name)
        
        return val
        
    class Meta:
        unique_together = ("module", "level")

class ModuleLevelEffect(models.Model):
    module_level = models.ForeignKey(ModuleLevel,on_delete=models.CASCADE, related_name="effects")
    choice_number = models.PositiveSmallIntegerField("choix")
    effect = models.ForeignKey(Effect,on_delete=models.PROTECT,verbose_name="effet", related_name="moduleleveleffects", blank=True, null=True)
    effect_level = models.SmallIntegerField("niv. effet")
    damage = models.SmallIntegerField("dégâts")
    violence = models.SmallIntegerField()
    forced_by_previous_level = models.SmallIntegerField("forcé par niveau précédent")
    effect_condition = models.CharField("condition", max_length=128, blank=True, default='')
    
    def __str__(self):
        return str(self.module_level) + self.get_title()
    
    def get_title(self):
        damage_text = gettext('Dégâts')
        violence_text = gettext('Violence')
        
        if self.damage != 0:
            if self.damage > 0:
                last_part = ('%s +%dD6' % (damage_text, self.damage))
            else:
                last_part = ('%s %dD6' % (damage_text, self.damage))
        elif self.violence != 0:
            if self.violence > 0:
                last_part = ('%s +%dD6' % (violence_text, self.violence))
            else:
                last_part = ('%s %dD6' % (violence_text, self.violence))
        else:
            last_part = ' ' + self.get_name()
        return last_part
    
    def get_name(self):
        if self.effect is not None:
            if self.effect_level > 0:
                return self.effect.name.replace(' X', ' (' + str(self.effect_level) + ')')
            else:
                return self.effect.name
        else:
            return ''
    
    def get_description(self):
        if self.effect is not None:
            if self.effect_level > 0:
                return self.effect.short_description.replace('$X', str(self.effect_level))
            else:
                return self.effect.short_description
        else:
            return ''
    
    class Meta:
        unique_together = ("module_level", "choice_number", "effect", "damage", "violence", "effect_condition")

class ModuleSlot(models.Model):
    module = models.ForeignKey(Module,on_delete=models.PROTECT, related_name="slots")
    head = models.SmallIntegerField("tête")
    left_arm = models.SmallIntegerField("bras g.")
    right_arm = models.SmallIntegerField("bras d.")
    torso = models.SmallIntegerField("torse")
    left_leg = models.SmallIntegerField("jambe g.")
    right_leg = models.SmallIntegerField("jambe d.")
    
    def __str__(self):
        return 'H' + str(self.head) + ' LA' + str(self.left_arm) + ' RA' + str(self.right_arm) + ' T' + str(self.torso) + ' LL' + str(self.left_leg) + ' RL' + str(self.right_leg)
    
    class Meta:
        unique_together = ("module", "head", "left_arm", "right_arm", "torso", "left_leg", "right_leg")

class Overdrive(models.Model):
    characteristic = models.ForeignKey(Characteristic,on_delete=models.PROTECT,verbose_name="caractéristique", related_name="overdrives")
    level = models.PositiveSmallIntegerField("niveau")
    description = MartorField(blank=True)
    rarity = models.ForeignKey(Rarity,on_delete=models.PROTECT,verbose_name="rareté")
    cost = models.PositiveSmallIntegerField("prix")
    slug = models.SlugField(null=True, blank=True, unique=True)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.full_name()
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
    def full_name(self):
        return self.characteristic.name + ' niv. ' + str(self.level)
            
    def get_absolute_url(self):
        return reverse('gm:overdrive_detail', args=[self.slug])
    
    class Meta:
        unique_together = ("characteristic", "level")

class EnhancementEffect(models.Model):
    enhancement = models.ForeignKey(Enhancement, on_delete=models.CASCADE, verbose_name="amélioration", related_name="effects")
    effect = models.ForeignKey(Effect, on_delete=models.PROTECT, verbose_name="effet", blank=True, null=True, related_name="enhancements")
    effect_level = models.SmallIntegerField("niv. effet")
    damage_dice = models.SmallIntegerField("dés dégât",default=0)
    violence_dice = models.SmallIntegerField("dés violence",default=0)
    effect_condition = models.CharField("condition", max_length=128, blank=True)
    def __str__(self):
        return self.enhancement.name + ' ' + self.get_name()
    
    def get_name(self):
        if self.effect is not None:
            if self.effect_level != 0:
                return self.effect.name.replace(' X', ' (' + str(self.effect_level) + ')')
            else:
                return self.effect.name
        else:
            effects = []
            damage_text = gettext('Dégâts')
            violence_text = gettext('Violence')
            
            if self.damage_dice > 0:
                effects.append('%s +%dD6' % (damage_text, self.damage_dice))
            elif self.damage_dice < 0:
                effects.append('%s %dD6' % (damage_text, self.damage_dice))
                
            if self.violence_dice > 0:
                effects.append('%s +%dD6' % (violence_text, self.violence_dice))
            elif self.violence_dice < 0:
                effects.append('%s %dD6' % (violence_text, self.violence_dice))
            
            return ', '.join(effects)
    
    def get_description(self):
        if self.effect is not None:
            if self.effect_level > 0:
                return self.effect.short_description.replace('$X', str(self.effect_level))
            elif self.effect_level < 0:
                return self.effect.short_description.replace('+$X', str(self.effect_level)).replace('$X', str(self.effect_level))
            else:
                return self.effect.short_description
        else:
            return ''
    
    class Meta:
        unique_together = ("enhancement", "effect")

class Weapon(models.Model):
    name = models.CharField("nom", max_length=256)
    description = MartorField()
    nicknames = models.CharField("surnoms", max_length=256,blank=True)
    rarity = models.ForeignKey(Rarity,on_delete=models.PROTECT,verbose_name="rareté")
    category = models.ForeignKey(WeaponCategory,on_delete=models.PROTECT,verbose_name="catégorie",related_name="weapons")
    cost = models.PositiveSmallIntegerField("prix")
    enhancements = models.ManyToManyField(Enhancement,verbose_name="améliorations",blank=True)
    source = models.ForeignKey(Source, on_delete=models.CASCADE, related_name="weapons")
    faq_corrected = models.BooleanField("Corrigé par FAQ", default=False)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    slug = models.SlugField(null=True, blank=True, unique=True)
    player_enabled = models.BooleanField("accessible PJ", default=True)
    npc_enabled = models.BooleanField("accessible PNJ", default=True)
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
    def origin(self):
        if self.faq_corrected:
            return '%s (corrigé par FAQ)' % self.source.name
        else:
            return self.source.name
            
    def get_absolute_url(self):
        return reverse('gm:weapon_detail', args=[self.slug])

class WeaponAttack(models.Model):
    weapon = models.ForeignKey(Weapon,on_delete=models.CASCADE,verbose_name="arme", related_name="attacks")
    position = models.PositiveSmallIntegerField()
    name = models.CharField("nom", max_length=64)
    damage_dice = models.SmallIntegerField("dés dégât")
    damage_bonus = models.SmallIntegerField("dégât bonus")
    characteristic_damage_bonus = models.ForeignKey(Characteristic, on_delete=models.PROTECT, verbose_name="Carac. bonus dégâts", null=True, blank=True, related_name="weapon_attack_damage_bonus")
    violence_dice = models.SmallIntegerField("dés violence")
    violence_bonus = models.SmallIntegerField("violence bonus")
    characteristic_violence_bonus = models.ForeignKey(Characteristic, on_delete=models.PROTECT, verbose_name="Carac. bonus violence", null=True, blank=True, related_name="weapon_attack_violence_bonus")
    reach = models.ForeignKey(Reach,on_delete=models.PROTECT,verbose_name="portée")
    energy = models.SmallIntegerField("énergie")
    
    def __str__(self):
        return self.name
    
    def get_damage(self):
        val = ''
        if self.damage_dice > 0:
            val = str(self.damage_dice) + 'D6 (' + str(self.damage_dice * 3) + ')'
        if self.damage_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.damage_bonus)
            
        if self.characteristic_damage_bonus:
            if val != '':
                val = val + ' + '
            
            val = val + self.characteristic_damage_bonus.name
        
        for fx in self.effects.all():
            if fx.effect and fx.effect.characteristic_damage_bonus:
                if val != '':
                    val = val + ' + '
                
                val = "%s %s (par %s)" % (val, fx.effect.characteristic_damage_bonus.name, fx.effect.name)
        
        return val
    
    def get_violence(self):
        val = ''
        if self.violence_dice > 0:
            val = str(self.violence_dice) + 'D6 (' + str(self.violence_dice * 3) + ')'
        if self.violence_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.violence_bonus)
            
        if self.characteristic_violence_bonus:
            if val != '':
                val = val + ' + '
            
            val = val + self.characteristic_violence_bonus.name
        
        for fx in self.effects.all():
            if fx.effect and fx.effect.characteristic_violence_bonus:
                if val != '':
                    val = val + ' + '
                
                val = "%s %s (par %s)" % (val, fx.effect.characteristic_violence_bonus.name, fx.effect.name)
        
        return val
    
    def max_effect_energy(self):
        energy = 0
        
        for effect in self.effects.all():
            if effect.energy > energy:
                energy = effect.energy
        
        return energy
    
    class Meta:
        unique_together = ("weapon", "position")

class WeaponAttackEffect(models.Model):
    weapon_attack = models.ForeignKey(WeaponAttack,on_delete=models.CASCADE,verbose_name="attaque", related_name="effects")
    effect = models.ForeignKey(Effect, on_delete=models.CASCADE, verbose_name="effet", related_name="weaponattackeffects")
    effect_level = models.SmallIntegerField("niv. effet")
    energy = models.SmallIntegerField("énergie")
    effect_condition = models.CharField("condition", max_length=128, blank=True, default='')
    
    def __str__(self):
        return self.get_name()
    
    def get_name(self):
        if self.effect_level > 0:
            return self.effect.name.replace(' X', ' (' + str(self.effect_level) + ')')
        else:
            return self.effect.name
    
    def get_description(self):
        if self.effect_level > 0:
            return self.effect.short_description.replace('$X', str(self.effect_level))
        else:
            return self.effect.short_description
    
    class Meta:
        unique_together = ("weapon_attack", "effect", "effect_level", "energy")
        ordering = ['energy', 'effect__name']

class DivisionModule(models.Model):
    division = models.ForeignKey(Division, on_delete=models.CASCADE, related_name="modules")
    module_level = models.ForeignKey(ModuleLevel, on_delete=models.CASCADE, related_name="divisions")
    forced_choice_effect = models.ForeignKey(Effect, on_delete=models.CASCADE, blank=True, null=True) # Forced with division Ogre: alternate view predator (vue alternative prédatrice).
    
    def __str__(self):
        return self.division.name + ' ' + self.module_level.module.name + ''
    
    class Meta:
        unique_together = ("division", "module_level")

class DivisionWeapon(models.Model):
    division = models.ForeignKey(Division, on_delete=models.CASCADE, related_name="weapons")
    weapon = models.ForeignKey(Weapon, on_delete=models.CASCADE, related_name="divisions")
    
    def __str__(self):
        return self.division.name + ' ' + self.weapon.name
    
    class Meta:
        unique_together = ("division", "weapon")

class ArmourAbilityVariant(models.Model):
    armour_ability = models.ForeignKey(ArmourAbility, on_delete=models.CASCADE, verbose_name="capacité armure", related_name="variants")
    name = models.CharField("nom", max_length=64)
    description = MartorField()
    energy = models.CharField("énergie", max_length=128, blank=True) # yes, this is confusing, but energy in armour abilities are so conditionals...
    hope = models.CharField("espoir", max_length=128, null=True, blank=True) # here again, not an int but a char because it may vary depending on a lot of things...
    activation = models.CharField("activation", max_length=256, blank=True) # yes, also confusing, but again: activation can be very conditionals here.
    duration = models.CharField("durée", max_length=128, blank=True)
    damage_dice = models.SmallIntegerField("dés dégât", blank=True, null=True)
    damage_bonus = models.SmallIntegerField("dégât bonus", blank=True, null=True)
    violence_dice = models.SmallIntegerField("dés violence", blank=True, null=True)
    violence_bonus = models.SmallIntegerField("violence bonus", blank=True, null=True)
    reach = models.ForeignKey(Reach,on_delete=models.PROTECT,verbose_name="portée", blank=True, null=True)
    npc = models.ForeignKey(NonPlayerCharacter, on_delete=models.CASCADE, null=True, blank=True, limit_choices_to={"simplified": False})
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def get_damage(self):
        val = ''
        if self.damage_dice is not None and self.damage_dice > 0:
            val = str(self.damage_dice) + 'D6 (' + str(self.damage_dice * 3) + ')'
        if self.damage_bonus is not None and self.damage_bonus  > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.damage_bonus)
        return val
    
    def get_violence(self):
        val = ''
        if self.violence_dice is not None and self.violence_dice > 0:
            val = str(self.violence_dice) + 'D6 (' + str(self.violence_dice * 3) + ')'
        if self.violence_bonus is not None and self.violence_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.violence_bonus)
        return val

class NonPlayerCharacterWeapon(models.Model):
    weapon = models.ForeignKey(Weapon, on_delete=models.CASCADE, related_name="npcs", limit_choices_to={'npc_enabled': True})
    npc = models.ForeignKey(NonPlayerCharacter, on_delete=models.CASCADE, related_name="weapons")
    quantity = models.PositiveSmallIntegerField("quantité", default=1)
    
    def __str__(self):
        return "%s - %s (×%d)" % (self.npc.name, self.weapon.name, self.quantity)
    
    class Meta:
        unique_together = ("weapon", "npc")

class NonPlayerCharacterModuleLevel(models.Model):
    module_level = models.ForeignKey(ModuleLevel, on_delete=models.CASCADE, related_name="owning_npcs")
    npc = models.ForeignKey(NonPlayerCharacter, on_delete=models.CASCADE, related_name="modules")
    
    def __str__(self):
        return "%s %s (niv. %d)" % (self.npc.name, self.module_level.module.name, self.module_level.level)
    
    class Meta:
        unique_together = ("module_level", "npc")

class HeroicSkillCategory(models.Model):
    name = models.CharField("nom", max_length=64)
    def __str__(self):
        return self.name

class HeroicSkill(models.Model):
    name = models.CharField("nom", max_length=64)
    description = MartorField()
    xp_cost = models.PositiveSmallIntegerField("acquisition (XP)")
    category = models.ForeignKey(HeroicSkillCategory, on_delete=models.CASCADE, related_name="skills")
    activation = models.ForeignKey(Activation,on_delete=models.PROTECT)
    duration = models.CharField("durée", max_length=128)
    heroic_cost = models.PositiveSmallIntegerField("coût (héroïsme)")
    source = models.ForeignKey(Source, on_delete=models.CASCADE, related_name="heroicskills")
    faq_corrected = models.BooleanField("Corrigé par FAQ", default=False)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    slug = models.SlugField(unique=True)
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
    def origin(self):
        if self.faq_corrected:
            return '%s (corrigé par FAQ)' % self.source.name
        else:
            return self.source.name
            
    def get_absolute_url(self):
        return reverse('gm:skill_detail', args=[self.slug])

class Vehicle(models.Model):
    name = models.CharField("nom", max_length=64)
    description = MartorField(blank=True)
    armour_points = models.PositiveSmallIntegerField("points d'armure (pa)", default=0)
    energy_points = models.PositiveSmallIntegerField("points d'énergie (pe)", default=0)
    force_field = models.PositiveSmallIntegerField("champ de force (cdf)",default=0)
    maneuverability = models.SmallIntegerField("manœuvrabilité", default=0)
    speed = models.TextField("vitesse", max_length=64)
    crew =  models.TextField("équipage", max_length=64)
    weaponry = MartorField(blank=True)
    slug = models.SlugField(null=True, blank=True, unique=True)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def crew_order(self):
        for elem in self.crew.split():
            if elem.isnumeric():
                return int(elem)
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def weaponry_text_only(self):
        return bleach.clean(markdown(self.weaponry), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
            
    def get_absolute_url(self):
        return reverse('gm:vehicle_detail', args=[self.slug])

class ModuleVehicle(models.Model):
    module = models.ForeignKey(Module, on_delete=models.CASCADE, related_name='vehicles')
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE, related_name='modules')

class MechaArmourAction(models.Model):
    name = models.CharField("nom", max_length=64)
    description = MartorField()
    activation = models.CharField("activation", max_length=256, blank=True) # see armourabilityvariant activation for explanation of the non-ForeignKey.
    core = models.CharField("noyaux", max_length=128, blank=True)
    duration = models.CharField("durée", max_length=256, blank=True)
    reach = models.ForeignKey(TitanReach,on_delete=models.PROTECT, verbose_name="portée", blank=True, null=True)
    damage_dice = models.SmallIntegerField("dés dégât", blank=True, null=True)
    damage_bonus = models.SmallIntegerField("dégât bonus", blank=True, null=True)
    violence_dice = models.SmallIntegerField("dés violence", blank=True, null=True)
    violence_bonus = models.SmallIntegerField("violence bonus", blank=True, null=True)
    effects = MartorField(blank=True) # some fulltext effect exists, so we'll use the power of markdown to link effects when needed...
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
        
    def effects_text_only(self):
        return bleach.clean(markdown(self.effects), tags=[], strip=True)
        
    def get_damage(self):
        val = ''
        if self.damage_dice is not None and self.damage_dice > 0:
            val = str(self.damage_dice) + 'D6 (' + str(self.damage_dice * 3) + ')'
        if self.damage_bonus is not None and self.damage_bonus  > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.damage_bonus)
        return val
    
    def get_violence(self):
        val = ''
        if self.violence_dice is not None and self.violence_dice > 0:
            val = str(self.violence_dice) + 'D6 (' + str(self.violence_dice * 3) + ')'
        if self.violence_bonus is not None and self.violence_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.violence_bonus)
        return val
    
class MechaArmourActionCommon(MechaArmourAction):
    mecha = models.ForeignKey(MechaArmour, on_delete=models.CASCADE, verbose_name="mécha-armure", related_name="actions", blank=True, null=True) # Either mecha or configuration must be set, but not both
    
class MechaArmourActionConfiguration(MechaArmourAction):
    configuration = models.ForeignKey(MechaArmourConfiguration, on_delete=models.CASCADE, related_name="actions", blank=True, null=True) # Either mecha or configuration must be set, but not both
    
class UltimateAptitudeType(models.Model):
    name = models.CharField("nom", max_length=16)
    
    def __str__(self):
        return self.name

class UltimateAptitude(models.Model):
    name = models.CharField("nom", max_length=128)
    type = models.ForeignKey(UltimateAptitudeType, on_delete=models.CASCADE, null=True, blank=True)
    restriction = models.CharField(max_length=512, null=True, blank=True)
    quote = models.CharField(max_length=512)
    description = MartorField()
    cost = models.CharField("coût", max_length=128, null=True, blank=True) # because energy can also contain heroic points costs...
    activation = models.CharField("activation", max_length=256, null=True, blank=True)
    duration = models.CharField("durée", max_length=128, null=True, blank=True)
    reach = models.ForeignKey(Reach, on_delete=models.PROTECT, verbose_name="portée", null=True, blank=True)
    armour = models.ForeignKey(Armour, on_delete=models.CASCADE, verbose_name="armure", null=True, blank=True)
    crest = models.ForeignKey(Crest, on_delete=models.CASCADE, verbose_name="blason", null=True, blank=True)
    slug = models.SlugField(null=True, blank=True, unique=True)
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)

class ActivationI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=256),
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)

class EffectI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=256),
        description = MartorField(),
        example = models.TextField(gettext_lazy("exemple"), blank=True),
        short_description = models.TextField(default='', blank=True),
        slug = models.SlugField(null=True, blank=True),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    
    source = models.ForeignKey(SourceI18n, on_delete=models.CASCADE, related_name="effects")
    faq_corrected = models.BooleanField(gettext_lazy("corrigé par FAQ"), default=False)
    characteristic_damage_bonus = models.ForeignKey(CharacteristicI18n, on_delete=models.PROTECT, verbose_name=gettext_lazy('Carac. bonus dégâts'), null=True, blank=True, related_name="effect_damage_bonus")
    characteristic_violence_bonus = models.ForeignKey(CharacteristicI18n, on_delete=models.PROTECT, verbose_name=gettext_lazy('Carac. bonus violence'), null=True, blank=True, related_name="effect_violence_bonus")
    calculator_behaviour = models.CharField(max_length=3,choices=(
        ('BST', gettext_lazy('Boost')), # Create a special form to manage boost
        ('RCH', gettext_lazy('Reach')), # Create a special form to increase reach
    ), null=True, blank=True)
    
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name=gettext_lazy("faq"),
        blank=True,
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def origin(self):
        if self.faq_corrected:
            return '%s (%s)' % (self.source.safe_translation_getter('name', any_language=True), gettext("corrigé par FAQ"))
        else:
            return self.source.safe_translation_getter('name', any_language=True)
    
    def get_calculator_name(self):
        # Supprimer explicitement " X" à la fin si elle est présente
        text = re.sub(r' X$', '', self.name)
        # Utiliser une expression régulière pour capturer le contenu entre crochets
        match = re.match(r'^\[(.*?)\]\s?(.*)$', text)
        if match:
            return match.group(1).strip() + (f' {match.group(2).strip()}' if match.group(2) else '')
        return text.strip()
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
    
    def get_absolute_url(self):
        return reverse('gm:effect_detail', args=[self.slug])
    
    class Meta:
        ordering = ['translations__name']

class ModuleCategoryI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        short_name = models.CharField(gettext_lazy("nom court"), max_length=8, blank=True, default=''),
    )
    
    filterable = models.BooleanField(gettext_lazy("filtrable"), blank=True, default=True)
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)

class WeaponCategoryI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64)
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def get_src(self):
        if self.id == 1:
            return "army-knife"
        else:
            return "gun"

class RarityI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=32)
    )
    
    unlocked = models.PositiveSmallIntegerField(gettext_lazy("condition d'accès"))
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)

class ReachI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=32),
        short_description = models.TextField(gettext_lazy("description courte"), max_length=32, blank=True, null=True),
        description = models.TextField(),
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)

class TitanReachI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=32),
        short_description = models.TextField(gettext_lazy("description courte"), max_length=32, blank=True, null=True),
        description = models.TextField(),
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)

class EnhancementI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        description = MartorField(blank=True),
        restriction = models.CharField(blank=True, max_length=512),
        slug = models.SlugField(null=True, blank=True),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    
    category = models.ForeignKey(WeaponCategoryI18n, on_delete=models.PROTECT, verbose_name=gettext_lazy("catégorie"))
    group = models.CharField(max_length=2,choices=(
        ('CS', gettext_lazy('Structurelle')),
        ('CO', gettext_lazy('Ornementale'))
    ), blank=True, default='')
    cost = models.PositiveSmallIntegerField(gettext_lazy("prix"))
    source = models.ForeignKey(SourceI18n, on_delete=models.CASCADE, related_name="enhancements")
    faq_corrected = models.BooleanField(gettext_lazy("corrigé par FAQ"), default=False)
    
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name=gettext_lazy("faq"),
        blank=True,
    )
    
    def __str__(self):
        if self.group:
            return '[%s] %s - %s' % (self.get_group_display(), self.category.safe_translation_getter('name', any_language=True), self.safe_translation_getter('name', any_language=True))
        
        return '%s - %s' % (self.category.safe_translation_getter('name', any_language=True), self.safe_translation_getter('name', any_language=True))
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def origin(self):
        if self.faq_corrected:
            return '%s (%s)' % (self.source.safe_translation_getter('name', any_language=True), gettext("corrigé par FAQ"))
        else:
            return self.source.safe_translation_getter('name', any_language=True)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
    
    def get_absolute_url(self):
        return reverse('gm:enhancement_detail', args=[self.slug])
    
    class Meta:
        ordering = ['category', 'group', 'translations__name']

class ModuleI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        gear = models.CharField(gettext_lazy("équipement"), max_length=256, blank=True),
        slug = models.SlugField(null=True, blank=True),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    
    category = models.ForeignKey(ModuleCategoryI18n, on_delete=models.PROTECT, verbose_name=gettext_lazy("catégorie"), related_name="modules")
    source = models.ForeignKey(SourceI18n, on_delete=models.CASCADE, related_name="modules")
    faq_corrected = models.BooleanField(gettext_lazy("corrigé par FAQ"), default=False)
    
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name=gettext_lazy("faq"),
        blank=True,
    )
    enhancements = models.ManyToManyField(
        EnhancementI18n,
        verbose_name=gettext_lazy("améliorations"),
        related_name="modules",
        blank=True,
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def origin(self):
        if self.faq_corrected:
            return '%s (%s)' % (self.source.safe_translation_getter('name', any_language=True), gettext("corrigé par FAQ"))
        else:
            return self.source.safe_translation_getter('name', any_language=True)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
    
    def get_absolute_url(self):
        return reverse('gm:module_detail', args=[self.slug])

class ModuleLevelI18n(TranslatableModel):
    translations = TranslatedFields(
        description = MartorField(),
        duration = models.CharField(gettext_lazy("durée"), max_length=128)
    )
    
    module = models.ForeignKey(ModuleI18n,on_delete=models.PROTECT, related_name="levels")
    level = models.PositiveSmallIntegerField(gettext_lazy("niveau"))
    rarity = models.ForeignKey(RarityI18n,on_delete=models.PROTECT,verbose_name=gettext_lazy("rareté"))
    cost = models.PositiveSmallIntegerField(gettext_lazy("prix"))
    activation = models.ForeignKey(ActivationI18n,on_delete=models.PROTECT, blank=True, null=True)
    damage_dice = models.SmallIntegerField(gettext_lazy("dés dégât"))
    damage_bonus = models.SmallIntegerField(gettext_lazy("dégât bonus"))
    characteristic_damage_bonus = models.ForeignKey(CharacteristicI18n, on_delete=models.PROTECT, verbose_name='Carac. bonus dégâts', null=True, blank=True, related_name="module_level_damage_bonus")
    characteristic_violence_bonus = models.ForeignKey(CharacteristicI18n, on_delete=models.PROTECT, verbose_name='Carac. bonus violence', null=True, blank=True, related_name="module_level_violence_bonus")
    violence_dice = models.SmallIntegerField(gettext_lazy("dés violence"))
    violence_bonus = models.SmallIntegerField(gettext_lazy("violence bonus"))
    reach = models.ForeignKey(ReachI18n,on_delete=models.PROTECT,verbose_name=gettext_lazy("portée"), blank=True, null=True)
    energy = models.SmallIntegerField(gettext_lazy("énergie"))
    
    npcs = models.ManyToManyField(
        NonPlayerCharacterI18n,
        related_name="modules_source",
        blank=True
    )
    
    def __str__(self):
        if self.module.levels.count() > 1:
            return "%s %s%d" % (self.module.safe_translation_getter('name', any_language=True), gettext('niv.'), self.level)
        else:
            return self.module.safe_translation_getter('name', any_language=True)
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)
        
    def get_damage(self):
        language = get_language()
        val = ''
        if self.damage_dice > 0:
            val = str(self.damage_dice) + 'D6 (' + str(self.damage_dice * 3) + ')'
        if self.damage_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.damage_bonus)
        
        if self.characteristic_damage_bonus:
            if val != '':
                val = val + ' + '
            
            val = val + self.characteristic_damage_bonus.safe_translation_getter('name', any_language=True)
        
        effects = (
            self
            .effects
            .filter(
                translations__language_code=language,
                effect__translations__language_code=language,
                module_level__translations__language_code=language,
                module_level__module__translations__language_code=language
            )
            .all()
        )
        
        for fx in effects:
            if fx.choice_number == 0 and fx.effect and fx.effect.characteristic_damage_bonus:
                if val != '':
                    val = val + ' + '
                
                val = "%s %s (%s)" % (val, fx.effect.characteristic_damage_bonus.safe_translation_getter('name', any_language=True), fx.effect.safe_translation_getter('name', any_language=True))
        
        return val
    
    def get_violence(self):
        language = get_language()
        val = ''
        if self.violence_dice > 0:
            val = str(self.violence_dice) + 'D6 (' + str(self.violence_dice * 3) + ')'
        if self.violence_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.violence_bonus)
        
        if self.characteristic_violence_bonus:
            if val != '':
                val = val + ' + '
            
            val = val + self.characteristic_violence_bonus.safe_translation_getter('name', any_language=True)
        
        effects = (
            self
            .effects
            .filter(
                translations__language_code=language,
                effect__translations__language_code=language,
                module_level__translations__language_code=language,
                module_level__module__translations__language_code=language
            )
            .all()
        )
        
        for fx in effects:
            if fx.choice_number == 0 and fx.effect and fx.effect.characteristic_violence_bonus:
                if val != '':
                    val = val + ' + '
                
                val = "%s %s (%s)" % (val, fx.effect.characteristic_violence_bonus.safe_translation_getter('name', any_language=True), fx.effect.safe_translation_getter('name', any_language=True))
        
        return val
    
    class Meta:
        unique_together = ("module", "level")

class ModuleLevelEffectI18n(TranslatableModel):
    translations = TranslatedFields(
        effect_condition = models.CharField(gettext_lazy("condition"), max_length=128, blank=True, default='')
    )
    
    module_level = models.ForeignKey(ModuleLevelI18n,on_delete=models.CASCADE, related_name="effects")
    choice_number = models.PositiveSmallIntegerField(gettext_lazy("choix"))
    effect = models.ForeignKey(EffectI18n,on_delete=models.PROTECT,verbose_name=gettext_lazy("effet"), related_name="moduleleveleffects", blank=True, null=True)
    effect_level = models.SmallIntegerField(gettext_lazy("niv. effet"))
    damage = models.SmallIntegerField(gettext_lazy("dégâts"))
    violence = models.SmallIntegerField()
    forced_by_previous_level = models.SmallIntegerField(gettext_lazy("forcé par niveau précédent"))
    
    def __str__(self):
        return str(self.module_level) + self.get_title()
    
    def get_title(self, lang_code=None):
        if lang_code:
            activate(lang_code)
        
        damage_text = gettext('Dégâts')
        violence_text = gettext('Violence')
        
        if self.damage != 0:
            if self.damage > 0:
                last_part = ('%s +%dD6' % (damage_text, self.damage))
            else:
                last_part = ('%s %dD6' % (damage_text, self.damage))
        elif self.violence != 0:
            if self.violence > 0:
                last_part = ('%s +%dD6' % (violence_text, self.violence))
            else:
                last_part = ('%s %dD6' % (violence_text, self.violence))
        else:
            last_part = self.get_name(lang_code=None)
        
        if lang_code:
            deactivate()
        return last_part
    
    def get_name(self, lang_code=None):
        if self.effect is not None:
            if lang_code:
                effect_name = self.effect.safe_translation_getter('name', language_code=lang_code, any_language=True)
            else:
                effect_name = self.effect.safe_translation_getter('name', any_language=True)
            
            if self.effect_level > 0:
                return effect_name.replace(' X', ' (' + str(self.effect_level) + ')')
            else:
                return effect_name
        else:
            return ''
    
    def get_description(self):
        if self.effect is not None:
            if self.effect_level > 0:
                return self.effect.safe_translation_getter('short_description', any_language=True).replace('$X', str(self.effect_level))
            else:
                return self.effect.safe_translation_getter('short_description', any_language=True)
        else:
            return ''
    
    class Meta:
    #     unique_together = ("module_level", "choice_number", "effect", "damage", "violence", "effect_condition")
        ordering = ['module_level__module__translations__name', 'module_level__level', 'choice_number', 'effect__translations__name']

class ModuleSlotI18n(models.Model):
    module = models.ForeignKey(ModuleI18n,on_delete=models.PROTECT, related_name="slots")
    
    head = models.SmallIntegerField(gettext_lazy("tête"))
    left_arm = models.SmallIntegerField(gettext_lazy("bras g."))
    right_arm = models.SmallIntegerField(gettext_lazy("bras d."))
    torso = models.SmallIntegerField(gettext_lazy("torse"))
    left_leg = models.SmallIntegerField(gettext_lazy("jambe g."))
    right_leg = models.SmallIntegerField(gettext_lazy("jambe d."))
    
    def __str__(self):
        return "%d/%d/%d/%d/%d/%d" % (self.head, self.left_arm, self.right_arm, self.torso, self.left_leg, self.right_leg)
    
    class Meta:
        unique_together = ("module", "head", "left_arm", "right_arm", "torso", "left_leg", "right_leg")

class OverdriveI18n(TranslatableModel):
    translations = TranslatedFields(
        description = MartorField(blank=True),
        slug = models.SlugField(null=True, blank=True),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    
    characteristic = models.ForeignKey(CharacteristicI18n,on_delete=models.PROTECT,verbose_name=gettext_lazy("caractéristique"), related_name="overdrives")
    level = models.PositiveSmallIntegerField(gettext_lazy("niveau"))
    rarity = models.ForeignKey(RarityI18n,on_delete=models.PROTECT,verbose_name=gettext_lazy("rareté"))
    cost = models.PositiveSmallIntegerField(gettext_lazy("prix"))
    
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name=gettext_lazy("faq"),
        blank=True,
    )
    
    def __str__(self):
        return self.full_name()
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def full_name(self, lang_code=None):
        if lang_code:
            activate(lang_code)
            charac = self.characteristic.safe_translation_getter('name', language_code=lang_code, any_language=True)
        else:
            charac = self.characteristic.safe_translation_getter('name', any_language=True)
            
        level = gettext('niv.')
        
        if lang_code:
            deactivate()
        return "%s %s %d" % (charac, level, self.level)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
            
    def get_absolute_url(self):
        return reverse('gm:overdrive_detail', args=[self.slug])
    
    class Meta:
        unique_together = ("characteristic", "level")

class EnhancementEffectI18n(TranslatableModel):
    translations = TranslatedFields(
        effect_condition = models.CharField(gettext_lazy("condition"), max_length=128, blank=True),
    )
    
    enhancement = models.ForeignKey(EnhancementI18n, on_delete=models.CASCADE, verbose_name=gettext_lazy("amélioration"), related_name="effects")
    effect = models.ForeignKey(EffectI18n, on_delete=models.PROTECT, verbose_name=gettext_lazy("effet"), blank=True, null=True, related_name="enhancements")
    effect_level = models.SmallIntegerField(gettext_lazy("niv. effet"))
    damage_dice = models.SmallIntegerField(gettext_lazy("dés dégât"),default=0)
    violence_dice = models.SmallIntegerField(gettext_lazy("dés violence"),default=0)
    
    def __str__(self):
        return "%s %s" % (self.enhancement.safe_translation_getter('name', any_language=True), self.get_name())
    
    def get_name(self, lang_code=None):
        if self.effect is not None:
            if lang_code:
                effect_name = self.effect.safe_translation_getter('name', language_code=lang_code, any_language=True)
            else:
                effect_name = self.effect.safe_translation_getter('name', any_language=True)
            
            if self.effect_level != 0:
                return effect_name.replace(' X', " (%d)" % (self.effect_level))
            else:
                return effect_name
        else:
            if lang_code:
                activate(lang_code)
            effects = []
            damage_text = gettext('Dégâts')
            violence_text = gettext('Violence')
            
            if self.damage_dice > 0:
                effects.append('%s +%dD6' % (damage_text, self.damage_dice))
            elif self.damage_dice < 0:
                effects.append('%s %dD6' % (damage_text, self.damage_dice))
                
            if self.violence_dice > 0:
                effects.append('%s +%dD6' % (violence_text, self.violence_dice))
            elif self.violence_dice < 0:
                effects.append('%s %dD6' % (violence_text, self.violence_dice))
            
            if lang_code:
                deactivate()
            return ', '.join(effects)
    
    def get_description(self, lang_code=None):
        if self.effect is not None:
            if lang_code:
                effect_desc = self.effect.safe_translation_getter('short_description', language_code=lang_code, any_language=True)
            else:
                effect_desc = self.effect.safe_translation_getter('short_description', any_language=True)
            
            if self.effect_level > 0:
                return effect_desc.replace('$X', str(self.effect_level))
            elif self.effect_level < 0:
                return effect_desc.replace('+$X', str(self.effect_level)).replace('$X', str(self.effect_level))
            else:
                return effect_desc
        else:
            return ''
    
    class Meta:
        unique_together = ("enhancement", "effect")

class WeaponI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=256),
        description = MartorField(),
        nicknames = models.CharField(gettext_lazy("surnoms"), max_length=256,blank=True),
        slug = models.SlugField(null=True, blank=True),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    
    rarity = models.ForeignKey(RarityI18n, on_delete=models.PROTECT, verbose_name=gettext_lazy("rareté"))
    category = models.ForeignKey(WeaponCategoryI18n, on_delete=models.PROTECT, verbose_name=gettext_lazy("catégorie"), related_name="weapons")
    cost = models.PositiveSmallIntegerField(gettext_lazy("prix"))
    source = models.ForeignKey(SourceI18n, on_delete=models.CASCADE, related_name="weapons")
    faq_corrected = models.BooleanField(gettext_lazy("Corrigé par FAQ"), default=False)
    player_enabled = models.BooleanField(gettext_lazy("accessible PJ"), default=True)
    npc_enabled = models.BooleanField(gettext_lazy("accessible PNJ"), default=True)
    
    calculator = models.BooleanField(gettext_lazy("calculateur"), blank=True, default=False)
    calculator_max_effects_by_list = models.SmallIntegerField(gettext_lazy("nb max effets par liste calculateur"), null=True, blank=True)
    
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name=gettext_lazy("faq"),
        blank=True,
    )
    enhancements = models.ManyToManyField(
        EnhancementI18n,
        verbose_name=gettext_lazy("améliorations"),
        related_name="weapons",
        blank=True
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def origin(self):
        if self.faq_corrected:
            return '%s (%s)' % (self.source.safe_translation_getter('name', any_language=True), gettext_lazy("corrigé par FAQ"))
        else:
            return self.source.safe_translation_getter('name', any_language=True)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
    
    def get_absolute_url(self):
        return reverse('gm:weapon_detail', args=[self.slug])

class WeaponAttackI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        calculator_name = models.CharField(gettext_lazy("nom calculateur"), max_length=64, null=True, blank=True),
    )
    
    weapon = models.ForeignKey(WeaponI18n,on_delete=models.CASCADE,verbose_name=gettext_lazy("arme"), related_name="attacks")
    position = models.PositiveSmallIntegerField()
    damage_dice = models.SmallIntegerField(gettext_lazy("dés dégât"))
    damage_bonus = models.SmallIntegerField(gettext_lazy("dégât bonus"))
    characteristic_damage_bonus = models.ForeignKey(CharacteristicI18n, on_delete=models.PROTECT, verbose_name=gettext_lazy("Carac. bonus dégâts"), null=True, blank=True, related_name="weapon_attack_damage_bonus")
    violence_dice = models.SmallIntegerField(gettext_lazy("dés violence"))
    violence_bonus = models.SmallIntegerField(gettext_lazy("violence bonus"))
    characteristic_violence_bonus = models.ForeignKey(CharacteristicI18n, on_delete=models.PROTECT, verbose_name=gettext_lazy("Carac. bonus violence"), null=True, blank=True, related_name="weapon_attack_violence_bonus")
    reach = models.ForeignKey(ReachI18n,on_delete=models.PROTECT,verbose_name=gettext_lazy("portée"))
    energy = models.SmallIntegerField(gettext_lazy("énergie"))
    calculator_order_appliance = models.SmallIntegerField(gettext_lazy("ordre d'application en calculateur"), help_text=gettext_lazy("Donne l'ordre d'application lorsque l'arme est utilisée dans le calculateur. Plus le chiffre est gros, plus l'effet de cette attaque sera appliquée tard dans la pile des modifications apportées par le calculateur."), null=True, blank=True)
    calculator_behaviour = models.CharField(max_length=3,choices=(
        ('INI', gettext_lazy('Initial')),
        ('REP', gettext_lazy('Remplacement')),
        ('LST', gettext_lazy('Liste')),
        ('REM', gettext_lazy('Suppression')),
        ('RED', gettext_lazy('Réduction')),
    ), null=True, blank=True)
    calculator_energy_reduction = models.SmallIntegerField(gettext_lazy("réduction PE en calculateur"), null=True, blank=True)
    calculator_energy_min = models.SmallIntegerField(gettext_lazy("minimum PE en calculateur"), null=True, blank=True)
    calculator_min_energy_list = models.SmallIntegerField(gettext_lazy("minimum d'énergie liste calculateur"), help_text=gettext_lazy("Niveau minimal de coût en énergie (de base) pour que l'effet soit considéré dans une liste."), null=True, blank=True)
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def get_damage(self):
        language = get_language()
        val = ''
        if self.damage_dice > 0:
            val = str(self.damage_dice) + 'D6 (' + str(self.damage_dice * 3) + ')'
        if self.damage_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.damage_bonus)
            
        if self.characteristic_damage_bonus:
            if val != '':
                val = val + ' + '
            
            val = val + self.characteristic_damage_bonus.safe_translation_getter('name', any_language=True)
        
        effects = (
            self
            .effects
            .filter(
                translations__language_code=language,
                effect__translations__language_code=language,
                weapon_attack__translations__language_code=language
            )
            .all()
        )
        
        for fx in effects:
            if fx.effect and fx.effect.characteristic_damage_bonus:
                if val != '':
                    val = val + ' + '
                
                val = "%s %s (%s)" % (val, fx.effect.characteristic_damage_bonus.safe_translation_getter('name', any_language=True), fx.effect.safe_translation_getter('name', any_language=True))
        
        return val
    
    def get_violence(self):
        language = get_language()
        val = ''
        if self.violence_dice > 0:
            val = str(self.violence_dice) + 'D6 (' + str(self.violence_dice * 3) + ')'
        if self.violence_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.violence_bonus)
            
        if self.characteristic_violence_bonus:
            if val != '':
                val = val + ' + '
            
            val = val + self.characteristic_violence_bonus.safe_translation_getter('name', any_language=True)
        
        effects = (
            self
            .effects
            .filter(
                translations__language_code=language,
                effect__translations__language_code=language,
                weapon_attack__translations__language_code=language
            )
            .all()
        )
        
        for fx in effects:
            if fx.effect and fx.effect.characteristic_violence_bonus:
                if val != '':
                    val = val + ' + '
                
                val = "%s %s (%s)" % (val, fx.effect.characteristic_violence_bonus.safe_translation_getter('name', any_language=True), fx.effect.safe_translation_getter('name', any_language=True))
        
        return val
    
    def max_effect_energy(self):
        energy = 0
        
        for effect in self.effects.all():
            if effect.energy > energy:
                energy = effect.energy
        
        return energy
    
    def get_calculator_effect_list(self):
        if self.calculator_behaviour != 'LST' and self.calculator_behaviour != 'INI':
            return []
        
        if self.calculator_min_energy_list is None:
            logging.warning('None in calculator_min_energy_list for attack %s / calculator_name: %s (%d)...' % (self.safe_translation_getter('name', any_language=True), self.safe_translation_getter('calculator_name', any_language=True), self.pk))
            return []
        
        language = get_language()
        
        return WeaponAttackEffectI18n.objects.filter(
            translations__language_code=language,
            effect__translations__language_code=language,
            weapon_attack=self,
            energy__gte=self.calculator_min_energy_list
        ).select_related('effect').order_by('energy', 'effect__translations__name').all()
    
    def get_calculator_base_effects(self):
        if self.calculator_behaviour != 'INI':
            return []
        
        language = get_language()
        
        rem_attack_id_subquery = WeaponAttackEffectI18n.objects.filter(
            weapon_attack__weapon=self.weapon,  # Same weapon
            weapon_attack__calculator_behaviour='REM',  # Type REM
            effect=models.OuterRef('effect')  # Same effect
        ).values('weapon_attack__id')[:1]  # First id
        
        
        return (
            WeaponAttackEffectI18n
            .objects
            .filter(
                translations__language_code=language,
                effect__translations__language_code=language,
                weapon_attack=self,
                energy=0
            )
            .select_related('effect')
            .annotate(
                rem_attack_id=models.Subquery(
                    rem_attack_id_subquery,
                    output_field=models.IntegerField()
                )
            )
            .order_by('effect__translations__name')
            .all()
        )
    
    def get_calculator_special_effects(self):
        
        if self.calculator_behaviour != 'INI' and self.calculator_behaviour != 'REP':
            return []
        
        language = get_language()
        
        return (
            WeaponAttackEffectI18n
            .objects
            .filter(
                ~models.Q(effect__calculator_behaviour=None),
                translations__language_code=language,
                effect__translations__language_code=language,
                weapon_attack=self
            )
            .select_related('effect')
            .order_by('effect__translations__name')
            .all()
        )
    
    class Meta:
        unique_together = ("weapon", "position")

class WeaponAttackEffectI18n(TranslatableModel):
    translations = TranslatedFields(
        effect_condition = models.CharField(gettext_lazy("condition"), max_length=128, blank=True, default=''),
    )
    
    weapon_attack = models.ForeignKey(WeaponAttackI18n,on_delete=models.CASCADE,verbose_name=gettext_lazy("attaque"), related_name="effects")
    effect = models.ForeignKey(EffectI18n, on_delete=models.CASCADE, verbose_name=gettext_lazy("effet"), related_name="weaponattackeffects")
    effect_level = models.SmallIntegerField(gettext_lazy("niv. effet"))
    energy = models.SmallIntegerField(gettext_lazy("énergie"))
    
    def __str__(self):
        return self.get_name()
    
    def get_name(self, lang_code=None):
        if lang_code:
            effect_name = self.effect.safe_translation_getter('name', language_code=lang_code, any_language=True)
        else:
            effect_name = self.effect.safe_translation_getter('name', any_language=True)
            
        if self.effect_level > 0:
            return effect_name.replace(' X', ' (%d)' % (self.effect_level))
        else:
            return effect_name
    
    def get_description(self, lang_code=None):
        if lang_code:
            effect_desc = self.effect.safe_translation_getter('short_description', language_code=lang_code, any_language=True)
        else:
            effect_desc = self.effect.safe_translation_getter('short_description', any_language=True)
            
        if self.effect_level > 0:
            return effect_desc.replace('$X', str(self.effect_level))
        else:
            return effect_desc
    
    class Meta:
        unique_together = ("weapon_attack", "effect", "effect_level", "energy")
        ordering = ['energy', 'effect__translations__name']

class DivisionModuleI18n(models.Model):
    division = models.ForeignKey(DivisionI18n, on_delete=models.CASCADE, related_name="modules")
    module_level = models.ForeignKey(ModuleLevelI18n, on_delete=models.CASCADE, related_name="divisions")
    forced_choice_effect = models.ForeignKey(EffectI18n, on_delete=models.CASCADE, blank=True, null=True) # Forced with division Ogre: alternate view predator (vue alternative prédatrice).
    
    def __str__(self):
        return self.division.safe_translation_getter('name', any_language=True) + ' ' + self.module_level.module.safe_translation_getter('name', any_language=True) + ''
    
    class Meta:
        unique_together = ("division", "module_level")

class DivisionWeaponI18n(models.Model):
    division = models.ForeignKey(DivisionI18n, on_delete=models.CASCADE, related_name="weapons")
    weapon = models.ForeignKey(WeaponI18n, on_delete=models.CASCADE, related_name="divisions")
    
    def __str__(self):
        return self.division.safe_translation_getter('name', any_language=True) + ' ' + self.weapon.safe_translation_getter('name', any_language=True)
    
    class Meta:
        unique_together = ("division", "weapon")

class ArmourAbilityVariantI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        description = MartorField(),
        energy = models.CharField(gettext_lazy("énergie"), max_length=128, blank=True), # yes, this is confusing, but energy in armour abilities are so conditionals...
        hope = models.CharField(gettext_lazy("espoir"), max_length=128, null=True, blank=True), # here again, not an int but a char because it may vary depending on a lot of things...
        activation = models.CharField(gettext_lazy("activation"), max_length=256, blank=True), # yes, also confusing, but again: activation can be very conditionals here.
        duration = models.CharField(gettext_lazy("durée"), max_length=128, blank=True),
    )
    
    armour_ability = models.ForeignKey(ArmourAbilityI18n, on_delete=models.CASCADE, verbose_name=gettext_lazy("capacité armure"), related_name="variants")
    damage_dice = models.SmallIntegerField(gettext_lazy("dés dégât"), blank=True, null=True)
    damage_bonus = models.SmallIntegerField(gettext_lazy("dégât bonus"), blank=True, null=True)
    violence_dice = models.SmallIntegerField(gettext_lazy("dés violence"), blank=True, null=True)
    violence_bonus = models.SmallIntegerField(gettext_lazy("violence bonus"), blank=True, null=True)
    reach = models.ForeignKey(ReachI18n,on_delete=models.PROTECT,verbose_name=gettext_lazy("portée"), blank=True, null=True)
    npc = models.ForeignKey(NonPlayerCharacterI18n, on_delete=models.CASCADE, null=True, blank=True, limit_choices_to={"simplified": False})
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def get_damage(self):
        val = ''
        if self.damage_dice is not None and self.damage_dice > 0:
            val = str(self.damage_dice) + 'D6 (' + str(self.damage_dice * 3) + ')'
        if self.damage_bonus is not None and self.damage_bonus  > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.damage_bonus)
        return val
    
    def get_violence(self):
        val = ''
        if self.violence_dice is not None and self.violence_dice > 0:
            val = str(self.violence_dice) + 'D6 (' + str(self.violence_dice * 3) + ')'
        if self.violence_bonus is not None and self.violence_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.violence_bonus)
        return val

class NonPlayerCharacterWeaponI18n(models.Model):
    weapon = models.ForeignKey(WeaponI18n, on_delete=models.CASCADE, related_name="npcs", limit_choices_to={'npc_enabled': True})
    npc = models.ForeignKey(NonPlayerCharacterI18n, on_delete=models.CASCADE, related_name="weapons")
    quantity = models.PositiveSmallIntegerField(gettext_lazy("quantité"), default=1)
    
    def __str__(self):
        return "%s - %s (×%d)" % (self.npc.safe_translation_getter('name', any_language=True), self.weapon.safe_translation_getter('name', any_language=True), self.quantity)
    
    class Meta:
        unique_together = ("weapon", "npc")

class NonPlayerCharacterModuleLevelI18n(models.Model):
    module_level = models.ForeignKey(ModuleLevelI18n, on_delete=models.CASCADE, related_name="owning_npcs")
    npc = models.ForeignKey(NonPlayerCharacterI18n, on_delete=models.CASCADE, related_name="modules")
    
    def __str__(self):
        return "%s %s (niv. %d)" % (self.npc.safe_translation_getter('name', any_language=True), self.module_level.module.safe_translation_getter('name', any_language=True), self.module_level.level)
    
    class Meta:
        unique_together = ("module_level", "npc")

class HeroicAbilityCategoryI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)

class HeroicAbilityI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        description = MartorField(),
        duration = models.CharField(gettext_lazy("durée"), max_length=128),
        slug = models.SlugField(),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    
    xp_cost = models.PositiveSmallIntegerField(gettext_lazy("acquisition (XP)"))
    category = models.ForeignKey(HeroicAbilityCategoryI18n, on_delete=models.CASCADE, related_name="skills")
    activation = models.ForeignKey(ActivationI18n,on_delete=models.PROTECT)
    heroic_cost = models.PositiveSmallIntegerField(gettext_lazy("coût (héroïsme)"))
    source = models.ForeignKey(SourceI18n, on_delete=models.CASCADE, related_name="heroicAbilities")
    faq_corrected = models.BooleanField(gettext_lazy("Corrigé par FAQ"), default=False)
    
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name=gettext_lazy("faq"),
        blank=True,
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def origin(self):
        if self.faq_corrected:
            return '%s (%s)' % (self.source.safe_translation_getter('name', any_language=True), gettext_lazy("corrigé par FAQ"))
        else:
            return self.source.safe_translation_getter('name', any_language=True)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
    
    def get_absolute_url(self):
        return reverse('gm:heroic_ability_detail', args=[self.slug])

class VehicleI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        description = MartorField(blank=True),
        speed = models.TextField(gettext_lazy("vitesse"), max_length=64),
        crew =  models.TextField(gettext_lazy("équipage"), max_length=64),
        weaponry = MartorField(blank=True),
        slug = models.SlugField(null=True, blank=True),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    
    armour_points = models.PositiveSmallIntegerField(gettext_lazy("points d'armure (pa)"), default=0)
    energy_points = models.PositiveSmallIntegerField(gettext_lazy("points d'énergie (pe)"), default=0)
    force_field = models.PositiveSmallIntegerField(gettext_lazy("champ de force (cdf)"),default=0)
    maneuverability = models.SmallIntegerField(gettext_lazy("manœuvrabilité"), default=0)
    
    questions = models.ManyToManyField(
        QuestionI18n,
        verbose_name=gettext_lazy("faq"),
        blank=True,
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def crew_order(self):
        for elem in self.crew.split():
            if elem.isnumeric():
                return int(elem)
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def weaponry_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('weaponry', language_code=lang_code, any_language=True)), tags=[], strip=True)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
    
    def get_absolute_url(self):
        return reverse('gm:vehicle_detail', args=[self.slug])

class ModuleVehicleI18n(models.Model):
    module = models.ForeignKey(ModuleI18n, on_delete=models.CASCADE, related_name='vehicles')
    vehicle = models.ForeignKey(VehicleI18n, on_delete=models.CASCADE, related_name='modules')

class UltimateAptitudeTypeI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=16),
    )
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)

class UltimateAptitudeI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=128),
        restriction = models.CharField(max_length=512, null=True, blank=True),
        quote = models.CharField(max_length=512),
        description = MartorField(),
        cost = models.CharField(gettext_lazy("coût"), max_length=128, null=True, blank=True), # because energy can also contain heroic points costs...
        activation = models.CharField(gettext_lazy("activation"), max_length=256, null=True, blank=True),
        duration = models.CharField(gettext_lazy("durée"), max_length=128, null=True, blank=True),
        slug = models.SlugField(null=True, blank=True),
        meta = {'unique_together': [('language_code', 'slug')]},
    )
    type = models.ForeignKey(UltimateAptitudeTypeI18n, on_delete=models.CASCADE, null=True, blank=True)
    reach = models.ForeignKey(ReachI18n, on_delete=models.PROTECT, verbose_name=gettext_lazy("portée"), null=True, blank=True)
    armour = models.ForeignKey(ArmourI18n, on_delete=models.CASCADE, verbose_name=gettext_lazy("armure"), null=True, blank=True)
    crest = models.ForeignKey(CrestI18n, on_delete=models.CASCADE, verbose_name=gettext_lazy("blason"), null=True, blank=True)
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def get_slug(self):
        return self.safe_translation_getter('slug', any_language=True)
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)

class MechaArmourActionI18n(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(gettext_lazy("nom"), max_length=64),
        description = MartorField(),
        activation = models.CharField(gettext_lazy("activation"), max_length=256, blank=True), # see armourabilityvariant activation for explanation of the non-ForeignKey.
        core = models.CharField(gettext_lazy("noyaux"), max_length=128, blank=True),
        duration = models.CharField(gettext_lazy("durée"), max_length=256, blank=True),
        effects = MartorField(gettext_lazy("effets"), blank=True), # some fulltext effect exists, so we'll use the power of markdown to link effects when needed...
    )
    reach = models.ForeignKey(TitanReachI18n,on_delete=models.PROTECT, verbose_name="portée", blank=True, null=True)
    damage_dice = models.SmallIntegerField("dés dégât", blank=True, null=True)
    damage_bonus = models.SmallIntegerField("dégât bonus", blank=True, null=True)
    violence_dice = models.SmallIntegerField("dés violence", blank=True, null=True)
    violence_bonus = models.SmallIntegerField("violence bonus", blank=True, null=True)
    
    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
    
    def description_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('description', language_code=lang_code, any_language=True)), tags=[], strip=True)
        
    def effects_text_only(self, lang_code=None):
        return bleach.clean(markdown(self.safe_translation_getter('effects', language_code=lang_code, any_language=True)), tags=[], strip=True)
        
    def get_damage(self):
        val = ''
        if self.damage_dice is not None and self.damage_dice > 0:
            val = str(self.damage_dice) + 'D6 (' + str(self.damage_dice * 3) + ')'
        if self.damage_bonus is not None and self.damage_bonus  > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.damage_bonus)
        return val
    
    def get_violence(self):
        val = ''
        if self.violence_dice is not None and self.violence_dice > 0:
            val = str(self.violence_dice) + 'D6 (' + str(self.violence_dice * 3) + ')'
        if self.violence_bonus is not None and self.violence_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.violence_bonus)
        return val

class MechaArmourActionCommonI18n(MechaArmourActionI18n):
    mecha = models.ForeignKey(MechaArmourI18n, on_delete=models.CASCADE, verbose_name="mécha-armure", related_name="actions", blank=True, null=True) # Either mecha or configuration must be set, but not both

class MechaArmourActionConfigurationI18n(MechaArmourActionI18n):
    configuration = models.ForeignKey(MechaArmourConfigurationI18n, on_delete=models.CASCADE, related_name="actions", blank=True, null=True) # Either mecha or configuration must be set, but not both
