from django.conf import settings
from django.utils.translation import get_language
from .models.character import (
    ArmourI18n, TraumaI18n, MechaArmourI18n,
    NonPlayerCharacterAbilityI18n
)

def get_allowed_armours(user):
    """Determine allowed armours based on user status."""
    language = get_language()
    
    base_query = (
        ArmourI18n
        .objects
        .filter(
            password=None,
            translations__language_code=language
        )
        .all()
    )
    
    if user.is_authenticated:
        if user.is_gest_game_master():
            return (
                ArmourI18n
                .objects
                .filter(
                    translations__language_code=language
                )
                .all()
            )
        elif user.game_master:
            base_query = (
                base_query |
                ArmourI18n.objects.filter(
                    gest_spoiler=False,
                    translations__language_code=language
                )
                .all()
            )
            
        return (
            base_query |
            user
            .unlocked_armours
            .filter(
                translations__language_code=language
            )
            .all()
        )
    
    return base_query

def get_allowed_mechas(user):
    """Determine allowed mechas based on user status."""
    language = get_language()
    
    base_query = (
        MechaArmourI18n
        .objects
        .filter(
            password=None,
            translations__language_code=language
        )
        .all()
    )
    
    if user.is_authenticated:
        if user.is_gest_game_master():
            return (
                MechaArmourI18n
                .objects
                .filter(
                    translations__language_code=language
                )
                .all()
            )
        else:
            return (
                base_query |
                user
                .unlocked_mechas
                .filter(
                    translations__language_code=language
                )
                .all()
            )
    
    return base_query

def get_allowed_traumas(user):
    """Determine allowed traumas based on user status."""
    language = get_language()
    
    base_query = (
        TraumaI18n
        .objects
        .filter(
            password=None,
            translations__language_code=language
        )
        .all()
    )
    
    if user.is_authenticated:
        if user.is_gest_game_master():
            return (
                TraumaI18n
                .objects
                .filter(
                    translations__language_code=language
                )
                .all()
            )
        else:
            return (
                base_query |
                user
                .unlocked_traumas
                .filter(
                    translations__language_code=language
                )
                .all()
            )
    
    return base_query

def get_allowed_npc_abilities(user):
    """Determine allowed NPC Abilities based on user status."""
    language = get_language()
    
    if user.is_authenticated:
        if user.is_gest_game_master():
            return (
                NonPlayerCharacterAbilityI18n
                .objects
                .filter(
                    translations__language_code=language
                )
                .all()
            )
        elif user.game_master:
            return (
                NonPlayerCharacterAbilityI18n
                .objects
                .filter(
                    gest_spoiler=False,
                    translations__language_code=language
                )
                .all()
            )
    
    return (
        NonPlayerCharacterAbilityI18n
        .objects
        .filter(
            pk=-1, # We don't want any for the unauthenticated/non gm user
            translations__language_code=language
        )
        .all()
    )

def get_bound_questions(object, user):
    language = get_language()
    if user.is_authenticated and user.game_master:
        return (
            object
            .questions
            .filter(
                translations__language_code=language
            )
            .all()
        )
    
    return (
        object
        .questions
        .filter(
            gm=False,
            translations__language_code=language
        )
        .all()
    )

def strip_language_prefix_from_url(url):
    """
    Remove the language prefix from the given URL.
    """
    # Les langues supportées (dans settings.py)
    languages = dict(settings.LANGUAGES).keys()
    
    # Vérifier si l'URL commence avec un code de langue
    parts = url.split('/')
    if parts[1] in languages:
        # Retirer le préfixe de langue
        return '/' + '/'.join(parts[2:])
    return url


def build_new_language_url(new_language_code, current_url):
    """
    Génère une nouvelle URL en remplaçant le préfixe de langue.
    """
    # Supprimer l'ancien préfixe de langue
    url_without_language = strip_language_prefix_from_url(current_url)
    
    # Ajouter le nouveau préfixe de langue
    return f'/{new_language_code}{url_without_language}'
